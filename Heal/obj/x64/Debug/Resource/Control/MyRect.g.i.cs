﻿#pragma checksum "..\..\..\..\..\Resource\Control\MyRect.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "05428465983C4DD51F2AE2462671D4B908C9CC1E2A3BAAD1EE2AC5B4F0FC173C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Heal.Resource.Control;
using Heal.Resource.Convertor;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Heal.Resource.Control {
    
    
    /// <summary>
    /// MyRect
    /// </summary>
    public partial class MyRect : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\..\..\..\Resource\Control\MyRect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Heal.Resource.Control.MyRect self;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\..\..\..\Resource\Control\MyRect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Media.RotateTransform Rotate;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\..\..\Resource\Control\MyRect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border btCore1;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\..\..\Resource\Control\MyRect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border btCore2;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\..\..\Resource\Control\MyRect.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imbIsFlag;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FII-AOI System;component/resource/control/myrect.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Resource\Control\MyRect.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.self = ((Heal.Resource.Control.MyRect)(target));
            
            #line 10 "..\..\..\..\..\Resource\Control\MyRect.xaml"
            this.self.MouseRightButtonUp += new System.Windows.Input.MouseButtonEventHandler(this.self_MouseRightButtonUp);
            
            #line default
            #line hidden
            
            #line 11 "..\..\..\..\..\Resource\Control\MyRect.xaml"
            this.self.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.self_MouseLeftButtonDown);
            
            #line default
            #line hidden
            
            #line 13 "..\..\..\..\..\Resource\Control\MyRect.xaml"
            this.self.SizeChanged += new System.Windows.SizeChangedEventHandler(this.self_SizeChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.Rotate = ((System.Windows.Media.RotateTransform)(target));
            return;
            case 3:
            this.btCore1 = ((System.Windows.Controls.Border)(target));
            return;
            case 4:
            this.btCore2 = ((System.Windows.Controls.Border)(target));
            return;
            case 5:
            this.imbIsFlag = ((System.Windows.Controls.Image)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

