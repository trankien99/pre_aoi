﻿#pragma checksum "..\..\..\..\..\View\Setup\CopyBlock.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "4DA13DB9C758EBA6E4776F564882F2B5D4807777BAC72CBCE9B3E63914B92DC3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Heal.View.Setup;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Heal.View.Setup {
    
    
    /// <summary>
    /// CopyBlock
    /// </summary>
    public partial class CopyBlock : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpMain;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbBlockID;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btOK;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btCancel;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbBlockIDEnable;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbAction;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btOKEnable;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btCancelEnable;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbBlockIDDelete;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btOKDelete;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btCancelDelete;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FII-AOI System;component/view/setup/copyblock.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.stpMain = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 2:
            this.cbBlockID = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            
            #line 25 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
            ((System.Windows.Controls.TextBox)(target)).PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.TextBox_PreviewKeyDown);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btOK = ((System.Windows.Controls.Button)(target));
            
            #line 40 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
            this.btOK.Click += new System.Windows.RoutedEventHandler(this.btOK_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btCancel = ((System.Windows.Controls.Button)(target));
            
            #line 41 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
            this.btCancel.Click += new System.Windows.RoutedEventHandler(this.btCancel_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.cbBlockIDEnable = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.cbAction = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.btOKEnable = ((System.Windows.Controls.Button)(target));
            
            #line 62 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
            this.btOKEnable.Click += new System.Windows.RoutedEventHandler(this.btOKEnable_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btCancelEnable = ((System.Windows.Controls.Button)(target));
            
            #line 63 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
            this.btCancelEnable.Click += new System.Windows.RoutedEventHandler(this.btCancel_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.cbBlockIDDelete = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 11:
            this.btOKDelete = ((System.Windows.Controls.Button)(target));
            
            #line 79 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
            this.btOKDelete.Click += new System.Windows.RoutedEventHandler(this.btOKDelete_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btCancelDelete = ((System.Windows.Controls.Button)(target));
            
            #line 80 "..\..\..\..\..\View\Setup\CopyBlock.xaml"
            this.btCancelDelete.Click += new System.Windows.RoutedEventHandler(this.btCancel_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

