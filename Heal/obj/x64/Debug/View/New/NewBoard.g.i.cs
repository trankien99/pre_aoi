﻿#pragma checksum "..\..\..\..\..\View\New\NewBoard.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "8EB5E1FF6431DF1895AC01113B9B7B71DB82B8BE38A852041C95B43E4011CC29"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Heal.View.New;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Heal.View.New {
    
    
    /// <summary>
    /// NewBoard
    /// </summary>
    public partial class NewBoard : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\..\..\View\New\NewBoard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Heal.View.New.NewBoard thisPage;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\..\..\View\New\NewBoard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBoardName;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\..\..\View\New\NewBoard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBoardLength;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\..\..\View\New\NewBoard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBoardWidth;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\..\..\View\New\NewBoard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNumberOfBlock;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\..\..\View\New\NewBoard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBoardThickness;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\..\..\View\New\NewBoard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBack;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\..\..\View\New\NewBoard.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNext;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FII-AOI System;component/view/new/newboard.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\View\New\NewBoard.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.thisPage = ((Heal.View.New.NewBoard)(target));
            return;
            case 2:
            this.txtBoardName = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.txtBoardLength = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.txtBoardWidth = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.txtNumberOfBlock = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.txtBoardThickness = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.btnBack = ((System.Windows.Controls.Button)(target));
            return;
            case 8:
            this.btnNext = ((System.Windows.Controls.Button)(target));
            
            #line 68 "..\..\..\..\..\View\New\NewBoard.xaml"
            this.btnNext.Click += new System.Windows.RoutedEventHandler(this.btnNext_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

