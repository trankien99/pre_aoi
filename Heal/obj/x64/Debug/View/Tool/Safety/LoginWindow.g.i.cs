﻿#pragma checksum "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "19AF892ABA92A13AD25EBD7D8902E1A8D626C1C4AA4B2209C3DA0DE719A8675B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Heal.View.Tool;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Heal.View.Tool.Safety {
    
    
    /// <summary>
    /// LoginWindow
    /// </summary>
    public partial class LoginWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 40 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtUser;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txtPassWord;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbRegister;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbChangePass;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btLogin;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btCancel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FII-AOI System;component/view/tool/safety/loginwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 11 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
            ((Heal.View.Tool.Safety.LoginWindow)(target)).Initialized += new System.EventHandler(this.Window_Initialized);
            
            #line default
            #line hidden
            
            #line 11 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
            ((Heal.View.Tool.Safety.LoginWindow)(target)).KeyDown += new System.Windows.Input.KeyEventHandler(this.Window_KeyDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.txtUser = ((System.Windows.Controls.TextBox)(target));
            
            #line 44 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
            this.txtUser.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txtUser_TextChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.txtPassWord = ((System.Windows.Controls.PasswordBox)(target));
            
            #line 54 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
            this.txtPassWord.PasswordChanged += new System.Windows.RoutedEventHandler(this.txtPassWord_PasswordChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.lbRegister = ((System.Windows.Controls.Label)(target));
            
            #line 74 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
            this.lbRegister.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.lbRegister_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            case 5:
            this.lbChangePass = ((System.Windows.Controls.Label)(target));
            return;
            case 6:
            this.btLogin = ((System.Windows.Controls.Button)(target));
            
            #line 92 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
            this.btLogin.Click += new System.Windows.RoutedEventHandler(this.btLogin_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btCancel = ((System.Windows.Controls.Button)(target));
            
            #line 96 "..\..\..\..\..\..\View\Tool\Safety\LoginWindow.xaml"
            this.btCancel.Click += new System.Windows.RoutedEventHandler(this.btCancel_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

