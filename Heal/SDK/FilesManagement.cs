﻿using System;
using System.IO;
using NLog;

namespace Heal.SDK
{
    class FilesManagement
    {
        private static Logger mLog = LogCtl.GetInstance();
        public static void DeleteFiles(string path, int hours, bool subfolder = false, bool WriteLog = true, bool CheckByDirectory = false)
        {
            if(Directory.Exists(path))
            {
                if(CheckByDirectory)
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(path);
                    if (dirInfo.LastWriteTime.AddHours(hours) < DateTime.Now)
                    {
                        return;
                    }
                }
                string[] subPath = Directory.GetDirectories(path);
                string[] files = Directory.GetFiles(path);
                int count = 0;
                foreach (string item in files)
                {
                    FileInfo fi = new FileInfo(item);
                    if (fi.LastWriteTime.AddHours(hours) < DateTime.Now)
                    {
                        try
                        {
                            fi.Delete();
                            count++;
                        }
                        catch (Exception e)
                        {
                            mLog.Warn(e.Message);
                        }
                    }
                }
                if (count > 0 && WriteLog)
                {
                    mLog.Info($"[FileManagement] Remove {count}/{ files.Length} files at {path} complete!");
                }
                foreach (string item in subPath)
                {
                    DeleteFiles(item, hours, subfolder: true, WriteLog:WriteLog);
                }
                if (subfolder)
                {
                    if (files == null || (files.Length == count  && subPath.Length == 0))
                    { 
                        Directory.Delete(path);
                    }
                }
            }
        }
    }
}
