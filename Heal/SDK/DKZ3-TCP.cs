﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Threading;

namespace Heal.SDK
{
    public class DKZ3_TCP
    {
        private string _IP { get => "192.168.1.16"; }
        private int _Port { get => 1200; }
        private static Object synLock = new Object();
        public int GetPing()
        {
            Ping p = new Ping();
            PingReply r;
            try
            {
                r = p.Send(_IP);
            }
            catch
            {
                return -1;
            }
            return r.Status == IPStatus.Success ? 0 : -1;
        }
        public int ActiveOne(int Channel, int Mode)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x03, 0x32, 0x00, 0x00 };
            data[4] = Convert.ToByte(Channel - 1);
            data[5] = Convert.ToByte(Mode);
            return send_data(data);
        }

        public int ActiveFour(int Mode1, int Mode2, int Mode3, int Mode4)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x05, 0x34, 0x00, 0x00, 0x00, 0x00 };
            data[4] = Convert.ToByte(Mode1);
            data[5] = Convert.ToByte(Mode2);
            data[6] = Convert.ToByte(Mode3);
            data[7] = Convert.ToByte(Mode4);
            return send_data(data);
        }
        public int SetOne(int Channel, int Value)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x03, 0x31, 0x00, 0x00 };
            data[4] = Convert.ToByte(Channel - 1);
            data[5] = Convert.ToByte(Value);
            return send_data(data);
        }
        public int SetFour(int Value1, int Value2, int Value3, int Value4)
        {
            byte[] data = new byte[] { 0xab, 0xba, 0x05, 0x33, 0x00, 0x00, 0x00, 0x00 };
            data[4] = Convert.ToByte(Value1);
            data[5] = Convert.ToByte(Value2);
            data[6] = Convert.ToByte(Value3);
            data[7] = Convert.ToByte(Value4);
            return send_data(data);
        }
        private int send_data(byte[] data)
        {
            bool succ = false;
            try
            {
                using (TcpClient client = new TcpClient())
                {
                    if (client.ConnectAsync(_IP, _Port).Wait(1000))
                    {
                        client.SendTimeout = 1000;
                        client.ReceiveTimeout = 1000;
                        using (NetworkStream stream = client.GetStream())
                        {
                            stream.Write(data, 0, data.Length);
                            byte[] buff = new byte[1024];
                            stream.Read(buff, 0, 1024);
                            stream.Close();
                            client.Close();
                            string result = System.Text.Encoding.UTF8.GetString(buff);
                            int index = 0;
                            while (result.Substring(index, 1) != "\0")
                            {
                                index++;
                            }
                            result = result.Substring(0, index);
                            if (result.Contains("ok"))
                            {
                                succ = true;
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                return -2;
            }
            return succ ? 0 : -1;
        }
    }
}
