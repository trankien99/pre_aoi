﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Heal.SDK
{
    public class CSTStrobe
    {
        private string mIP { get; set; }
        private long mHandle = 0;
        public CSTStrobe(string IP)
        {
            this.mIP = IP;
        }

        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CST_EthernetConnectIP(string IP, ref Int64 Handle);
        public int Connect()
        {
            if (!string.IsNullOrEmpty(mIP))
            {
                int RET = CST_EthernetConnectIP(mIP, ref mHandle);
                if (RET == 10000)
                    CST_EthernetSetIntCycleValue(60, ref mHandle);
                return RET;
            }
            else
                return -1;
        }


        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CST_EthernetConnectStop(ref Int64 Handle);
        public int Disconnect()
        {
            if (!string.IsNullOrEmpty(mIP))
            {
                int RET = CST_EthernetConnectStop(ref mHandle);
                return RET;
            }
            else
                return -1;
        }


        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetGetLightState(ref Int64 Handle);
        public int GetLightState()
        {
            return CST_EthernetGetLightState(ref mHandle);
        }


        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetSetLightState(int Mode, ref Int64 Handle);
        public int SetLightState(int Mode)
        {
            return CST_EthernetSetLightState(Mode, ref mHandle);
        }



        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]

        private static extern UInt16 CST_EthernetGetStrobeValue(int CH, ref Int64 Handle);
        public int GetIntensity(int CH)
        {
            return CST_EthernetGetStrobeValue(CH, ref mHandle);
        }
        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetSetStrobeValue(int CH, int Light, ref Int64 Handle);
        public int SetIntensity(int CH, int Value)
        {
            return CST_EthernetSetStrobeValue(CH, Value, ref mHandle);
        }
        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetSetMulStrobeValue(MulDigitalValue[] mMulDigitalValue, int length, ref Int64 Handle);
        public int SetIntensities(int[] Value)
        {
            MulDigitalValue[] data = new MulDigitalValue[Value.Length];
            for (int i = 0; i < data.Length; i++)
            {
                data[i].channelIndex = i + 1;
                data[i].DigitalValue = Value[i];
            }
            int RET = CST_EthernetSetMulStrobeValue(data, Value.Length, ref mHandle);
            return RET;
        }

        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CST_EthernetGetLightDelayValue(int CH, ref Int64 Handle);
        public int GetDelay(int CH)
        {
            int RET = CST_EthernetGetLightDelayValue(CH, ref mHandle);
            return RET;
        }


        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CST_EthernetSetLightDelayValue(int CH, int Delay, ref Int64 Handle);
        public int SetDelay(int CH, int Delay)
        {
            int RET = CST_EthernetSetLightDelayValue(CH, Delay, ref mHandle);
            return RET;
        }

        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CST_EthernetSetMulLightDelayValue(MulDigitalValue[] mMulDigitalValue, int length, ref Int64 Handle);
        public int SetDelays(int[] Delay)
        {
            MulDigitalValue[] data = new MulDigitalValue[Delay.Length];
            for (int i = 0; i < data.Length; i++)
            {
                data[i].channelIndex = i + 1;
                data[i].DigitalValue = Delay[i];
            }
            int RET = CST_EthernetSetMulLightDelayValue(data, Delay.Length, ref mHandle);
            return RET;
        }


        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetGetCameraDelayValue(int CH, ref long Handle);
        public int GetCameraDelay(int CH)
        {
            int RET = CST_EthernetGetCameraDelayValue(CH, ref mHandle);
            return RET;
        }

        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetSetCameraDelayValue(int CH, int Delay, ref long Handle);
        public int SetCameraDelay(int CH, int Delay)
        {
            
            int RET = CST_EthernetSetCameraDelayValue(CH, Delay, ref mHandle);
            return RET;
        }

        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetSetMulCameraDelayValue(MulDigitalValue[] mMulDigitalValue, int length, ref Int64 Handle);
        public int SetCameraDelays(int[] Delay)
        {
            MulDigitalValue[] data = new MulDigitalValue[Delay.Length];
            for (int i = 0; i < data.Length; i++)
            {
                data[i].channelIndex = i + 1;
                data[i].DigitalValue = Delay[i];
            }
            int RET = CST_EthernetSetMulCameraDelayValue(data, Delay.Length, ref mHandle);
            return RET;
        }


        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetSetIntCycleValue(int Value, ref Int64 Handle);
        public int SetCycle(int Value)
        {
            int RET = CST_EthernetSetIntCycleValue(Value, ref mHandle);
            return RET;
        }


        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetGetIntCycleValue(ref Int64 Handle);
        public int GetCycle()
        {
            int RET = CST_EthernetGetIntCycleValue(ref mHandle);
            return RET;
        }

        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetSetLightTriMode(int Mode, ref Int64 Handle);
        public int SetTriggerMode(TriggerMode Mode)
        {
            int RET = CST_EthernetSetLightTriMode(Convert.ToInt32(Mode), ref mHandle);
            return RET;
        }


        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetGetLightTriMode(ref Int64 Handle);
        public TriggerMode GetTriggerMode()
        {
            TriggerMode trigger = TriggerMode.Fail;
            int RET = CST_EthernetGetLightTriMode(ref mHandle);
            switch (RET)
            {
                case 0:
                    trigger = TriggerMode.External;
                    break;
                case 1:
                    trigger = TriggerMode.Internal;
                    break;
                default:
                    trigger = TriggerMode.Fail;
                    break;
            }
            return trigger;
        }

        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetSetCamTriEdge(int Mode, ref Int64 Handle);
        public int SetCameraTriggerEdge(CameraTriggerEdge Mode)
        {
            int RET = CST_EthernetSetCamTriEdge(Convert.ToInt32(Mode), ref mHandle);
            return RET;
        }

        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern UInt16 CST_EthernetGetCamTriEdge(ref Int64 Handle);
        public CameraTriggerEdge GetCameraTriggerEdge()
        {
            CameraTriggerEdge trigger = CameraTriggerEdge.Fail;
            int RET = CST_EthernetGetCamTriEdge(ref mHandle);
            switch (RET)
            {
                case 0:
                    trigger = CameraTriggerEdge.FallingEdge;
                    break;
                case 1:
                    trigger = CameraTriggerEdge.RisingEdge;
                    break;
                default:
                    trigger = CameraTriggerEdge.Fail;
                    break;
            }
            return trigger;
        }
        public struct MulDigitalValue
        {
            public int channelIndex;
            public int DigitalValue;
        }

        [DllImport("CSTControllerDll.dll", CallingConvention = CallingConvention.Cdecl)]
        private static extern int CST_CST_GetAdapter(IntPtr mAdapterPrmPtr);

        [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        public struct Adapter_prm
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 132)]
            public char[] cSn;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
            public char[] cIp;
        }

    }
    public enum TriggerMode
    {
        External = 0,
        Internal = 1,
        Fail = 2
    }
    public enum CameraTriggerEdge
    {
        FallingEdge = 0,
        RisingEdge = 1,
        Fail = 2
    }
}
