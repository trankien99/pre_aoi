﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.Features2D;
using Emgu.CV.CvEnum;
using Emgu.CV.Flann;
using System.Drawing;
using Emgu.CV.XFeatures2D;

namespace Heal.SDK
{
    public class Alignment
    {
        public static Image<Bgr, byte> MatchFeature(Image<Bgr, byte> inputImage, Image<Bgr, byte> templateImage, DetectAlgoritm detectAlgoritm, int maxFeatures, double keep = 0.1)
        {

            using (Image<Gray, byte> originGray = new Image<Gray, byte>(templateImage.Size))
            using (Image<Gray, byte> inputGray = new Image<Gray, byte>(inputImage.Size))
            using (VectorOfKeyPoint inKps = new VectorOfKeyPoint())
            using (VectorOfKeyPoint tmpKps = new VectorOfKeyPoint())
            using (Mat inDescs = new Mat())
            using (Mat tmpDescs = new Mat())
            {
                CvInvoke.CvtColor(inputImage, originGray, ColorConversion.Bgr2Gray);
                CvInvoke.CvtColor(templateImage, inputGray, ColorConversion.Bgr2Gray);
                Feature2D detector = null;
                if (detectAlgoritm == DetectAlgoritm.ORB)
                {
                    detector = new ORBDetector(maxFeatures, WTK_A: 3);

                }
                else if (detectAlgoritm == DetectAlgoritm.KAZE)
                {
                    detector = new KAZE();

                }
                else
                {
                    detector = new AKAZE();
                }
                detector.DetectAndCompute(originGray, null, inKps, inDescs, false);
                detector.DetectAndCompute(inputGray, null, tmpKps, tmpDescs, false);
                List<PointF> inPts = new List<PointF>();
                List<PointF> tmpPts = new List<PointF>();
                if ((detectAlgoritm == DetectAlgoritm.ORB) | (detectAlgoritm == DetectAlgoritm.AKAZE))
                {
                    VectorOfDMatch matches = new VectorOfDMatch();
                    BFMatcher matcher = new BFMatcher(DistanceType.Hamming2, true);
                    matcher.Match(inDescs, tmpDescs, matches);
                    MDMatch[] matchesArr = matches.ToArray();
                    Array.Sort(matchesArr, delegate (MDMatch inMatch, MDMatch tmpMatch)
                    {
                        return inMatch.Distance.CompareTo(tmpMatch.Distance);
                    });

                    double keepIdx = matchesArr.Length * keep;


                    for (int i = 0; i < keepIdx; i++)
                    {
                        MDMatch match = matchesArr[i];
                        inPts.Add(inKps[match.QueryIdx].Point);
                        tmpPts.Add(tmpKps[match.TrainIdx].Point);
                    }

                }
                else
                {
                    VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch();
                    List<MDMatch> goodList = new List<MDMatch>();
                    var ip = new LinearIndexParams();
                    SearchParams sp = new SearchParams();
                    FlannBasedMatcher matcher = new FlannBasedMatcher(ip, sp);
                    matcher.Add(tmpDescs);
                    matcher.KnnMatch(inDescs, matches, 2);
                    MDMatch[][] matchesArray = matches.ToArrayOfArray();
                    for (int i = 0; i < matchesArray.Length; i++)
                    {
                        MDMatch first = matchesArray[i][0];
                        float dist1 = matchesArray[i][0].Distance;
                        float dist2 = matchesArray[i][1].Distance;

                        if (dist1 < 0.7 * dist2)
                        {
                            goodList.Add(first);
                        }
                    }

                    foreach (MDMatch match in goodList)
                    {

                        inPts.Add(inKps[match.QueryIdx].Point);
                        tmpPts.Add(tmpKps[match.TrainIdx].Point);
                    }
                }



                Mat affineMatrix = CvInvoke.EstimateAffinePartial2D(new VectorOfPointF(inPts.ToArray()),
                    new VectorOfPointF(tmpPts.ToArray()), null,
                Emgu.CV.CvEnum.RobustEstimationAlgorithm.Ransac, 3, 2000, 0.99, 10);
                Matrix<double> matrix = new Matrix<double>(affineMatrix.Rows, affineMatrix.Cols);
                affineMatrix.CopyTo(matrix);
                double scale = Math.Sqrt(matrix[0, 0] * matrix[0, 0] + matrix[0, 1] * matrix[0, 1]);
                for (int i = 0; i < affineMatrix.Rows; i++)
                {
                    for (int j = 0; j < affineMatrix.Cols; j++)
                    {
                        matrix[i, j] = matrix[i, j] / scale;
                    }
                }
                Image<Bgr, byte> alignedImg = new Image<Bgr, byte>(templateImage.Size);
                CvInvoke.WarpAffine(inputImage, alignedImg, matrix, templateImage.Size);
                return alignedImg;
            }
        }
        public static KeyFeatureResult GetFeature(Image<Bgr, byte> Image, DetectAlgoritm detectAlgoritm, int maxFeatures)
        {
            KeyFeatureResult feature = new KeyFeatureResult();
            using (Image<Gray, byte> inputGray = new Image<Gray, byte>(Image.Size))
            {
                CvInvoke.CvtColor(Image, inputGray, ColorConversion.Bgr2Gray);
                Feature2D detector = null;
                if (detectAlgoritm == DetectAlgoritm.ORB)
                {
                    detector = new ORBDetector(maxFeatures, WTK_A: 3);

                }
                else if (detectAlgoritm == DetectAlgoritm.KAZE)
                {
                    detector = new KAZE();
                }
                else
                {
                    detector = new AKAZE();
                }
                detector.DetectAndCompute(inputGray, null, feature.KeyPoint, feature.Description, false);
            }
            return feature;
        }
        public static Image<Bgr, byte> MatchFeature(Image<Bgr, byte> Image, KeyFeatureResult Template, DetectAlgoritm detectAlgoritm, int MaxFeature, double Keep = 0.1)
        {
            KeyFeatureResult inputFeature = GetFeature(Image, detectAlgoritm, MaxFeature);
            using (VectorOfKeyPoint inKps = inputFeature.KeyPoint)
            using (Mat inDescs = inputFeature.Description)
            {
                List<PointF> inPts = new List<PointF>();
                List<PointF> tmpPts = new List<PointF>();
                if ((detectAlgoritm == DetectAlgoritm.ORB) | (detectAlgoritm == DetectAlgoritm.AKAZE))
                {
                    VectorOfDMatch matches = new VectorOfDMatch();
                    BFMatcher matcher = new BFMatcher(DistanceType.Hamming2, true);
                    matcher.Match(inDescs, Template.Description, matches);
                    MDMatch[] matchesArr = matches.ToArray();
                    Array.Sort(matchesArr, delegate (MDMatch inMatch, MDMatch tmpMatch)
                    {
                        return inMatch.Distance.CompareTo(tmpMatch.Distance);
                    });

                    double keepIdx = matchesArr.Length * Keep;


                    for (int i = 0; i < keepIdx; i++)
                    {
                        MDMatch match = matchesArr[i];
                        inPts.Add(inKps[match.QueryIdx].Point);
                        tmpPts.Add(Template.KeyPoint[match.TrainIdx].Point);
                    }

                }
                else
                {
                    VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch();
                    List<MDMatch> goodList = new List<MDMatch>();
                    var ip = new LinearIndexParams();
                    SearchParams sp = new SearchParams();
                    FlannBasedMatcher matcher = new FlannBasedMatcher(ip, sp);
                    matcher.Add(Template.Description);
                    matcher.KnnMatch(inDescs, matches, 2);
                    MDMatch[][] matchesArray = matches.ToArrayOfArray();
                    for (int i = 0; i < matchesArray.Length; i++)
                    {
                        MDMatch first = matchesArray[i][0];
                        float dist1 = matchesArray[i][0].Distance;
                        float dist2 = matchesArray[i][1].Distance;

                        if (dist1 < 0.7 * dist2)
                        {
                            goodList.Add(first);
                        }
                    }

                    foreach (MDMatch match in goodList)
                    {

                        inPts.Add(inKps[match.QueryIdx].Point);
                        tmpPts.Add(Template.KeyPoint[match.TrainIdx].Point);
                    }
                }



                Mat affineMatrix = CvInvoke.EstimateAffinePartial2D(new VectorOfPointF(inPts.ToArray()),
                    new VectorOfPointF(tmpPts.ToArray()), null,
                Emgu.CV.CvEnum.RobustEstimationAlgorithm.Ransac, 3, 2000, 0.99, 10);
                Matrix<double> matrix = new Matrix<double>(affineMatrix.Rows, affineMatrix.Cols);
                affineMatrix.CopyTo(matrix);
                double scale = Math.Sqrt(matrix[0, 0] * matrix[0, 0] + matrix[0, 1] * matrix[0, 1]);
                for (int i = 0; i < affineMatrix.Rows; i++)
                {
                    for (int j = 0; j < affineMatrix.Cols; j++)
                    {
                        matrix[i, j] = matrix[i, j] / scale;
                    }
                }
                Image<Bgr, byte> alignedImg = new Image<Bgr, byte>(Image.Size);
                CvInvoke.WarpAffine(Image, alignedImg, matrix, Image.Size);
                inputFeature.Dispose();
                return alignedImg;
            }
                
        }
    }
    public class KeyFeatureResult
    {
        public Mat Description { get; set; }
        public VectorOfKeyPoint KeyPoint { get; set; }
        public KeyFeatureResult()
        {
            this.Description = new Mat();
            this.KeyPoint = new VectorOfKeyPoint();
        }
        public void Dispose()
        {
            if(this.Description != null)
            {
                this.Description.Dispose();
                this.Description = null;
            }
            if (this.KeyPoint != null)
            {
                this.KeyPoint.Dispose();
                this.KeyPoint = null;
            }
        }
    }
    public enum MatcherAlgorithm
    {
        BF_MATCHER,
        FLANN_MATCHER
    }
    public enum DetectAlgoritm
    {
        ORB,
        KAZE,
        AKAZE
    }
}
