﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using MongoDB.Bson;
using MongoDB.Driver.Core;
using MongoDB.Driver;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using NLog;
using Heal.Models;
using System.IO;
using Heal.Models.Run;

namespace Heal.DB
{
    public class MyMongo
    {
        private Logger _Log = Heal.SDK.LogCtl.GetInstance();
        private Properties.Settings _Param = Properties.Settings.Default;
        private MongoClient _Client;
        private IMongoDatabase _DataBase;
        private IMongoCollection<BsonDocument> _BoardCollection;
        private IMongoCollection<BsonDocument> _SMDCollection;
        private string _DBUrl = "localhost";
        private int _DBPort = 27017;
        private string _DBName = "AOIMachine";
        private string _BoardCol = "BoardReuslt";
        public bool IsConnected { get; set; }
        public int Connect()
        {
            try
            {
                _Client = new MongoClient($"mongodb://{_DBUrl}:{_DBPort}");
                _DataBase = _Client.GetDatabase(_DBName);
                _BoardCollection = _DataBase.GetCollection<BsonDocument>(_BoardCol);
                //_SMDCollection = _DataBase.GetCollection<BsonDocument>(_SMDCol);
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message);
                return -1;
            }
            IsConnected = true;
            return 0;
        }
        public int InsertBoard(BoardResult Board)
        {
            Board.SaveImage(_Param.SavePath);
            Board.Dispose();
            string jsonStr = JsonConvert.SerializeObject(Board);
            BsonDocument doc = BsonDocument.Parse(jsonStr);
            try
            {
                _BoardCollection.InsertOne(doc);
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message);
                return -1;
            }
            return 0;
        }
        //public int InsertSMD(SMDAlarm SMD)
        //{
        //    string jsonStr = JsonConvert.SerializeObject(SMD);
        //    BsonDocument doc = BsonDocument.Parse(jsonStr);
        //    try
        //    {
        //        _SMDCollection.InsertOne(doc);
        //    }
        //    catch (Exception ex)
        //    {
        //        _Log.Error(ex.Message);
        //        return -1;
        //    }
        //    return 0;
        //}
    }
}
