﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Newtonsoft.Json;
using NLog;
using System.Net.Sockets;
using System.IO;

namespace Heal.Utils
{
    class TCP_Client
    {
        private static Logger _Log = Heal.SDK.LogCtl.GetInstance();
        public static bool Send(string IP, int Port, object Obj)
        {
            bool succ = false;
            try
            {
                string s = JsonConvert.SerializeObject(Obj);
                Byte[] bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(s);
                using (TcpClient client = new TcpClient())
                {
                    client.Connect(IP, Port);
                    if(client.Connected)
                    {
                        using (NetworkStream stream = client.GetStream())
                        {
                           stream.Write(bytes, 0, bytes.Length);
                           stream.Flush();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message);
            }
            return succ;
        }
    }
}
