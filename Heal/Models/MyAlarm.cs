﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using Emgu.CV.Util;
using System.IO;
using NLog;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace Heal.Models
{
    public class MyAlarm : BaseProperties
    {
        private AlarmType _AlarmType { get; set; }
        public AlarmType AlarmType
        {
            get => _AlarmType;
            set
            {
                _AlarmType = value;
                OnPropertyChanged("AlarmType");
            }
        }
        private Alg.MyAlgorithm _Alg { get; set; }
        public Alg.MyAlgorithm Alg
        {
            get => _Alg;
            set 
            {
                _Alg = value;
                OnPropertyChanged("Alg");
            }
        }
        private double _OffsetX { get; set; }
        public double OffsetX
        {
            get => _OffsetX;
            set
            {
                _OffsetX = value;
                OnPropertyChanged("OffsetX");
            }
        }
        private double _OffsetY { get; set; }
        public double OffsetY
        {
            get => _OffsetY;
            set
            {
                _OffsetY = value;
                OnPropertyChanged("OffsetY");
            }
        }
        private bool _IsEnable { get; set; }
        public bool IsEnable
        {
            get => _IsEnable;
            set 
            { 
                _IsEnable = value;
                OnPropertyChanged("IsEnable");
            }
        }
        public ObservableCollection<MyAlarm> Alarms { get; set; }
        public MyAlarm()
        {
            this.Alg = new Alg.None();
            this.AlarmType = AlarmType.General;
            this.Alarms = new ObservableCollection<MyAlarm>();
            this.OffsetX = 0;
            this.OffsetY = 0;
            this.Width = Const.Millimeter2Pixel(3);
            this.Height = Const.Millimeter2Pixel(3);
            this.IsEnable = true;
        }
        public AlarmType Preview(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            var status = this.Alg.Preview(InputImage, SmdCenter, Parent);
            if (status != AlarmType.General)
                return status;
            else
            {
                for (int i = 0; i < Alarms.Count; i++)
                {
                    Alarms[i].Alg.Result = this.Alg.Result.Copy();
                    status = this.Alg.Preview(InputImage, SmdCenter, Alarms[i]);
                    if (status != AlarmType.General)
                        return status;
                }
                return AlarmType.General;
            }
        }
        public AlarmType Run(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            var status =  this.Alg.Run(InputImage, SmdCenter, Parent);
            if (status != AlarmType.General)
                return status;
            else
            {
                for (int i = 0; i < Alarms.Count; i++)
                {
                    Alarms[i].Alg.Result = this.Alg.Result.Copy();
                    status = this.Alg.Run(InputImage, SmdCenter, Alarms[i]);
                    if (status != AlarmType.General)
                        return status;
                }
                return AlarmType.General;
            }
        }
        public MyAlarm Copy()
        {
            string s = JsonConvert.SerializeObject(this);
            var item = JsonConvert.DeserializeObject<MyAlarm>(s, new MyJsonConverter());
            return item;
        }
    }
}
