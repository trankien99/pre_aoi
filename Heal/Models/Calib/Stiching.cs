﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.Features2D;
using Emgu.CV.CvEnum;
using Emgu.CV.Flann;
using System.Drawing;
using Emgu.CV.XFeatures2D;
using Emgu.CV.Stitching;
using System.IO;
using Heal.SDK;
using NLog;
using System.Threading;

namespace Heal.Models.Calib
{
    
   public  class Stiching
   {
        private static Logger mLog = LogCtl.GetInstance();
        public static void TestStiching()
        {
            
            //int w = 1787;
            //Image<Bgr, byte> image1 = new Image<Bgr, byte>(@"E:\Heal\Projects\test\1.png");
            //Image<Bgr, byte> image2 = new Image<Bgr, byte>(@"E:\Heal\Projects\test\2.png");
            //var image = MatchFeature(image1, image2, DetectAlgoritm.AKAZE, 500);
            //CvInvoke.Imwrite("E:\\Heal\\Projects\\test\\result.png", image);

            //Stitcher stitcher = new Stitcher();
            //VectorOfMat arrays = new VectorOfMat();
            //string[] files = Directory.GetFiles(@"C:\Users\FII AOI 1\MVS\Data", "*.bmp");
            //foreach (var f in files)
            //{
            //    arrays.Push(new Mat(f));
            //}
            //Mat paramo = new Mat();
            //stitcher.Stitch(arrays, paramo);
            //paramo.Save("result.png");
            StichingHonizital();

        }
        private static void StichingHonizital()
        {
            int w = 1787;
            Image<Bgr, byte> image = new Image<Bgr, byte>(w * 10, 2048);
            KeyFeatureResult feature = null;

            for (int i = 0; i < 10; i++)
            {
                string path = $"E:\\Heal\\Projects\\test\\{i}.png";
                using (Image<Bgr, byte> img = new Image<Bgr, byte>(path))
                {
                    Rectangle ROI = new Rectangle(img.Width / 2 - w / 2, 0, w, 2048);
                    Rectangle ROIDiagram = new Rectangle(0, 0, w, 2048);
                    ROIDiagram.X += (9 - i) * w;
                    image.ROI = ROIDiagram;
                    if (feature != null)
                    {
                        var matrix = GetMatrix(img, feature, DetectAlgoritm.AKAZE, 500);
                        var imageAlign = AlignImage(img, matrix);
                        feature.Dispose();
                        feature = GetFeature(imageAlign, DetectAlgoritm.AKAZE, 500);
                        CvInvoke.Imwrite($"E:\\Heal\\Projects\\test\\a{i}.png", imageAlign);
                        imageAlign.ROI = ROI;
                        imageAlign.CopyTo(image);
                    }
                    else
                    {
                        img.ROI = ROI;
                        img.CopyTo(image);
                        img.ROI = new Rectangle();
                        feature = GetFeature(img, DetectAlgoritm.AKAZE, 500);
                    }

                    image.ROI = new Rectangle();
                    CvInvoke.Imwrite("test.png", image);
                }
            }
            CvInvoke.Imwrite("E:\\Heal\\Projects\\test\\result.png", image);
        }
        public static Image<Bgr, byte> MatchFeature(Image<Bgr, byte> inputImage, Image<Bgr, byte> templateImage, DetectAlgoritm detectAlgoritm, int maxFeatures, double keep = 0.1)
        {

            using (Image<Gray, byte> originGray = new Image<Gray, byte>(templateImage.Size))
            using (Image<Gray, byte> inputGray = new Image<Gray, byte>(templateImage.Size))
            using (VectorOfKeyPoint inKps = new VectorOfKeyPoint())
            using (VectorOfKeyPoint tmpKps = new VectorOfKeyPoint())
            using (Mat inDescs = new Mat())
            using (Mat tmpDescs = new Mat())
            {
                CvInvoke.CvtColor(inputImage, originGray, ColorConversion.Bgr2Gray);
                CvInvoke.CvtColor(templateImage, inputGray, ColorConversion.Bgr2Gray);
                Feature2D detector = null;
                if (detectAlgoritm == DetectAlgoritm.ORB)
                {
                    detector = new ORBDetector(maxFeatures, WTK_A: 3);

                }
                else if (detectAlgoritm == DetectAlgoritm.KAZE)
                {
                    detector = new KAZE();

                }
                else
                {
                    detector = new AKAZE();
                }
                detector.DetectAndCompute(originGray, null, inKps, inDescs, false);
                detector.DetectAndCompute(inputGray, null, tmpKps, tmpDescs, false);
                List<PointF> inPts = new List<PointF>();
                List<PointF> tmpPts = new List<PointF>();
                if ((detectAlgoritm == DetectAlgoritm.ORB) | (detectAlgoritm == DetectAlgoritm.AKAZE))
                {
                    VectorOfDMatch matches = new VectorOfDMatch();
                    BFMatcher matcher = new BFMatcher(DistanceType.Hamming2, true);
                    matcher.Match(inDescs, tmpDescs, matches);
                    MDMatch[] matchesArr = matches.ToArray();
                    Array.Sort(matchesArr, delegate (MDMatch inMatch, MDMatch tmpMatch)
                    {
                        return inMatch.Distance.CompareTo(tmpMatch.Distance);
                    });

                    double keepIdx = matchesArr.Length * keep;


                    for (int i = 0; i < keepIdx; i++)
                    {
                        MDMatch match = matchesArr[i];
                        inPts.Add(inKps[match.QueryIdx].Point);
                        tmpPts.Add(tmpKps[match.TrainIdx].Point);
                    }

                }
                else
                {
                    VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch();
                    List<MDMatch> goodList = new List<MDMatch>();
                    var ip = new LinearIndexParams();
                    SearchParams sp = new SearchParams();
                    FlannBasedMatcher matcher = new FlannBasedMatcher(ip, sp);
                    matcher.Add(tmpDescs);
                    matcher.KnnMatch(inDescs, matches, 2);
                    MDMatch[][] matchesArray = matches.ToArrayOfArray();
                    for (int i = 0; i < matchesArray.Length; i++)
                    {
                        MDMatch first = matchesArray[i][0];
                        float dist1 = matchesArray[i][0].Distance;
                        float dist2 = matchesArray[i][1].Distance;

                        if (dist1 < 0.7 * dist2)
                        {
                            goodList.Add(first);
                        }
                    }

                    foreach (MDMatch match in goodList)
                    {

                        inPts.Add(inKps[match.QueryIdx].Point);
                        tmpPts.Add(tmpKps[match.TrainIdx].Point);
                    }
                }
                Image<Bgr, byte> alignedImg = new Image<Bgr, byte>(inputImage.Size);
                Mat affineMatrix  = CvInvoke.FindHomography(inPts.ToArray(), tmpPts.ToArray(), RobustEstimationAlgorithm.Ransac);
                CvInvoke.WarpPerspective(inputImage, alignedImg, affineMatrix, new Size(alignedImg.Width, alignedImg.Height));
                return alignedImg;
            }
        }
        public static KeyFeatureResult GetFeature(Image<Bgr, byte> Image, DetectAlgoritm detectAlgoritm, int maxFeatures)
        {
            KeyFeatureResult feature = new KeyFeatureResult();
            using (Image<Gray, byte> inputGray = new Image<Gray, byte>(Image.Size))
            {
                CvInvoke.CvtColor(Image, inputGray, ColorConversion.Bgr2Gray);
                Feature2D detector = null;
                if (detectAlgoritm == DetectAlgoritm.ORB)
                {
                    detector = new ORBDetector(maxFeatures, WTK_A: 3);

                }
                else if (detectAlgoritm == DetectAlgoritm.KAZE)
                {
                    detector = new KAZE();

                }
                else
                {
                    detector = new AKAZE();
                }
                detector.DetectAndCompute(inputGray, null, feature.KeyPoint, feature.Description, false);
            }
            return feature;
        }
        public static Mat GetMatrix(Image<Bgr, byte> Image, KeyFeatureResult Template, DetectAlgoritm detectAlgoritm, int MaxFeature, MatrixType Type = MatrixType.WarpAffine , double Keep = 1)
        {
            KeyFeatureResult inputFeature = GetFeature(Image, detectAlgoritm, MaxFeature);
            using (VectorOfKeyPoint inKps = inputFeature.KeyPoint)
            using (Mat inDescs = inputFeature.Description)
            {
                List<PointF> inPts = new List<PointF>();
                List<PointF> tmpPts = new List<PointF>();
                if ((detectAlgoritm == DetectAlgoritm.ORB) | (detectAlgoritm == DetectAlgoritm.AKAZE))
                {

                    VectorOfDMatch matches = new VectorOfDMatch();
                    BFMatcher matcher = new BFMatcher(DistanceType.Hamming2, true);
                    matcher.Match(inDescs, Template.Description, matches);
                    MDMatch[] matchesArr = matches.ToArray();
                    Array.Sort(matchesArr, delegate (MDMatch inMatch, MDMatch tmpMatch)
                    {
                        return inMatch.Distance.CompareTo(tmpMatch.Distance);
                    });
                    double keepIdx = matchesArr.Length * Keep;
                    for (int i = 0; i < keepIdx; i++)
                    {
                        MDMatch match = matchesArr[i];
                        inPts.Add(inKps[match.QueryIdx].Point);
                        tmpPts.Add(Template.KeyPoint[match.TrainIdx].Point);
                    }

                }
                else
                {
                    VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch();
                    List<MDMatch> goodList = new List<MDMatch>();
                    var ip = new LinearIndexParams();
                    SearchParams sp = new SearchParams();
                    FlannBasedMatcher matcher = new FlannBasedMatcher(ip, sp);
                    matcher.Add(Template.Description);
                    matcher.KnnMatch(inDescs, matches, 2);
                    MDMatch[][] matchesArray = matches.ToArrayOfArray();
                    for (int i = 0; i < matchesArray.Length; i++)
                    {
                        MDMatch first = matchesArray[i][0];
                        float dist1 = matchesArray[i][0].Distance;
                        float dist2 = matchesArray[i][1].Distance;

                        if (dist1 < 0.7 * dist2)
                        {
                            goodList.Add(first);
                        }
                    }

                    foreach (MDMatch match in goodList)
                    {
                        inPts.Add(inKps[match.QueryIdx].Point);
                        tmpPts.Add(Template.KeyPoint[match.TrainIdx].Point);
                    }
                }
                Mat affineMatrix = null;
                if (Type == MatrixType.Perspective)
                {
                    affineMatrix = CvInvoke.FindHomography(inPts.ToArray(), tmpPts.ToArray(), RobustEstimationAlgorithm.Ransac);
                }
                else if(Type == MatrixType.WarpAffine)
                {
                    affineMatrix = CvInvoke.EstimateAffinePartial2D(new VectorOfPointF(inPts.ToArray()),
                        new VectorOfPointF(tmpPts.ToArray()), null,
                    Emgu.CV.CvEnum.RobustEstimationAlgorithm.Ransac, 3, 5000, 0.99, 10);
                    Image<Gray, double> matrix = affineMatrix.ToImage<Gray, double>();
                    affineMatrix.CopyTo(matrix);
                    double scale = Math.Sqrt(matrix[0, 0].Intensity * matrix[0, 0].Intensity + matrix[0, 1].Intensity * matrix[0, 1].Intensity);
                    for (int i = 0; i < affineMatrix.Rows; i++)
                    {
                        for (int j = 0; j < affineMatrix.Cols; j++)
                        {
                            matrix[i, j] = new Gray(matrix[i, j].Intensity / scale);
                        }
                    }
                    affineMatrix = matrix.Mat;
                }
                inputFeature.Dispose();
                return affineMatrix;
            }
        }
        public static Image<Bgr, byte> AlignImage(Image<Bgr, byte> Image, Mat matrix, MatrixType Type = MatrixType.WarpAffine)
        {
            Image<Bgr, byte> alignedImg = new Image<Bgr, byte>(Image.Size.Width , Image.Size.Height);
            if(Type == MatrixType.WarpAffine)
                CvInvoke.WarpAffine(Image, alignedImg, matrix, alignedImg.Size);
            else if(Type == MatrixType.Perspective)
                CvInvoke.WarpPerspective(Image, alignedImg, matrix, alignedImg.Size);
            return alignedImg;
        }
    }
    public class KeyFeatureResult
    {
        public Mat Description { get; set; }
        public VectorOfKeyPoint KeyPoint { get; set; }
        public KeyFeatureResult()
        {
            this.Description = new Mat();
            this.KeyPoint = new VectorOfKeyPoint();
        }
        public void Dispose()
        {
            if (this.Description != null)
            {
                this.Description.Dispose();
                this.Description = null;
            }
            if (this.KeyPoint != null)
            {
                this.KeyPoint.Dispose();
                this.KeyPoint = null;
            }
        }
    }
    public enum DetectAlgoritm
    {
        ORB,
        AKAZE,
        KAZE
    }
    public enum MatrixType
    {
        Perspective,
        WarpAffine
    }
}
