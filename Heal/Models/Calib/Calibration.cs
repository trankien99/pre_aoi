﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.Features2D;
using Emgu.CV.CvEnum;
using Emgu.CV.Flann;
using System.Drawing;
using Emgu.CV.XFeatures2D;
using Emgu.CV.Stitching;
using System.IO;
using Heal.SDK;
using NLog;
using System.Threading;
using Newtonsoft.Json;
using System.Runtime.InteropServices;

namespace Heal.Models.Calib
{
    public class Calibration
    {
        public CalibrationAgrs Agrs { get; set; }
        public List<ImageBlockCalib[]> Images { get; set; }
        public List<ImageBlockCalib[]> Images05 { get; set; }
        public Mat ReferenceMatrix { get; set; }
        private Devices.PLC.MyPLC mPLC = new Devices.PLC.MyPLC();
        private HikCamera mCamera = Devices.Camera.MyCamera.GetInstance();
        private static Devices.Light.MyLight _Light = Devices.Light.MyLight.GetInstance();
        private static Properties.Settings _Param = Properties.Settings.Default;
        private Logger mLog = LogCtl.GetInstance();
        public int NumWidth { get; set; }
        public int NumHeight { get; set; }
        public double XCameraAngle { get; set; }
        public double XYAngle { get; set; }
        public static Calibration _Calibration { get; set; }
        public static Calibration GetInstance()
        {
            if (_Calibration == null)
            {
                _Calibration = new Calibration();
            }
            return _Calibration;
        }
        public static void LoadFromFile(string path)
        {
            _Calibration = LoadMatrix(path);
        }

        public Calibration()
        {
            Agrs = new CalibrationAgrs();
            Agrs.LoadFromSetting();
            Images = new List<ImageBlockCalib[]>();
            Images05 = new List<ImageBlockCalib[]>();
        }
        public void Refresh()
        {
            NumWidth = Agrs.MaxWidth % Agrs.DistanceTriggerX == 0 ? (int)(Agrs.MaxWidth / Agrs.DistanceTriggerX) : (int)(Agrs.MaxWidth / Agrs.DistanceTriggerX) + 1;
            NumHeight = Agrs.MaxHeight % Agrs.DistanceTriggerY == 0 ? (int)(Agrs.MaxHeight / Agrs.DistanceTriggerY) : (int)(Agrs.MaxHeight / Agrs.DistanceTriggerY) + 1;
        }
        public void Clear()
        {
            foreach (var array in Images)
            {
                foreach (var item in array)
                {
                    item.Dispose();
                }
            }
            foreach (var array in Images05)
            {
                foreach (var item in array)
                {
                    item.Dispose();
                }
            }
            Images05 = new List<ImageBlockCalib[]>();
            Images = new List<ImageBlockCalib[]>();

            foreach (var array in Images)
            {
                foreach (var item in array)
                {
                    item.Dispose();
                }
            }
            foreach (var array in Images05)
            {
                foreach (var item in array)
                {
                    item.Dispose();
                }
            }
            Images05 = new List<ImageBlockCalib[]>();
            Images = new List<ImageBlockCalib[]>();
        }
        public void Imaging0dot5(View.Tool.Loading Loading, double disX = 5, double disY = 5)
        {
            Refresh();
            int wImg = Convert.ToInt32(disX * Agrs.DPI / 25.4);
            int hImg = Convert.ToInt32(disY * Agrs.DPI / 25.4);
            Rectangle ROI = new Rectangle(Agrs.ImageSize.Width / 2 - wImg / 2, Agrs.ImageSize.Height / 2 - hImg / 2, wImg, hImg);
            // find trigger point
            var go = GoOrigin(Agrs.OriginCoordinate);
            if(go == 0)
            {
                _Light.SetOne(1, _Param.LightCH1);
                _Light.SetOne(2, _Param.LightCH2);
                _Light.SetOne(3, _Param.LightCH3);
                _Light.SetOne(4, _Param.LightCH4);
                mPLC.TOP.SetSpeed(20, 20);
                mCamera.Open();
                mCamera.StartGrabbing();
                mCamera.SetParameter(KeyName.ExposureTime, Agrs.ExposureTime);
                mCamera.SetParameter(KeyName.TriggerMode, 1);
                Thread.Sleep(1000);
                mCamera.ReleaseBuffer();
                mPLC.TOP.GoLeft();
                while (true)
                {
                    using (var bm = mCamera.GetOneBitmap(10000))
                    {
                        if (bm != null)
                        {
                            mPLC.TOP.ResetGoLeft();
                            break;
                        }
                    }
                }
                Point trigger = mPLC.TOP.GetXY();
                Console.WriteLine($"Trigger in {trigger}");
                var NumWidth05 = Agrs.MaxWidth % disX == 0 ? (int)(Agrs.MaxWidth / disX) : (int)(Agrs.MaxWidth / disX) + 1;
                var NumHeight05 = Agrs.MaxHeight % disY == 0 ? (int)(Agrs.MaxHeight / disY) : (int)(Agrs.MaxHeight / disY) + 1;
                mCamera.SetParameter(KeyName.TriggerMode, 0);
                mPLC.TOP.SetSpeed(_Param.ImagingSpeed, _Param.ImagingSpeed);

                
                for (int y = 0; y < NumHeight05; y++)
                {
                    Images05.Add(new ImageBlockCalib[NumWidth05]);
                    for (int x = 0; x < NumWidth05; x++)
                    {
                        Loading.Status = $"Captured {NumWidth05 * y + x + 1}/{NumHeight05 * NumWidth05} Images..."; 
                        //Rectangle location = new Rectangle((NumWidth - 1 - x) * wImg, (NumHeight - 1 - y) * hImg, wImg, hImg);
                        Point xy = new Point(Convert.ToInt32(trigger.X - (x * disX * Agrs.PPMX)), Convert.ToInt32(trigger.Y - (y * disY * Agrs.PPMY)));
                        mPLC.TOP.GoXY(xy);
                        mPLC.TOP.GoXYFinish();
                        mPLC.TOP.ResetGoFinish();
                        Thread.Sleep(150);
                        using (var bm = mCamera.GetOneBitmap(10000))
                        {
                            if(bm != null)
                            {
                                var block = new ImageBlockCalib(x, y, bm);
                                Images05[y][x] = block;
                            }
                        }
                    }
                }
                _Light.SetOne(1, 0);
                _Light.SetOne(2, 0);
                _Light.SetOne(3, 0);
                _Light.SetOne(4, 0);
            }
        }
        public void ImagingAll(Heal.View.Tool.Loading Loading)
        {
            Refresh();
            if (mCamera == null)
            {
                mLog.Error("Camera is emty! (Not found or cant connect camera)!");
                return;
            }
            mCamera.Open();
            mCamera.StartGrabbing();
            mCamera.SetParameter(KeyName.ExposureTime, Agrs.ExposureTime);
            mCamera.SetParameter(KeyName.TriggerMode, 1);
            mPLC.TOP.ResetGoFinish();
            int go = GoOrigin(Agrs.OriginCoordinate);
            int speed = Properties.Settings.Default.ImagingSpeed;
            mPLC.TOP.SetSpeed(speed, speed);
            if (go == 0)
            {
                _Light.SetOne(1, _Param.LightCH1);
                _Light.SetOne(2, _Param.LightCH2);
                _Light.SetOne(3, _Param.LightCH3);
                _Light.SetOne(4, _Param.LightCH4);
                for (int y = 0; y < NumHeight; y++)
                {
                    Point xy_y = mPLC.TOP.GetXY();
                    xy_y.Y = y > 0 ? xy_y.Y - Convert.ToInt32(Agrs.DistanceTriggerY * Agrs.PPMY) : xy_y.Y;
                    mPLC.TOP.GoXY(xy_y);
                    int goFinish = mPLC.TOP.GoXYFinish(20000);
                    mPLC.TOP.ResetGoFinish();
                    if(goFinish == 0)
                    {
                        mCamera.ReleaseBuffer();
                        Point xy_x = mPLC.TOP.GetXY();
                        xy_x.X = y % 2 == 0 ? Agrs.OriginCoordinate.X - NumWidth * Convert.ToInt32(Agrs.DistanceTriggerX * Agrs.PPMX) : Agrs.OriginCoordinate.X;
                        Images.Add(new ImageBlockCalib[NumWidth]);
                        mPLC.TOP.GoXY(xy_x);
                        Bitmap[] bms = new Bitmap[NumWidth];
                        for (int x = 0; x < NumWidth; )
                        {
                            Loading.Status = $"Captured {NumWidth * y + x + 1}/{NumHeight * NumWidth} images...";

                            var bm = mCamera.GetOneBitmap(10000);
                            if(bm != null)
                            {
                                bms[x] = bm;
                                x++;
                            }
                            else
                            {
                                mLog.Error("null image!");
                            }
                            //int wImg = Convert.ToInt32(Agrs.DistanceTriggerX * Agrs.DPI / 25.4);
                            
                        }
                        for (int x = 0; x < NumWidth; x++)
                        {
                            if(bms[x] != null)
                            {
                                int idX = y % 2 == 0 ? x : NumWidth - x - 1;
                                int idY = y;
                                var block = new ImageBlockCalib(idX, idY, bms[x]);
                                Images[idY][idX] = block;
                                bms[x].Dispose();
                                bms[x] = null;
                            }
                        }
                        mPLC.TOP.GoXYFinish(20000);
                        mPLC.TOP.ResetGoFinish();
                    }
                }
                _Light.SetOne(1, 0);
                _Light.SetOne(2, 0);
                _Light.SetOne(3, 0);
                _Light.SetOne(4, 0);
            }
        }
        public void ComputeMatrixAlign05(Heal.View.Tool.Loading Loading, double disX, double disY)
        {
            int wImg = Convert.ToInt32(disX * Agrs.DPI / 25.4);
            int hImg = Convert.ToInt32(disY * Agrs.DPI / 25.4);
            Rectangle ROI = new Rectangle(Agrs.ImageSize.Width / 2 - wImg / 2, Agrs.ImageSize.Height / 2 - hImg / 2, wImg, hImg);
            int ix = (int)(Agrs.DistanceTriggerX) / (int)disX;
            // compute matrix 0.5
            var NumWidth05 = Agrs.MaxWidth % disX == 0 ? (int)(Agrs.MaxWidth / disX) : (int)(Agrs.MaxWidth / disX) + 1;
            var NumHeight05 = Agrs.MaxHeight % disY == 0 ? (int)(Agrs.MaxHeight / disY) : (int)(Agrs.MaxHeight / disY) + 1;
            for (int y = 0; y < NumHeight05; y++)
            {
                for (int x = 0; x < NumWidth05; x++)
                {
                    Loading.Status = $"Compute {NumWidth05 * y + x + 1}/{NumHeight05 * NumWidth05} matrix alignment for 0.5mm...";
                    var block = Images05[y][x];
                    var refImage = GetReferenceImage2(x, y, wImg, hImg, ROI);
                    var featureDst = Stiching.GetFeature(refImage, DetectAlgoritm.AKAZE, 500);
                    var crImage = block.Image;
                    Mat matrix = Stiching.GetMatrix(crImage, featureDst, DetectAlgoritm.AKAZE, 500, MatrixType.WarpAffine);
                    block.ImageAlign = Stiching.AlignImage(crImage, matrix, MatrixType.WarpAffine);
                    //CvInvoke.Imwrite("0.png", refImage);
                    //CvInvoke.Imwrite("1.png", crImage);
                    //CvInvoke.Imwrite("2.png", block.ImageAlign);
                    featureDst.Dispose();
                    matrix.Dispose();
                }

            }
            for (int y = 0; y < NumHeight05; y++)
            {
                for (int x = 0; x < NumWidth05; x++)
                {
                    Loading.Status = $"Release {NumWidth05 * y + x + 1}/{NumHeight05 * NumWidth05} image 0.5mm...";
                    var block = Images05[y][x];
                    block.Image = null;
                    // release memory
                    if (x % ix != 0)
                    {
                        block.ImageAlign = null;
                    }
                }

            }

        }
        public void ComputeMatrixFrom0dot5(Heal.View.Tool.Loading Loading, double disX, double disY)
        {
            int wImg = Convert.ToInt32(Agrs.DistanceTriggerX * Agrs.DPI / 25.4);
            int hImg = Convert.ToInt32(Agrs.DistanceTriggerY * Agrs.DPI / 25.4);
            //Rectangle ROI = new Rectangle(Agrs.ImageSize.Width / 2 - wImg / 2, Agrs.ImageSize.Height / 2 - hImg / 2, wImg, hImg);
            int ix = (int)(Agrs.DistanceTriggerX) / (int)disX;
            int iy = (int)(Agrs.DistanceTriggerY) / (int)disY;
            // compute matrix 0.5
            ComputeMatrixAlign05(Loading, disX, disY);
            for (int y = 0; y < NumHeight; y++)
            {
                for (int x = 0; x < NumWidth; x++)
                {
                    Loading.Status = $"Compute {NumWidth * y + x + 1}/{NumHeight * NumWidth} matrix alignment...";
                    var block = Images[y][x];
                    using (Image<Bgr, byte> refImage = Images05[y * iy][x * ix].ImageAlign)
                    {
                        var crImage = block.Image;
                        KeyFeatureResult featureDst = Stiching.GetFeature(refImage, DetectAlgoritm.AKAZE, 500);
                        Mat matrix = Stiching.GetMatrix(crImage, featureDst, DetectAlgoritm.AKAZE, 500, MatrixType.WarpAffine);
                        featureDst.Dispose();
                        block.Matrix = matrix;
                    }
                }
            }
        }
        public void ComputeMatrix(Heal.View.Tool.Loading Loading)
        {
            int wImg = Convert.ToInt32(Agrs.DistanceTriggerX * Agrs.DPI / 25.4);
            int hImg = Convert.ToInt32(Agrs.DistanceTriggerY * Agrs.DPI / 25.4);
            Rectangle ROI = new Rectangle(Agrs.ImageSize.Width / 2 - wImg / 2, Agrs.ImageSize.Height / 2 - hImg / 2, wImg, hImg);
            //if (ReferenceMatrix == null)
            //    return;
            Image<Bgr, byte> refImage = null;
            for (int y = 0; y < NumHeight; y++)
            {
                for (int x = 0; x < NumWidth; x++)
                {
                    Loading.Status = $"Compute {NumWidth * y + x + 1}/{NumHeight* NumWidth} matrix alignment...";
                    var block = Images[y][x];
                    if (refImage != null)
                    {
                        refImage.Dispose();
                    }
                    refImage = GetReferenceImage(x, y, wImg, hImg, ROI);
                    var crImage = block.Image;
                    KeyFeatureResult featureDst = Stiching.GetFeature(refImage, DetectAlgoritm.AKAZE, 500);
                    Mat matrix = Stiching.GetMatrix(crImage, featureDst, DetectAlgoritm.AKAZE, 500, MatrixType.WarpAffine);
                    featureDst.Dispose();
                    block.Matrix = matrix;
                    block.ImageAlign = Stiching.AlignImage(crImage, block.Matrix, MatrixType.WarpAffine);
                }
            }
        }
        public Image<Bgr, byte> AdjustMatrix(ImageBlockCalib block)
        {
            int wImg = Convert.ToInt32(Agrs.DistanceTriggerX * Agrs.DPI / 25.4);
            int hImg = Convert.ToInt32(Agrs.DistanceTriggerY * Agrs.DPI / 25.4);
            Rectangle ROI = new Rectangle(Agrs.ImageSize.Width / 2 - wImg / 2, Agrs.ImageSize.Height / 2 - hImg / 2, wImg, hImg);
            Image<Bgr, byte> refImage = GetReferenceImage(block.X, block.Y, wImg, hImg, ROI);
            var crImage = block.Image;
            if(block.ImageAlign != null)
            {
                block.ImageAlign.Dispose();
                block.ImageAlign = null;
            }
            block.ImageAlign = Stiching.AlignImage(crImage, block.Matrix, MatrixType.WarpAffine);
            block.ImageAlign.ROI = ROI;
            refImage.ROI = ROI;
            block.ImageAlign.CopyTo(refImage);
            refImage.ROI = new Rectangle();
            block.ImageAlign.ROI = new Rectangle();
            Rectangle newROI = new Rectangle(ROI.X, ROI.Y, refImage.Width - ROI.X, refImage.Height - ROI.Y);
            refImage.ROI = newROI;
            return refImage;
        }
        public Mat SetMatrixValue(Mat mat, int row, int col, double value)
        {
            Marshal.Copy(new double[1] { value }, 0, mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, 1);
            return mat;
        }
        public double GetMatrixValue(Mat mat, int row, int col)
        {
            double[] value = new double[1];
            Marshal.Copy(mat.DataPointer + (row * mat.Cols + col) * mat.ElementSize, value, 0, 1);
            return value[0];
        }

        private Image<Bgr, byte> GetReferenceImage2(int x, int y, int wImg, int hImg, Rectangle ROI)
        {
            Image<Bgr, byte> refImage = null;
            if (y > 0)
            {
                // y > 0, x == 0
                refImage = new Image<Bgr, byte>(Agrs.ImageSize.Width, Agrs.ImageSize.Height + hImg);
                Rectangle L1 = new Rectangle(0, hImg, Agrs.ImageSize.Width, Agrs.ImageSize.Height);

                refImage.ROI = L1;
                Images05[y-1][x].ImageAlign.CopyTo(refImage);
                refImage.ROI = Rectangle.Empty;
            }
            else
            {
                if (x > 0)
                {
                    // y == 0, x > 0
                    refImage = new Image<Bgr, byte>(Agrs.ImageSize.Width + wImg , Agrs.ImageSize.Height);
                    Rectangle L1 = new Rectangle(wImg, 0, Agrs.ImageSize.Width, Agrs.ImageSize.Height);

                    refImage.ROI = L1;
                    Images05[y][x - 1].ImageAlign.CopyTo(refImage);
                    refImage.ROI = Rectangle.Empty;
                }
                else
                {
                    // x = 0, y == 0

                    if(Images05.Count > 0)
                    {
                        if(Images05[0].Length > 0)
                        {
                            if (Images05[y][x] != null)
                            {
                                if (Images05[y][x].Image != null)
                                {
                                    var image = Images05[y][x].Image.Copy();
                                    Heal.Utils.CvUtils.ImageRotation(image, new Point(image.Width / 2, image.Height / 2), this.Agrs.CamXAngle * Math.PI / 180.0);
                                    refImage = image;
                                }
                            }
                                
                        }
                    }
                    
                }
            }
            return refImage;
        }
        private Image<Bgr, byte> GetReferenceImage(int x, int y, int wImg, int hImg, Rectangle ROI)
        {
            Image<Bgr, byte> refImage = null;
            if (y > 0 && x == 0)
            {
                // y > 0, x == 0
                refImage = new Image<Bgr, byte>(wImg + ROI.X, 2 * hImg + ROI.Y);
                Images[y - 1][x].ImageAlign.ROI = ROI;
                Rectangle L1 = new Rectangle(ROI.X, ROI.Y + hImg, wImg, hImg);
                refImage.ROI = L1;
                Images[y - 1][x].ImageAlign.CopyTo(refImage);
                refImage.ROI = Rectangle.Empty;
                Images[y - 1][x].ImageAlign.ROI = Rectangle.Empty;
            }
            else if (y > 0 && x > 0)
            {

                /*   
                x  1
                2  3
                 */
                var im1 = Images[y][x - 1].ImageAlign;
                var im2 = Images[y - 1][x].ImageAlign;
                var im3 = Images[y - 1][x - 1].ImageAlign;
                refImage = new Image<Bgr, byte>(im1.Width + wImg, im1.Height + hImg);
                Rectangle L1 = new Rectangle(wImg, 0, wImg + ROI.X, im1.Height);
                Rectangle L2 = new Rectangle(0, hImg, im1.Width, hImg + ROI.Y);
                Rectangle L3 = new Rectangle(im1.Width - ROI.X, im1.Height - ROI.Y, wImg + ROI.X, hImg + ROI.Y);
                Rectangle ROI1 = ROI;
                ROI1.X = 0;
                ROI1.Y = 0;
                ROI1.Width += ROI.X;
                ROI1.Height = im1.Height;
                Rectangle ROI2 = ROI;
                ROI2.Y = 0;
                ROI2.X = 0;
                ROI2.Width = im1.Width;
                ROI2.Height += ROI.Y;

                Rectangle ROI3 = ROI;
                ROI3.Width += ROI.X;
                ROI3.Height += ROI.Y;
                Image<Bgr, byte>[] im = new Image<Bgr, byte>[] { im1, im2, im3 };
                Rectangle[] L = new Rectangle[] { L1, L2, L3 };
                Rectangle[] R = new Rectangle[] { ROI1, ROI2, ROI3 };
                //CvInvoke.Imwrite("1.png", im1);
                //CvInvoke.Imwrite("2.png", im2);
                //CvInvoke.Imwrite("3.png", im3);
                for (int i = 0; i < L.Length; i++)
                {
                    refImage.ROI = L[i];
                    im[i].ROI = R[i];
                    im[i].CopyTo(refImage);
                    im[i].ROI = Rectangle.Empty;
                    refImage.ROI = Rectangle.Empty;
                    //CvInvoke.Imwrite("11.png", refImage);
                }


            }
            else
            {
                if (x > 0)
                {
                    // y == 0, x > 0
                    refImage = new Image<Bgr, byte>(2 * wImg + ROI.X, hImg + ROI.Y);
                    Images[y][x - 1].ImageAlign.ROI = ROI;
                    Rectangle L1 = new Rectangle(wImg + ROI.X, ROI.Y, wImg, hImg);
                    refImage.ROI = L1;
                    Images[y][x - 1].ImageAlign.CopyTo(refImage);
                    refImage.ROI = Rectangle.Empty;
                    Images[y][x - 1].ImageAlign.ROI = Rectangle.Empty;
                }
                else
                {
                    // x = 0, y == 0
                    refImage = Images[y][x].Image.Copy();
                }
            }
            return refImage;
        }
        public Mat GetParallelMatrix()
        {
            //Refresh();
            //if (mCamera == null)
            //{
            //    mLog.Error("Camera is emty!");
            //    return null;
            //}
            //if (mPLC == null)
            //{
            //    mLog.Error("PLC is emty!");
            //    return null;
            //}
            //mCamera.Open();
            //mCamera.StartGrabbing();
            //mCamera.SetParameter(KeyName.ExposureTime, Agrs.ExposureTime);
            //mCamera.SetParameter(KeyName.TriggerMode, 0);
            //// go start point
            //int go = GoOrigin(new Point(30000,30000));
            //if(go == 0)
            //{ 
            //    Point triggerPoint = new Point();
            //    Image<Bgr, byte> imageTrg = null;
            //    mCamera.ReleaseBuffer();
            //    Thread.Sleep(1000);
            //    while(true)
            //    {
            //        using (var bm = mCamera.GetOneBitmap(30000))
            //        {
            //            if (bm != null)
            //            {
            //                mPLC.TOP.ResetGoLeft();
            //                triggerPoint = mPLC.TOP.GetXY();
            //                imageTrg = new Image<Bgr, byte>(bm);
            //                CvInvoke.GaussianBlur(imageTrg, imageTrg, new Size(5, 5), 1);
            //                break;
            //            }
            //        }
            //    }
            //    if (triggerPoint != new Point())
            //    {
            //        mCamera.SetParameter(KeyName.TriggerMode, 0);
            //        mPLC.TOP.SetSpeed(30000, 30000);
            //        Thread.Sleep(500);
            //        List<Image<Bgr, byte>> images = new List<Image<Bgr, byte>>(); 
            //        for (int x = -1; x < 2; x += 2)
            //        {
            //            for (int y = -1; y < 2; y += 2)
            //            {
            //                int newx = x * 1000 + triggerPoint.X;
            //                int newy = y * 1000 + triggerPoint.Y;
            //                Point newPoint = new Point(newx, newy);
            //                mPLC.TOP.GoXY(newPoint);
            //                mPLC.TOP.GoXYFinish();
            //                mPLC.TOP.ResetGoFinish();
            //                Thread.Sleep(1000);
            //                using (var bm = mCamera.GetOneBitmap(30000))
            //                {
            //                    if (bm != null)
            //                    {
            //                        using (var xbm = mCamera.GetOneBitmap(10000))
            //                        {
            //                            if (xbm != null)
            //                            {
            //                                Image<Bgr, byte> im = new Image<Bgr, byte>(xbm);
            //                                CvInvoke.GaussianBlur(im, im, new Size(5, 5), 1);
            //                                images.Add(im);
            //                            }
            //                        }
            //                    }
            //                }
            //            }
            //        }
            //        mPLC.SEND.Logout();
            //        KeyFeatureResult originFeature = Stiching.GetFeature(imageTrg, DetectAlgoritm.AKAZE, 500);
            //        List<Image<Gray, double>> matrixs = new List<Image<Gray, double>>();
            //        double totalX = 0;
            //        double totalY = 0;
            //        PointF center = new PointF(imageTrg.Width / 2.0f, imageTrg.Height / 2.0f);
            //        List<PointF> srcPoint = new List<PointF>();
            //        List<PointF> dstPoint = new List<PointF>();
            //        for (int i = 0; i < images.Count; i++)
            //        {
            //            var im = images[i];
            //            var matrix = Stiching.GetMatrix(im, originFeature, DetectAlgoritm.AKAZE, 500, MatrixType.Perspective);
            //            matrixs.Add(matrix.ToImage<Gray, double>());
            //            double x = matrixs[i][0, 2].Intensity;
            //            double y = matrixs[i][1, 2].Intensity;
            //            totalX += Math.Abs( x);
            //            totalY += Math.Abs(y);
            //            srcPoint.Add(new PointF((float)(center.X + x), (float)(center.Y + y)));
            //        }
            //        totalX /= 4;
            //        totalY /= 4; double beta = this.XYAngle * Math.PI / 180.0;
            //        double mx = (totalX - Math.Cos(beta) * totalX); 
            //        dstPoint.Add(new PointF((float)(center.X - totalX - mx), (float)(center.Y - totalY)));
            //        dstPoint.Add(new PointF((float)(center.X - totalX + mx), (float)(center.Y + totalY)));
            //        dstPoint.Add(new PointF((float)(center.X + totalX - mx), (float)(center.Y - totalY)));
            //        dstPoint.Add(new PointF((float)(center.X + totalX + mx), (float)(center.Y + totalY)));
            //        Mat affineMatrix = CvInvoke.FindHomography(srcPoint.ToArray(), dstPoint.ToArray(), RobustEstimationAlgorithm.Ransac);
            //        for (int i = 0; i < matrixs.Count; i++)
            //        {
            //            matrixs[i].Dispose();
            //            matrixs[i] = null;
            //        }
            //        originFeature.Dispose();
            //        ReferenceMatrix = affineMatrix;
            //        //var align = Stiching.AlignImage(imageTrg, ReferenceMatrix, MatrixType.Perspective);
            //        return affineMatrix;
            //    }
                
            //}
            //mPLC.SEND.Logout();
            return null;
        }
        public double FindXCamAngle()
        {
            Refresh();
            if (mCamera == null)
            {
                mLog.Error("Camera is emty!");
                return -1000;
            }
            if (mPLC == null)
            {
                mLog.Error("PLC is emty!");
                return -1000;
            }
            mCamera.Open();
            mCamera.StartGrabbing();
            mCamera.SetParameter(KeyName.ExposureTime, Agrs.ExposureTime);
            mCamera.SetParameter(KeyName.TriggerMode, 0);
            // go start point
            Point[] p = new Point[] { new Point(30000, 30000), new Point(27750, 30000) , new Point(30000, 28000) };
            Image<Bgr, byte>[] images = new Image<Bgr, byte>[3];
            for (int i = 0; i < images.Length; i++)
            {
                int go = GoOrigin(p[i]);
                if (go == 0)
                {
                    mCamera.ReleaseBuffer();
                    Thread.Sleep(1000);
                    using (var bm = mCamera.GetOneBitmap(1000))
                    {
                        if (bm != null)
                        {
                            images[i] = new Image<Bgr, byte>(bm);
                            CvInvoke.GaussianBlur(images[i], images[i], new Size(5, 5), 1);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            if (images[0] != null && images[1] != null && images[2] != null)
            {
                // CamX Angle
                {
                    var feature = Stiching.GetFeature(images[0], DetectAlgoritm.AKAZE, 500);

                    Mat matrix = Stiching.GetMatrix(images[1], feature, DetectAlgoritm.AKAZE, 500, MatrixType.WarpAffine);
                    using (Image<Gray, double> matrixImage = matrix.ToImage<Gray, double>())
                    {
                        double atan = matrixImage[1, 2].Intensity / matrixImage[0, 2].Intensity;
                        double angle = Math.Atan(atan);
                        this.XCameraAngle = angle * 180.0 / Math.PI;
                    }
                }
                // XY Angle
                {
                    //CvInvoke.Imwrite("1.png", images[0]);
                    Heal.Utils.CvUtils.ImageRotation(images[0], new Point(images[0].Width / 2, images[0].Height / 2), this.XCameraAngle * Math.PI / 180);
                    Heal.Utils.CvUtils.ImageRotation(images[2], new Point(images[2].Width / 2, images[2].Height / 2), this.XCameraAngle * Math.PI / 180);
                    //CvInvoke.Imwrite("11.png", images[0]);
                    var feature = Stiching.GetFeature(images[0], DetectAlgoritm.AKAZE, 500);
                    Mat matrix = Stiching.GetMatrix(images[2], feature, DetectAlgoritm.AKAZE, 500, MatrixType.WarpAffine);
                    using (Image<Gray, double> matrixImage = matrix.ToImage<Gray, double>())
                    {
                        double atan = matrixImage[0, 2].Intensity / matrixImage[1, 2].Intensity; 
                        double angle = Math.Atan(atan);
                        this.XYAngle = angle * 180.0 / Math.PI;
                    }
                }

                return 0;

            }
            else return -1000;
            
        }
        public int GoOrigin(Point P)
        {
            mPLC.TOP.SetSpeed(100000, 100000);
            Point xy = mPLC.TOP.GetXY();
            if(xy.X != P.X || P.Y != xy.Y)
            {
                mPLC.SEND.Login();
                int goXY = mPLC.TOP.GoXY(P);
                if (goXY == 0)
                {
                    int goFinish = mPLC.TOP.GoXYFinish();
                    mPLC.TOP.ResetGoFinish();
                    if (goFinish == 0)
                    {
                        return 0;
                    }
                }
                return -1;
            }
            return 0;
        }
        public void Save()
        {
            string s = JsonConvert.SerializeObject(this);
            System.Windows.Forms.SaveFileDialog dialog = new System.Windows.Forms.SaveFileDialog();
            dialog.Filter = "Json File | *.json";
            dialog.DefaultExt = "json";
            if(dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                File.WriteAllText(dialog.FileName, s);
            }
        }
        public void Save(string path)
        {
            string s = JsonConvert.SerializeObject(this);
            File.WriteAllText(path, s);
        }
        public void SaveMatrix(string path)
        {
            foreach (var array in this.Images)
            {
                foreach (var block in array)
                {
                    if(block != null)
                    {
                        //if (block.Image != null)
                        //    block.Image.Dispose();
                        block.Image = null;
                        //if (block.ImageAlign != null)
                        //    block.ImageAlign.Dispose();
                        block.ImageAlign = null;
                    }
                    
                }
            }
            foreach (var array in this.Images05)
            {
                foreach (var block in array)
                {
                    if (block != null)
                    {
                        //if (block.Image != null)
                        //    block.Image.Dispose();
                        block.Image = null;
                        //if (block.ImageAlign != null)
                        //    block.ImageAlign.Dispose();
                        block.ImageAlign = null;
                    }
                }
            }
            string s = JsonConvert.SerializeObject(this);
            File.WriteAllText(path, s);
        }
        public void SaveBoardImage(string path)
        {
            foreach (var array in this.Images)
            {
                foreach (var block in array)
                {
                    string imPath = $"{path}/{block.X}_{block.Y}.png";
                    CvInvoke.Imwrite(imPath, block.Image);
                }
            }
            
        }
        public static Calibration LoadMatrix(string path)
        {
            Calibration o = null;
            if(File.Exists(path))
            {
                string s = File.ReadAllText(path);
                try
                {
                    o = JsonConvert.DeserializeObject<Calibration>(s);
                }
                catch (Exception)
                {

                }
            }
            if(o != null)
            {
                foreach (var array in o.Images)
                {
                    foreach (var block in array)
                    {
                        if (block != null)
                        {
                            block._ImagePath = block._ImagePath1;
                            block._ImageAlign = block._ImageAlign1;
                        }

                    }
                }
                foreach (var array in o.Images05)
                {
                    foreach (var block in array)
                    {
                        if (block != null)
                        {
                            block._ImagePath = block._ImagePath1;
                            block._ImageAlign = block._ImageAlign1;
                        }
                    }
                }
            }
            return o;
        }
    }
    public class  CalibrationAgrs
    {
        public Size ImageSize { get; set; }
        public double DistanceTriggerX { get; set; }
        public double DistanceTriggerY { get; set; }
        public double DPI { get; set; }
        public double MaxWidth { get; set; }
        public double MaxHeight { get; set; }
        public double PPMX { get; set; }
        public double PPMY { get; set; }
        public Point OriginCoordinate { get; set; }
        public int ExposureTime { get; set; }
        public double CamXAngle { get; set; }
        public void LoadFromSetting()
        {
            Properties.Settings param = Properties.Settings.Default;
            this.ImageSize = new Size(param.ImageSize.Width, param.ImageSize.Height);
            this.DistanceTriggerX = param.DistanceTrgX;
            this.DistanceTriggerY = param.DistanceTrgY;
            this.DPI = param.DPI;
            this.MaxHeight = param.Max_Board_Height;
            this.MaxWidth = param.Max_Board_Width;
            this.PPMX = param.PPMX;
            this.PPMY = param.PPMY;
            this.OriginCoordinate = new Point(param.OriginCoordinate.X, param.OriginCoordinate.Y);
            this.ExposureTime = param.ExposureTime;
            this.CamXAngle = param.Cam_X_Angle;
        }
    }
    public class ImageBlockCalib
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string savePath  = "temp";
        public string _ImagePath { get; set; }
        public string _ImageAlign { get; set; }
        public string _ImagePath1 { get; set; }
        public string _ImageAlign1 { get; set; }
        public Image<Bgr, byte> Image { 
            get
            {
                if(File.Exists(_ImagePath))
                {
                    return new Image<Bgr, byte>(_ImagePath);
                }
                return null;
            }
            set
            {
                if(value != null)
                {
                    if (string.IsNullOrEmpty(_ImagePath))
                    {
                        _ImagePath = $"{savePath}/image_{Y}_{X}_{DateTime.Now.ToBinary().ToString()}.png";
                        _ImagePath1 = _ImagePath;
                    }
                    if (!Directory.Exists(savePath))
                        Directory.CreateDirectory(savePath);
                    CvInvoke.Imwrite(_ImagePath, value);
                    value.Dispose();
                }
                else
                {
                    _ImagePath = null;
                }
            }
        }
        public Image<Bgr, byte> ImageAlign
        {
            get
            {
                if (File.Exists(_ImageAlign))
                {
                    return new Image<Bgr, byte>(_ImageAlign);
                }
                return null;
            }
            set
            {
                if (value != null)
                {
                    if (string.IsNullOrEmpty(_ImageAlign))
                    {
                        _ImageAlign = $"{savePath}/align_{Y}_{X}_{DateTime.Now.ToBinary().ToString()}.png";
                        _ImageAlign1 = _ImageAlign;
                    }
                    if (!Directory.Exists(savePath))
                        Directory.CreateDirectory(savePath);
                    CvInvoke.Imwrite(_ImageAlign, value);
                    value.Dispose();
                }
                else
                {
                    _ImageAlign = null;
                }
            }
        }
        public Point CapturePoint { get; set; }
        public Mat Matrix { get; set; }
        public int ReturnWidth { get; set; }
        public int ReturnHeight { get; set; }
        public double X_CameraAngle { get; set; }
        public double XY_AxisAngle { get; set; }
        public ImageBlockCalib()
        { }
        public ImageBlockCalib(int x, int y, Bitmap bm)
        {
            this.X = x;
            this.Y = y;
            this.Image = new Image<Bgr, byte>(bm);
        }
        public ImageBlockCalib(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
        public  void Dispose()
        {
            if(this.Image != null)
            {
                this.Image.Dispose();
                this.Image = null;
                if (!string.IsNullOrEmpty(_ImagePath))
                {
                    File.Delete(_ImagePath);
                }
            }
            if (this.ImageAlign != null)
            {
                this.ImageAlign.Dispose();
                this.ImageAlign = null;
                if (!string.IsNullOrEmpty(_ImageAlign))
                {
                    File.Delete(_ImageAlign);
                }
            }
        }
    }
}
