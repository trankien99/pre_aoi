﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV.Features2D;
using Emgu.CV.CvEnum;
using Emgu.CV.Flann;
using System.Drawing;
using Emgu.CV.XFeatures2D;
using Emgu.CV.Stitching;
using System.IO;
using Heal.SDK;
using NLog;
using System.Threading;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Heal.Models.Calib
{
    
   public  class Imaging
    {
        private Devices.PLC.MyPLC _PLC = Devices.PLC.MyPLC.GetInstance();
        private HikCamera _Camera = Devices.Camera.MyCamera.GetInstance();
        private Devices.Light.MyLight _Light = Devices.Light.MyLight.GetInstance();
        private Properties.Settings _Param = Properties.Settings.Default;
        private Logger _Log = LogCtl.GetInstance();
        public Calibration Calib { get; set; }
        public int NumWidth { get; set; }
        public int NumHeight { get; set; }
        public Heal.Resource.Control.PLCMonitor _PLCMonitor { get; set; }
        public Imaging(Calibration Calib, double Width, double Height, Heal.Resource.Control.PLCMonitor PLCMonitor = null)
        {
            this.Calib = Calib;
            if(_Camera != null)
            {
                _Camera.Open();
                _Camera.SetParameter(KeyName.TriggerMode, 1);
                _Camera.SetParameter(KeyName.ExposureTime, _Param.ExposureTime);
                _Camera.StartGrabbing();
            }
            NumWidth = Width % Calib.Agrs.DistanceTriggerX == 0 ? (int)(Width / Calib.Agrs.DistanceTriggerX) : (int)(Width / Calib.Agrs.DistanceTriggerX) + 1;
            NumHeight = Height % Calib.Agrs.DistanceTriggerY == 0 ? (int)(Height / Calib.Agrs.DistanceTriggerY) : (int)(Height / Calib.Agrs.DistanceTriggerY) + 1;
           
            this._PLCMonitor = PLCMonitor;
        }
        public int WriteArrayXY()
        {
            Point[] points = GetArrayPoint();
            return _PLC.TOP.WriteArrayXY(points);
        }
        public MyImage RunMode2()
        {
            _PLC = new Devices.PLC.MyPLC();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MyImage myImage = new MyImage();
            double scaleStd = _Param.STD_DPI / _Param.DPI;
            int wImg = Convert.ToInt32(Calib.Agrs.DistanceTriggerX * Calib.Agrs.DPI / 25.4);
            int hImg = Convert.ToInt32(Calib.Agrs.DistanceTriggerY * Calib.Agrs.DPI / 25.4);
            myImage.ImageWidth = Convert.ToInt32(wImg * NumWidth * scaleStd);
            myImage.ImageHeight = Convert.ToInt32(hImg * NumHeight * scaleStd);
            Rectangle ROI_Origin = new Rectangle(Calib.Agrs.ImageSize.Width / 2 - wImg / 2, Calib.Agrs.ImageSize.Height / 2 - hImg / 2, wImg, hImg);
            List<ImageBlock> blocks = new List<ImageBlock>();
            if (_Camera == null)
            {
                _Log.Error("Camera is emty! (Not found or cant connect camera)!");
                return null;
            }
            _PLC.TOP.SetSpeed(_Param.ImagingSpeed, _Param.ImagingSpeed);
            GoPoint(this.Calib.Agrs.OriginCoordinate, _Param.ImagingSpeed, _Param.ImagingSpeed, true);
            
            int fail = 0;
            TurnOnLight();
            _Log.Info($"Init imaging in {sw.ElapsedMilliseconds}");
            Point[] points = GetArrayPoint();
            if (_PLCMonitor != null)
            {
                this._PLCMonitor.Dispatcher.Invoke(() => {
                    this._PLCMonitor.CurrentPoint = new System.Windows.Point(Calib.Agrs.OriginCoordinate.X, Calib.Agrs.OriginCoordinate.Y);
                });
            }

            _Camera.ReleaseBuffer();
            _PLC.TOP.ResetGoFinish();
            _PLC.TOP.SetAutoGoArrayXY();
            for (int y = 0; y < NumHeight; y++)
            {
                for (int x = 0; x < NumWidth;)
                {
                    using (var bm = _Camera.GetOneBitmap(Convert.ToInt32(30000000 / _Param.ImagingSpeed)))
                    {
                        if (bm != null)
                        {
                            int idX = y % 2 == 0 ? x : NumWidth - x - 1;
                            int idY = y;
                            var block = new ImageBlock(idX, idY, bm);
                            blocks.Add(block);
                            fail = 0;
                            bool transfer = false;
                            Thread processingThread = new Thread(() => {
                                lock (block)
                                {
                                    int i = blocks.Count;
                                    var _block = block;
                                    int dx = _block.X;
                                    int dy = _block.Y;
                                    var matrix = Calib.Images[dy][dx].Matrix;
                                    var ROI = ROI_Origin;
                                    transfer = true;
                                    //CvInvoke.Imwrite($"debug/{dx}_{dy}.jpg", _block.Image);
                                    var imageAligned = Stiching.AlignImage(_block.Image, matrix, MatrixType.WarpAffine);
                                    imageAligned.ROI = ROI;
                                    Rectangle location = new Rectangle((NumWidth - 1 - dx) * wImg, (NumHeight - 1 - dy) * hImg, wImg, hImg);
                                    location.X = Convert.ToInt32(location.X * scaleStd);
                                    location.Y = Convert.ToInt32(location.Y * scaleStd);
                                    location.Width = Convert.ToInt32(location.Width * scaleStd);
                                    location.Height = Convert.ToInt32(location.Height * scaleStd);
                                    Image<Bgr, byte> imageScale = imageAligned.Resize(location.Width, location.Height, Inter.Linear);
                                    Models.ImageBlock programBlock = new Models.ImageBlock(i, imageScale, location);
                                    lock (myImage)
                                        myImage.Blocks.Add(programBlock);
                                    _block.Dispose();
                                    _block = null;
                                    imageAligned.Dispose();
                                    imageAligned = null;
                                    imageScale.Dispose();
                                    imageScale = null;
                                    if (_PLCMonitor != null)
                                    {
                                        //var crPoint = new System.Windows.Point(xy_y.X, xy_y.Y);
                                        //double subPulse = (xy_x.X - crPoint.X) / NumWidth;
                                        //crPoint.X += y % 2 == 0 ? dx * subPulse : -dx * subPulse + subPulse * NumWidth;
                                        //this._PLCMonitor.Dispatcher.Invoke(() =>
                                        //{
                                        //    this._PLCMonitor.CurrentPoint = new System.Windows.Point(crPoint.X, crPoint.Y);
                                        //});
                                    }
                                }

                            });
                            processingThread.Start();
                            while (transfer) ;
                            x++;
                        }
                        else
                        {
                            _Log.Error("Imaging is emty. Return null image!");
                            fail++;
                        }
                        if (fail == 3)
                            break;
                    }
                }

               
                if (fail == 3)
                    break;
                if (_PLCMonitor != null && y % 2 == 0)
                {
                    //var crPoint = new System.Windows.Point(xy_y.X, xy_y.Y);
                    //double subPulse = (xy_x.X - crPoint.X) / NumWidth;
                    //crPoint.X += y % 2 == 0 ? NumWidth * subPulse : -NumWidth * subPulse + subPulse * NumWidth;
                    //this._PLCMonitor.Dispatcher.Invoke(() =>
                    //{
                    //    this._PLCMonitor.CurrentPoint = new System.Windows.Point(crPoint.X, crPoint.Y);
                    //});
                }
            }
            TurnOffLight();
            GoPoint(this.Calib.Agrs.OriginCoordinate, _Param.ImagingSpeed, _Param.ImagingSpeed, false);
            if (fail == 3)
                return null;

            if (_PLCMonitor != null)
            {
                this._PLCMonitor.Dispatcher.Invoke(() => {
                    this._PLCMonitor.CurrentPoint = new System.Windows.Point(Calib.Agrs.OriginCoordinate.X, Calib.Agrs.OriginCoordinate.Y);
                });
            }
            for (int i = 0; i < blocks.Count; i++)
            {
                var block = blocks[i];
                if (block != null)
                    block.Dispose();
                block = null;
            }
            _Log.Info($"Imaging in {sw.ElapsedMilliseconds} ms...");
            return myImage;
        }
        private Point[] GetArrayPoint()
        {
            List<Point> ps = new List<Point>();
            int distanceX = NumWidth * Convert.ToInt32(Calib.Agrs.DistanceTriggerX * Calib.Agrs.PPMX);
            int distanceY = Convert.ToInt32(Calib.Agrs.DistanceTriggerY * Calib.Agrs.PPMY);
            //Point start = new Point(Calib.Agrs.OriginCoordinate.X, Calib.Agrs.OriginCoordinate.Y);
            //ps.Add(start);
            for (int y = 0; y < NumHeight; y++)
            {
                Point xy_y = new Point(Calib.Agrs.OriginCoordinate.X, Calib.Agrs.OriginCoordinate.Y);
                xy_y.X = y % 2 != 0 ? Calib.Agrs.OriginCoordinate.X - distanceX : Calib.Agrs.OriginCoordinate.X;
                xy_y.Y = Calib.Agrs.OriginCoordinate.Y - y * distanceY;
                ps.Add(xy_y);
                Point xy_x = new Point(xy_y.X, xy_y.Y);
                xy_x.X = y % 2 == 0 ? Calib.Agrs.OriginCoordinate.X - distanceX : Calib.Agrs.OriginCoordinate.X;
                ps.Add(xy_x);
            }
            return ps.ToArray();
        }
        public MyImage Run()
        {
            _PLC = new Devices.PLC.MyPLC();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            MyImage myImage = new MyImage();
            double scaleStd = _Param.STD_DPI / _Param.DPI;
            int wImg = Convert.ToInt32(Calib.Agrs.DistanceTriggerX * Calib.Agrs.DPI / 25.4);
            int hImg = Convert.ToInt32(Calib.Agrs.DistanceTriggerY * Calib.Agrs.DPI / 25.4);
            myImage.ImageWidth = Convert.ToInt32(wImg * NumWidth * scaleStd);
            myImage.ImageHeight = Convert.ToInt32(hImg * NumHeight * scaleStd); 
            Rectangle ROI_Origin = new Rectangle(Calib.Agrs.ImageSize.Width / 2 - wImg / 2, Calib.Agrs.ImageSize.Height / 2 - hImg / 2, wImg, hImg);
            List<ImageBlock> blocks = new List<ImageBlock>();
            if (_Camera == null)
            {
                _Log.Error("Camera is emty! (Not found or cant connect camera)!");
                return null;
            }_PLC.TOP.SetSpeed(_Param.ImagingSpeed, _Param.ImagingSpeed);
            GoPoint(this.Calib.Agrs.OriginCoordinate, _Param.ImagingSpeed, _Param.ImagingSpeed, true);
            _Camera.ReleaseBuffer();
            _PLC.TOP.ResetGoFinish();
            int fail = 0;
            int distanceX = NumWidth * Convert.ToInt32(Calib.Agrs.DistanceTriggerX * Calib.Agrs.PPMX);
            int distanceY = Convert.ToInt32(Calib.Agrs.DistanceTriggerY * Calib.Agrs.PPMY);
            TurnOnLight();
            _Log.Info($"Init imaging in {sw.ElapsedMilliseconds}");
            if (_PLCMonitor != null)
            {
                this._PLCMonitor.Dispatcher.Invoke(() => {
                    this._PLCMonitor.CurrentPoint = new System.Windows.Point(Calib.Agrs.OriginCoordinate.X, Calib.Agrs.OriginCoordinate.Y);
                });
            }
            

            for (int y = 0; y < NumHeight; y++)
            {
                Point xy_y = new Point(Calib.Agrs.OriginCoordinate.X, Calib.Agrs.OriginCoordinate.Y);
                xy_y.X = y % 2 != 0 ? Calib.Agrs.OriginCoordinate.X - distanceX : Calib.Agrs.OriginCoordinate.X;
                xy_y.Y = Calib.Agrs.OriginCoordinate.Y - y * distanceY;
                if (y != 0)
                {
                    _PLC.TOP.GoXY(xy_y);
                    int goFinish = _PLC.TOP.GoXYFinish(10000);
                    _PLC.TOP.ResetGoFinish();
                    if (_PLCMonitor != null)
                    {
                        this._PLCMonitor.Dispatcher.Invoke(() =>
                        {
                            this._PLCMonitor.CurrentPoint = new System.Windows.Point(xy_y.X, xy_y.Y);
                        });
                    }
                }
                Point xy_x = new Point(xy_y.X, xy_y.Y);
                xy_x.X = y % 2 == 0 ? Calib.Agrs.OriginCoordinate.X - distanceX : Calib.Agrs.OriginCoordinate.X;
                _PLC.TOP.GoXY(xy_x);
                
                for (int x = 0; x < NumWidth;)
                {
                    using (var bm = _Camera.GetOneBitmap(Convert.ToInt32(10000000 / _Param.ImagingSpeed)))
                    {
                        if (bm != null)
                        {
                            int idX = y % 2 == 0 ? x : NumWidth - x - 1;
                            int idY = y;
                            var block = new ImageBlock(idX, idY, bm);
                            blocks.Add(block);
                            fail = 0;
                            bool transfer = false;
                            Thread processingThread = new Thread(() => {
                                lock (block)
                                {
                                    int i = blocks.Count;
                                    var _block = block;
                                    int dx = _block.X;
                                    int dy = _block.Y;
                                    var matrix = Calib.Images[dy][dx].Matrix;
                                    var ROI = ROI_Origin;
                                    transfer = true;
                                    var imageAligned = Stiching.AlignImage(_block.Image, matrix, MatrixType.WarpAffine);
                                    imageAligned.ROI = ROI;
                                    Rectangle location = new Rectangle((NumWidth - 1 - dx) * wImg, (NumHeight - 1 - dy) * hImg, wImg, hImg);
                                    location.X = Convert.ToInt32(location.X * scaleStd);
                                    location.Y = Convert.ToInt32(location.Y * scaleStd);
                                    location.Width = Convert.ToInt32(location.Width * scaleStd);
                                    location.Height = Convert.ToInt32(location.Height * scaleStd);
                                    Image<Bgr, byte> imageScale = imageAligned.Resize(location.Width, location.Height, Inter.Linear);
                                    Models.ImageBlock programBlock = new Models.ImageBlock(i, imageScale, location);
                                    lock (myImage)
                                        myImage.Blocks.Add(programBlock);
                                    _block.Dispose();
                                    _block = null;
                                    imageAligned.Dispose();
                                    imageAligned = null;
                                    imageScale.Dispose();
                                    imageScale = null;
                                    if (_PLCMonitor != null)
                                    {
                                        var crPoint = new System.Windows.Point(xy_y.X, xy_y.Y);
                                        double subPulse = (xy_x.X - crPoint.X) / NumWidth;
                                        crPoint.X += y % 2 == 0 ? dx * subPulse : -dx * subPulse + subPulse * NumWidth;
                                        this._PLCMonitor.Dispatcher.Invoke(() => {
                                            this._PLCMonitor.CurrentPoint = new System.Windows.Point(crPoint.X, crPoint.Y);
                                        });
                                    }
                                }

                            });
                            processingThread.Start();
                            while (transfer) ;
                            x++;
                        }
                        else
                        {
                            _Log.Error("Imaging is emty. Return null image!");
                            fail++;
                        }
                        if (fail == 3)
                            break;
                    }
                }
                
                _PLC.TOP.GoXYFinish();
                _PLC.TOP.ResetGoFinish();
                if (fail == 3)
                    break;
                if (_PLCMonitor != null && y % 2 == 0)
                {
                    var crPoint = new System.Windows.Point(xy_y.X, xy_y.Y);
                    double subPulse = (xy_x.X - crPoint.X) / NumWidth;
                    crPoint.X += y % 2 == 0 ? NumWidth * subPulse : -NumWidth * subPulse + subPulse * NumWidth;
                    this._PLCMonitor.Dispatcher.Invoke(() =>
                    {
                        this._PLCMonitor.CurrentPoint = new System.Windows.Point(crPoint.X, crPoint.Y);
                    });
                }
            }
            TurnOffLight();
            GoPoint(this.Calib.Agrs.OriginCoordinate, _Param.ImagingSpeed, _Param.ImagingSpeed, false);
            if (fail == 3)
                return null;

            if (_PLCMonitor != null)
            {
                this._PLCMonitor.Dispatcher.Invoke(() => {
                    this._PLCMonitor.CurrentPoint = new System.Windows.Point(Calib.Agrs.OriginCoordinate.X, Calib.Agrs.OriginCoordinate.Y);
                });
            }
            for (int i = 0; i < blocks.Count; i++)
            {
                var block = blocks[i]; 
                if (block!= null)
                    block.Dispose(); 
                block = null;
            }
            _Log.Info($"Imaging in {sw.ElapsedMilliseconds} ms...");
            return myImage;
        }
        public void TurnOnLight()
        {
            _Light.SetOne(1, _Param.LightCH1);
            _Light.SetOne(2, _Param.LightCH2);
            _Light.SetOne(3, _Param.LightCH3);
            _Light.SetOne(4, _Param.LightCH4);
        }
        public void TurnOffLight()
        {
            _Light.SetOne(1, 0);
            _Light.SetOne(2, 0);
            _Light.SetOne(3, 0);
            _Light.SetOne(4, 0);
        }
        public int GoPoint(Point origin, int SpeedX, int SpeedY, bool CheckGoFinish = false)
        {
            //_PLC.TOP.SetSpeed(SpeedX, SpeedY);
            int goXY = _PLC.TOP.GoXY(origin);
            if(CheckGoFinish)
            {
                if(goXY == 0)
                {
                    int goFinish = _PLC.TOP.GoXYFinish(10000);
                    return goFinish;
                }
                return -1;
            }
            return goXY;
        }
    }
   public class ImageBlock
   {
        public Image<Bgr, byte>  Image { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public Rectangle ROI { get; set; }
        public Rectangle Location { get; set; }
        public ImageBlock(int x, int y, Bitmap bm)
        {
            this.X = x;
            this.Y = y;
            this.Image = new Image<Bgr, byte>(bm);
        }
        public void Dispose()
        {
            if(this.Image != null)
            {
                this.Image.Dispose();
                this.Image = null;
            }
        }
    }
}
