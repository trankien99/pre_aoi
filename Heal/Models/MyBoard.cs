﻿using Emgu.CV;
using Emgu.CV.Structure;
using Heal.SDK;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace Heal.Models
{
    public class MyBoard : BaseProperties
    {
        private static Logger mLog = LogCtl.GetInstance();
        public BoardSide Side { get; set; }
        public double Thickness { get; set; }
        public double DPI { get; set; }
        public MyImage Image { get; set; }
        public int PCBBlocks { get; set; }
        public bool IsEnable { get; set; }
        public ObservableCollection<Catalog> Catalogs { get; set; }
        public ObservableCollection<Library> AlarmLibrary { get; set; }
        public static MyBoard Currently { get; set; }
        public MyBoard()
        {
            this.Name = "My Program";
            this.Side = BoardSide.TOP;
            this.Width = 165;
            this.Height = 186;
            this.Thickness = 1.6;
            this.PCBBlocks = 1;
            this.Image = new MyImage();
            this.DPI = Const.STD_DPI;
            this.Catalogs = new ObservableCollection<Catalog>();
            this.AlarmLibrary = new ObservableCollection<Library>();
        }
        public ObservableCollection<SMD> GetAllSMD()
        {
            ObservableCollection<SMD> smds = new ObservableCollection<SMD>();
            foreach (var catalog in Catalogs)
            {
                foreach (var item in catalog.SMDs)
                {
                    smds.Add(item);
                }
            }
            return smds;
        }
        public void AdjustSMDCenter()
        {
            Run.Inspect inspect = new Run.Inspect(this.Image);
            var dev = inspect.GetDeviation(this);
            if(dev != null)
            {
                foreach (var catalog in Catalogs)
                {
                    foreach (var smd in catalog.SMDs)
                    {
                        Point crPoint = new Point(smd.X, smd.Y);
                        Point newPoint = inspect.GetRealLocation(crPoint, dev);
                        smd.X = newPoint.X;
                        smd.Y = newPoint.Y;
                    }
                }
            }
        }
        public void CopyBlock(int SrcBlock, int DstBlock, double X, double Y, double Angle)
        {
            List<SMD> smds = new List<SMD>();
            // get all smd in block
            foreach (var catalog in Catalogs)
            {
                foreach (var component in catalog.SMDs)
                {
                    if (component.Block == SrcBlock)
                    {
                        smds.Add(component);
                    }
                }
            }
            int x = Const.Millimeter2Pixel(X);
            int y = Const.Millimeter2Pixel(Y);
            double angle = Angle;
            Point srcBlockCenter = GetBlockCenter(SrcBlock);
            srcBlockCenter.X += x;
            srcBlockCenter.Y += y;
            foreach (var smd in smds)
            {
                Catalog catalogContain = GetCatalog(smd);
                if (catalogContain != null)
                {
                    SMD newSmd = smd.Copy();
                    newSmd.Block = DstBlock;
                    newSmd.Translate(x, y);
                    newSmd.Rotate(angle, srcBlockCenter);
                    catalogContain.SMDs.Add(newSmd);
                }
            }
        }
        public void EnableBlock(int Block, bool IsEnable)
        {
            List<SMD> smds = new List<SMD>();
            // get all smd in block
            foreach (var catalog in Catalogs)
            {
                foreach (var component in catalog.SMDs)
                {
                    if (component.Block == Block)
                    {
                        smds.Add(component);
                    }
                }
            }
            foreach (var item in smds)
            {
                item.IsEnable = IsEnable;
            }
        }
        public void DeleteBlock(int Block)
        {
            List<SMD> smds = new List<SMD>();
            // get all smd in block
            foreach (var catalog in Catalogs)
            {
                foreach (var component in catalog.SMDs)
                {
                    if (component.Block == Block)
                    {
                        smds.Add(component);
                    }
                }
            }
            foreach (var smd in smds)
            {
                Catalog catalogContain = GetCatalog(smd);
                if (catalogContain != null)
                {
                    catalogContain.SMDs.Remove(smd);
                }
            }

        }
        public Catalog AddCatalog(Catalog RefCatalog, Action A = Action.New)
        {
            Catalog c = null;
            if (RefCatalog != null)
            {
                c = RefCatalog.Copy();
                if (A == Action.New)
                {
                    c.Name += ".1";
                    c.SMDs.Clear();
                }
                else if(A == Action.Copy)
                {
                    c.Name = RefCatalog.Name + "_Copy";
                }
                Catalogs.Add(c);
            }
            else
            {
                c = new Catalog();
                c.Name = DateTime.Now.ToString("CHHmmss");
                Catalogs.Add(c);
            }
            Sort();
            return c;
        }
        public void RemoveCatalog(Catalog catalog)
        {
            Catalogs.Remove(catalog);
            Sort();
        }
        public void RemoveSMD(SMD smd)
        {
            Catalogs[smd.CatalogID].SMDs.Remove(smd);
            Sort();
        }
        public SMD AddSMD(SMD RefSMD, Action A = Action.New)
        {
            SMD smd = null;
            if (RefSMD != null)
            {
                smd = RefSMD.Copy();
                if(A == Action.New)
                {
                    smd.Name += ".1";
                }
                else if(A == Action.Copy)
                    smd.Name = RefSMD.Name + "_Copy";
                Catalogs[RefSMD.CatalogID].SMDs.Add(smd);
            }
            return smd;
        }
        public SMD AddSMD(int CatalogID)
        {
            SMD smd = new SMD(CatalogID);
            smd.Name = DateTime.Now.ToString("RHHmmss");
            Catalogs[CatalogID].SMDs.Add(smd);
            return smd;
        }
        public bool CanRun
        {
            get
            {
                bool canFoundMark = false;
                foreach (var catalog in this.Catalogs)
                {
                    if (catalog.Type == SMDType.Mark)
                    {
                        if (catalog.SMDs.Count >= 2)
                        {
                            canFoundMark = true;
                            break;
                        }
                    }
                }
                return canFoundMark;
            }
        }
        public object Find(string Name)
        {
            foreach (var catalog in Catalogs)
            {
                if (catalog.Name.Contains(Name))
                    return catalog;
                else
                {
                    foreach (var smd in catalog.SMDs)
                    {
                        if (smd.Name.Contains(Name))
                            return smd;
                    }
                }
            }
            return null;
        }
        public void LoadCad(List<CadItem> Cads)
        {
            for (int i = 0; i < Cads.Count; i++)
            {
                int id = GetIDComponentCode(Cads[i].Code);
                if (id == -1)
                {
                    id = Catalogs.Count;
                    Catalog cmp = new Catalog(Cads[i], id);
                    Catalogs.Add(cmp);
                }
                else
                {
                    Catalog cmp = Catalogs[id];
                    cmp.AddSMD(Cads[i], id);
                }
            }
        }
        private int GetIDComponentCode(string CompCode)
        {
            for (int i = 0; i < Catalogs.Count; i++)
            {
                if (CompCode == Catalogs[i].Name)
                {
                    return i;
                }
            }
            return -1;
        }
        public int CountSMD()
        {
            int count = 0;
            foreach (var catalog in this.Catalogs)
            {
                count += catalog.SMDs.Count;
            }
            return count;
        }
        public void Sort()
        {
            var CatalogSorted = Catalogs.OrderBy(i => i.Name);
            Catalogs = new ObservableCollection<Catalog>();
            for (int i = 0; i < CatalogSorted.Count(); i++)
            {
                Catalog c = CatalogSorted.ElementAt(i);
                c.Sort();
                Catalogs.Add(c);
                foreach (var smd in c.SMDs)
                {
                    smd.CatalogID = i;
                }
            }
        }
        public void SetEnable(bool Action)
        {
            IsEnable = Action;
            foreach (var item in Catalogs)
            {
                item.IsEnable = Action;
                //item.SetEnable(Action);
            }
        }
        public Catalog GetCatalog(SMD smd)
        {
            if (smd != null)
                return Catalogs[smd.CatalogID];
            else
                return null;
        }
        private Point GetBlockCenter(int Block)
        {
            Point ct = new Point();
            double x_min = 99999;
            double x_max = -1;
            double y_min = 99999;
            double y_max = -1;
            foreach (var catalog in Catalogs)
            {
                foreach (var smd in catalog.SMDs)
                {
                    if (smd.Block == Block)
                    {
                        x_min = x_min > smd.X ? smd.X : x_min;
                        y_min = y_min > smd.Y ? smd.Y : y_min;
                        x_max = x_max < smd.X ? smd.X : x_max;
                        y_max = y_max < smd.Y ? smd.Y : y_max;
                    }
                }
            }
            if (x_max > -1 && y_max > -1)
            {
                ct.X = Convert.ToInt32((x_max - x_min) / 2 + x_min);
                ct.Y = Convert.ToInt32((y_max - y_min) / 2 + y_min);
            }
            return ct;
        }
        public void Rotate(int Block, double Angle)
        {
            Point ct = GetBlockCenter(Block);
            foreach (var catalog in Catalogs)
            {
                foreach (var smd in catalog.SMDs)
                {
                    if (smd.Block == Block)
                    {
                        smd.Rotate(Angle, ct);
                    }
                }
            }

        }
        public void Translate(int Block, double X, double Y)
        {
            foreach (var catalog in Catalogs)
            {
                foreach (var smd in catalog.SMDs)
                {
                    if (smd.Block == Block)
                    {
                        smd.Translate(X, Y);
                    }
                }
            }
        }
        public void Flip(int Block, FlipMode F)
        {
            Point ct = GetBlockCenter(Block);
            foreach (var catalog in Catalogs)
            {
                foreach (var smd in catalog.SMDs)
                {
                    if (smd.Block == Block)
                    {
                        smd.Flip(F, ct);
                    }
                }
            }
        }
        public void SaveLibrary(string Dir)
        {
            foreach (var item in Catalogs)
            {
                item.SaveLibrary(Dir);
            }
        }
        public void LoadLibrary(string[] libs)
        {
            foreach (var p in libs)
            {
                var lib = Library.Load(p);
                if (lib != null)
                {
                    var crLib = ContainAlarmLibrary(lib.Name);
                    if (crLib == null)
                    {
                        AlarmLibrary.Add(lib);
                    }
                }
            }
            ApplyLibToCatalog();
        }
        public void ApplyLibToCatalog()
        {
            foreach (var lib in AlarmLibrary)
            {
                foreach (var catalog in Catalogs)
                {
                    if (lib.Name == catalog.Name)
                    {
                        foreach (var smd in catalog.SMDs)
                        {
                            if (smd.Lib != null)
                            {
                                if (smd.Lib.Name != lib.Name)
                                {
                                    continue;
                                }
                            }
                            smd.Lib = lib;
                        }
                    }
                }
            }
        }
        public static MyBoard Load(string Path)
        {
            MyBoard program = null;
            FileInfo fi = new FileInfo(Path);
            string tempPath = $"temp/models";
            if (Directory.Exists(tempPath))
            {
                try
                {
                    Directory.Delete(tempPath, true);
                }
                catch
                {
                    return null;
                }

            }
            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }
            string desPath = $"{tempPath}/{fi.Name}";
            string programDir = desPath.Replace(".fiaoi", "");
            string boardInfoPath = $"{programDir}/BoardInfo.json";
            try
            {
                File.Copy(Path, desPath);
                ZipFile.ExtractToDirectory(Path, desPath.Replace(".fiaoi", ""));
                string s = File.ReadAllText(boardInfoPath);

                program = JsonConvert.DeserializeObject<MyBoard>(s, new MyJsonConverter());
            }
            catch (Exception ex)
            {
                mLog.Error(ex.Message);
                return null;
            }
            program.Init();
            if(program.Image != null)
            {
                for (int i = 0; i < program.Image.Blocks.Count; i++)
                {
                    bool RET = program.Image.Blocks[i].Load(programDir);
                    if (RET == false)
                    {
                        program.Dispose();
                        return null;
                    }
                }
                // refresh inspected result
            }
            program?.ClearInspectedResult();
            FilesManagement.DeleteFiles(tempPath, 0, true, false);
            return program;
        }
        public void  ClearInspectedResult()
        {
            foreach (var catalog in this.Catalogs)
            {
                catalog.Inspected = InspectedStatus.Good;
                foreach (var smd in catalog.SMDs)
                {
                    smd.Inspected = InspectedStatus.Good;
                }
            }
        }
        private void Init()
        {
            AlarmLibrary = new ObservableCollection<Library>();
            foreach (var catalog in Catalogs)
            {
                foreach (var smd in catalog.SMDs)
                {
                    smd.IsSetFlag = false;
                    if (smd.Lib != null)
                    {
                        var lib = ContainAlarmLibrary(smd.Lib.Name);
                        if (lib == null)
                        {
                            AlarmLibrary.Add(smd.Lib);
                        }
                    }
                }
            }
            foreach (var catalog in Catalogs)
            {
                foreach (var smd in catalog.SMDs)
                {
                    if (smd.Lib != null)
                    {
                        var lib = ContainAlarmLibrary(smd.Lib.Name);
                        if (lib != null)
                        {
                            smd.Lib = lib;
                        }
                    }
                }
            }
        }
        private Library ContainAlarmLibrary(string Name)
        {
            foreach (var item in AlarmLibrary)
            {
                if (item.Name == Name)
                    return item;
            }
            return null;
        }
        public void Save(string path, SaveProgramMode SaveMode = SaveProgramMode.ProgramAndImage)
        {
            FileInfo fi = new FileInfo(path);
            string tempPath = $"temp/models/{this.Name}";
            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }
           
            var tempImage = this.Image;
            Image<Bgr, byte>[] image = new Image<Bgr, byte>[0];
            if (SaveMode == SaveProgramMode.Program)
            {
                this.Image = null;
            }
            else
            {
                if (this.Image != null)
                {
                    this.Image.DisposeImageChannel(); 
                    image = new Image<Bgr, byte>[this.Image.Blocks.Count];
                    for (int i = 0; i < this.Image.Blocks.Count; i++)
                    {
                        this.Image.Blocks[i].Save(tempPath);
                        image[i] = this.Image.Blocks[i].Image;
                        this.Image.Blocks[i].Image = null;
                    }
                }
                
            }
            string s = JsonConvert.SerializeObject(this);
            File.WriteAllText($"{tempPath}/BoardInfo.json", s);
            File.Delete(fi.FullName);
            ZipFile.CreateFromDirectory(tempPath, fi.FullName);
            if (SaveMode == SaveProgramMode.Program)
            {
                this.Image = tempImage;
            }
            else
            {
                if (this.Image != null)
                {
                    for (int i = 0; i < this.Image.Blocks.Count; i++)
                    {
                        this.Image.Blocks[i].Image = image[i];
                    }
                }
            }
            FilesManagement.DeleteFiles(tempPath, 0, true, false);
        }
        public void Dispose()
        {
            foreach (var catalog in Catalogs)
            {
                if (catalog != null)
                {
                    catalog.Dispose();
                }
            }
            if (this.Image != null)
            {
                this.Image.Dispose();
            }
        }
    }
}
