﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Drawing;
using Heal.Models;

namespace Heal.Models.Alg
{
    public class MarkTracing : MyAlgorithm
    {
        private MarkShape _Shape { get; set; }
        private MyRange _Threshold { get; set; }
        private MyRange _OKRange { get; set; }
        private double _Angle { get; set; }
        private double _Radius { get; set; }
        public MarkShape Shape
        {
            get => _Shape;
            set
            {
                _Shape = value;
                OnPropertyChanged("Shape");
            }
        }
        public MyRange Threshold
        {
            get => _Threshold;
            set
            {
                _Threshold = value;
                OnPropertyChanged("Threshold");
            }
        }
        public MyRange OKRange
        {
            get => _OKRange;
            set
            {
                _OKRange = value;
                OnPropertyChanged("OKRange");
            }
        }
        public double Radius
        {
            get => _Radius;
            set
            {
                _Radius = value;
                OnPropertyChanged("Radius");
            }
        }
        public double Angle
        {
            get => _Angle;
            set
            {
                _Angle = value;
                OnPropertyChanged("Angle");
            }
        }

        public MarkTracing()
        {
            this.Radius = Const.Millimeter2Pixel(3);
            this.Threshold = new MyRange(127, 255, 0 , 255);
            this.OKRange = new MyRange(60, 100, 0 , 100);
            this.Name = "Mark Tracing";
        }
        public override AlarmType Preview(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            this.ClearCalculatedValue();
            if(this.ImageProcessed != null)
            {
                this.ImageProcessed.Dispose();
                this.ImageProcessed = null;
            }
            this.ImageProcessed = new Image<Bgr, byte>(InputImage.Size);
            double radius = this.Radius;
            double low = this.Threshold.Lower;
            double up = this.Threshold.Upper;
            using (Image<Gray, byte> imgGray = new Image<Gray, byte>(InputImage.Size))
            using (Image<Bgr, byte> imgOutput = new Image<Bgr, byte>(InputImage.Size))
            {
                CvInvoke.CvtColor(InputImage, imgGray, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
                var imgMask = GetMaskInRange(imgGray, low, up);
                using (Image<Bgr, byte> imageMaskBgr = new Image<Bgr, byte>(imgMask.Size))
                {
                    CvInvoke.CvtColor(imgMask, imgOutput, Emgu.CV.CvEnum.ColorConversion.Gray2Bgr);
                }
                (double score, Point[] cnts) = GetContourMaxScore(imgMask, radius);
                this.Result.Score = Math.Round(score, 5);
                if (cnts.Length > 0)
                {
                    CircleF cc = CvInvoke.MinEnclosingCircle(new VectorOfPoint(cnts));
                    this.Result.OffsetX = Convert.ToInt32(cc.Center.X - SmdCenter.X);
                    this.Result.OffsetY = Convert.ToInt32(cc.Center.Y - SmdCenter.Y);
                    CvInvoke.Circle(imgOutput, Point.Round(cc.Center), Convert.ToInt32(radius), new MCvScalar(0, 255, 0), 2);
                }
                if (imgMask != null)
                {
                    imgMask.Dispose();
                }
                this.ImageProcessed = imgOutput.Copy();
            }
            return IsMatched(this.Result.Score, OKRange) ? AlarmType.General : AlarmType.BadMark;
        }
        public override AlarmType Run(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            this.ClearCalculatedValue();
            double radius = this.Radius;
            double low = this.Threshold.Lower;
            double up = this.Threshold.Upper;
            using (Image<Gray, byte> imgGray = new Image<Gray, byte>(InputImage.Size))
            using (Image<Bgr, byte> imgOutput = new Image<Bgr, byte>(InputImage.Size))
            {
                CvInvoke.CvtColor(InputImage, imgGray, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
                var imgMask = GetMaskInRange(imgGray, low, up);
                using (Image<Bgr, byte> imageMaskBgr = new Image<Bgr, byte>(imgMask.Size))
                {
                    CvInvoke.CvtColor(imgMask, imgOutput, Emgu.CV.CvEnum.ColorConversion.Gray2Bgr);
                }
                (double score, Point[] cnts) = GetContourMaxScore(imgMask, radius);
                this.Result.Score = Math.Round(score, 5);
                if (cnts.Length > 0)
                {
                    CircleF cc = CvInvoke.MinEnclosingCircle(new VectorOfPoint(cnts));
                    this.Result.OffsetX = Convert.ToInt32(cc.Center.X - SmdCenter.X);
                    this.Result.OffsetY = Convert.ToInt32(cc.Center.Y - SmdCenter.Y);
                }
                if (imgMask != null)
                {
                    imgMask.Dispose();
                }
                this.ImageProcessed = imgOutput.Copy();
            }
            return IsMatched(this.Result.Score, OKRange) ? AlarmType.General : AlarmType.BadMark;
        }
        private (double, Point[]) GetContourMaxScore(Image<Gray, byte> imgMask, double radius)
        {
            double maxScore = 0;
            Point[] p = new Point[0];
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                CvInvoke.FindContours(imgMask, contours, null, Emgu.CV.CvEnum.RetrType.Ccomp, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                double sc = Math.PI * radius * radius;
                int idMaxScore = -1;
                for (int i = 0; i < contours.Size; i++)
                {
                    CircleF cc = CvInvoke.MinEnclosingCircle(contours[i]);
                    int cNone = 0;
                    using (Image<Gray, byte> img = new Image<Gray, byte>(imgMask.Size))
                    {
                        CvInvoke.Circle(img, new Point((int)cc.Center.X, (int)cc.Center.Y), Convert.ToInt32(cc.Radius), new MCvScalar(255), -1);
                        CvInvoke.BitwiseAnd(img, imgMask, img);
                        cNone = CvInvoke.CountNonZero(img);
                    }
                    double s = cc.Area;
                    double score1 = Math.Min(s, sc) / Math.Max(s, sc);
                    double score2 = Math.Min(cNone, sc) / Math.Max(cNone, sc);
                    double score = Math.Min(score1, score2);
                    if (maxScore < score)
                    {
                        maxScore = score;
                        idMaxScore = i;
                        p = contours[i].ToArray();
                    }
                }
            }
            return (maxScore, p);
        }
        private Image<Gray, byte> GetMaskInRange(Image<Gray, byte> imgGray, double low, double up)
        {
            Image<Gray, byte> imgMask = null;
            if (Threshold.Lower < Threshold.Upper)
            {
                imgMask = imgGray.InRange(new Gray(low), new Gray(up));
            }
            else
            {
                imgMask = imgGray.InRange(new Gray(up), new Gray(255));
                using (var img2 = imgGray.InRange(new Gray(0), new Gray(low)))
                {
                    CvInvoke.BitwiseOr(imgMask, img2, imgMask);
                }
            }
            return imgMask;
        }
        public bool IsMatched(double s, MyRange OKrange)
        {
            s = s * 100;
            if (OKrange.Lower < OKrange.Upper)
            {
                if (s >= OKrange.Lower && s <= OKrange.Upper)
                {
                    return true;
                }
            }
            else
            {
                if (s >= OKrange.Lower || s < OKrange.Upper)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
