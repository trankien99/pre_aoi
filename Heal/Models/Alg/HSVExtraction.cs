﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Heal.Models.Alg
{
    public class HSVExtraction : MyAlgorithm
    {
        public MyRange _Hue { get; set; }
        public MyRange _Saturation { get; set; }
        public MyRange _Value { get; set; }
        public MyRange _OKRange { get; set; }
        public MyRange Hue
        {
            get => _Hue;
            set
            {
                _Hue = value;
                OnPropertyChanged("Hue");
            }
        }
        public MyRange Saturation
        {
            get => _Saturation;
            set
            {
                _Saturation = value;
                OnPropertyChanged("Saturation");
            }
        }
        public MyRange Value
        {
            get => _Value;
            set
            {
                _Value = value;
                OnPropertyChanged("Value");
            }
        }
        public MyRange OKRange
        {
            get => _OKRange;
            set
            {
                _OKRange = value;
                OnPropertyChanged("OKRange");
            }
        }
        public HSVExtraction()
        {
            this.Name = "HSV Extraction";
            this.Hue = new MyRange(150, 30, 0, 179);
            this.Saturation = new MyRange(0, 255, 0, 255);
            this.Value = new MyRange(0, 255, 0, 255);
            this.OKRange = new MyRange(80, 100, 0, 100);
        }
        public override AlarmType Preview(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            if(InputImage.Width < 1 || InputImage.Height < 1)
            {
                return AlarmType.General;
            }
            this.ImageProcessed = new Image<Bgr, byte>(InputImage.Size);
            using (Image<Gray, byte> mask = GetHSVMask(InputImage))
            {
                int countMask = InputImage.Width * InputImage.Height;
                int countHSV = CvInvoke.CountNonZero(mask);
                AlgResult result = new AlgResult();
                result.Score = (double)countHSV / countMask;
                result.Qty = countHSV;
                // preview
                using (Image<Bgr, byte> maskPink = new Image<Bgr, byte>(InputImage.Size.Width, InputImage.Size.Height, new Bgr(193,31,248)))
                {
                    CvInvoke.Add(ImageProcessed, maskPink, ImageProcessed, mask: mask);
                    CvInvoke.BitwiseNot(mask, mask);
                    using (Image<Bgr, byte> maskWhite = new Image<Bgr, byte>(mask.Size) )
                    {
                        CvInvoke.CvtColor(mask, maskWhite, Emgu.CV.CvEnum.ColorConversion.Gray2Bgr);
                        CvInvoke.Add(ImageProcessed, maskWhite, ImageProcessed, mask: mask);
                    }
                }
                this.Result = result;
                return IsMatched(result.Score, OKRange) ? AlarmType.General : Parent.AlarmType;
            }
        }
        public override AlarmType Run(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            if (InputImage.Width < 1 || InputImage.Height < 1)
            {
                return AlarmType.General;
            }
            using (Image<Gray, byte> mask = GetHSVMask(InputImage))
            {
                int countMask = InputImage.Width * InputImage.Height;
                int countHSV = CvInvoke.CountNonZero(mask);
                AlgResult result = new AlgResult();
                result.Score = (double)countHSV / countMask;
                result.Qty = countHSV;
                this.Result = result;
                return IsMatched(result.Score, OKRange) ? AlarmType.General : Parent.AlarmType;
            }
        }
        private Image<Gray, byte> GetHSVMask(Image<Bgr, byte> InputImage)
        {
            Image<Gray, byte> imgMaskHSVExtracted = null;
            double Hl = Hue.Lower;
            double Hu = Hue.Upper;
            double Sl = Saturation.Lower;
            double Su = Saturation.Upper;
            double Vl = Value.Lower;
            double Vu = Value.Upper;
            using (Image<Hsv, byte> imgHsv = new Image<Hsv, byte>(InputImage.Size))
            {
                CvInvoke.CvtColor(InputImage, imgHsv, Emgu.CV.CvEnum.ColorConversion.Bgr2Hsv);
                if (Hl < Hu)
                {
                    imgMaskHSVExtracted = imgHsv.InRange(new Hsv(Hl, Math.Min(Sl, Su), Math.Min(Vl, Vu)), new Hsv(Hu, Math.Max(Sl, Su), Math.Max(Vl, Vu)));
                }
                else
                {
                    imgMaskHSVExtracted = imgHsv.InRange(new Hsv(Hl, Math.Min(Sl, Su), Math.Min(Vl, Vu)), new Hsv(179, Math.Max(Sl, Su), Math.Max(Vl, Vu)));
                    using (var img2 = imgHsv.InRange(new Hsv(0, Math.Min(Sl, Su), Math.Min(Vl, Vu)), new Hsv(Hu, Math.Max(Sl, Su), Math.Max(Vl, Vu))))
                    {
                        CvInvoke.BitwiseOr(imgMaskHSVExtracted, img2, imgMaskHSVExtracted);
                    }
                }
            }
            return imgMaskHSVExtracted;
        }
        
        public bool IsMatched(double s, MyRange OKrange)
        {
            s = s * 100;
            if (OKrange.Lower < OKrange.Upper)
            {
                if (s >= OKrange.Lower && s <= OKrange.Upper)
                {
                    return true;
                }
            }
            else
            {
                if (s >= OKrange.Lower || s < OKrange.Upper)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
