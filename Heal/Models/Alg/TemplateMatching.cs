﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heal.Utils;
using System.Collections.ObjectModel;

namespace Heal.Models.Alg
{
    public class TemplateMatching : MyAlgorithm
    {
        private ObservableCollection<Image<Bgr, byte>> _Templates { get; set; }
        public ObservableCollection<Image<Bgr, byte>> Templates
        {
            get => _Templates;
            set
            {
                _Templates = value;
                OnPropertyChanged("Templates");
            }
        }
        private MyRange _OKRange { get; set; }
        public MyRange OKRange
        {
            get => _OKRange;
            set
            {
                _OKRange = value;
                OnPropertyChanged("OKRange");
            }
        }
        private int _MaxOffsetX { get; set; }
        public int MaxOffsetX
        {
            get => _MaxOffsetX;
            set
            {
                _MaxOffsetX = value;
                OnPropertyChanged("MaxOffsetX");
            }
        }
        private int _MaxOffsetY { get; set; }
        public int MaxOffsetY
        {
            get => _MaxOffsetY;
            set
            {
                _MaxOffsetY = value;
                OnPropertyChanged("MaxOffsetY");
            }
        }
        private bool _ReverseTemplate { get; set; }
        public bool ReverseTemplate
        {
            get => _ReverseTemplate;
            set
            {
                _ReverseTemplate = value;
                OnPropertyChanged("ReverseTemplate");
            }
        }
        public TemplateMatching()
        {
            this.Name = "Template Matching";
            this.Templates = new ObservableCollection<Image<Bgr, byte>>();
            this.ReverseTemplate = false;
            this.OKRange = new MyRange(80,100,0,100);
            this.Result = new AlgResult();
            this.MaxOffsetX = Const.Millimeter2Pixel(1);
            this.MaxOffsetY = Const.Millimeter2Pixel(1);
        }
        public override AlarmType Preview(Image<Bgr, byte> InputImage, Point _SmdCenter, MyAlarm Parent)
        {
            return Run(InputImage, _SmdCenter, Parent, true);
        }
        public override AlarmType Run(Image<Bgr, byte> InputImage, Point _SmdCenter, MyAlarm Parent)
        {
            return Run(InputImage, _SmdCenter, Parent, false);
        }
        public AlarmType Run(Image<Bgr, byte> InputImage, Point _SmdCenter, MyAlarm Parent, bool Break = false)
        {
            this.ClearCalculatedValue();
            double maxScore = 0;
            Point maxLoc = new Point();
            using (Image<Gray, byte> imgGray = new Image<Gray, byte>(InputImage.Size))
            {
                CvInvoke.CvtColor(InputImage, imgGray, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
                for (int i = 0; i < Templates.Count; i++)
                {
                    var temp = Templates[i];
                    using (Image<Gray, byte> tempGray = new Image<Gray, byte>(temp.Size))
                    {
                        CvInvoke.CvtColor(temp, tempGray, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
                        (Point loc, double s) = Matching(imgGray, tempGray);
                        if (maxScore < s)
                        {
                            maxScore = s;
                            maxLoc = new Point(loc.X + tempGray.Width / 2, loc.Y + tempGray.Height / 2 );
                            if(Break && IsMatched(maxScore, OKRange))
                            {
                                break;
                            }
                        }
                        if (ReverseTemplate)
                        {
                            CvUtils.ImageRotation(tempGray, new Point(tempGray.Width / 2, tempGray.Height / 2), Math.PI);
                            (Point loc1, double s1) = Matching(imgGray, tempGray);
                            if (maxScore < s1)
                            {
                                maxScore = s1;
                                maxLoc = new Point(loc1.X + tempGray.Width / 2, loc1.Y + tempGray.Height / 2);
                                if (Break && IsMatched(maxScore, OKRange))
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
                this.Result.Score = maxScore;
                this.Result.OffsetX = maxLoc.X - _SmdCenter.X;
                this.Result.OffsetY = maxLoc.Y - _SmdCenter.Y; 
                if(IsMatched(maxScore, OKRange))
                {
                    if (this.Result.OffsetX > this.MaxOffsetX || this.Result.OffsetY > this.MaxOffsetY)
                        return AlarmType.Shift;
                    else
                        return AlarmType.General;
                }
                else
                    return Parent.AlarmType;
            }
        }
        public bool IsMatched(double s, MyRange OKrange)
        {
            s = s * 100;
            if (OKrange.Lower < OKrange.Upper)
            {
                if (s >= OKrange.Lower && s <= OKrange.Upper)
                {
                    return true;
                }
            }
            else
            {
                if (s >= OKrange.Lower || s < OKrange.Upper)
                {
                    return true;
                }
            }
            return false;
        }
        public (Point, double) Matching(Image<Gray, byte> image, Image<Gray, byte> template)
        {
            if (image.Width < template.Width || image.Height < template.Height)
                return (new Point(), 0);
            Point minLoc = new Point();
            Point maxLoc = new Point();
            double minVal = 0;
            double maxVal = 0;
            using (Mat s = new Mat())
            {
                CvInvoke.MatchTemplate(image, template, s, Emgu.CV.CvEnum.TemplateMatchingType.CcoeffNormed);
                CvInvoke.MinMaxLoc(s, ref minVal, ref maxVal, ref minLoc, ref maxLoc);
            }
            return (maxLoc, maxVal);
        }
    }
}
