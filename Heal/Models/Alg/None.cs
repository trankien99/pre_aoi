﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Heal.Models.Alg
{
    class None : MyAlgorithm, IStructAlgorithm
    {
        public None()
        {
            this.ClassType = this.ToString();
            Name = "(None)";
        }
        public override AlarmType Preview(Image<Bgr, byte> InputImage,  Point SmdCenter, MyAlarm Parent)
        {
            return base.Preview(InputImage, SmdCenter, Parent);
        }
        public override AlarmType Run(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            return base.Run(InputImage, SmdCenter, Parent);
        }

    }
}
