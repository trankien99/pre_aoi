﻿using Emgu.CV;
using Emgu.CV.Structure;
using Heal.Models.Alg;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZXing;
using ZXing.Common;
using ZXing.Datamatrix;
using ZXing.QrCode;

namespace Heal.Models.Alg
{
    public class CodeRecognition : MyAlgorithm
    {
        public CodeFormat Format { get; set; }
        public CodeRecognition()
        {
            this.Format = CodeFormat.Code128;
            this.Name = "Code Recognition";
        }
        public override AlarmType Preview(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            AlgResult result = new AlgResult();
            result.Content = Decode(InputImage, 5);
            this.Result = result;
            return this.Result.Content != "NOT FOUND" ? AlarmType.General : Parent.AlarmType;
        }
        public override AlarmType Run(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            AlgResult result = new AlgResult();
            result.Content = Decode(InputImage, 5);
            this.Result = result;
            return this.Result.Content != "NOT FOUND" ? AlarmType.General : Parent.AlarmType;
        }
        public string Decode(Image<Bgr, byte> InputImage, int step = 2)
        {
            string data = string.Empty;
            
            if (InputImage != null)
            {
                if (InputImage.Width > 0 && InputImage.Height > 0)
                {
                    using (Image<Gray, byte> imgGray = new Image<Gray, byte>(InputImage.Size))
                    {
                        CvInvoke.CvtColor(InputImage, imgGray, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
                        for (int s = -5; s < 3; s++)
                        {
                            double scale = Math.Pow(2, s);
                            int w = Convert.ToInt32(scale * InputImage.Width);
                            int h = Convert.ToInt32(scale * InputImage.Height);
                            if (w > 0 && h > 0)
                            {
                                using (Image<Gray, byte> imgScale = imgGray.Resize(w, h, Emgu.CV.CvEnum.Inter.Linear))
                                {
                                    for (int i = 0; i < 26; i += step)
                                    {
                                        double gama = 0.5 + i * 0.1;
                                        using (Image<Gray, byte> imgGamma = new Image<Gray, byte>(imgScale.Size))
                                        {
                                            CvInvoke.ConvertScaleAbs(imgScale, imgGamma, gama, 0);
                                            using (Bitmap image = imgGamma.ToBitmap())
                                            {
                                                var result = ReadByZxing(image, this.Format);
                                                if (result != null)
                                                {
                                                    data = result.Text;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (!string.IsNullOrEmpty(data))
                                        break;
                                }
                            }
                            else
                            {
                                data = this.Result.Content;
                            }
                        }
                    }
                }
            }
            if (data == string.Empty)
            {
                data = "NOT FOUND";
            }
            return data;
        }
        private Result ReadByZxing(Bitmap image, CodeFormat format)
        {
            List<BarcodeFormat> formats = new List<BarcodeFormat>();
            switch (format)
            {
                case CodeFormat.Codabar:
                    formats.Add(BarcodeFormat.CODABAR);
                    break;
                case CodeFormat.Code39:
                    formats.Add(BarcodeFormat.CODE_39);
                    break;
                case CodeFormat.Code128:
                    formats.Add(BarcodeFormat.CODE_128);
                    break;
                case CodeFormat.Code93:
                    formats.Add(BarcodeFormat.CODE_93);
                    break;
                case CodeFormat.EAN13:
                    formats.Add(BarcodeFormat.EAN_13);
                    break;
                case CodeFormat.EAN8:
                    formats.Add(BarcodeFormat.EAN_8);
                    break;
                case CodeFormat.IMB:
                    formats.Add(BarcodeFormat.IMB);
                    break;
                case CodeFormat.ITF:
                    formats.Add(BarcodeFormat.ITF);
                    break;
                case CodeFormat.MaxiCode:
                    formats.Add(BarcodeFormat.MAXICODE);
                    break;
                case CodeFormat.PDF417:
                    formats.Add(BarcodeFormat.PDF_417);
                    break;
                case CodeFormat.UPCA:
                    formats.Add(BarcodeFormat.UPC_A);
                    break;
                case CodeFormat.UPCE:
                    formats.Add(BarcodeFormat.UPC_E);
                    break;
                case CodeFormat.QRCode:
                    formats.Add(BarcodeFormat.QR_CODE);
                    break;
                case CodeFormat.DataMatrix:
                    formats.Add(BarcodeFormat.DATA_MATRIX);
                    break;
                default:
                    break;
            }
            
            DecodingOptions options = new DecodingOptions();
            options.TryHarder = true;
            options.ReturnCodabarStartEnd = true;
            options.TryHarder = true;
            options.PossibleFormats = formats;
            //---------------- reader -------
            BarcodeReader reader = new BarcodeReader();
            reader.AutoRotate = true;
            reader.TryInverted = true;
            reader.Options = options;
            Result result = reader.Decode(image);
            return result;
        }
        
    }

}
