﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Heal.Models.Alg
{
    public class LuminanceExtractionQty : MyAlgorithm
    {
        public MyRange _Threshold { get; set; }
        public MyRange _OKRange { get; set; }
        public MyRange Threshold
        {
            get => _Threshold;
            set
            {
                _Threshold = value;
                OnPropertyChanged("ThresholdQty");
            }
        }
        public MyRange OKRange
        {
            get => _OKRange;
            set
            {
                _OKRange = value;
                OnPropertyChanged("OKRangeQty");
            }
        }
        public LuminanceExtractionQty()
        {
            this.Name = "Luminance Extraction (Qty)";
            this.Threshold = new MyRange(70, 255, 0, 255);
            this.OKRange = new MyRange(0, 10, 0, 999999);
        }
        public override AlarmType Preview(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            this.ImageProcessed = new Image<Bgr, byte>(InputImage.Size);
            using (Image<Gray, byte> mask = GetThresholdMask(InputImage))
            {
                int countMask = InputImage.Width * InputImage.Height;
                int count = CvInvoke.CountNonZero(mask);
                AlgResult result = new AlgResult();
                result.Score = (double)count / countMask;
                result.Qty = count;
                // preview
                CvInvoke.CvtColor(mask, this.ImageProcessed, Emgu.CV.CvEnum.ColorConversion.Gray2Bgr);
                this.Result = result;
                return IsMatched(result.Qty, OKRange) ? AlarmType.General : Parent.AlarmType;
            }
        }
        public override AlarmType Run(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            using (Image<Gray, byte> mask = GetThresholdMask(InputImage))
            {
                if(mask == null)
                {
                    AlgResult result = new AlgResult();
                    this.Result = result;
                    return AlarmType.General;
                }
                else
                {
                    int countMask = InputImage.Width * InputImage.Height;
                    int count = CvInvoke.CountNonZero(mask);
                    AlgResult result = new AlgResult();
                    result.Score = (double)count / countMask;
                    result.Qty = count;
                    this.Result = result;
                    return IsMatched(result.Qty, OKRange) ? AlarmType.General : Parent.AlarmType;
                }
                
            }
        }
        private Image<Gray, byte> GetThresholdMask(Image<Bgr, byte> InputImage)
        {
            if(InputImage.Width <= 0 || InputImage.Height <= 0)
            {
                return null;
            }
            Image<Gray, byte> imgMaskHSVExtracted = null;
            double thresholdLow = Threshold.Lower;
            double thresholdUp = Threshold.Upper;
            using (Image<Gray, byte> imgGray = new Image<Gray, byte>(InputImage.Size))
            {
                CvInvoke.CvtColor(InputImage, imgGray, Emgu.CV.CvEnum.ColorConversion.Bgr2Gray);
                if(thresholdLow < thresholdUp)
                {
                    imgMaskHSVExtracted = imgGray.InRange(new Gray(thresholdLow), new Gray(thresholdUp));
                }
                else
                {
                    imgMaskHSVExtracted = new Image<Gray, byte>(imgGray.Size);
                    CvInvoke.BitwiseOr(imgGray.InRange(new Gray(thresholdLow), new Gray(255)), imgGray.InRange(new Gray(0), new Gray(thresholdUp)), imgMaskHSVExtracted);
                }
            }
            return imgMaskHSVExtracted;
        }
        
        public bool IsMatched(double s, MyRange OKrange)
        {
            if (OKrange.Lower < OKrange.Upper)
            {
                if (s >= OKrange.Lower && s <= OKrange.Upper)
                {
                    return true;
                }
            }
            else
            {
                if (s >= OKrange.Lower || s < OKrange.Upper)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
