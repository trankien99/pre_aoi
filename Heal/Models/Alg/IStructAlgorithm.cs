﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Drawing;

namespace Heal.Models.Alg
{
    interface IStructAlgorithm
    {
        AlarmType Preview(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent);
        AlarmType Run(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent);
    }
    
}
