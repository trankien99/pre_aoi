﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using Heal.Models;
using Newtonsoft.Json;

namespace Heal.Models.Alg
{
    public class MyAlgorithm : BaseProperties, IStructAlgorithm
    {
        public string ClassType { get; set; }
        private AlgResult _Result { get; set; }
        public AlgResult Result
        {
            get => _Result;
            set
            {
                _Result = value;
                OnPropertyChanged("Result");
            }
        }
        public Image<Bgr, byte> ImageProcessed { get; set; }
        private static List<MyAlgorithm> _AllAlgorithm { get; set; } = new List<MyAlgorithm>();
        public MyAlgorithm()
        {
            this.ClassType = this.ToString();
            this.Result = new AlgResult();
        }
        
        public static MyAlgorithm GetInstance(Type Type)
        {
            if(_AllAlgorithm.Count == 0)
            {
                InitInstance();
            }
            try
            {
                foreach (var instance in _AllAlgorithm)
                {
                    if(instance.GetType().ToString() == Type.ToString())
                    {
                        return instance.Copy();
                    }
                }
            }
            catch {}
            return null;
        }
        private static void InitInstance()
        {
            string nameSpace = (new MyAlgorithm()).GetType().Namespace;
            Type[] types = System.Reflection.Assembly.GetExecutingAssembly().GetTypes().Where(t => String.Equals(t.Namespace, nameSpace, StringComparison.Ordinal)).ToArray();
            foreach (var type in types)
            {
                try
                {
                    MyAlgorithm item = (MyAlgorithm)Activator.CreateInstance(type);
                    _AllAlgorithm.Add(item);
                }
                catch { }
            }
        }
        public static Type[] GetAllAlgorithmType()
        {
            string nameSpace = (new MyAlgorithm()).GetType().Namespace;
            Type[] types = System.Reflection.Assembly.GetExecutingAssembly().GetTypes().Where(t => String.Equals(t.Namespace, nameSpace, StringComparison.Ordinal)).ToArray();
            return types;
        }
        public static MyAlgorithm GetInstanceFromName(string Name)
        {
            MyAlgorithm instance = null;
            //string nameSpace = (new MyAlgorithm()).GetType().Namespace;
            //Type[] types = System.Reflection.Assembly.GetExecutingAssembly().GetTypes().Where(t => String.Equals(t.Namespace, nameSpace, StringComparison.Ordinal)).ToArray();
            foreach (var algorithm in _AllAlgorithm)
            {
                //MyAlgorithm i = MyAlgorithm.GetInstance(algorithm);
                //if (i != null)
                //{
                if (!string.IsNullOrEmpty(algorithm.Name) && algorithm.Name == Name)
                {
                    instance = algorithm.Copy();
                    break;
                }                //}
            }
            return instance;
        }
        public void Dispose()
        {
            this.ImageProcessed?.Dispose();
            this.ImageProcessed = null;
        }
        public virtual AlarmType Preview(Image<Bgr, byte> InputImage,Point SmdCenter, MyAlarm Parent)
        {
            Dispose();
            return AlarmType.General;
        }

        public virtual AlarmType Run(Image<Bgr, byte> InputImage, Point SmdCenter, MyAlarm Parent)
        {
            return AlarmType.General;
        }
        public void ClearCalculatedValue()
        {
           this.Dispose();
            this.Result = new AlgResult();
        }
        public MyAlgorithm Copy()
        {
            string s = JsonConvert.SerializeObject(this);
            MyAlgorithm item = JsonConvert.DeserializeObject<MyAlgorithm>(s, new MyJsonConverter());
            return item;
        }
    }
}
