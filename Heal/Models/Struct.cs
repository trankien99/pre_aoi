﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.IO;
using NLog;
using Newtonsoft.Json;
using Heal.Models.Alg;
using Newtonsoft.Json.Linq;
using System.Drawing;
using System.ComponentModel;

namespace Heal.Models
{
    public class BaseProperties : INotifyPropertyChanged
    {
        private string _Name { get; set; }
        private double _Width { get; set; }
        private double _Height { get; set; }
        public string Name
        {
            get => _Name;
            set
            {
                _Name = value;
                OnPropertyChanged("Name");
            }
        }
        public double Width
        {
            get => _Width;
            set
            {
                _Width = value < 1 ? 1 : value;
                OnPropertyChanged("Width");
            }
        }
        public double Height
        {
            get => _Height;
            set
            {
                _Height = value < 1 ? 1 : value;
                OnPropertyChanged("Height");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string Name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Name));
        }
    }
    public class MarkDev
    {
        public double Angle { get; set; }
        public int OffsetX { get; set; }
        public int OffsetY { get; set; }
        public Point Center { get; set; }
        public MarkDev()
        {
            Center = new Point();
        }
    }
    public class SMDAlarm
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public Point Location { get; set; }
        public int Block { get; set; }
        public string Catalog { get; set; }
        public string AlarmType { get; set; }
        public SMDAlarm(int ID)
        {
            this.ID = ID;
        }
    }
    public class Summary
    {
        public string AlarmName { get; set; }
        public int Count { get; set; }
        public int PPM { get; set; }
        private double _Rate { get; set; }
        public double Rate
        {
            get => Math.Round(_Rate, 2);
            set => _Rate = value;
        }
        public Summary(string AlarmName)
        {
            this.AlarmName = AlarmName;
        }
    }
    public class BarcodeResult
    {
        public string Content { get; set; }
        public int Block { get; set; }
    }
    public class MyRange : INotifyPropertyChanged
    {
        private double _Lower { get; set; }
        private double _Upper { get; set; }

        public double Maximum { get; set; }
        public double Minimum { get; set; }
        public double Lower
        {
            get => _Lower;
            set
            {
                _Lower = value;
                _Lower = _Lower < Minimum ? Minimum : _Lower > Maximum ? Maximum : _Lower;
                OnPropertyChanged("Lower");
            }
        }
        public double Upper
        {
            get => _Upper;
            set
            {
                _Upper = value;
                _Upper = _Upper < Minimum ? Minimum : _Upper > Maximum ? Maximum : _Upper;
                OnPropertyChanged("Upper");
            }
        }
        public MyRange() { }
        public MyRange(double Lower, double Upper, double Minimum, double Maximum)
        {
            this.Maximum = Maximum;
            this.Minimum = Minimum;
            this.Upper = Upper;
            this.Lower = Lower;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string Name = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Name));
        }
    }
    public class MyJsonConverter : JsonConverter
    {
        
        public override bool CanConvert(Type objectType)
        {
            return typeof(MyAlgorithm).IsAssignableFrom(objectType);
        }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            //if (existingValue == null)
            //    return null;
            JObject jo = JObject.Load(reader);
            // Using a nullable bool here in case "is_album" is not present on an item
            string classType = (string)jo["ClassType"];
            Type type = Type.GetType(classType);
            var instance = Activator.CreateInstance(type);
            serializer.Populate(jo.CreateReader(), instance);
            return instance;
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
    public class AlgResult : BaseProperties
    {
        private int _OffsetX { get; set; }
        private int _OffsetY { get; set; }
        private double _Score { get; set; }
        private int _Qty { get; set; }
        private string _Content { get; set; }
        public int OffsetX
        {
            get => _OffsetX;
            set
            {
                _OffsetX = value;
                OnPropertyChanged("OffsetX");
            }
        }
        public int OffsetY
        {
            get => _OffsetY;
            set
            {
                _OffsetY = value;
                OnPropertyChanged("OffsetY");
            }
        }
        public double Score
        {
            get => _Score;
            set
            {
                _Score = value;
                OnPropertyChanged("Score");
            }
        }
        public int Qty
        {
            get => _Qty;
            set
            {
                _Qty = value;
                OnPropertyChanged("Qty");
            }
        }
        public string Content
        {
            get => _Content;
            set
            {
                _Content = value;
                OnPropertyChanged("Content");
            }
        }
        public AlgResult Copy()
        {
            AlgResult alg = new AlgResult();
            alg.Content = this.Content;
            alg.Height = this.Height;
            alg.Name = this.Name;
            alg.OffsetX = this.OffsetX;
            alg.OffsetY = this.OffsetY;
            alg.Qty = this.Qty;
            alg.Score = this.Score;
            return alg;
        }
    }
}
