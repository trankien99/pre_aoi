﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.IO;
using NLog;
using Newtonsoft.Json;
using System.Drawing;
using System.Collections.ObjectModel;
using Heal.SDK;

namespace Heal.Models
{
    public class Library
    {
        private static Logger mLog = LogCtl.GetInstance();
        public string Name { get; set; }
        public string Light { get; set; }
        public ObservableCollection<MyAlarm> Alarms { get; set; }
        public Library()
        {
            Alarms = new ObservableCollection<MyAlarm>();
        }
        public Library(string Name)
        {
            Alarms = new ObservableCollection<MyAlarm>();
            this.Name = Name;
        }
        public void Clean()
        {
            Clean(this.Alarms);
        }
        public void Save(string Dir)
        {
            string libPath = $"{Dir}/{this.Name}.filib";
            if (File.Exists(libPath))
                return;
            try
            {
                string s = JsonConvert.SerializeObject(this);
                File.WriteAllText(libPath, s);
            }
            catch (Exception ex)
            {
                mLog.Error(ex.Message);
            }
        }
        public static Library Load(string path)
        {
            Library lib = null;
            string content = File.ReadAllText(path);
            try
            {
                lib = JsonConvert.DeserializeObject<Library>(content, new MyJsonConverter());
            }
            catch (Exception ex)
            {
                mLog.Error(ex.Message);
            }
            return lib;
        }
        public List<int> GetDirectionID(MyAlarm Alarm)
        {
            List<int> dir = new List<int>();
            Contains(Alarms, Alarm, ref dir);
            dir.Reverse();
            return dir;
        }
        public int GetLayerAlarm(MyAlarm Alarm)
        {
            List<int> dir = GetDirectionID(Alarm);
            return dir.Count - 1;
        }
        private bool Contains(ObservableCollection<MyAlarm> ListAlarm, MyAlarm Alarm, ref List<int> dir)
        {
            for (int i = 0; i < ListAlarm.Count; i++)
            {
                if (Alarm == ListAlarm[i])
                {
                    dir.Add(i);
                    return true;
                }
                else
                {
                    if (ListAlarm[i].Alarms.Count > 0)
                    {
                        if (Contains(ListAlarm[i].Alarms, Alarm, ref dir) == true)
                        {
                            dir.Add(i);
                            return true;
                        }

                    }
                }
            }
            return false;
        }
        private bool Clean(ObservableCollection<MyAlarm> ListAlarm)
        {
            for (int i = 0; i < ListAlarm.Count; i++)
            {
                ListAlarm[i].Alg.Dispose();
                Clean(ListAlarm[i].Alarms);
            }
            return false;
        }
        public Library Copy()
        {
            string s = JsonConvert.SerializeObject(this);
            var item = JsonConvert.DeserializeObject<Library>(s, new MyJsonConverter());
            return item;
        }
    }
}
