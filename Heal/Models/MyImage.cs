﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using Emgu.CV.Util;
using System.IO;
using NLog;
using Newtonsoft.Json;
using Heal.SDK;
using System.Diagnostics;
using System.IO.Compression;

namespace Heal.Models
{
    public class MyImage : BaseProperties
    {
        private static Logger _Log = LogCtl.GetInstance();
        public int ImageWidth { get; set; }
        public int ImageHeight { get; set; }
        public List<ImageBlock> Blocks { get; set; }

        private static string _TempPath = $"temp/images";
        public Image<Gray, byte> ImgBlue { get; set; }
        public Image<Gray, byte> ImgGreen { get; set; }
        public Image<Gray, byte> ImgRed { get; set; }
        private bool _IsGetChanel { get; set; }
        public Heal.Resource.Control.MetaImageBlock[] GetMetaImageBlocks()
        {
            List<Heal.Resource.Control.MetaImageBlock> blocks = new List<Resource.Control.MetaImageBlock>();
            for (int i = 0; i < Blocks.Count; i++)
            {
                var block = new Heal.Resource.Control.MetaImageBlock();
                var loc = Blocks[i].Location;
                block.Location = new System.Windows.Rect(loc.X, loc.Y, loc.Width, loc.Height);
                if (Blocks[i].Image != null)
                    block.Source = Heal.Resource.Convertor.Bm2Bms.Bitmap2BitmapSource(Blocks[i].Image.Bitmap);
                blocks.Add(block);
            }
            return blocks.ToArray();
        }
        public MyImage()
        {
            Blocks = new List<ImageBlock>();
        }
        public bool IsGetChanel()
        {
            return _IsGetChanel;
        }
        public void GetChannel()
        {
            _IsGetChanel = true;
            ImgBlue = new Image<Gray, byte>(ImageWidth, ImageHeight);
            ImgGreen = new Image<Gray, byte>(ImageWidth, ImageHeight);
            ImgRed = new Image<Gray, byte>(ImageWidth, ImageHeight);
            foreach (var item in Blocks)
            {
                if(item != null)
                {
                    if(item.Image != null)
                    {
                        Rectangle ROI = item.Location;
                        ImgBlue.ROI = ROI;
                        ImgGreen.ROI = ROI;
                        ImgRed.ROI = ROI;
                        item.Image[0].CopyTo(ImgBlue);
                        item.Image[1].CopyTo(ImgGreen);
                        item.Image[2].CopyTo(ImgRed);
                        ImgBlue.ROI = Rectangle.Empty;
                        ImgGreen.ROI = Rectangle.Empty;
                        ImgRed.ROI = Rectangle.Empty;
                    }
                }
            }
        }
        public void Dispose()
        {
            for (int i = 0; i < Blocks.Count; i++)
            {
                Blocks[i].Dispose();
            }
            DisposeImageChannel();
        }
        public void DisposeImageChannel()
        {
            if (ImgBlue != null)
            {
                ImgBlue.Dispose();
                ImgBlue = null;
            }
            if (ImgGreen != null)
            {
                ImgGreen.Dispose();
                ImgGreen = null;
            }
            if (ImgRed != null)
            {
                ImgRed.Dispose();
                ImgRed = null;
            }
            _IsGetChanel = false;
        }
        public (Image<Bgr, byte>, double) GetBoardImage(Size MaxSize)
        {
            if (this.ImageWidth < 1 || this.ImageHeight < 1)
                return (null, 0);
            double scale = Math.Min((double)MaxSize.Width / this.ImageWidth, (double)MaxSize.Height / this.ImageHeight);
            Image<Bgr, byte> boardImg = null;
            int width = Convert.ToInt32(this.ImageWidth * scale);
            int height = Convert.ToInt32(this.ImageHeight * scale);
            boardImg = new Image<Bgr, byte>(width, height);
            for (int i = 0; i < Blocks.Count; i++)
            {
                if (Blocks[i].Image != null)
                {
                    Rectangle newROI = Blocks[i].Location;
                    newROI.X = Convert.ToInt32(newROI.X * scale);
                    newROI.Y = Convert.ToInt32(newROI.Y * scale);
                    newROI.Width = Convert.ToInt32(newROI.Width * scale);
                    newROI.Height = Convert.ToInt32(newROI.Height * scale);
                    boardImg.ROI = newROI;
                    using (Image<Bgr, byte> imageRZ = new Image<Bgr, byte>(newROI.Width, newROI.Height))
                    {
                        CvInvoke.Resize(Blocks[i].Image, imageRZ, new Size(newROI.Width, newROI.Height));
                        imageRZ.CopyTo(boardImg);
                    }
                    boardImg.ROI = Rectangle.Empty;
                }
                else
                {
                    return (null, 0);
                }
            }
            return (boardImg, scale);
        }
        public Image<Bgr, byte> TryGraft()
        {
            try
            {
                Image<Bgr, byte> image = new Image<Bgr, byte>(this.ImageWidth, this.ImageHeight);
                for (int i = 0; i < Blocks.Count; i++)
                {
                    image.ROI = Blocks[i].Location;
                    if (Blocks[i].Image != null)
                    {
                        Blocks[i].Image.CopyTo(image);
                    }
                    image.ROI = new Rectangle();
                }
                return image;
            }
            catch { return null; }
        }
        public bool SaveImage(string path, int Quality = 50)
        {
            double scale = 0.6;
            Image<Bgr, byte> image = new Image<Bgr, byte>(Convert.ToInt32(this.ImageWidth * scale), Convert.ToInt32(this.ImageHeight * scale));
            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                for (int i = 0; i < Blocks.Count; i++)
                {
                    Rectangle ROI = Blocks[i].Location;
                    ROI.X = Convert.ToInt32(ROI.X * scale);
                    ROI.Y = Convert.ToInt32(ROI.Y * scale);
                    ROI.Width = Convert.ToInt32(ROI.Width * scale);
                    ROI.Height = Convert.ToInt32(ROI.Height * scale);
                    if (Blocks[i].Image != null)
                    {
                        image.ROI = ROI;
                        Image<Bgr, byte> imgScale = Blocks[i].Image.Resize(image.Width, image.Height, Emgu.CV.CvEnum.Inter.Linear);
                        imgScale.CopyTo(image);
                        imgScale.Dispose();
                    }
                    image.ROI = new Rectangle();
                }
                CvInvoke.Imwrite(path, image, new KeyValuePair<Emgu.CV.CvEnum.ImwriteFlags, int>(Emgu.CV.CvEnum.ImwriteFlags.JpegQuality, Quality));
                _Log.Info($"Save board image successfully in {sw.ElapsedMilliseconds}ms...");
                return true;
            }
            catch (Exception ex)
            {
                _Log.Error($"Save board image failed.{ex.Message}");
                return false;
            }
            finally
            {
                image.Dispose();
            }
        }
        public MyImage Copy()
        {
            MyImage item = new MyImage();
            item.Width = this.ImageWidth;
            item.Height = this.ImageHeight;
            for (int i = 0; i < this.Blocks.Count; i++)
            {
                item.Blocks.Add(this.Blocks[i].Copy());
            }
            return item;
        }
        public bool SaveImagePython(string path)
        {
            FileInfo fi = new FileInfo(path);
            string tempPath = $"{_TempPath}/save_image";
            DirectoryInfo di = new DirectoryInfo(tempPath);

            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }
            Image<Bgr, byte>[] image = new Image<Bgr, byte>[this.Blocks.Count];
            for (int i = 0; i < this.Blocks.Count; i++)
            {
                this.Blocks[i].Save(tempPath);
                image[i] = this.Blocks[i].Image;
                this.Blocks[i].Image = null;
            }
            string s = JsonConvert.SerializeObject(this);
            File.WriteAllText($"{tempPath}/ImageInfo.json", s);
            string agrs = $"./Publish/save_image.pyw -ip \"{di.FullName}\" -is \"{fi.FullName}\"";
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = @"C:\Program Files\Python37\python.exe";
            start.Arguments = string.Format("{0}", agrs);
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string result = reader.ReadToEnd();
                }
            }

            for (int i = 0; i < this.Blocks.Count; i++)
            {
                this.Blocks[i].Image = image[i];
            }
            FilesManagement.DeleteFiles(tempPath, 0, true, false);
            return true;
        }
        public static MyImage Load(string Path)
        {
            MyImage image = null;
            FileInfo fi = new FileInfo(Path);
            if (Directory.Exists(_TempPath))
            {
                try
                {
                    Directory.Delete(_TempPath, true);
                }
                catch
                {
                    return null;
                }
            }
            if (!Directory.Exists(_TempPath))
            {
                Directory.CreateDirectory(_TempPath);
            }
            string desPath = $"{_TempPath}/{fi.Name}";
            string imageInDir = desPath.Replace(".fiimg", "");
            string imageInfoPath = $"{imageInDir}/ImageInfo.json";
            try
            {
                File.Copy(Path, desPath);
                ZipFile.ExtractToDirectory(Path, desPath.Replace(".fiimg", ""));
                string s = File.ReadAllText(imageInfoPath);
                image = JsonConvert.DeserializeObject<MyImage>(s, new MyJsonConverter());
            }
            catch (Exception ex)
            {
                _Log.Error(ex.Message);
                return null;
            }
            for (int i = 0; i < image.Blocks.Count; i++)
            {
                bool RET = image.Blocks[i].Load(imageInDir);
                if (RET == false)
                {
                    image.Dispose();
                    return null;
                }
            }
            FilesManagement.DeleteFiles(_TempPath, 0, true, false);
            return image;
        }
        public void Save(string path)
        {
            FileInfo fi = new FileInfo(path);
            string tempPath = $"{_TempPath}/my_image";
            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }
            Image<Bgr, byte>[] image = new Image<Bgr, byte>[this.Blocks.Count];
            for (int i = 0; i < this.Blocks.Count; i++)
            {
                this.Blocks[i].Save(tempPath);
                image[i] = this.Blocks[i].Image;
                this.Blocks[i].Image = null;
            }
            this.DisposeImageChannel();
            string s = JsonConvert.SerializeObject(this);
            File.WriteAllText($"{tempPath}/ImageInfo.json", s);
            File.Delete(fi.FullName);
            ZipFile.CreateFromDirectory(tempPath, fi.FullName);
            for (int i = 0; i < this.Blocks.Count; i++)
            {
                this.Blocks[i].Image = image[i];
            }
            FilesManagement.DeleteFiles(tempPath, 0, true, false);
        }
    }
    public class ImageBlock
    {
        public int ID { get; set; }
        public Image<Bgr, byte> Image { get; set; }
        public Rectangle Location { get; set; }
        public string SavePath { get; set; }
        public ImageBlock()
        {
            this.Location = Rectangle.Empty;
        }
        public ImageBlock(int ID, Image<Bgr, byte> Image, Rectangle Location)
        {
            if (!Directory.Exists(Const.ProgramImagePath))
            {
                Directory.CreateDirectory(Const.ProgramImagePath);
            }
            this.ID = ID;
            this.Image = Image.Copy();
            this.Location = Location;
            this.SavePath = $"{Const.ProgramImagePath}/{this.ID}.png";
        }
        public ImageBlock Copy()
        {
            ImageBlock item = new ImageBlock();
            item.ID = this.ID;
            item.Image = this.Image != null ? this.Image.Copy() : null;
            item.Location = this.Location;
            item.SavePath = this.SavePath;
            return item;
        }
        public void Dispose()
        {
            if (Image != null)
            {
                Image.Dispose();
                Image = null;
            }
        }
        public void Save(string Path)
        {
            string path = $"{Path}/{SavePath}";
            FileInfo fi = new FileInfo(path);
            if (!Directory.Exists(fi.DirectoryName))
            {
                Directory.CreateDirectory(fi.DirectoryName);
            }
            if (Image != null)
            {
                CvInvoke.Imwrite(fi.FullName, Image);
            }
        }
        public bool Load(string Path)
        {
            string path = $"{Path}/{SavePath}";
            if (File.Exists(path))
            {
                try
                {
                    Image = new Image<Bgr, byte>(path);
                }
                catch
                {
                    return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
