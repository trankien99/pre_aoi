﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Drawing;
using Heal.Utils;
using System.Diagnostics;

namespace Heal.Models
{
    class Utils
    {
        public static Image<Bgr, byte> GetSMDImage(MyImage myImage, Point Loc, int Width, int Height, int OffsetX, int OffsetY, double Angle)
        {
            
            if (myImage != null)
            {
                if (!myImage.IsGetChanel())
                {
                    myImage.GetChannel();
                }
                
                if (Angle == 0)
                {
                    Loc = new Point(Loc.X + OffsetX, Loc.Y + OffsetY);
                    Rectangle ROI = new Rectangle(Loc, new Size());
                    ROI.Inflate(Width / 2, Height / 2);
                    myImage.ImgBlue.ROI = ROI;
                    myImage.ImgGreen.ROI = ROI;
                    myImage.ImgRed.ROI = ROI;
                    Image<Bgr, byte> smdImg = new Image<Bgr, byte>(new Image<Gray, byte>[] { myImage.ImgBlue, myImage.ImgGreen, myImage.ImgRed });
                    myImage.ImgBlue.ROI = Rectangle.Empty;
                    myImage.ImgGreen.ROI = Rectangle.Empty;
                    myImage.ImgRed.ROI = Rectangle.Empty;
                    return smdImg;
                }
                else
                {
                    Point Loc2 = new Point(Loc.X + OffsetX, Loc.Y + OffsetY);
                    Loc = CvUtils.PointRotation(Loc2, Loc, Angle * Math.PI / 180);
                    Rectangle ROI_Normal = new Rectangle(Loc, new Size());
                    Rectangle ROIBound = new Rectangle(Loc, new Size());
                    ROI_Normal.Inflate(Width / 2, Height / 2);
                    int distance = Convert.ToInt32(CvUtils.DistanceTwoPoint(new Point(ROI_Normal.X, ROI_Normal.Y), Loc) + 1);
                    ROIBound.Inflate(distance, distance);
                    Point newLoc = new Point(Loc.X - ROIBound.X, Loc.Y - ROIBound.Y);
                    Rectangle ROI = new Rectangle(newLoc, new Size());
                    ROI.Inflate(Width / 2, Height / 2);
                    myImage.ImgBlue.ROI = ROIBound;
                    myImage.ImgGreen.ROI = ROIBound;
                    myImage.ImgRed.ROI = ROIBound;
                    using (Image<Bgr, byte> smdImg = new Image<Bgr, byte>(new Image<Gray, byte>[] { myImage.ImgBlue, myImage.ImgGreen, myImage.ImgRed }))
                    {
                        using (Image<Bgr, byte> smdImgRotated = CvUtils.ImageRotation(smdImg, newLoc, -Angle * Math.PI / 180.0))
                        {
                            myImage.ImgBlue.ROI = Rectangle.Empty;
                            myImage.ImgGreen.ROI = Rectangle.Empty;
                            myImage.ImgRed.ROI = Rectangle.Empty;
                            smdImg.ROI = ROI; 
                            return smdImgRotated.Copy();
                        }
                    }
                }
            }
            return null;
        }
        public static Point GetRealLocation(Point Loc, MarkDev dev)
        {
            Point rP = CvUtils.PointRotation(Loc, dev.Center, -dev.Angle * Math.PI / 180.0);
            rP.X -= dev.OffsetX;
            rP.Y -= dev.OffsetY;
            return rP;
        }
        public static (Rectangle Bouding, Point[] Vertices) RotateRect(Rectangle R, int OffsetX, int OffsetY, double Angle, Point Center)
        {
            Rectangle RStride = new Rectangle(R.X + OffsetX, R.Y + OffsetY, R.Width, R.Height);
            Point[] vs = CvUtils.GetVertices(RStride);
            int x_min = 65535;
            int y_min = 65535;
            int x_max = 0;
            int y_max = 0;
            for (int i = 0; i < vs.Length; i++)
            {
                vs[i] = CvUtils.PointRotation(vs[i], Center, Angle);
                x_min = x_min > vs[i].X ? vs[i].X : x_min;
                y_min = y_min > vs[i].Y ? vs[i].Y : y_min;
                x_max = x_max < vs[i].X ? vs[i].X : x_max;
                y_max = y_max < vs[i].Y ? vs[i].Y : y_max;
            }
            Rectangle bouding = new Rectangle(x_min, y_min, x_max - x_min, y_max - y_min);
            for (int i = 0; i < vs.Length; i++)
            {
                vs[i].X -= bouding.X;
                vs[i].Y -= bouding.Y;
            }
            return (bouding, vs);
        }
    }
}
