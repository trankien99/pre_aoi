﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Heal.Models.Run
{
    public class PCBResult
    {
        public string BoardID { get; set; }
        public int CompoenntCount { get; set; }
        public InspectedStatus Status { get; set; }
        public string Barcode { get; set; }
        public ObservableCollection<SMDResult> SMDs { get; set; }
        public int Block { get; set; }
        public PCBResult()
        {
            SMDs = new ObservableCollection<SMDResult>();
        }
        public void Dispose()
        {
            for (int i = 0; i < SMDs?.Count; i++)
            {
                SMDs?[i].Dispose();
            }
        }
    }
}
