﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heal.Models;
using Heal.Models.Alg;
using Heal.Utils;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Diagnostics;
using System.Collections.ObjectModel;
using Heal.View.Tool;

namespace Heal.Models.Run
{
    public class Inspect
    {
        private MyImage _MyImage { get; set; }
        private MarkDev _MarkDev { get; set; }
        private Properties.Settings _Param = Properties.Settings.Default;
        public Inspect(MyImage image, MarkDev dev)
        {
            _MyImage = image;
            _MarkDev = dev;
        }
        public Inspect(MyImage image)
        {
            _MyImage = image;
        }
        public BoardResult Run(MyBoard Board , Loading LoadingStatus = null)
        {
            BoardResult boardResult = InitBoardResult(Board);
            // find mark point
            
            MarkDev mark = GetDeviation(Board);
            if (mark != null)
            {
                // read barcode
                var barcodes = ReadCode(Board);
                foreach (var bc in barcodes)
                {
                    if(bc.Block <= boardResult.PCBs.Count && bc.Block > 0)
                    {
                        boardResult.PCBs[bc.Block - 1].Barcode = bc.Content;
                        if (!string.IsNullOrEmpty(boardResult.Barcodes))
                            boardResult.Barcodes += ", ";
                        boardResult.Barcodes += bc.Content;
                    }
                }
                // visual inspection
                int i = 1;
                foreach (var catalog in Board.Catalogs)
                {
                    if (catalog.Type == SMDType.Mark || catalog.Type == SMDType.Barcode)
                        continue;
                    var smds = Run(catalog);
                    if (LoadingStatus != null)
                        LoadingStatus.Status = $"Inspecting {i}/{Board.Catalogs.Count} catalogs...";
                    foreach (var smd in smds)
                    {
                        
                        smd.BoardID = boardResult.BoardID;
                        if (smd.Block <= boardResult.PCBs.Count && smd.Block > 0)
                        {
                            boardResult.PCBs[smd.Block - 1].Status = InspectedStatus.NG;
                            boardResult.Status = InspectedStatus.NG;
                            boardResult.PCBs[smd.Block - 1].SMDs.Add(smd);
                        }
                    }
                    i++;
                }
            }
            else
            {
                boardResult.Status = InspectedStatus.MarkFail;
            }
            return boardResult;
        }
        private BoardResult InitBoardResult(MyBoard Board)
        {
            DateTime now = DateTime.Now;
            BoardResult boardResult = new BoardResult();
            boardResult.BoardID = Guid.NewGuid().ToString().ToUpper();
            boardResult.BoardName = Board.Name;
            boardResult.InspectedTime = now.ToString("yyyy-MM-dd HH:mm:ss");
            boardResult.Line = _Param.Line;
            boardResult.Operator = "User";
            boardResult.ProductLots = "NO READ";
            boardResult.Side = Board.Side.ToString();
            boardResult.Verified = false;
            boardResult.Station = "Pre-AOI";
            boardResult.Status = InspectedStatus.Good;
            boardResult.PCBs = new ObservableCollection<PCBResult>();
            for (int i = 0; i < Board.PCBBlocks; i++)
            {
                PCBResult pcb = new PCBResult();
                pcb.Block = i + 1;
                pcb.BoardID = boardResult.BoardID;
                pcb.CompoenntCount = 0;
                pcb.SMDs = new ObservableCollection<SMDResult>();
                pcb.Status = InspectedStatus.Good;
                boardResult.PCBs.Add(pcb);
            }
            return boardResult;
        }
        public (Point, Image<Bgr, byte>) RunPreview(SMD smd, MyAlarm alarm)
        {
            Image<Bgr, byte> imgPreview = null;
            Point loc = new Point(-1, -1);
            if (_MyImage != null && alarm != null && smd != null)
            {
                if (_MyImage.Blocks.Count > 0)
                {
                    Point smdLoc = new Point(smd.X, smd.Y);
                    smdLoc = GetRealLocation(smdLoc, _MarkDev);
                    double angle = smd.Angle;
                    int alarmWidth = Convert.ToInt32(alarm.Width);
                    int alarmHeight = Convert.ToInt32(alarm.Height);
                    int offsetX = Convert.ToInt32(alarm.OffsetX);
                    int offsetY = Convert.ToInt32(alarm.OffsetY);
                    using (var smdImage = Utils.GetSMDImage(_MyImage, smdLoc, alarmWidth, alarmHeight, offsetX, offsetY, angle))
                    {
                        loc = new Point(smdLoc.X + offsetX, smdLoc.Y + offsetY);
                        loc = CvUtils.PointRotation(loc, smdLoc, angle * Math.PI / 180.0);
                        Point smdLocOnImage = new Point(smdImage.Width / 2, smdImage.Height / 2);
                        var status = alarm.Preview(smdImage, smdLocOnImage, alarm);
                        smd.Inspected = status == AlarmType.General ? InspectedStatus.Good : InspectedStatus.NG;
                        imgPreview = alarm.Alg.ImageProcessed;
                        _MyImage.ImgBlue.ROI = Rectangle.Empty;
                        _MyImage.ImgGreen.ROI = Rectangle.Empty;
                        _MyImage.ImgRed.ROI = Rectangle.Empty;
                    }

                }
            }
            return (loc, imgPreview);
        }
        public ObservableCollection<BarcodeResult> ReadCode(MyBoard _Board)
        {
            ObservableCollection<BarcodeResult> codeResults = new ObservableCollection<BarcodeResult>();
            foreach (var catalog in _Board.Catalogs)
            {
                if (catalog.Type == SMDType.Barcode)
                {
                    foreach (var smd in catalog.SMDs)
                    {
                        BarcodeResult cResult = ReadCode(smd);
                        if (cResult != null)
                        {
                            codeResults.Add(cResult);
                        }
                    }
                }
            }
            return codeResults;
        }
        private BarcodeResult ReadCode(SMD smd)
        {
            BarcodeResult result = null;
            if (smd.Lib != null)
            {
                foreach (var alarm in smd.Lib.Alarms)
                {
                    Point smdLoc = new Point(smd.X, smd.Y);
                    smdLoc = GetRealLocation(smdLoc, _MarkDev);
                    double angle = smd.Angle;
                    int alarmWidth = Convert.ToInt32(alarm.Width);
                    int alarmHeight = Convert.ToInt32(alarm.Height);
                    int offsetX = Convert.ToInt32(alarm.OffsetX);
                    int offsetY = Convert.ToInt32(alarm.OffsetY);
                    using (var smdImage = Utils.GetSMDImage(_MyImage, smdLoc, alarmWidth, alarmHeight, offsetX, offsetY, angle))
                    {
                        alarm.Run(smdImage, smdLoc, alarm);
                        result = new BarcodeResult();
                        result.Content = alarm.Alg.Result.Content;
                        result.Block = smd.Block;
                    }
                }
            }
            return result;
        }
        public MarkDev GetDeviation(MyBoard program)
        {
            Catalog markCatalog = null;
            foreach (var catalog in program.Catalogs)
            {
                if (catalog.Type == SMDType.Mark)
                {
                    markCatalog = catalog;
                    break;
                }
            }
            MarkDev dev = GetMarkPoint(markCatalog);
            _MarkDev = dev;
            return dev;
        }
        private MarkDev GetMarkPoint(Catalog Catalogs)
        {
            if (Catalogs == null)
                return new MarkDev();
            List<System.Windows.Point> p = new List<System.Windows.Point>();
            List<System.Windows.Point> pr = new List<System.Windows.Point>();
            MarkDev dev = null;
            foreach (var smd in Catalogs.SMDs)
            {
                if (dev == null)
                {
                    dev = new MarkDev();
                    dev.Center = new Point(smd.X, smd.Y);
                }
                System.Windows.Point smdLoc = new System.Windows.Point(smd.X, smd.Y);
                System.Windows.Point curMark = new System.Windows.Point(smd.X, smd.Y);
                Point loc = new Point(smd.X, smd.Y);
                smd.Inspected = InspectedStatus.Good;
                pr.Add(smdLoc);
                double smdAngle = smd.Angle * Math.PI / 180.0;
                if(smd.Lib != null)
                {
                    foreach (var alarm in smd.Lib.Alarms)
                    {
                        if (alarm.Alg is MarkTracing)
                        {
                            int alarmWidth = Convert.ToInt32(alarm.Width);
                            int alarmHeight = Convert.ToInt32(alarm.Height);
                            int offsetX = Convert.ToInt32(alarm.OffsetX);
                            int offsetY = Convert.ToInt32(alarm.OffsetY);
                            using (var smdImage = Utils.GetSMDImage(_MyImage, loc, alarmWidth, alarmHeight, offsetX, offsetY, smdAngle))
                            {
                                loc = new Point(smdImage.Width / 2, smdImage.Height / 2);
                                alarm.Run(smdImage, loc, alarm);
                                curMark.X += alarm.Alg.Result.OffsetX;
                                curMark.Y += alarm.Alg.Result.OffsetY;
                                break;
                            }
                        }
                    }
                }
                p.Add(curMark);
            }
            if(pr.Count >=2 && p.Count >= 2)
            {
                double angle = CvUtils.AngleBetween(pr[0], pr[1], p[0], p[1]);
                dev.Angle = angle * 180.0 / Math.PI;
                dev.OffsetX = Convert.ToInt32(pr[0].X - p[0].X);
                dev.OffsetY = Convert.ToInt32(pr[0].Y - p[0].Y);
            }
            return dev;
        }
        public ObservableCollection<SMDResult> Run(Catalog catalog)
        {
            ObservableCollection<SMDResult> smdResults = new ObservableCollection<SMDResult>();
            bool catalogNG = false;
            if(_MyImage != null && catalog != null && _MarkDev != null)
            {
                foreach (var smd in catalog.SMDs)
                {
                    smd.Inspected = InspectedStatus.Good;
                    Point smdLoc = new Point(smd.X, smd.Y);
                    smdLoc = GetRealLocation(smdLoc, _MarkDev);
                    double angle = smd.Angle * Math.PI / 180.0;
                    if(smd.Lib != null)
                    {
                        foreach (var alarm in smd.Lib.Alarms)
                        {
                            int alarmWidth = Convert.ToInt32(alarm.Width);
                            int alarmHeight = Convert.ToInt32(alarm.Height);
                            int offsetX = Convert.ToInt32(alarm.OffsetX);
                            int offsetY = Convert.ToInt32(alarm.OffsetY);
                            using (var image = Utils.GetSMDImage(_MyImage, smdLoc, alarmWidth, alarmHeight, offsetX, offsetY, smd.Angle))
                            {
                                Point smdCenter = new Point(image.Width / 2, image.Height / 2);
                                if(smd.Name == "B_C215")
                                {

                                }
                                var status = alarm.Alg.Run(image, smdCenter, alarm);
                                if (status != AlarmType.General)
                                {
                                    smd.Inspected = InspectedStatus.NG;
                                    catalogNG = true;
                                    SMDResult smdFailed = new SMDResult();
                                    smdFailed.Alarm = status;
                                    smdFailed.Location = smdLoc;
                                    smdFailed.Block = smd.Block;
                                    smdFailed.Name = smd.Name;
                                    smdFailed.Catalog = catalog.Name;
                                    int width = Convert.ToInt32(smd.Width);
                                    int height = Convert.ToInt32(smd.Height);
                                    smdFailed.ReferenceImage = catalog.RefImage?.Copy();
                                    if (catalog.RefImage != null)
                                    {
                                        width = catalog.RefImage.Width;
                                        height = catalog.RefImage.Height;
                                    }
                                    smdFailed.RepairImage = Utils.GetSMDImage(_MyImage, smdLoc, width, height, 0, 0, smd.Angle);
                                    width += 500;
                                    height += 500;
                                    smdFailed.OverRepairImage = Utils.GetSMDImage(_MyImage, smdLoc, width, height, 0, 0, smd.Angle);
                                    smdResults.Add(smdFailed);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catalog.Inspected = catalogNG ? InspectedStatus.NG : InspectedStatus.Good;
            return smdResults;
        }
        public Point GetRealLocation(Point Loc, MarkDev dev)
        {
            Point rP = CvUtils.PointRotation(Loc, dev.Center, -dev.Angle * Math.PI / 180.0);
            rP.X -= dev.OffsetX;
            rP.Y -= dev.OffsetY;
            return rP;
        }
        public (Rectangle Bouding, Point[] Vertices) RotateRect(Rectangle R, int OffsetX, int OffsetY, double Angle, Point Center)
        {
            Rectangle RStride = new Rectangle(R.X + OffsetX, R.Y + OffsetY, R.Width, R.Height);
            Point[] vs = CvUtils.GetVertices(RStride);
            int x_min = 65535;
            int y_min = 65535;
            int x_max = 0;
            int y_max = 0;
            for (int i = 0; i < vs.Length; i++)
            {
                vs[i] = CvUtils.PointRotation(vs[i], Center, Angle);
                x_min = x_min > vs[i].X ? vs[i].X : x_min;
                y_min = y_min > vs[i].Y ? vs[i].Y : y_min;
                x_max = x_max < vs[i].X ? vs[i].X : x_max;
                y_max = y_max < vs[i].Y ? vs[i].Y : y_max;
            }
            Rectangle bouding = new Rectangle(x_min, y_min, x_max - x_min, y_max - y_min);
            for (int i = 0; i < vs.Length; i++)
            {
                vs[i].X -= bouding.X;
                vs[i].Y -= bouding.Y;
            }
            return (bouding, vs);
        }
    }
    
}
