﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Drawing;

namespace Heal.Models.Run
{
    public class SMDResult
    {
        public string BoardID { get; set; }
        public string ReferenceImagePath { get; set; }
        public Image<Bgr, byte> ReferenceImage { get; set; }
        public string RepairImagePath { get; set; }
        public Image<Bgr, byte> RepairImage { get; set; }
        public string OverRepairImagePath { get; set; }
        public Image<Bgr, byte> OverRepairImage { get; set; }
        public int Block { get; set; }
        public AlarmType Alarm { get; set; }
        public string Name { get; set; }
        public string Catalog { get; set; }
        public Point Location { get; set; }
        public bool Verified { get; set; }
        public void Dispose()
        {
            if(this.ReferenceImage != null)
            {
                this.ReferenceImage.Dispose();
                this.ReferenceImage = null;
            }
            if (this.RepairImage != null)
            {
                this.RepairImage.Dispose();
                this.RepairImage = null;
            }
            if (this.OverRepairImage != null)
            {
                this.OverRepairImage.Dispose();
                this.OverRepairImage = null;
            }
        }
    }
}
