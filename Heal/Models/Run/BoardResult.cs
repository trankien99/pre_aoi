﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Drawing;
using System.Collections.ObjectModel;
using System.IO;
using Emgu.CV.CvEnum;

namespace Heal.Models.Run
{
    public class BoardResult
    {
        Properties.Settings _Param = Properties.Settings.Default;
        public int ID { get; set; }
        public string BoardID { get; set; }
        public bool Verified { get; set; }
        public string InspectedTime { get; set; }
        public string BoardName { get; set; }
        public string ProductLots { get; set; }
        public string Line { get; set; }
        public string Station { get; set; }
        public string Operator { get; set; }
        public string Side { get; set; }
        public double CycleTime { get; set; }
        public string Barcodes { get; set; }
        public int SMDCount { get; set; }
        public InspectedStatus Status { get; set; }
        public double ImageScale { get; set; }
        public string BoardImagePath { get; set; }
        public Image<Bgr, byte> BoardImage { get; set; }
        public ObservableCollection<PCBResult> PCBs { get; set; }
        public BoardResult()
        {
            this.PCBs = new ObservableCollection<PCBResult>();
        }
        public void Sort()
        {
            foreach (var pcb in PCBs)
            {
                if(pcb.SMDs.Count > 1)
                {
                    var sorted = pcb.SMDs.OrderBy(ii => ii.Name);
                    pcb.SMDs.Clear();
                    for (int i = 0; i < sorted.Count(); i++)
                    {
                        pcb.SMDs.Add(sorted.ElementAt(i));
                    }
                }
            }
        }
        public void SaveImage(string Dir)
        {
            DateTime date = DateTime.Parse(InspectedTime);
            string dirPath = $"{Dir}/{this.BoardName}/{date.Year}_{date.Month}_{date.Day}/[{date.Hour}_{date.Minute}_{date.Second}]_[{this.Barcodes}]";
            if(!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }
            if(BoardImage != null)
            {
                this.BoardImagePath = $"{dirPath}/board.jpg";
                CvInvoke.Imwrite(BoardImagePath, BoardImage, new KeyValuePair<ImwriteFlags, int>(ImwriteFlags.JpegQuality, _Param.SaveImageQuality));
            }
            if (PCBs != null)
            {
                foreach (var pcb in PCBs)
                {
                    if (pcb != null)
                    {
                        foreach (var item in pcb.SMDs)
                        {
                            string smdDir = $"{dirPath}/{item.Name}_{item.Block}";
                            if (!Directory.Exists(smdDir))
                            {
                                Directory.CreateDirectory(smdDir);
                            }
                            if(item.OverRepairImage != null)
                            {
                                item.OverRepairImagePath = $"{smdDir}/OverRepairImage.jpg";
                                CvInvoke.Imwrite(item.OverRepairImagePath, item.OverRepairImage);
                            }
                            if (item.RepairImage != null)
                            {
                                item.RepairImagePath = $"{smdDir}/RepairImage.jpg";
                                CvInvoke.Imwrite(item.RepairImagePath, item.RepairImage);
                            }
                            if (item.ReferenceImage != null)
                            {
                                item.ReferenceImagePath = $"{smdDir}/ReferenceImage.jpg";
                                CvInvoke.Imwrite(item.ReferenceImagePath, item.ReferenceImage);
                            }
                        }
                    }
                }
            }

        }
        public void Dispose()
        {
            if(BoardImage != null)
            {
                BoardImage.Dispose();
                BoardImage = null;
            }
            if(PCBs != null)
            {
                foreach (var item in PCBs)
                {
                    if (item != null)
                        item.Dispose();
                }
            }
            
        }
    }
}
