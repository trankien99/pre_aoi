﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using Emgu.CV.Util;
using System.IO;
using NLog;
using Newtonsoft.Json;
using Heal.SDK;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Heal.Models
{
    public class Catalog : BaseProperties
    {
        private static Logger mLog = LogCtl.GetInstance();
        public bool _IsEnable { get; set; }
        public bool IsEnable
        {
            get
            {
                return _IsEnable;
            }
            set
            {
                _IsEnable = value;
                if(SMDs != null)
                    for (int i = 0; i < SMDs.Count; i++)
                    {
                        SMDs[i].IsEnable = value;
                    }
                OnPropertyChanged("IsEnable");
            }
        }
        private string _Description { get; set; }
        public string Description
        {
            get => _Description;
            set
            {
                _Description = value;
                OnPropertyChanged("Description");
            }
        }
        private SMDType _Type { get; set; }
        public SMDType Type
        {
            get => _Type;
            set
            {
                _Type = value;
                OnPropertyChanged("Type");
            }
        }
        public ObservableCollection<SMD> SMDs { get; set; }
        private InspectedStatus _Inspected { get; set; }
        public InspectedStatus Inspected
        {
            get => _Inspected;
            set
            {
                _Inspected = value;
                OnPropertyChanged("Inspected");
            }
        }
        private Image<Bgr, byte> _RefImage { get; set; }
        public Image<Bgr, byte> RefImage
        {
            get => _RefImage;
            set
            {
                _RefImage = value;
                OnPropertyChanged("RefImage");
            }
        }

        public Catalog()
        {
            this.Description = "";
            this.SMDs = new ObservableCollection<SMD>();
            this.IsEnable = true;
            this.Width = 25;
            this.Height = 25;
            this.Inspected =  InspectedStatus.Good;
        }
        
        public Catalog(CadItem cad, int ID)
        {
            this.Name = cad.Code;
            this.Width = 25;
            this.Height = 25; 
            this.IsEnable = true;
            this.Description = "";
            this.SMDs = new ObservableCollection<SMD>();
            AddSMD(cad, ID);
        }
        public void UpdateCatalogInspected()
        {
            Inspected = InspectedStatus.Good;
            foreach (var item in SMDs)
            {
                if(item.Inspected != InspectedStatus.Good)
                {
                    Inspected = InspectedStatus.NG;
                    break;
                }
            }
        }
        public void SaveLibrary(string Dir)
        {
            if (!Directory.Exists(Dir))
            {
                Directory.CreateDirectory(Dir);
            }
            if (SMDs.Count > 0)
            {
                foreach (var item in SMDs)
                {
                    if (item.Lib != null)
                    {
                        item.Lib.Save(Dir);
                    }
                }
            }
        }
        public void Sort()
        {
            var smdSorted = SMDs.OrderBy(i => i.Name);
            SMDs = new ObservableCollection<SMD>();
            for (int i = 0; i < smdSorted.Count(); i++)
            {
                SMD smd = smdSorted.ElementAt(i);
                SMDs.Add(smd);
            }
        }
        public void AddSMD(CadItem cad, int ID, int Block = 1)
        {
            SMD smd = new SMD(ID);
            smd.Angle = cad.Angle;
            smd.X = Convert.ToInt32(cad.X);
            smd.Y = Convert.ToInt32(cad.Y);
            smd.Name = cad.Name;
            smd.Width = 25;
            smd.Height = 25;
            smd.Block = Block;
            this.SMDs.Add(smd);
        }
        public Catalog Copy()
        {
            string s = JsonConvert.SerializeObject(this);
            Catalog obj = JsonConvert.DeserializeObject<Catalog>(s, new MyJsonConverter());
            return obj;
        }
        public void Dispose()
        {
            if (RefImage != null)
            {
                RefImage.Dispose();
                RefImage = null;
            }
            foreach (var smd in SMDs)
            {
                smd.Dispose();
            }
        }
    }
}
