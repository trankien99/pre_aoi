﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using Emgu.CV.Util;
using System.IO;
using NLog;
using Newtonsoft.Json;
using Heal.Utils;

namespace Heal.Models
{
    public class SMD : BaseProperties
    {
        private int _CatalogID { get; set; }
        public int CatalogID
        {
            get => _CatalogID;
            set
            {
                _CatalogID = value;
                OnPropertyChanged("CatalogID");
            }
        }
        private bool _IsEnable { get; set; }
        public bool IsEnable
        {
            get => _IsEnable;
            set
            {
                _IsEnable = value;
                OnPropertyChanged("IsEnable");
            }
        }
        private bool _IsUnlink { get; set; }
        public bool IsUnlink
        {
            get => _IsUnlink;
            set
            {
                _IsUnlink = value;
                OnPropertyChanged("IsUnlink");
            }
        }
        private bool _IsSetFlag { get; set; }
        public bool IsSetFlag
        {
            get => _IsSetFlag;
            set
            {
                _IsSetFlag = value;
                OnPropertyChanged("IsSetFlag");
            }
        }
        private int _X { get; set; }
        public int X
        {
            get => _X;
            set
            {
                _X = value;
                OnPropertyChanged("X");
            }
        }
        private int _Y { get; set; }
        public int Y
        {
            get => _Y;
            set
            {
                _Y = value;
                OnPropertyChanged("Y");
            }
        }
        private double _Angle { get; set; }
        public double Angle
        {
            get => _Angle;
            set
            {
                _Angle = value;
                OnPropertyChanged("Angle");
            }
        }
        private int _Block { get; set; }
        public int Block
        {
            get => _Block;
            set
            {
                _Block = value;
                OnPropertyChanged("Block");
            }
        }
        private Library _Lib { get; set; }
        public Library Lib
        {
            get => _Lib;
            set
            {
                _Lib = value;
                OnPropertyChanged("Lib");
            }
        }
        private InspectedStatus _Inspected { get; set; }
        public InspectedStatus Inspected
        {
            get => _Inspected;
            set
            {
                _Inspected = value;
                OnPropertyChanged("Inspected");
            }
        }
        public SMD(int CatalogID)
        {
            this.CatalogID = CatalogID;
            this.IsEnable = true;
            this.Lib = null;
            this.Inspected = InspectedStatus.Good;
            this.Angle = 0;
            this.X = 100;
            this.Y = 100;
            this.Width = 25;
            this.Height = 25;
        }
        public void Flip(FlipMode F, Point RotateCenter)
        {
            this.Angle = (this.Angle + 180) % 360;
            if (F == FlipMode.X)
            {
                int subx = RotateCenter.X - this.X;
                this.X = this.X + 2 * subx;
            }
            else
            {
                int suby = RotateCenter.Y - Y;
                this.Y = this.Y + 2 * suby;
            }
        }
        public void Rotate(double Angle, Point RotateCenter)
        {
            var point = CvUtils.PointRotation(new Point(this.X, this.Y), RotateCenter, Angle * Math.PI / 180.0);
            this.X = point.X;
            this.Y = point.Y;
        }
        public void Translate(double X, double Y)
        {
            this.X += Convert.ToInt32(X);
            this.Y += Convert.ToInt32(Y);
        }
        public SMD Copy()
        {
            string s = JsonConvert.SerializeObject(this);
            SMD obj = JsonConvert.DeserializeObject<SMD>(s, new MyJsonConverter());
            return obj;
        }
        public void Dispose()
        {

        }
    }
}
