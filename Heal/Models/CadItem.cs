﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Heal.Models
{
    public class CadItem
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public double Angle { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double RotateX { get; set; }
        public double RotateY { get; set; }
        public void Stride(double X, double Y)
        {
            this.X += X;
            this.Y += Y;
        }
        public static List<CadItem> GetCadFromString(string s)
        {
            List<CadItem> cadItems = new List<CadItem>();
            double _XMin = 999999;
            double _YMin = 999999;
            double _XMax = -999999;
            double _YMax = -999999;
            double stdDpi = Const.STD_DPI;
            string[] content = s.Split('\n');
            foreach (string item in content)
            {
                string line = item.Replace('\t', ' ');
                string[] arrall = line.Split(' ');
                List<string> arr = new List<string>();
                foreach (var str in arrall)
                {
                    if (str != "")
                    {
                        arr.Add(str);
                    }
                }
                CadItem cadItem = new CadItem();
                try
                {
                    cadItem.Name = arr[0];
                    cadItem.Angle = Convert.ToDouble(arr[3]);
                    cadItem.Code = arr[4].Trim();
                    cadItem.X = Math.Round(Convert.ToDouble(arr[1]) * stdDpi / 25.4, 2);
                    cadItem.Y = Math.Round(Convert.ToDouble(arr[2]) * stdDpi / 25.4, 2);
                    if (cadItem.X < _XMin)
                    {
                        _XMin = cadItem.X;
                    }
                    if (cadItem.Y < _YMin)
                    {
                        _YMin = cadItem.Y;
                    }
                    if (cadItem.X > _XMax)
                    {
                        _XMax = cadItem.X;
                    }
                    if (cadItem.Y > _YMax)
                    {
                        _YMax = cadItem.Y;
                    }
                }
                catch (Exception)
                {
                    continue;
                }
                cadItems.Add(cadItem);
            }
            double x = -_XMin;
            double y = -_YMin;
            for (int i = 0; i < cadItems.Count; i++)
            {
                cadItems[i].RotateX = (_XMax - _XMin) / 2 + _XMin;
                cadItems[i].RotateY = (_YMax - _YMin) / 2 + _YMin;
            }
            var cadSorted = cadItems.OrderBy(i => i.Code);
            cadItems = new List<CadItem>();
            for (int i = 0; i < cadSorted.Count(); i++)
            {
                cadItems.Add(cadSorted.ElementAt(i));
            }
            return cadItems;
        }
    }
}
