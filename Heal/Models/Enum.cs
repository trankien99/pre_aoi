﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.IO;
using NLog;
using Newtonsoft.Json;
namespace Heal.Models
{
    public class EmumControl
    {
        public static string[] GetBoardSide()
        {
            var array = Enum.GetValues(typeof(BoardSide)).Cast<BoardSide>();
            string[] alarmStr = new string[array.Count()];
            for (int i = 0; i < array.Count(); i++)
            {
                alarmStr[i] = array.ElementAt(i).ToString();
            }
            return alarmStr;
        }
        public static string[] GetCatalogType()
        {
            var array = Enum.GetValues(typeof(SMDType)).Cast<SMDType>();
            string[] alarmStr = new string[array.Count()];
            for (int i = 0; i < array.Count(); i++)
            {
                alarmStr[i] = array.ElementAt(i).ToString();
            }
            return alarmStr;
        }
        public static string[] GetAlarmType()
        {
            var array = Enum.GetValues(typeof(AlarmType)).Cast<AlarmType>();
            string[] alarmStr = new string[array.Count()];
            for (int i = 0; i < array.Count(); i++)
            {
                alarmStr[i] = array.ElementAt(i).ToString();
            }
            return alarmStr;
        }
        public static string[] GetFullMarkShape()
        {
            var array = Enum.GetValues(typeof(MarkShape)).Cast<MarkShape>();
            string[] items = new string[array.Count()];
            for (int i = 0; i < array.Count(); i++)
            {
                items[i] = array.ElementAt(i).ToString();
            }
            return items;
        }
        public static string[] GetBarcodeTypes()
        {
            var array = Enum.GetValues(typeof(CodeFormat)).Cast<CodeFormat>();
            string[] items = new string[array.Count()];
            for (int i = 0; i < array.Count(); i++)
            {
                items[i] = array.ElementAt(i).ToString();
            }
            return items;
        }
    }
    public enum Action
    {
        New,
        Copy,
    }
    public enum BoardSide
    {
        TOP,
        BOT
    }
    public enum FlipMode
    {
        X,
        Y
    }
    public enum InspectedStatus
    {
        Good,
        Pass,
        NG,
        MarkFail,
        ImagingFail
    }
    public enum MeasureUnit
    {
        Millimeters,
        Inch,
        Percent
    }
    public enum SaveProgramMode
    {
        Program,
        ProgramAndImage,
    }
    public enum AlarmType
    {
        General = 0,
        Missing = 1,
        Shift = 2,
        Billboard = 3,
        Tombstone = 4,
        WrongPart = 5,
        Polarity = 6,
        Broken = 7,
        Scratch = 8,
        BadMark = 9,
    }
    public enum SMDType
    {
        SMD,
        Mark,
        Barcode,
        Others,
        //Virtual,
        //Mark,
        //SideMark,
        //BadMark,
        //Barcode,
        //ChipResistor,
        //ChipCapacitor,
        //Inductor,
        //TantalumCapacitor,
        //ElectrolyticCapacitor,
        //ResistorPack,
        //Diode,
        //SOT,
        //Connector,
        //IC,
        //BGA,
        //MELFS,
        //SOT23,
        //SOT89,
        //SOD123,
        //SOT143,
        //SOT223,
        //TO252,
        //SOIC,
        //SSOIC,
        //SOPIC,
        //TSOP,
        //CFP,
        //SOJ,
        //FQFP,
        //PLCC,
        //DIP,
        //PRBGA,
        //Others
    }

    public enum ImageType
    {
        Over,
        Bouding,
        Algorithm
    }
    public enum CodeFormat
    {
        Codabar,
        Code39,
        Code128,
        Code93,
        EAN13,
        EAN8,
        IMB,
        ITF,
        MaxiCode,
        PDF417,
        UPCA,
        UPCE,
        QRCode,
        DataMatrix,
    }
    public enum MarkShape
    {
        Circle,
    }
    public enum RelativeTarget
    {
        Component,
        Parent
    }
}
