﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Heal.Models;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using Heal.Models.Alg;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Heal.Resource.Convertor
{
    public class Action
    {
        public static void UpdateBindingSourceTextBox(object sender)
        {
            TextBox tBox = (TextBox)sender;
            DependencyProperty prop = TextBox.TextProperty;
            BindingExpression binding = BindingOperations.GetBindingExpression(tBox, prop);
            if (binding != null) { binding.UpdateSource(); }
        }
        public static void Change(object sender, int delta, double Value, bool Nagative = false)
        {
            TextBox tBox = (TextBox)sender;
            double value = delta > 0 ? Convert.ToDouble(tBox.Text) + Value : Convert.ToDouble(tBox.Text) - Value;
            if (!Nagative & value < 0)
            {
                value = 0;
            }
            tBox.Text = value.ToString();
            DependencyProperty prop = TextBox.TextProperty;
            BindingExpression binding = BindingOperations.GetBindingExpression(tBox, prop);
            if (binding != null) { binding.UpdateSource(); }
        }
        public static void Change0dot5(object sender, int delta, bool Nagative = false)
        {
            TextBox tBox = (TextBox)sender;
            double value = delta > 0 ? Convert.ToDouble(tBox.Text) + 0.5 : Convert.ToDouble(tBox.Text) - 0.5;
            if (!Nagative & value < 0)
            {
                value = 0;
            }
            tBox.Text = value.ToString();
            DependencyProperty prop = TextBox.TextProperty;
            BindingExpression binding = BindingOperations.GetBindingExpression(tBox, prop);
            if (binding != null) { binding.UpdateSource(); }
        }
        public static void Change0dot2(object sender, int delta, bool Nagative = false)
        {
            TextBox tBox = (TextBox)sender;
            double value = delta > 0 ? Convert.ToDouble(tBox.Text) + 0.2 : Convert.ToDouble(tBox.Text) - 0.2;
            if (!Nagative & value < 0)
            {
                value = 0;
            }
            tBox.Text = value.ToString();
            DependencyProperty prop = TextBox.TextProperty;
            BindingExpression binding = BindingOperations.GetBindingExpression(tBox, prop);
            if (binding != null) { binding.UpdateSource(); }
        }
        public static void Change2(object sender, int delta, bool Nagative = false)
        {
            TextBox tBox = (TextBox)sender;
            double value = delta > 0 ? Convert.ToDouble(tBox.Text) + 2 : Convert.ToDouble(tBox.Text) - 2;
            if(!Nagative & value < 0)
            {
                value = 0;
            }
            tBox.Text = value.ToString();
            DependencyProperty prop = TextBox.TextProperty;
            BindingExpression binding = BindingOperations.GetBindingExpression(tBox, prop);
            if (binding != null) { binding.UpdateSource(); }
        }
        public static void Change5(object sender, int delta, bool Nagative = false)
        {
            TextBox tBox = (TextBox)sender;
            double value = delta > 0 ? Convert.ToDouble(tBox.Text) + 5 : Convert.ToDouble(tBox.Text) - 5;
            if (!Nagative & value < 0)
            {
                value = 0;
            }
            tBox.Text = value.ToString();
            DependencyProperty prop = TextBox.TextProperty;
            BindingExpression binding = BindingOperations.GetBindingExpression(tBox, prop);
            if (binding != null) { binding.UpdateSource(); }
        }
    }
    public class Bool2Visiblity : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class BoardSide2Int : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int x = System.Convert.ToInt32(value);
            return x;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((int)value)
            {
                case 0:
                    return BoardSide.TOP;
                case 1:
                    return BoardSide.TOP;
                default:
                    break;
            }
            return BoardSide.TOP;
        }
    }
    public class CatalogType2Int : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int x = System.Convert.ToInt32(value);
            return x;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (SMDType)(System.Convert.ToInt32(value));
        }
    }
    public class Pixel2Millimeter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                return Math.Round(Const.Pixel2Millimeter(System.Convert.ToInt32(value)), 3);
            }
            catch { return null; }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {

                return Const.Millimeter2Pixel(System.Convert.ToDouble(value));
            }
            catch { return null; }
        }
    }
    public class CanvasSetCvt : IMultiValueConverter
    {
        public CanvasSetCvt() { }
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return values[0];
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class Enable2Color : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Brushes.Green : Brushes.Gray;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class BoardNotNull : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is MyBoard ? true : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class BoardNotNull2Visiblity : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is MyBoard ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class BoardnImageNotNull : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is MyBoard)
            {
                MyBoard board = value as MyBoard;
                if (board.Image?.Blocks.Count > 0)
                    return true;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class MetaImageBoxZoomScaleConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value * 100;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class BoardnImageIsNotNull : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is MyBoard)
            {
                if ((value as MyBoard).Image != null)
                {
                    return true;
                }
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ObjectIsCatalogOrSMD : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Catalog || value is SMD)
                return true;
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ObjectNotNull: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ObjectNotNull2Visiblity : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value != null ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ObjectNotEmty : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string s = System.Convert.ToString(value);
            return !string.IsNullOrEmpty(s);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class AlarmType2Index : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            AlarmType alarmType = (AlarmType)value;
            return System.Convert.ToInt32(alarmType);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int index = System.Convert.ToInt32(value);
            AlarmType alarmType = (AlarmType)index;
            return alarmType;
        }
    }
    public class MarkShape2Index : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            MarkShape alarmType = (MarkShape)value;
            return System.Convert.ToInt32(alarmType);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int index = System.Convert.ToInt32(value);
            MarkShape alarmType = (MarkShape)index;
            return alarmType;
        }
    }
    public class Algorithm2String : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            MyAlgorithm alg = (MyAlgorithm)value;
            if (alg == null)
                return null;
            else
                return alg.Name;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string name = (string)value;
            MyAlgorithm algorithm = MyAlgorithm.GetInstanceFromName(name);
            return algorithm;
        }
    }
    public class Convert2Percent : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToDouble(value) * 100;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return 1;
        }
    }
    public class ScoreInRange2Brush : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                double score = System.Convert.ToDouble(values[0]);
                double low = System.Convert.ToDouble(values[1]);
                double up = System.Convert.ToDouble(values[2]);
                if (low <= up)
                {
                    return score * 100 >= low && score * 100 <= up ? Brushes.Transparent : Brushes.Red;
                }
                else
                {
                    return score * 100 <= low || score * 100 >= up ? Brushes.Transparent : Brushes.Red;
                }
            }
            catch { return null; }
            
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ValueInRange2Brush : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                double score = System.Convert.ToDouble(values[0]);
                double low = System.Convert.ToDouble(values[1]);
                double up = System.Convert.ToDouble(values[2]);
                if (low <= up)
                {
                    return score  >= low && score  <= up ? Brushes.Transparent : Brushes.Red;
                }
                else
                {
                    return score  <= low || score  >= up ? Brushes.Transparent : Brushes.Red;
                }
            }
            catch { return null; }

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class EmguImage2MediaImage : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Image<Bgr, byte>)
            {
                BitmapSource bms = Control.MetaImageBox.Bitmap2BitmapSource((value as Image<Bgr, byte>).Bitmap);
                return bms;
            }
            else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    public class Bool2ColorEnable : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool)value ? Brushes.White : Brushes.Gray;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
    public class CodeFormat2Index : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            CodeFormat alarmType = (CodeFormat)value;
            return System.Convert.ToInt32(alarmType);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int index = System.Convert.ToInt32(value);
            CodeFormat alarmType = (CodeFormat)index;
            return alarmType;
        }
    }
    public class OverOpacityConvertor : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {

                return System.Convert.ToDouble(values[0]) * System.Convert.ToDouble(values[1]) / System.Convert.ToDouble(values[2]);
            }
            catch { return 0; }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class Inspected2Color : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            InspectedStatus status = (InspectedStatus)value;
            return status == InspectedStatus.Good || status == InspectedStatus.Pass ? Brushes.Transparent : Brushes.Red;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int index = System.Convert.ToInt32(value);
            CodeFormat alarmType = (CodeFormat)index;
            return alarmType;
        }
    }
}
