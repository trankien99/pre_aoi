﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Heal.Resource.Control
{
    /// <summary>
    /// Interaction logic for ImageBox.xaml
    /// 12 / 08/ 2021 init
    /// </summary>
    public partial class ImageBox : UserControl, INotifyPropertyChanged
    {
        private ImageSource mSource = null;
        private Rect mSelectRectangle = new Rect();
        public double mScale = 1;
        private double mMaxZoomScale = 5;
        private double mMinZoomScale = 0.2;
        private bool mIsDrawing = false;
        private Point mStartPoint = new Point(0, 0);
        private bool mIsSelectRect = false;
        public ImageBox()
        {
            InitializeComponent();
            SelectByCtr = false;
            this.DataContext = this;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(""));
        }
        public void FillScreen()
        {
            if(Source != null)
            {
                double scaleX = (scroll.ActualWidth - 5) / Source.Width;
                double scaleY = (scroll.ActualHeight - 5) / Source.Height;
                SetScale(Math.Min(scaleX, scaleY));
            }
        }
        public bool SelectByCtr { get; set; }
        public bool IsSelectRect {
            get => mIsSelectRect;
            set {
                mIsSelectRect = value;
                if(mIsSelectRect)
                {
                    grid.Cursor = Cursors.Cross;
                }
                else
                {
                    grid.Cursor = Cursors.Arrow;
                }
                NotifyPropertyChanged();
            } 
        }
        public Rect RectangleSelected
        {
            get => mSelectRectangle;
            set
            {
                if (value == null)
                    mSelectRectangle = new Rect();
                else
                    mSelectRectangle = value;
                SetRectSelected();
            }
        }
        public double ZoomScale
        {
            get => mScale;
            set
            {
                if(value <= mMaxZoomScale && value >= mMinZoomScale)
                {
                    SetScale(value);
                }
                else
                {
                    throw new InvalidOperationException("Zoom scale out of range!");
                }
            }
        }
        public double MaxZoomScale
        {
            get => mMaxZoomScale;
            set
            {
                mMaxZoomScale = value;
            }
        }
        public double MinZoomScale
        {
            get => mMinZoomScale;
            set
            {
                mMinZoomScale = value;
            }
        }
        public ImageSource Source
        {
            get { return mSource; }
            set
            {
                mSource = value;
                mSelectRectangle = new Rect();
                NotifyPropertyChanged();
            }
        }

        public System.Drawing.Bitmap SourceFromBitmap
        {
            set
            {
                this.Source = Bitmap2BitmapSource(value);
            }
        }
        private void SetScale(double Scale)
        {
            if(mSource != null)
            {
                if (Scale <= mMaxZoomScale && Scale >= mMinZoomScale)
                {
                    Scale = Math.Round(Scale, 2);
                    grid.Width = mSource.Width * Scale;
                    grid.Height = mSource.Height * Scale;
                    mScale = Scale;
                    SetRectSelected();
                    scroll.UpdateLayout();
                    grid.UpdateLayout();
                }
            }
            NotifyPropertyChanged();
        }
        private void GetRectSelected()
        {
            mSelectRectangle.X = rect.Margin.Left / mScale;
            mSelectRectangle.Y = rect.Margin.Top / mScale;
            mSelectRectangle.Width = rect.Width / mScale;
            mSelectRectangle.Height = rect.Height / mScale;
            Console.WriteLine(mSelectRectangle);
        }
        public void SetRectSelected()
        {
            double x = mSelectRectangle.X * mScale;
            double y = mSelectRectangle.Y * mScale;
            double w = mSelectRectangle.Width * mScale;
            double h = mSelectRectangle.Height * mScale;
            rect.Width = w;
            rect.Height = h;
            rect.Margin = new Thickness(x, y, 0, 0);
        }
        public  Point GetMousePoint()
        {
            var P =  Mouse.GetPosition(imb);
            double x = P.X / mScale;
            double y = P.Y / mScale;
            return new Point(x, y);
        }
        public static BitmapSource Bitmap2BitmapSource(System.Drawing.Bitmap bitmap, bool Release = true)
        {
            var pixelFormat = bitmap.PixelFormat;
            PixelFormat format = PixelFormats.Bgr24;
            switch (pixelFormat)
            {
                case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                    format = PixelFormats.Gray8;
                    break;
                case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
                    format = PixelFormats.Bgr24;
                    break;
                case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
                    format = PixelFormats.Bgra32;
                    break;
                default:
                    break;
            }
            var bitmapData = bitmap.LockBits(
                new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);
            var bitmapSource = BitmapSource.Create(
                bitmapData.Width, bitmapData.Height,
                bitmap.HorizontalResolution, bitmap.VerticalResolution,
                format, null,
                bitmapData.Scan0, bitmapData.Stride * bitmapData.Height, bitmapData.Stride);
            bitmap.UnlockBits(bitmapData);
            if (Release)
            {
                bitmap.Dispose();
            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return bitmapSource;
        }
        public Brush RectangleStroke
        {
            get => rect.Stroke;
            set
            {
                rect.Stroke = value;
            }
        }
        private void mnZoomIn_Click(object sender, RoutedEventArgs e)
        {
            SetScale(1.2 * mScale);
        }

        private void mnZoomOut_Click(object sender, RoutedEventArgs e)
        {
            SetScale(0.8 * mScale);
        }

        private void mnReset_Click(object sender, RoutedEventArgs e)
        {
            SetScale(1);
        }
        private void mnResetSelection_Click(object sender, RoutedEventArgs e)
        {
            RectangleSelected = new Rect();
        }
        private void imb_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if(IsSelectRect)
            {
                mStartPoint = Mouse.GetPosition(sender as UIElement);
                mIsDrawing = true;
            }
        }
        private void imb_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if(IsSelectRect)
            {
                mIsDrawing = false;
                IsSelectRect = false;
                GetRectSelected();
                SetRectSelected();
                var args = new SelectRectangleArgs(mSelectRectangle);
                RectangleChanged(this, args);
            }
        }

        private void imb_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if(mIsDrawing && IsSelectRect)
            {
                Point p = Mouse.GetPosition(sender as UIElement);
                double x = Math.Min(p.X, mStartPoint.X);
                double y = Math.Min(p.Y, mStartPoint.Y);
                double w = Math.Abs(p.X - mStartPoint.X);
                double h = Math.Abs(p.Y - mStartPoint.Y);
                rect.Width = w;
                rect.Height = h;
                rect.Margin = new Thickness(x, y, 0, 0);
                rect.Visibility = Visibility.Visible;
            }
        }

        private void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl)
            {
                if(SelectByCtr && mSource != null)
                {
                    IsSelectRect = true;
                }
            }
        }

        private void UserControl_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftCtrl || e.Key == Key.RightCtrl)
            {
                if (SelectByCtr)
                {
                    IsSelectRect = false;
                }
            }
        }
        public void GoPoint(Point P, bool ResetScale = false)
        {
            if(ResetScale)
            {
                SetScale(1);
            }
            double x = P.X * mScale;
            double y = P.Y * mScale;
            double crOffsetX = scroll.HorizontalOffset;
            double crOffsetY = scroll.VerticalOffset;
            double lX = scroll.ViewportWidth;
            double lY = scroll.ViewportHeight;
            double offsetX = crOffsetX +  lX/ 2;
            double offsetY = crOffsetY + lY/ 2;
            if(x > lX / 2)
            {
                scroll.ScrollToHorizontalOffset(x - lX / 2);
            }
            if (y > lY / 2)
            {
                scroll.ScrollToVerticalOffset(y - lY / 2);
            }
        }

        private void scroll_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            //Console.WriteLine($"H:{scroll.HorizontalOffset}, V:{scroll.VerticalOffset}, W:{scroll.ViewportWidth}");
        }

        public event ClickHandler Clicked;
        protected void MouseClicked(object sender, ClickEventArgs e)
        {
            if (this.Clicked != null)
                this.Clicked(sender, e);
        }
        public event SelectRectangleHandler SelectRectangleChanged;
        protected void RectangleChanged(object sender, SelectRectangleArgs e)
        {
            if (this.SelectRectangleChanged != null)
                this.SelectRectangleChanged(sender, e);
        }
        private void grid_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if(Source != null)
            {
                Point P = Mouse.GetPosition(sender as UIElement);
                double x = P.X / mScale;
                double y = P.Y / mScale;
                var args = new ClickEventArgs(new Point(x, y));
                MouseClicked(this, args);
            }
        }

        private void mnFillScreen_Click(object sender, RoutedEventArgs e)
        {
            FillScreen();
        }
    }
    public delegate void ClickHandler(object sender, ClickEventArgs e);
    public class ClickEventArgs : EventArgs
    {
        public Point ClickPoint { get; set; }
        public ClickEventArgs(Point P)
        {
            ClickPoint = P;
        }
    }
    public delegate void SelectRectangleHandler(object sender, SelectRectangleArgs e);
    public class SelectRectangleArgs : EventArgs
    {
        public Rect RectangleSelected { get; set; }
        public SelectRectangleArgs(Rect R)
        {
            RectangleSelected = R;
        }
    }
}
