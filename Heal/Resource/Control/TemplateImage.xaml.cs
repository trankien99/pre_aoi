﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Heal.Resource.Control
{
    /// <summary>
    /// Interaction logic for TemplateImage.xaml
    /// </summary>
    public partial class TemplateImage : UserControl
    {
        public int Index { get; set; }
        public static readonly DependencyProperty SourceProperty = DependencyProperty.Register("Source", typeof(BitmapSource), typeof(TemplateImage));
        public BitmapSource Source
        {
            get { return (BitmapSource)GetValue(SourceProperty); }
            set
            {
                SetValue(SourceProperty, value);
            }
        }
        public TemplateImage()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        public event DeleteTemplateHandler DeleteButtonClicked;
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            DeleteButtonClicked?.Invoke(this, new DeleteTemplateHandlerArgs(Index));
        }
    }
    public delegate void DeleteTemplateHandler(object sender, DeleteTemplateHandlerArgs e);
    public class DeleteTemplateHandlerArgs : EventArgs
    {
        public int Index { get; set; }
        public DeleteTemplateHandlerArgs(int index)
        {
            this.Index = index;
        }
        public DeleteTemplateHandlerArgs()
        {
        }
    }

}
