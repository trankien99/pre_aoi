﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Heal.Models;
using Heal.Models.Alg;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using Heal.Resource.Control.Alg;

namespace Heal.Resource.Control
{
    /// <summary>
    /// Interaction logic for LibraryProperties.xaml
    /// </summary>
    public partial class LibraryProperties : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty AlarmTypeProperty = DependencyProperty.Register("AAlarmType", typeof(AlarmType), typeof(MyProperty));
        public AlarmType AAlarmType
        {
            get { return (AlarmType)GetValue(AlarmTypeProperty); }
            set {
                SetValue(AlarmTypeProperty, value);
            }
        }
        public static readonly DependencyProperty AlarmOffsetXProperty = DependencyProperty.Register("AAlarmOffsetX", typeof(int), typeof(MyProperty));
        public int AAlarmOffsetX
        {
            get { return (int)GetValue(AlarmOffsetXProperty); }
            set 
            { 
                SetValue(AlarmOffsetXProperty, value); 
                OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty AlarmOffsetYProperty = DependencyProperty.Register("AAlarmOffsetY", typeof(int), typeof(MyProperty));
        public int AAlarmOffsetY
        {
            get { return (int)GetValue(AlarmOffsetYProperty); }
            set { 
                SetValue(AlarmOffsetYProperty, value); 
                OnAlgPropertyChanged(); 
            }
        }
        public static readonly DependencyProperty AlarmWidthProperty = DependencyProperty.Register("AAlarmWidth", typeof(int), typeof(MyProperty));
        public int AAlarmWidth
        {
            get { return (int)GetValue(AlarmWidthProperty); }
            set 
            { 
                SetValue(AlarmWidthProperty, value); 
                OnAlgPropertyChanged(); 
            }
        }
        public static readonly DependencyProperty AlarmHeightProperty = DependencyProperty.Register("AAlarmHeight", typeof(int), typeof(MyProperty));
        public int AAlarmHeight
        {
            get { return (int)GetValue(AlarmHeightProperty); }
            set 
            { 
                SetValue(AlarmHeightProperty, value); 
                OnAlgPropertyChanged(); 
            }
        }
        public static readonly DependencyProperty AlarmAlgProperty = DependencyProperty.Register("AAlarmAlg", typeof(MyAlgorithm), typeof(MyProperty));
        public MyAlgorithm AAlarmAlg
        {
            get { return (MyAlgorithm)GetValue(AlarmAlgProperty); }
            set { SetValue(AlarmAlgProperty, value); }
        }
        public ObservableCollection<MyAlgorithm> MyAlgorithms { get; set; } = new ObservableCollection<MyAlgorithm>();
        public event AlgSelectChangedHandler AlgSelectChanged;
        public event AlgPropertyChangedHandler AlgPropertyChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        public LibraryProperties()
        {
            InitializeComponent();
            this.DataContext = this;
            LoadUI();
        }
        public void OnPropertyChanged(string Name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Name));
        }
        public void OnAlgPropertyChanged()
        {
            AlgPropertyChanged?.Invoke(this, new EventArgs());
        }
        public void SetParameter(MyAlarm Alarm)
        {
            BindingOperations.ClearBinding(cbAlgorithm, ComboBox.SelectedItemProperty);
            LoadUI();
            // ---- Type ---
            string[] path = new string[] { "AlarmType", "OffsetX" ,"OffsetY", "Width", "Height" , "Alg" };
            DependencyProperty[] dp = new DependencyProperty[] { AlarmTypeProperty , AlarmOffsetXProperty , AlarmOffsetYProperty , AlarmWidthProperty , AlarmHeightProperty , AlarmAlgProperty };
            for (int i = 0; i < path.Length; i++)
            {
                Binding bin = new Binding(path[i]);
                bin.Mode = BindingMode.TwoWay;
                bin.Source = Alarm;
                this.SetBinding(dp[i], bin);
            }
            Binding binAlg2 = new Binding("AAlarmAlg");
            binAlg2.Mode = BindingMode.TwoWay;
            binAlg2.Source = this;

            if (Alarm.Alg != null)
            {
                for (int i = 0; i < MyAlgorithms.Count; i++)
                {
                    if (MyAlgorithms[i].Name == Alarm.Alg.Name)
                    {
                        MyAlgorithms[i].Dispose();
                        MyAlgorithms[i] = Alarm.Alg;
                        break;
                    }
                }
            }
            cbAlgorithm.SetBinding(ComboBox.SelectedItemProperty, binAlg2);
            OnPropertyChanged();
        }
        private void LoadUI()
        {
            string[] alarmType = Models.EmumControl.GetAlarmType();
            cbType.ItemsSource = alarmType;
            Type[] typesAlgorithm = MyAlgorithm.GetAllAlgorithmType();
            foreach (var item in MyAlgorithms)
            {
                if (item != null)
                {
                    item.Dispose();
                }
            }
            MyAlgorithms.Clear();
            foreach (var algorithm in typesAlgorithm)
            {
                MyAlgorithm instance = MyAlgorithm.GetInstance(algorithm);
                if (instance != null)
                {
                    if(!string.IsNullOrEmpty(instance.Name))
                        MyAlgorithms.Add(instance);
                }
            }
            MyAlgorithms.Reverse();
        }
        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                Resource.Convertor.Action.UpdateBindingSourceTextBox(sender);
        }
        private void TextBox_0dot2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Convertor.Action.Change0dot2(sender, e.Delta);
        }
        private void TextBox_0dot2_PreviewMouseWheel_2(object sender, MouseWheelEventArgs e)
        {
            Convertor.Action.Change0dot2(sender, e.Delta, true);
        }
        private void cbAlgorithm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AlgSelectChanged?.Invoke(this, new AlgSelectChangedArgs(AAlarmAlg));
        }
        private void btGoOriginH_Click(object sender, RoutedEventArgs e)
        {
            AAlarmOffsetX = 0;
            OnPropertyChanged();
        }
        private void btGoOriginV_Click(object sender, RoutedEventArgs e)
        {
            AAlarmOffsetY = 0; 
            OnPropertyChanged();
        }

    }
    public delegate void AlgSelectChangedHandler(object sender, AlgSelectChangedArgs e);
    public class AlgSelectChangedArgs : EventArgs
    {
        public MyAlgorithm Item { get; set; }
        public AlgSelectChangedArgs(MyAlgorithm item)
        {
            this.Item = item;
        }
    }
}
