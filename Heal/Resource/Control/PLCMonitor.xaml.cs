﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Heal.Resource.Control
{
    /// <summary>
    /// Interaction logic for PLCMonitor.xaml
    /// </summary>
    public partial class PLCMonitor : UserControl, INotifyPropertyChanged   
    {
        private Point _CurrentPoint { get; set; }
        public event EventHandler StartButtonClick;
        public event EventHandler StopButtonClick;
        public Point CurrentPoint
        {
            get => _CurrentPoint;
            set
            {
                _CurrentPoint = value;
                OnPropertyChanged();
                UpdateBoardLayout();
            }
        }
        private Size _BoardSize { get; set; }
        public Size BoardSize
        {
            get => _BoardSize;
            set
            {
                _BoardSize = value;
                OnPropertyChanged();
                UpdateBoardLayout();
            }
        }
        public bool HasPanel { get; set; }
        public Visibility ShowPanel
        {
            get => HasPanel ? Visibility.Visible : Visibility.Hidden;
        }
        private Properties.Settings _Param = Properties.Settings.Default;
        public PLCMonitor()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
        }
        private void UpdateBoardLayout()
        {
            double w = canvas.ActualWidth - imCamera.ActualWidth / 2;
            double h = canvas.ActualHeight - 2 * imCamera.ActualHeight /  3;
            this.Dispatcher.Invoke(() => {
                boardRect.Width = BoardSize.Width * w / 400;
                boardRect.Height = BoardSize.Height * h / 400;
                Canvas.SetLeft(imCamera, CurrentPoint.X * w / (400 * 100));
                Canvas.SetTop(imCamera, CurrentPoint.Y * h / (400 * 100));
            });
        }
        private void btAlarm_Click(object sender, RoutedEventArgs e)
        {
            Heal.View.Tool.GuidingWindow guiding = null;
            foreach (Window win in Application.Current.Windows)
            {
                string windowType = win.GetType().ToString();
                if (windowType.Equals("Heal.View.Tool.GuidingWindow"))
                {
                    guiding = win as Heal.View.Tool.GuidingWindow;
                }
            }
            if (guiding == null)
            {
                guiding = new View.Tool.GuidingWindow();
            }
            guiding.ShowDialog();
        }
        private void btStart_Click(object sender, RoutedEventArgs e)
        {
            StartButtonClick?.Invoke(sender, new EventArgs());
        }
        private void btStop_Click(object sender, RoutedEventArgs e)
        {
            StopButtonClick?.Invoke(sender, new EventArgs());
        }
    }
}
