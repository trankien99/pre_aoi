﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Heal.Models;

namespace Heal.Resource.Control
{
    /// <summary>
    /// Interaction logic for CatalogProperties.xaml
    /// </summary>
    public partial class CatalogProperties : UserControl
    {
        public static readonly DependencyProperty CatalogTypeProperty = DependencyProperty.Register("CatalogType", typeof(int), typeof(CatalogProperties));
        public int CatalogType
        {
            get { return (int)GetValue(CatalogTypeProperty); }
            set { SetValue(CatalogTypeProperty, value); }
        }
        public static readonly DependencyProperty CatalogNameProperty = DependencyProperty.Register("CatalogName", typeof(string), typeof(CatalogProperties));
        public string CatalogName
        {
            get { return (string)GetValue(CatalogNameProperty); }
            set { SetValue(CatalogNameProperty, value); }
        }

        public static readonly DependencyProperty CatalogDescriptionProperty = DependencyProperty.Register("CatalogDescription", typeof(string), typeof(CatalogProperties));
        public string CatalogDescription
        {
            get { return (string)GetValue(CatalogDescriptionProperty); }
            set { SetValue(CatalogDescriptionProperty, value); }
        }

        public static readonly DependencyProperty CatalogWidthProperty = DependencyProperty.Register("CatalogWidth", typeof(double), typeof(CatalogProperties));
        public double CatalogWidth
        {
            get { return (double)GetValue(CatalogWidthProperty); }
            set { SetValue(CatalogWidthProperty, value); }
        }

        public static readonly DependencyProperty CatalogHeightProperty = DependencyProperty.Register("CatalogHeight", typeof(double), typeof(CatalogProperties));
        public double CatalogHeight
        {
            get { return (double)GetValue(CatalogHeightProperty); }
            set { SetValue(CatalogHeightProperty, value); }
        }
        public CatalogProperties()
        {
            InitializeComponent();
            string[] types = Models.EmumControl.GetCatalogType();
            foreach (var item in types)
            {
                cbType.Items.Add(item);
            }
            this.DataContext = this;
        }
        public void SetParam(Catalog catalog)
        {
            // ---- Name -----
            Binding binName = new Binding("Name");
            binName.Source = catalog;
            binName.Mode = BindingMode.TwoWay;
            this.SetBinding(CatalogProperties.CatalogNameProperty, binName);
            //--- Type --- 
            Binding binType = new Binding("Type");
            binType.Source = catalog;
            binType.Mode = BindingMode.TwoWay;
            binType.Converter = new Resource.Convertor.CatalogType2Int();
            this.SetBinding(CatalogProperties.CatalogTypeProperty, binType);
            //----Width---- -
            Binding binWidth = new Binding("Width");
            binWidth.Source = catalog;
            binWidth.Mode = BindingMode.TwoWay;
            binWidth.Converter = new Resource.Convertor.Pixel2Millimeter();
            this.SetBinding(CatalogProperties.CatalogWidthProperty, binWidth);
            // ---- Height -----
            Binding binHeight = new Binding("Height");
            binHeight.Source = catalog;
            binHeight.Mode = BindingMode.TwoWay;
            binHeight.Converter = new Resource.Convertor.Pixel2Millimeter();
            this.SetBinding(CatalogProperties.CatalogHeightProperty, binHeight);
            // ---- Description -----
            Binding binDescription = new Binding("Description");
            binDescription.Source = catalog;
            binDescription.Mode = BindingMode.TwoWay;
            this.SetBinding(CatalogProperties.CatalogDescriptionProperty, binDescription);
        }
        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                Resource.Convertor.Action.UpdateBindingSourceTextBox(sender);
            }
        }

        private void TextBox_0dot2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Convertor.Action.Change0dot2(sender, e.Delta);
        }
    }
}
