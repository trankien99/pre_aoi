﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.ComponentModel;

namespace Heal.Resource.Control
{
    /// <summary>
    /// Interaction logic for PreviewRect.xaml
    /// </summary>
    public partial class PreviewRect : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty IAngleProperty = DependencyProperty.Register("IAngle", typeof(double), typeof(PreviewRect));
        public double IAngle
        {
            get { return (double)GetValue(IAngleProperty); }
            set { SetValue(IAngleProperty, value); }
        }
        public static readonly DependencyProperty ISourceProperty = DependencyProperty.Register("ISource", typeof(BitmapSource), typeof(PreviewRect));
        public BitmapSource ISource
        {
            get { return (BitmapSource)GetValue(ISourceProperty); }
            set { SetValue(ISourceProperty, value); }
        }
        public static readonly DependencyProperty ResizeWidthProperty = DependencyProperty.Register("ResizeWidth", typeof(double), typeof(PreviewRect));
        public double ResizeWidth
        {
            get { return (double)GetValue(ResizeWidthProperty); }
            set { SetValue(ResizeWidthProperty, value); }
        }
        public static readonly DependencyProperty MoveWidthProperty = DependencyProperty.Register("MoveWidth", typeof(double), typeof(PreviewRect));
        public double MoveWidth
        {
            get { return (double)GetValue(MoveWidthProperty); }
            set { SetValue(MoveWidthProperty, value); }
        }
        public static readonly DependencyProperty ResizeHeightProperty = DependencyProperty.Register("ResizeHeight", typeof(double), typeof(PreviewRect));
        public double ResizeHeight
        {
            get { return (double)GetValue(ResizeHeightProperty); }
            set { SetValue(ResizeHeightProperty, value); }
        }
        public static readonly DependencyProperty LocationProperty = DependencyProperty.Register("ILocation", typeof(Point), typeof(PreviewRect));
        public Point ILocation
        {
            get { return (Point)GetValue(LocationProperty); }
            set { SetValue(LocationProperty, value); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
        }
        public event MoveRectEventHandler MoveRectMouseDown;
        public event AdjustRectEventHandler AdjustRectMouseDown;
        public PreviewRect()
        {
            InitializeComponent(); 
            ResizeWidth = 5;
            ResizeHeight = 5;
            MoveWidth = 3;
            this.DataContext = this;
        }
        public void SetVerSize(double Scale)
        {
            ResizeWidth = 5 / Scale;
            ResizeHeight = 5 / Scale;
            MoveWidth = 3 / Scale;
        }

        private void Rectangle_PreviewMouseLeftDownMove(object sender, MouseButtonEventArgs e)
        {
            MoveRectMouseDown?.Invoke(this, new EventArgs());
            e.Handled = true;
        }

        private void topleft_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Rectangle r = sender as Rectangle; 
            switch (r.Name)
            {
                case "topleft":
                    AdjustRectMouseDown?.Invoke(this, new AdjustRectArgs(Corner.TopLeft));
                    break;
                case "topright":
                    AdjustRectMouseDown?.Invoke(this, new AdjustRectArgs(Corner.TopRight));
                    break;
                case "botleft":
                    AdjustRectMouseDown?.Invoke(this, new AdjustRectArgs(Corner.BotLeft));
                    break;
                case "botright":
                    AdjustRectMouseDown?.Invoke(this, new AdjustRectArgs(Corner.BotRight));
                    break;
                default:
                    break;
            }
            e.Handled = true;
        }
    }
    public class CanvasMarginCvt : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToDouble(values[1] ) / 2 - System.Convert.ToDouble(values[0] ) / 2;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public delegate void MoveRectEventHandler(object sender, EventArgs e);
    public delegate void AdjustRectEventHandler(object sender, AdjustRectArgs e);
    public class AdjustRectArgs : EventArgs
    {
        public Corner Corner { get; set; }
        public AdjustRectArgs(Corner c)
        {
            this.Corner = c;
        }
    }
    public enum Corner
    {
        TopLeft,
        TopRight,
        BotLeft,
        BotRight
    }
}
