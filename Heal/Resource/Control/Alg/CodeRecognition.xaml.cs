﻿using Heal.Models.Alg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Heal.Models;

namespace Heal.Resource.Control.Alg
{
    /// <summary>
    /// Interaction logic for CodeRecognition.xaml
    /// </summary>
    public partial class CodeRecognition : UserControl, IAlgProperties
    {
        public static readonly DependencyProperty CCodeFormatProperty = DependencyProperty.Register("CCodeFormat", typeof(CodeFormat), typeof(MyProperty));
        public CodeFormat CCodeFormat
        {
            get { return (CodeFormat)GetValue(CCodeFormatProperty); }
            set { SetValue(CCodeFormatProperty, value);
                OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty CResultProperty = DependencyProperty.Register("CResult", typeof(AlgResult), typeof(MyProperty));
        public AlgResult CResult
        {
            get { return (AlgResult)GetValue(CResultProperty); }
            set
            {
                SetValue(CResultProperty, value);
            }
        }
        public CodeRecognition()
        {
            InitializeComponent();
            LoadUI();
            this.DataContext = this;
        }
        public void LoadUI()
        {
            var types = Models.EmumControl.GetBarcodeTypes();
            cbType.ItemsSource = types;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public event AlgPropertyChangedHandler AlgPropertyChanged;
        public event UpdateSpHelpChangedHandler UpdateSpHelpChanged;
        public void OnPropertyChanged(string Name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Name));
        }
        public void OnAlgPropertyChanged()
        {
            AlgPropertyChanged?.Invoke(this, new EventArgs());
            OnPropertyChanged();
        }
        public string[] GetSupporter()
        {
            return new string[2];
        }
        public void SetParameter(MyImage image, SMD smd, MyAlarm alarm)
        {
            if (alarm.Alg is Models.Alg.CodeRecognition)
            {
                string[] paths = new string[] { "Format", "Result" };
                DependencyProperty[] dps = new DependencyProperty[] { CCodeFormatProperty, CResultProperty };
                for (int i = 0; i < paths.Length; i++)
                {
                    Binding bin = new Binding(paths[i]);
                    bin.Source = alarm.Alg;
                    bin.Mode = BindingMode.TwoWay;
                    this.SetBinding(dps[i], bin);
                }
                OnPropertyChanged();
            }
        }
        //public void SetParameter(MyAlgorithm algorithm)
        //{
        //    Binding binding = new Binding("Format");
        //    binding.Source = algorithm;
        //    binding.Mode = BindingMode.TwoWay;
        //    this.SetBinding(CCodeFormatProperty, binding);
        //    var alg = (algorithm as Models.Alg.CodeRecognition);
        //    Binding bincontent = new Binding("Result");
        //    bincontent.Source = algorithm;
        //    bincontent.Mode = BindingMode.TwoWay;
        //    this.SetBinding(CResultProperty, bincontent);
        //}
        public string GetAlgName()
        {
            return "Code Recognition";
        }
    }
}
