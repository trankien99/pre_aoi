﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heal.Models.Alg;
using System.ComponentModel;
using System.Windows.Controls;
using Heal.Models;

namespace Heal.Resource.Control.Alg
{
    public class MyProperty : UserControl
    {

    }
    public interface IAlgProperties : INotifyPropertyChanged
    {
        event AlgPropertyChangedHandler AlgPropertyChanged;
        event UpdateSpHelpChangedHandler UpdateSpHelpChanged;
        void OnPropertyChanged(string Name = null);
        void SetParameter(MyImage image, SMD smd, MyAlarm alarm);
        string GetAlgName();
        string[] GetSupporter();
    }
    public delegate void AlgPropertyChangedHandler(object sender, EventArgs e);
    public delegate void UpdateSpHelpChangedHandler(object sender, EventArgs e);
}
