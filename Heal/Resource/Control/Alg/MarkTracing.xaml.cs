﻿using Heal.Models.Alg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Heal.Models;

namespace Heal.Resource.Control.Alg
{
    /// <summary>
    /// Interaction logic for MarkTracing.xaml
    /// </summary>
    public partial class MarkTracing : UserControl, IAlgProperties
    {
        public static readonly DependencyProperty MShapeProperty = DependencyProperty.Register("MShape", typeof(MarkShape), typeof(MyProperty));
        public MarkShape MShape
        {
            get { return (MarkShape)GetValue(MShapeProperty); }
            set { SetValue(MShapeProperty, value); }
        }
        public static readonly DependencyProperty MRadiusProperty = DependencyProperty.Register("MRadius", typeof(double), typeof(MyProperty));
        public double MRadius
        {
            get { return (double)GetValue(MRadiusProperty); }
            set { SetValue(MRadiusProperty, value);
                OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty MLowThreshProperty = DependencyProperty.Register("MLowThresh", typeof(double), typeof(MyProperty));
        public double MLowThresh
        {
            get { return (double)GetValue(MLowThreshProperty); }
            set { SetValue(MLowThreshProperty, value);
                //OnPropertyChanged();
                OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty MUpThreshProperty = DependencyProperty.Register("MUpThresh", typeof(double), typeof(MyProperty));
        public double MUpThresh
        {
            get { return (double)GetValue(MUpThreshProperty); }
            set { SetValue(MUpThreshProperty, value);
                //OnPropertyChanged();
                OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty MLowOKProperty = DependencyProperty.Register("MLowOK", typeof(double), typeof(MyProperty));
        public double MLowOK
        {
            get { return (double)GetValue(MLowOKProperty); }
            set { SetValue(MLowOKProperty, value);
                //OnPropertyChanged();
                OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty MUpOKProperty = DependencyProperty.Register("MUpOK", typeof(double), typeof(MyProperty));
        public double MUpOK
        {
            get { return (double)GetValue(MUpOKProperty); }
            set { SetValue(MUpOKProperty, value);
                //OnPropertyChanged();
                OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty MScoreProperty = DependencyProperty.Register("MScore", typeof(double), typeof(MyProperty));
        public double MScore
        {
            get { return (double)GetValue(MScoreProperty); }
            set { SetValue(MScoreProperty, value); }
        }
        
        public MarkTracing()
        {
            InitializeComponent();
            string[] markShapes = Heal.Models.EmumControl.GetFullMarkShape();
            cbShape.ItemsSource = markShapes;
            this.DataContext = this;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public event AlgPropertyChangedHandler AlgPropertyChanged;
        public event UpdateSpHelpChangedHandler UpdateSpHelpChanged;
        public void OnAlgPropertyChanged()
        {
            AlgPropertyChanged?.Invoke(this, new EventArgs());
            OnPropertyChanged();
        }
        public void SetParameter(MyImage image, SMD smd, MyAlarm alarm)
        {
            if (alarm.Alg is Models.Alg.MarkTracing)
            {
                string[] paths = new string[] { "Shape", "Radius", "Threshold.Lower", "Threshold.Upper", "OKRange.Lower", "OKRange.Upper", "Result.Score" };
                DependencyProperty[] dps = new DependencyProperty[] { MShapeProperty, MRadiusProperty, MLowThreshProperty, MUpThreshProperty, MLowOKProperty, MUpOKProperty, MScoreProperty };
                for (int i = 0; i < paths.Length; i++)
                {
                    Binding bin = new Binding(paths[i]);
                    bin.Source = alarm.Alg;
                    bin.Mode = BindingMode.TwoWay;
                    this.SetBinding(dps[i], bin);
                }
                OnPropertyChanged();
            }
        }
        public string GetAlgName()
        {
            return "Mark Tracing";
        }
        public string[] GetSupporter()
        {
            return new string[2];
        }
        public void OnPropertyChanged(string Name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Name));
        }
        public void LoadUI()
        {

        }
        private void TextBox_0dot2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Resource.Convertor.Action.Change0dot2(sender, e.Delta);
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                Resource.Convertor.Action.UpdateBindingSourceTextBox(sender);
        }

        private void TextBox_2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Resource.Convertor.Action.Change2(sender, e.Delta);
        }

        private void TextBox_0dot05_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Resource.Convertor.Action.Change0dot5(sender, e.Delta);
        }
    }
    
}
