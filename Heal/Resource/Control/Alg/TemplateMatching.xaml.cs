﻿using Heal.Models;
using Heal.Models.Alg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Emgu.CV;
using draw = System.Drawing;
using Emgu.CV.Structure;

namespace Heal.Resource.Control.Alg
{
    /// <summary>
    /// Interaction logic for TemplateMatching.xaml
    /// </summary>
    public partial class TemplateMatching : UserControl, IAlgProperties
    {
        public static readonly DependencyProperty TMPOKRangeUpperProperty = DependencyProperty.Register("TMPOKRangeUpper", typeof(double), typeof(MyProperty));
        public double OKRangeUpper
        {
            get { return (double)GetValue(TMPOKRangeUpperProperty); }
            set
            {
                SetValue(TMPOKRangeUpperProperty, value);
            }
        }
        public static readonly DependencyProperty TMPOKRangeLowerProperty = DependencyProperty.Register("TMPOKRangeLower", typeof(double), typeof(MyProperty));
        public double OKRangeLower
        {
            get { return (double)GetValue(TMPOKRangeLowerProperty); }
            set
            {
                SetValue(TMPOKRangeLowerProperty, value);
            }
        }

        public static readonly DependencyProperty TMPMaxOffsetXProperty = DependencyProperty.Register("TMPMaxOffsetX", typeof(int), typeof(MyProperty));
        public int TMPMaxOffsetX
        {
            get { return (int)GetValue(TMPMaxOffsetXProperty); }
            set
            {
                SetValue(TMPMaxOffsetXProperty, value);
            }
        }
        public static readonly DependencyProperty TMPMaxOffsetYProperty = DependencyProperty.Register("TMPMaxOffsetY", typeof(int), typeof(MyProperty));
        public int TMPMaxOffsetY
        {
            get { return (int)GetValue(TMPMaxOffsetYProperty); }
            set
            {
                SetValue(TMPMaxOffsetYProperty, value);
            }
        }

        public static readonly DependencyProperty TMPResultProperty = DependencyProperty.Register("TMPResult", typeof(AlgResult), typeof(MyProperty));
        public AlgResult Result
        {
            get { return (AlgResult)GetValue(TMPResultProperty); }
            set
            {
                SetValue(TMPResultProperty, value);
            }
        }
        public static readonly DependencyProperty TMPReverseProperty = DependencyProperty.Register("TMPReverse", typeof(bool), typeof(MyProperty));
        public bool Reverse
        {
            get { return (bool)GetValue(TMPReverseProperty); }
            set
            {
                SetValue(TMPReverseProperty, value);
            }
        }

        public static readonly DependencyProperty TMPTemplatesProperty = DependencyProperty.Register("TMPTemplates", typeof(ObservableCollection<Image<Bgr, byte>>), typeof(MyProperty));
        public ObservableCollection<Image<Bgr, byte>> Templates
        {
            get { return (ObservableCollection<Image<Bgr, byte>>)GetValue(TMPTemplatesProperty); }
            set
            {
                SetValue(TMPTemplatesProperty, value);
            }
        }
        public event AlgPropertyChangedHandler AlgPropertyChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        public event UpdateSpHelpChangedHandler UpdateSpHelpChanged;
        private MyImage _MyImage { get; set; }
        private SMD _SMD { get; set; }
        private MyAlarm _MyAlarm { get; set; }
        private Models.Alg.TemplateMatching _MyAlgorithm { get; set; }
        public TemplateMatching()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        public void OnAlgPropertyChanged(string Name = null)
        {
            AlgPropertyChanged?.Invoke(this, new EventArgs());
            OnPropertyChanged();
        }
        public void OnPropertyChanged(string Name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Name));
        }
        public string[] GetSupporter()
        {
            return new string[2] 
            {
                "Heal.Resource.Control.Alg.SP.TemplateManagement", 
                null
            };
        }
        public void SetParameter(MyImage image, SMD smd, MyAlarm alarm)
        {
            if (alarm.Alg is Models.Alg.TemplateMatching)
            {
                _MyImage = image;
                _SMD = smd;
                _MyAlarm = alarm;
                string[] paths = new string[] { "OKRange.Upper", "OKRange.Lower", "ReverseTemplate", "Result", "Templates", "MaxOffsetX", "MaxOffsetY" };
                DependencyProperty[] dps = new DependencyProperty[] { TMPOKRangeUpperProperty, TMPOKRangeLowerProperty, TMPReverseProperty, TMPResultProperty, TMPTemplatesProperty, TMPMaxOffsetXProperty, TMPMaxOffsetYProperty };
                for (int i = 0; i < paths.Length; i++)
                {
                    Binding bin = new Binding(paths[i]);
                    bin.Source = _MyAlarm.Alg;
                    bin.Mode = BindingMode.TwoWay;
                    this.SetBinding(dps[i], bin);
                }
                OnPropertyChanged();
            }
        }

        public string GetAlgName()
        {
            return "Template Matching";
        }
        private void TextBox_2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Resource.Convertor.Action.Change(sender, e.Delta, 5, false);
        }
        private void TextBox_0dot2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Resource.Convertor.Action.Change(sender, e.Delta, 0.2, false);
        }
        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                Resource.Convertor.Action.UpdateBindingSourceTextBox(sender);
        }

        private void btUpdateTemplate_Click(object sender, RoutedEventArgs e)
        {
            draw.Point loc = new draw.Point(_SMD.X, _SMD.Y);
            int width = Convert.ToInt32(_MyAlarm.Width);
            int height = Convert.ToInt32(_MyAlarm.Height);
            int offsetX = Convert.ToInt32(_MyAlarm.OffsetX);
            int offsetY = Convert.ToInt32(_MyAlarm.OffsetY);
            double angle = _SMD.Angle;
            using (Image<Bgr, byte> img = Models.Utils.GetSMDImage(_MyImage, loc, width, height, offsetX, offsetY, angle))
            {
                View.Setup.UpdateTemplateWindow window = new View.Setup.UpdateTemplateWindow();
                window.SetSource(img);
                window.ShowDialog();
                if(window.TemplateImage  != null)
                {
                    Templates.Add(window.TemplateImage.Copy());
                    UpdateSpHelpChanged?.Invoke(this, new EventArgs());
                    OnAlgPropertyChanged();
                }
                window.Dispose();
            }
        }
    }
}
