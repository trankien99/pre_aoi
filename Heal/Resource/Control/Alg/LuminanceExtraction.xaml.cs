﻿using Heal.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Heal.Resource.Control.Alg
{
    /// <summary>
    /// Interaction logic for LuminanceExtraction.xaml
    /// </summary>
    public partial class LuminanceExtraction : UserControl, IAlgProperties
    {
        public static readonly DependencyProperty LEThresholdLowerProperty = DependencyProperty.Register("LEThresholdLower", typeof(double), typeof(MyProperty));
        public double LEThresholdLower
        {
            get { return (double)GetValue(LEThresholdLowerProperty); }
            set { SetValue(LEThresholdLowerProperty, value); OnAlgPropertyChanged(); }
        }
        public static readonly DependencyProperty LEThresholdUpperProperty = DependencyProperty.Register("LEThresholdUpper", typeof(double), typeof(MyProperty));
        public double LEThresholdUpper
        {
            get { return (double)GetValue(LEThresholdUpperProperty); }
            set { SetValue(LEThresholdUpperProperty, value); OnAlgPropertyChanged(); }
        }
        public static readonly DependencyProperty LEOKRangeLowerProperty = DependencyProperty.Register("LEOKRangeLower", typeof(double), typeof(MyProperty));
        public double LEOKRangeLower
        {
            get { return (double)GetValue(LEOKRangeLowerProperty); }
            set
            {
                SetValue(LEOKRangeLowerProperty, value);
                OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty LEOKRangeUpperProperty = DependencyProperty.Register("LEOKRangeUpper", typeof(double), typeof(MyProperty));
        public double LEOKRangeUpper
        {
            get { return (double)GetValue(LEOKRangeUpperProperty); }
            set
            {
                SetValue(LEOKRangeUpperProperty, value);
                OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty LEScoreProperty = DependencyProperty.Register("LEScore", typeof(double), typeof(MyProperty));
        public double LEScore
        {
            get { return (double)GetValue(LEScoreProperty); }
            set { SetValue(LEScoreProperty, value); }
        }
        public event AlgPropertyChangedHandler AlgPropertyChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        public event UpdateSpHelpChangedHandler UpdateSpHelpChanged;

        public LuminanceExtraction()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        public string GetAlgName()
        {
            return "Luminance Extraction";
        }
        public string[] GetSupporter()
        {
            return new string[2];
        }
        public void OnAlgPropertyChanged()
        {
            AlgPropertyChanged?.Invoke(this, new EventArgs());
            OnPropertyChanged();
        }
        public void OnPropertyChanged(string Name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Name));
        }

        public void SetParameter(MyImage image, SMD smd, MyAlarm alarm)
        {
            if (alarm.Alg is Models.Alg.LuminanceExtraction)
            {
                string[] paths = new string[] { "Threshold.Lower", "Threshold.Upper", "OKRange.Lower", "OKRange.Upper", "Result.Score" };
                DependencyProperty[] dps = new DependencyProperty[] { LEThresholdLowerProperty, LEThresholdUpperProperty, LEOKRangeLowerProperty, LEOKRangeUpperProperty, LEScoreProperty };
                for (int i = 0; i < paths.Length; i++)
                {
                    Binding bin = new Binding(paths[i]);
                    bin.Source = alarm.Alg;
                    bin.Mode = BindingMode.TwoWay;
                    this.SetBinding(dps[i], bin);
                }
                OnPropertyChanged();
            }
        }

        private void TextBox_2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Resource.Convertor.Action.Change2(sender, e.Delta);
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                Resource.Convertor.Action.UpdateBindingSourceTextBox(sender);
        }
    }
}
