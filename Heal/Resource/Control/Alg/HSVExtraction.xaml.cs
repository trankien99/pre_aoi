﻿using Heal.Models.Alg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Heal.Models;

namespace Heal.Resource.Control.Alg
{
    /// <summary>
    /// Interaction logic for HSVExtraction.xaml
    /// </summary>
    public partial class HSVExtraction : UserControl, IAlgProperties
    {
        public double Maximum255 { get; set; } = 255;
        public double Maximum179 { get; set; } = 179;
        public double Maximum100 { get; set; } = 100;
        public static readonly DependencyProperty HueUpperProperty = DependencyProperty.Register("HSVHueUpper", typeof(double), typeof(MyProperty));
        public double HueUpper
        {
            get { return (double)GetValue(HueUpperProperty); }
            set { SetValue(HueUpperProperty, value); 
                OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty HueLowerProperty = DependencyProperty.Register("HSVHueLower", typeof(double), typeof(MyProperty));
        public double HueLower
        {
            get { return (double)GetValue(HueLowerProperty); }
            set
            {
                SetValue(HueLowerProperty, value); OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty SaturationUpperProperty = DependencyProperty.Register("HSVSaturationUpper", typeof(double), typeof(MyProperty));
        public double SaturationUpper
        {
            get { return (double)GetValue(SaturationUpperProperty); }
            set
            {
                SetValue(SaturationUpperProperty, value); OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty SaturationLowerProperty = DependencyProperty.Register("HSVSaturationLower", typeof(double), typeof(MyProperty));
        public double SaturationLower
        {
            get { return (double)GetValue(SaturationLowerProperty); }
            set
            {
                SetValue(SaturationLowerProperty, value); OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty ValueUpperProperty = DependencyProperty.Register("HSVValueUpper", typeof(double), typeof(MyProperty));
        public double ValueUpper
        {
            get { return (double)GetValue(ValueUpperProperty); }
            set
            {
                SetValue(ValueUpperProperty, value); OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty ValueLowerProperty = DependencyProperty.Register("HSVValueLower", typeof(double), typeof(MyProperty));
        public double ValueLower
        {
            get { return (double)GetValue(ValueLowerProperty); }
            set
            {
                SetValue(ValueLowerProperty, value); OnAlgPropertyChanged();
            }
        }
        public static readonly DependencyProperty HSVOKRangeUpperProperty = DependencyProperty.Register("HSVOKRangeUpper", typeof(double), typeof(MyProperty));
        public double OKRangeUpper
        {
            get { return (double)GetValue(HSVOKRangeUpperProperty); }
            set
            {
                SetValue(HSVOKRangeUpperProperty, value); OnPropertyChanged();
            }
        }
        public static readonly DependencyProperty HSVOKRangeLowerProperty = DependencyProperty.Register("HSVOKRangeLower", typeof(double), typeof(MyProperty));
        public double OKRangeLower
        {
            get { return (double)GetValue(HSVOKRangeLowerProperty); }
            set
            {
                SetValue(HSVOKRangeLowerProperty, value);
                OnPropertyChanged();
            }
        }
        public static readonly DependencyProperty HSVResultProperty = DependencyProperty.Register("HSVResult", typeof(AlgResult), typeof(MyProperty));
        public AlgResult HSVResult
        {
            get { return (AlgResult)GetValue(HSVResultProperty); }
            set
            {
                SetValue(HSVResultProperty, value);
            }
        }
        
        public HSVExtraction()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        public event AlgPropertyChangedHandler AlgPropertyChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        public event UpdateSpHelpChangedHandler UpdateSpHelpChanged;

        public string GetAlgName()
        {
            return "HSV Extraction";
        }
        public string[] GetSupporter()
        {
            return new string[] 
            {
                null,
                null
            };
        }
        public void OnPropertyChanged(string Name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Name));
        }
        public void OnAlgPropertyChanged()
        {
            AlgPropertyChanged?.Invoke(this, new EventArgs());
            OnPropertyChanged(null);
        }
        public void SetParameter(MyImage image, SMD smd, MyAlarm alarm)
        {
            if(alarm.Alg is Models.Alg.HSVExtraction)
            {
                string[] path = new string[] { "Hue.Upper", "Hue.Lower", "Saturation.Upper", "Saturation.Lower", "Value.Upper", "Value.Lower", "OKRange.Upper", "OKRange.Lower", "Result" };
                DependencyProperty[] dp = new DependencyProperty[] { HueUpperProperty, HueLowerProperty, SaturationUpperProperty,
                SaturationLowerProperty, ValueUpperProperty, ValueLowerProperty, HSVOKRangeUpperProperty, HSVOKRangeLowerProperty, HSVResultProperty };
                for (int i = 0; i < path.Length; i++)
                {
                    Binding bin = new Binding(path[i]);
                    bin.Mode = BindingMode.TwoWay;
                    bin.Source = alarm.Alg;
                    this.SetBinding(dp[i], bin);
                }
                OnPropertyChanged();
            }
        }
        private void txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                Resource.Convertor.Action.UpdateBindingSourceTextBox(sender);
        }

        private void txt_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            Resource.Convertor.Action.Change5(sender, e.Delta);
        }
    }
}
