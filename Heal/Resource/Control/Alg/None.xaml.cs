﻿using Heal.Models;
using Heal.Models.Alg;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Heal.Resource.Control.Alg
{
    /// <summary>
    /// Interaction logic for None.xaml
    /// </summary>
    public partial class None : UserControl, IAlgProperties
    {
        public None()
        {
            InitializeComponent();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event AlgPropertyChangedHandler AlgPropertyChanged; 
        public event UpdateSpHelpChangedHandler UpdateSpHelpChanged;
        public void SetParameter(MyImage image, SMD smd, MyAlarm alarm)
        {
            //throw new NotImplementedException();
        }
        public void OnUpdateSpHelpChanged()
        {
            UpdateSpHelpChanged?.Invoke(this, new EventArgs());
        }
        public void OnAlgPropertyChanged()
        {
            AlgPropertyChanged?.Invoke(this, new EventArgs());
        }
        public void OnPropertyChanged(string Name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Name));
        }
        public string GetAlgName()
        {
            return "None";
        }
        public string[] GetSupporter()
        {
            return new string[2];
        }
    }
}
