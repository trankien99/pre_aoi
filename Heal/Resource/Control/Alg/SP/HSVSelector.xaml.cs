﻿using Heal.Models;
using Heal.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace Heal.Resource.Control.Alg.SP
{
    /// <summary>
    /// Interaction logic for HSVSelector.xaml
    /// </summary>
    public partial class HSVSelector : UserControl, IAlgProperties
    {
        public static readonly DependencyProperty HueUpperProperty = DependencyProperty.Register("HSVHueUpper", typeof(double), typeof(HSVSelector));
        public double HueUpper
        {
            get { return (double)GetValue(HueUpperProperty); }
            set
            {
                SetValue(HueUpperProperty, value);
            }
        }
        public static readonly DependencyProperty HueLowerProperty = DependencyProperty.Register("HSVHueLower", typeof(double), typeof(HSVSelector));
        public double HueLower
        {
            get { return (double)GetValue(HueLowerProperty); }
            set
            {
                SetValue(HueLowerProperty, value);
            }
        }
        public static readonly DependencyProperty SaturationUpperProperty = DependencyProperty.Register("HSVSaturationUpper", typeof(double), typeof(HSVSelector));
        public double SaturationUpper
        {
            get { return (double)GetValue(SaturationUpperProperty); }
            set
            {
                SetValue(SaturationUpperProperty, value);
            }
        }
        public static readonly DependencyProperty SaturationLowerProperty = DependencyProperty.Register("HSVSaturationLower", typeof(double), typeof(HSVSelector));

        public event PropertyChangedEventHandler PropertyChanged;
        public event AlgPropertyChangedHandler AlgPropertyChanged;
        public event UpdateSpHelpChangedHandler UpdateSpHelpChanged;
        private ButtonDown _ButtonDown { get; set; } = ButtonDown.Unknow;
        public double SaturationLower
        {
            get { return (double)GetValue(SaturationLowerProperty); }
            set
            {
                SetValue(SaturationLowerProperty, value);
            }
        }
        public HSVSelector()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private void UserControl_MouseMove(object sender, MouseEventArgs e)
        {
            if(Mouse.LeftButton == MouseButtonState.Pressed)
            {
                double imbW = imb.ActualWidth;
                double imbH = imb.ActualHeight;
                Point ct = new Point(imbW / 2, imbH / 2);
                Vector v1 = new Vector(1, 0);
                Point crPoint = Mouse.GetPosition(imb);
                Vector v2 = new Vector(crPoint.X - ct.X, crPoint.Y - ct.Y);
                double Angle = Vector.AngleBetween( v2, v1);
                Angle = Angle % 360;
                Angle = Angle < 0 ? 360 + Angle : Angle;
                
                double value = Angle * 179 / 360;
                if (_ButtonDown == ButtonDown.Up)
                {
                    HueUpper = Convert.ToInt32(value);
                }
                else if(_ButtonDown == ButtonDown.Low)
                {
                    HueLower = Convert.ToInt32(value);
                }
                AlgPropertyChanged?.Invoke(this, new EventArgs());
                OnPropertyChanged();
            }
        }

        public void OnPropertyChanged(string Name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Name));
        }
        private void upper_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _ButtonDown = ButtonDown.Up;
        }

        private void lower_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _ButtonDown = ButtonDown.Low;
        }
        private void UserControl_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _ButtonDown = ButtonDown.Unknow;
        }
        public void SetParameter(MyImage image, SMD smd, MyAlarm alarm)
        {
            this.UpdateLayout();
            string[] path = new string[] { "Hue.Upper", "Hue.Lower", "Saturation.Upper", "Saturation.Lower"};
            DependencyProperty[] dp = new DependencyProperty[] { HueUpperProperty, HueLowerProperty, SaturationUpperProperty, SaturationLowerProperty };
            for (int i = 0; i < path.Length; i++)
            {
                Binding bin = new Binding(path[i]);
                bin.Mode = BindingMode.TwoWay;
                bin.Source = alarm.Alg;
                this.SetBinding(dp[i], bin);
            }
            OnPropertyChanged();
        }
        public string GetAlgName()
        {
            return "HSV Selector";
        }

        public string[] GetSupporter()
        {
            return new string[2];
        }
        enum ButtonDown
        {
            Up,
            Low,
            Unknow
        }

    }
    public class H2Degress : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                double dValue = System.Convert.ToDouble(values[0]);
                double selfWidth = System.Convert.ToDouble(values[1]);
                double selfHeight = System.Convert.ToDouble(values[2]);
                double radius = Math.Min(selfWidth, selfHeight);
                double angle = dValue * 360 / 179;
                Point startPoint = new Point(radius, 0);
                Point CurPoint = CvUtils.PointRotation(startPoint, new Point(0, 0), -angle * Math.PI / 180.0);
                return new Thickness(CurPoint.X, CurPoint.Y, 0, 0);

            }
            catch { return 0; }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class SourceColorWheel : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                using (Image<Bgr, byte> img = new Image<Bgr, byte>("im/color-wheel.png"))
                {
                    double lower = System.Convert.ToDouble(values[0]);
                    double upper = System.Convert.ToDouble(values[1]);
                    double radius = Math.Min(img.Width, img.Height) / 2;
                    double angle = lower * 360 / 179;
                    Point startPoint = new Point(radius, 0);
                    Point CurPoint = CvUtils.PointRotation(startPoint, new Point(0, 0), -angle * Math.PI / 180.0);
                    return new Thickness(CurPoint.X, CurPoint.Y, 0, 0);
                }
            }
            catch { return 0; }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
