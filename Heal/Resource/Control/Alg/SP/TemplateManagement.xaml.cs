﻿using Heal.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Heal.Resource.Control.Alg.SP
{
    /// <summary>
    /// Interaction logic for TemplateManagement.xaml
    /// </summary>
    public partial class TemplateManagement : UserControl, IAlgProperties
    {
        public static readonly DependencyProperty SPListTempalteProperty = DependencyProperty.Register("Templates", typeof(ObservableCollection<Image<Bgr, byte>>), typeof(MyProperty));
        public ObservableCollection<Image<Bgr, byte>> Templates
        {
            get { return (ObservableCollection<Image<Bgr, byte>>)GetValue(SPListTempalteProperty); }
            set
            {
                SetValue(SPListTempalteProperty, value);
                OnAlgPropertyChanged();
            }
        }
        public TemplateManagement()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        private void ShowImage()
        {
            mainGrid.Children.Clear();
            mainGrid.ColumnDefinitions.Clear();
            for (int i = 0; i < Templates.Count; i++)
            {
                if(Templates[i] != null)
                {
                    mainGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    Resource.Control.TemplateImage image = new TemplateImage();
                    image.DeleteButtonClicked += Image_DeleteButtonClicked;
                    image.Index = i;
                    image.Source = MetaImageBox.Bitmap2BitmapSource(Templates[i].Bitmap);
                    Grid.SetColumn(image, i);
                    mainGrid.Children.Add(image);
                }
            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        private void Image_DeleteButtonClicked(object sender, DeleteTemplateHandlerArgs e)
        {
            Templates[e.Index]?.Dispose();
            Templates.RemoveAt(e.Index);
            OnPropertyChanged();
        }

        public event AlgPropertyChangedHandler AlgPropertyChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        public event UpdateSpHelpChangedHandler UpdateSpHelpChanged;
        public string GetAlgName()
        {
            return "Template Management";
        }
        public string[] GetSupporter()
        {
            return new string[2];
        }
        public void OnAlgPropertyChanged()
        {
            AlgPropertyChanged?.Invoke(this, new EventArgs());
            OnPropertyChanged();
        }
        public void OnPropertyChanged(string Name = null)
        {
            ShowImage();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(Name));
        }
        public void SetParameter(MyImage image, SMD smd, MyAlarm alarm)
        {
            if (alarm.Alg is Models.Alg.TemplateMatching)
            {
                string[] paths = new string[] { "Templates",};
                DependencyProperty[] dps = new DependencyProperty[] { SPListTempalteProperty };
                for (int i = 0; i < paths.Length; i++)
                {
                    Binding bin = new Binding(paths[i]);
                    bin.Source = alarm.Alg;
                    bin.Mode = BindingMode.TwoWay;
                    this.SetBinding(dps[i], bin);
                }
                OnPropertyChanged();
            }
        }
    }
}
