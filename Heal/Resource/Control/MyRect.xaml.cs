﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using draw = System.Drawing;
using Heal.Models;
using System.Globalization;

namespace Heal.Resource.Control
{
    /// <summary>
    /// Interaction logic for MyRect.xaml
    /// </summary>
    public partial class MyRect : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty SMDNameProperty = DependencyProperty.Register("SMDName", typeof(string), typeof(MyRect));
        public string SMDName
        {
            get { return (string)GetValue(SMDNameProperty); }
            set { SetValue(SMDNameProperty, value); }
        }
        public static readonly DependencyProperty SMDAngleProperty = DependencyProperty.Register("SMDAngle", typeof(double), typeof(MyRect));
        public double SMDAngle
        {
            get { return (double)GetValue(SMDAngleProperty); }
            set { SetValue(SMDAngleProperty, value); }
        }

        public static readonly DependencyProperty SMDWidthProperty = DependencyProperty.Register("SMDWidth", typeof(double), typeof(MyRect));
        public double SMDWidth
        {
            get { return (double)GetValue(SMDWidthProperty); }
            set { 
                SetValue(SMDWidthProperty, value);
                OnPropertyChanged();
            }
        }
        public static readonly DependencyProperty BorderThicknessRectProperty = DependencyProperty.Register("BorderThicknessRect", typeof(double), typeof(MyRect));
        public double BorderThicknessRect
        {
            get { return (double)GetValue(BorderThicknessRectProperty); }
            set{SetValue(BorderThicknessRectProperty, value);
                OnPropertyChanged();}
        }

        public static readonly DependencyProperty SMDHeightProperty = DependencyProperty.Register("SMDHeight", typeof(double), typeof(MyRect));
        public double SMDHeight
        {
            get { return (double)GetValue(SMDHeightProperty); }
            set { SetValue(SMDHeightProperty, value); OnPropertyChanged(); }
        }
        public static readonly DependencyProperty SMDLocXProperty = DependencyProperty.Register("SMDLocX", typeof(int), typeof(MyRect));
        public int SMDLocX
        {
            get { return (int)GetValue(SMDLocXProperty); }
            set { SetValue(SMDLocXProperty, value); }
        }

        public static readonly DependencyProperty SMDLocYProperty = DependencyProperty.Register("SMDLocY", typeof(int), typeof(MyRect));
        public int SMDLocY
        {
            get { return (int)GetValue(SMDLocYProperty); }
            set { SetValue(SMDLocYProperty, value); }
        }
        
        public static readonly DependencyProperty SMDBlockProperty = DependencyProperty.Register("SMDBlock", typeof(int), typeof(MyRect));
        public int SMDBlock
        {
            get { return (int)GetValue(SMDBlockProperty); }
            set { SetValue(SMDBlockProperty, value); }
        }
        public static readonly DependencyProperty SMDIsEnableProperty = DependencyProperty.Register("SMDIsEnable", typeof(bool), typeof(MyRect));
        public bool SMDIsEnable
        {
            get { return (bool)GetValue(SMDIsEnableProperty); }
            set { SetValue(SMDIsEnableProperty, value); }
        }

        public static readonly DependencyProperty SMDIsSetFlagProperty = DependencyProperty.Register("SMDIsSetFlag", typeof(bool), typeof(MyRect));
        public bool SMDIsSetFlag
        {
            get { return (bool)GetValue(SMDIsSetFlagProperty); }
            set { SetValue(SMDIsSetFlagProperty, value); }
        }
        public static readonly DependencyProperty IsLockCadProperty = DependencyProperty.Register("IsLockCad", typeof(bool), typeof(MyRect));
        public bool IsLockCad
        {
            get { return (bool)GetValue(IsLockCadProperty); }
            set { SetValue(IsLockCadProperty, value); }
        }
        public static readonly DependencyProperty ZoomScaleProperty = DependencyProperty.Register("ZoomScale", typeof(double), typeof(MyRect));
        public double ZoomScale
        {
            get { return (double)GetValue(ZoomScaleProperty); }
            set { SetValue(ZoomScaleProperty, value); }
        }
        public int ID { get; set; }
        public event SelectClick SelectClick; 
        public event SelectClick SelectMoveClick;
        //public event EventHandler LocationChanged;
        public MyRect()
        {
            InitializeComponent();
            BorderThicknessRect = 3;
            this.DataContext = this;

        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void SetZoomScale(double scale)
        {
            if (scale <= 1)
                BorderThicknessRect = 3 / scale;
            
        }
        public void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void MyRect_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            //SetPoition();
        }

        //public void SetPoition()
        //{
        //    int x = SMDLocX;
        //    int y = SMDLocY;
        //    double w = SMDWidth;
        //    double h = SMDHeight;
        //    if(ZoomScale > 0)
        //    {
        //        w = ZoomScale * w > 10 ? w : 10 / (ZoomScale * w);
        //        h = ZoomScale * h > 10 ? h : 10 / (ZoomScale * h);
        //    }
        //    this.Width = w;
        //    this.Height = h;
        //    x = Convert.ToInt32(x - this.Width / 2);
        //    y = Convert.ToInt32(y - this.Height / 2);
        //    Canvas.SetLeft(this, x);
        //    Canvas.SetTop(this, y);
        //    //this.Margin = new Thickness(x, y, 0, 0);
        //}
        public (double, double) GetXY()
        {
            double w = SMDWidth;
            double h = SMDHeight;
            if (ZoomScale > 0)
            {
                w = ZoomScale * w > 10 ? w : 10 / (ZoomScale * w);
                h = ZoomScale * h > 10 ? h : 10 / (ZoomScale * h);
            }
            double x = Canvas.GetLeft(this);
            double y = Canvas.GetTop(this);
            x += w / 2;
            y += h / 2;
            return (x, y);
        }
        private void self_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            SelectClick?.Invoke(this, new SelectClickArgs(this.ID));
        }

        private void self_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SelectMoveClick?.Invoke(this, new SelectClickArgs(this.ID));
        }

        private void self_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }
    }
    public delegate void SelectClick(object sender, SelectClickArgs e);
    public class SelectClickArgs : EventArgs
    {
        public int ID  { get; set; }
        public SelectClickArgs(int ID)
        {
            this.ID = ID;
        }
    }
    public class IvalueConvertor : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                double value = System.Convert.ToDouble(System.Convert.ToInt32(values[0]) - System.Convert.ToInt32(values[1]) / 2);
                return value;
            }
            catch { return null; }
            
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
