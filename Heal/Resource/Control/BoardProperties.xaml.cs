﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Heal.Models;

namespace Heal.Resource.Control
{
    /// <summary>
    /// Interaction logic for BoardProperties.xaml
    /// </summary>
    public partial class BoardProperties : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty BoardNameProperty = DependencyProperty.Register("BoardName", typeof(string), typeof(BoardProperties));
        public string BoardName
        {
            get { return (string)GetValue(BoardNameProperty); }
            set
            {
                SetValue(BoardNameProperty, value);
                BoardPropertyChanged?.Invoke(this, new EventArgs());
            }
        }
        public static readonly DependencyProperty BoardSideProperty = DependencyProperty.Register("BoardSide", typeof(int), typeof(BoardProperties));
        public int BoardSide
        {
            get { return (int)GetValue(BoardSideProperty); }
            set { SetValue(BoardSideProperty, value); }
        }
        public static readonly DependencyProperty BoardWidthProperty = DependencyProperty.Register("BoardWidth", typeof(double), typeof(BoardProperties));
        public double BoardWidth
        {
            get { return (double)GetValue(BoardWidthProperty); }
            set { SetValue(BoardWidthProperty, value); }
        }
        public static readonly DependencyProperty BoardHeightProperty = DependencyProperty.Register("BoardHeight", typeof(double), typeof(BoardProperties));
        public double BoardHeight
        {
            get { return (double)GetValue(BoardHeightProperty); }
            set { SetValue(BoardHeightProperty, value); }
        }
        public static readonly DependencyProperty BoardThicknessProperty = DependencyProperty.Register("BoardThickness", typeof(double), typeof(BoardProperties));
        public double BoardThickness
        {
            get { return (double)GetValue(BoardThicknessProperty); }
            set { SetValue(BoardThicknessProperty, value); }
        }
        public static readonly DependencyProperty BoardBlocksProperty = DependencyProperty.Register("BoardBlocks", typeof(int), typeof(BoardProperties));
        public int BoardBlocks
        {
            get { return (int)GetValue(BoardBlocksProperty); }
            set { SetValue(BoardBlocksProperty, value); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler BoardPropertyChanged;
        public BoardProperties()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        public void SetParam(MyBoard Board)
        {
            // ---- Name ----
            Binding binName = new Binding("Name");
            binName.Source = Board;
            binName.Mode = BindingMode.TwoWay;
            this.SetBinding(BoardProperties.BoardNameProperty, binName);
            // ---- Side ----
            Binding binSide = new Binding("Side");
            binSide.Source = Board;
            binSide.Mode = BindingMode.TwoWay;
            binSide.Converter = new Resource.Convertor.BoardSide2Int();
            this.SetBinding(BoardProperties.BoardSideProperty, binSide);
            // ---- Width ----
            Binding binWidth = new Binding("Width");
            binWidth.Source = Board;
            binWidth.Mode = BindingMode.TwoWay;
            this.SetBinding(BoardProperties.BoardWidthProperty, binWidth);
            // ---- Height ----
            Binding binHeight = new Binding("Height");
            binHeight.Source = Board;
            binHeight.Mode = BindingMode.TwoWay;
            this.SetBinding(BoardProperties.BoardHeightProperty, binHeight);
            // ---- Thickness ----
            Binding binThickness = new Binding("Thickness");
            binThickness.Source = Board;
            binThickness.Mode = BindingMode.TwoWay;
            this.SetBinding(BoardProperties.BoardThicknessProperty, binThickness);
            Binding binBlocks = new Binding("PCBBlocks");
            binBlocks.Source = Board;
            binBlocks.Mode = BindingMode.TwoWay;
            this.SetBinding(BoardProperties.BoardBlocksProperty, binBlocks);
        }
        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                Convertor.Action.UpdateBindingSourceTextBox(sender);
        }
        public void OnPropertyChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }

        private void TextBox_0dot2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Convertor.Action.Change0dot2(sender, e.Delta);
        }

        private void TextBox_2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Convertor.Action.Change2(sender, e.Delta);
        }

        private void TextBox_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            
        }
    }
}
