﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Heal.Models;

namespace Heal.Resource.Control
{
    /// <summary>
    /// Interaction logic for SMDProperties.xaml
    /// </summary>
    public partial class SMDProperties : UserControl, INotifyPropertyChanged
    {
        
       
        public static readonly DependencyProperty SMDNameProperty = DependencyProperty.Register("SMDName", typeof(string), typeof(SMDProperties));
        public string SMDName
        {
            get { return (string)GetValue(SMDNameProperty); }
            set { SetValue(SMDNameProperty, value); }
        }
        public static readonly DependencyProperty SMDAngleProperty = DependencyProperty.Register("SMDAngle", typeof(double), typeof(SMDProperties));
        public double SMDAngle
        {
            get { return (double)GetValue(SMDAngleProperty); }
            set { SetValue(SMDAngleProperty, value); }
        }

        public static readonly DependencyProperty SMDWidthProperty = DependencyProperty.Register("SMDWidth", typeof(double), typeof(SMDProperties));
        public double SMDWidth
        {
            get { return (double)GetValue(SMDWidthProperty); }
            set { SetValue(SMDWidthProperty, value); }
        }

        public static readonly DependencyProperty SMDHeightProperty = DependencyProperty.Register("SMDHeight", typeof(double), typeof(SMDProperties));
        public double SMDHeight
        {
            get { return (double)GetValue(SMDHeightProperty); }
            set { SetValue(SMDHeightProperty, value); }
        }
        public static readonly DependencyProperty SMDLocXProperty = DependencyProperty.Register("SMDLocX", typeof(int), typeof(SMDProperties));
        public int SMDLocX
        {
            get { return (int)GetValue(SMDLocXProperty); }
            set { SetValue(SMDLocXProperty, value); }
        }
        public static readonly DependencyProperty SMDLocYProperty = DependencyProperty.Register("SMDLocY", typeof(int), typeof(SMDProperties));
        public int SMDLocY
        {
            get { return (int)GetValue(SMDLocYProperty); }
            set { SetValue(SMDLocYProperty, value); }
        }
        public static readonly DependencyProperty SMDBlockProperty = DependencyProperty.Register("SMDBlock", typeof(int), typeof(SMDProperties));
        public int SMDBlock
        {
            get { return (int)GetValue(SMDBlockProperty); }
            set { SetValue(SMDBlockProperty, value); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler SetRefImageClick;
        public event EventHandler SMDPropertyChanged;
        public SMDProperties()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        public void SetParam(SMD smd)
        {
            // ---- Name -----
            Binding binName = new Binding("Name");
            binName.Source = smd;
            binName.Mode = BindingMode.TwoWay;
            this.SetBinding(SMDProperties.SMDNameProperty, binName);
            //--- Type --- 
            Binding binAngle = new Binding("Angle");
            binAngle.Source = smd;
            binAngle.Mode = BindingMode.TwoWay;
            this.SetBinding(SMDProperties.SMDAngleProperty, binAngle);
            //----Width---- -
            Binding binWidth = new Binding("Width");
            binWidth.Source = smd;
            binWidth.Mode = BindingMode.TwoWay;
            binWidth.Converter = new Resource.Convertor.Pixel2Millimeter();
            this.SetBinding(SMDProperties.SMDWidthProperty, binWidth);
            // ---- Height -----
            Binding binHeight = new Binding("Height");
            binHeight.Source = smd;
            binHeight.Mode = BindingMode.TwoWay;
            binHeight.Converter = new Resource.Convertor.Pixel2Millimeter();
            this.SetBinding(SMDProperties.SMDHeightProperty, binHeight);
            // ---- Location X -----
            Binding binLocX = new Binding("X");
            binLocX.Source = smd;
            binLocX.Mode = BindingMode.TwoWay;
            this.SetBinding(SMDProperties.SMDLocXProperty, binLocX);
            // ---- Location Y -----
            Binding binLocY = new Binding("Y");
            binLocY.Source = smd;
            binLocY.Mode = BindingMode.TwoWay;
            this.SetBinding(SMDProperties.SMDLocYProperty, binLocY);
            // ---- Block -----
            Binding binBlock = new Binding("Block");
            binBlock.Source = smd;
            binBlock.Mode = BindingMode.TwoWay;
            this.SetBinding(SMDProperties.SMDBlockProperty, binBlock);
        }
        public void OnPropertyChanged(string name)
        {
            if(PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
        private void btUpdateCatalogImage_Click(object sender, RoutedEventArgs e)
        {
            SetRefImageClick?.Invoke(this, new EventArgs());
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                Convertor.Action.UpdateBindingSourceTextBox(sender);
                SMDPropertyChanged?.Invoke(this, new EventArgs());
            }
        }

        private void TextBox_2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Convertor.Action.Change2(sender, e.Delta);
            SMDPropertyChanged?.Invoke(this, new EventArgs());
        }
        private void TextBox_5_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Convertor.Action.Change5(sender, e.Delta);
            SMDPropertyChanged?.Invoke(this, new EventArgs());
        }
        private void TextBox_0dot2_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Convertor.Action.Change0dot2(sender, e.Delta);
            SMDPropertyChanged?.Invoke(this, new EventArgs());
        }
    }
}
