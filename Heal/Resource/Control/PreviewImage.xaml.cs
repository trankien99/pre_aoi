﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.ComponentModel;

namespace Heal.Resource.Control
{
    /// <summary>
    /// Interaction logic for PreviewRect.xaml
    /// </summary>
    public partial class PreviewImage : UserControl, INotifyPropertyChanged
    {
        public static readonly DependencyProperty ISourceProperty = DependencyProperty.Register("ISource", typeof(BitmapSource), typeof(PreviewImage));
        public BitmapSource ISource
        {
            get { return (BitmapSource)GetValue(ISourceProperty); }
            set { SetValue(ISourceProperty, value); }
        }
        public static readonly DependencyProperty LocationProperty = DependencyProperty.Register("ILocation", typeof(Point), typeof(PreviewImage));
        public Point ILocation
        {
            get { return (Point)GetValue(LocationProperty); }
            set { SetValue(LocationProperty, value); }
        }
        public static readonly DependencyProperty IAngleProperty = DependencyProperty.Register("IAngle", typeof(double), typeof(PreviewImage));
        public double IAngle
        {
            get { return (double)GetValue(IAngleProperty); }
            set { SetValue(IAngleProperty, value); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
        }
       
        public PreviewImage()
        {
            InitializeComponent(); 
            this.DataContext = this;
        }
        public void SetVerSize(double Scale)
        {
        }

        private void Rectangle_PreviewMouseLeftDownMove(object sender, MouseButtonEventArgs e)
        {

        }
    }
    //public class AngleReverseCvt : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        return - System.Convert.ToDouble(value);
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}
