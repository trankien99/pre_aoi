﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Interactivity;
using Heal.Models;
using draw = System.Drawing;
using Heal.Utils;

namespace Heal.Resource.Control
{
    /// <summary>
    /// Interaction logic for MetaImageBox.xaml
    /// </summary>
    public partial class MetaImageBox : UserControl, INotifyPropertyChanged
    {
        private Point _StartPoint = new Point();
        public double ImageWidth { get; set; }
        public double ImageHeight { get; set; }
        private List<double> _ZoomScaleRange = new List<double>();

        private static readonly DependencyProperty CurrCenterPointProperty = DependencyProperty.Register("CurrCenterPoint", typeof(Point), typeof(MetaImageBox));
        public Point CurrCenterPoint
        {
            get { return (Point)GetValue(CurrCenterPointProperty); }
            set { SetValue(CurrCenterPointProperty, value); }
        }
        private static readonly DependencyProperty CurrPointProperty = DependencyProperty.Register("CurrPoint", typeof(Point), typeof(MetaImageBox));
        public Point CurrPoint
        {
            get { return (Point)GetValue(CurrPointProperty); }
            set { SetValue(CurrPointProperty, value); }
        }
        private static readonly DependencyProperty ZoomScaleProperty = DependencyProperty.Register("ZoomScale", typeof(double), typeof(MetaImageBox), new PropertyMetadata(1.0));
        public double ZoomScale
        {
            get { return (double)GetValue(ZoomScaleProperty); }
            set { SetValue(ZoomScaleProperty, value); }
        }
        private static readonly DependencyProperty ROIProperty = DependencyProperty.Register("ROI", typeof(Rect), typeof(MetaImageBox), new PropertyMetadata(new Rect()));
        public Rect ROI
        {
            get { return (Rect)GetValue(ROIProperty); }
            set { SetValue(ROIProperty, value); }
        }
        public static readonly DependencyProperty IsLockCadProperty = DependencyProperty.Register("IsLockCad", typeof(bool), typeof(MetaImageBox));
        private MyRect _MyRectSelected { get; set; }
        private int _MyRectSelectedID { get; set; }
        public bool IsLockCad
        {
            get { return (bool)GetValue(IsLockCadProperty); }
            set { SetValue(IsLockCadProperty, value); }
        }
        public static readonly DependencyProperty IsPreviewProperty = DependencyProperty.Register("IsPreview", typeof(bool), typeof(MetaImageBox));
        public bool IsPreview
        {
            get { return (bool)GetValue(IsPreviewProperty); }
            set { SetValue(IsPreviewProperty, value); }
        }
        public static readonly DependencyProperty MouseXProperty = DependencyProperty.Register("MouseX", typeof(int), typeof(MetaImageBox));
        public int MouseX
        {
            get { return (int)GetValue(MouseXProperty); }
            set { SetValue(MouseXProperty, value); }
        }
        public static readonly DependencyProperty MouseYProperty = DependencyProperty.Register("MouseY", typeof(int), typeof(MetaImageBox));
        public int MouseY
        {
            get { return (int)GetValue(MouseYProperty); }
            set { SetValue(MouseYProperty, value); }
        }
        public Visibility IsShowLine
        {
            get
            {
                if(cvImage != null)
                {
                    if (cvImage.Children.Count > 0) return Visibility.Visible;
                }
                return Visibility.Hidden;
            }
            
        }
        public ObservableCollection<SMD> SMDSource { get; set; } = new ObservableCollection<SMD>();
        private ObservableCollection<SMD> SMDSetFlag { get; set; } = new ObservableCollection<SMD>();
        private bool _IsMoveRectAdjust { get; set; }
        private Corner _Corner { get; set; }
        private bool _IsAdjustRectAdjust { get; set; }
        private AdjustArgs _AdjustArg { get; set; }
        public event EventHandler AdjustRectMoved;
        public MetaImageBox()
        {
            _ZoomScaleRange.AddRange(new double[] { 0.0375, 0.065, 0.125, 0.25, 0.50, 0.75, 1.00, 2.00, 3.00, 4.00, 5.00, 6.00 });
            InitializeComponent();
            
            this.DataContext = this;
            OnPropertyChanged();
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public event SMDSelectChangedHandler SMDSelectChanged;
        public void OnPropertyChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
            for (int i = 0; i < cvSMDs.Children.Count; i++)
            {
                MyRect mr = cvSMDs.Children[i] as MyRect;
                mr.OnPropertyChanged();
            }
        }
        public void SetSMDSource(ObservableCollection<SMD>  SMDs)
        {
            SMDSource = SMDs;
            cvSMDs.Children.Clear();
            for (int i = 0; i < SMDSource.Count; i++)
            {
                var item = SMDSource[i];
                MyRect mr = new MyRect();
                mr.ID = i;
                mr.SelectClick += Mr_SelectClick;
                mr.SelectMoveClick += Mr_SelectMoveClick;
                cvSMDs.Children.Add(mr);
                UpdateBindingMyRect(i, item);
            }
        }
        public void SetPreview(AdjustArgs Args)
        {
            
            prvRect.Visibility = Visibility.Hidden;
            prvImage.Visibility = Visibility.Hidden;
            if (Args == null)
                return;
            if (Args.Alarm != null && cvImage.Children.Count > 0)
            {
                prvRect.Visibility = Visibility.Visible;
                _AdjustArg = Args;
                prvRect.Width = Args.Alarm.Width;
                prvRect.Height = Args.Alarm.Height;
                prvRect.IAngle = Args.SMD.Angle;
                prvRect.ILocation = new Point(Args.Location.X, Args.Location.Y);
                prvRect.OnPropertyChanged();
                if(IsPreview && Args.BMS != null)
                {
                    prvImage.Visibility = Visibility.Visible;
                    prvImage.ISource = Args.BMS;
                    prvImage.IAngle = Args.SMD.Angle;
                    prvImage.ILocation = new Point(Args.Location.X, Args.Location.Y);
                    prvImage.Width = Args.BMS.Width;
                    prvImage.Height = Args.BMS.Height;
                    prvImage.OnPropertyChanged();
                }
            }
            else
            {
                _AdjustArg = null;
            }
        }
        private void prvRect_MoveRectMouseDown(object sender, EventArgs e)
        {
            _StartPoint = Mouse.GetPosition(cvMain);
            _StartPoint = new Point(Convert.ToInt32(_StartPoint.X), Convert.ToInt32(_StartPoint.Y));
            _IsMoveRectAdjust = true;
            
        }
        private void prvRect_AdjustRectMouseDown(object sender, AdjustRectArgs e)
        {
            _StartPoint = Mouse.GetPosition(cvMain);
            _StartPoint = new Point(Convert.ToInt32(_StartPoint.X), Convert.ToInt32(_StartPoint.Y));
            _IsAdjustRectAdjust = true;
            _Corner = e.Corner;
        }
        private void cvMain_MouseMove(object sender, MouseEventArgs e)
        {
            Point currPoint = Mouse.GetPosition(cvImage);
            currPoint = new Point(Convert.ToInt32(currPoint.X), Convert.ToInt32(currPoint.Y));
            MouseX = Convert.ToInt32(currPoint.X);
            MouseY = Convert.ToInt32(currPoint.Y);
            if (Mouse.RightButton == MouseButtonState.Pressed)
            {
                
                int x = Convert.ToInt32(Math.Min(currPoint.X, _StartPoint.X));
                int y = Convert.ToInt32(Math.Min(currPoint.Y, _StartPoint.Y));
                int w = Convert.ToInt32(Math.Abs(currPoint.X - _StartPoint.X));
                int h = Convert.ToInt32(Math.Abs(currPoint.Y - _StartPoint.Y));
                ROI = new Rect(x, y, w, h);
            }
            else if (!IsLockCad && _MyRectSelected != null && Mouse.LeftButton == MouseButtonState.Pressed)
            {
                int subX = Convert.ToInt32(currPoint.X - _StartPoint.X);
                int subY = Convert.ToInt32(currPoint.Y - _StartPoint.Y);
                if (subX != 0 || subY != 0)
                {
                    var smdMove = SMDSource[_MyRectSelectedID];
                    if (smdMove.IsSetFlag)
                    {
                        for (int i = 0; i < SMDSetFlag.Count; i++)
                        {
                            SMDSetFlag[i].X += Convert.ToInt32(subX);
                            SMDSetFlag[i].Y += Convert.ToInt32(subY);
                        }
                    }
                    else
                    {
                        _MyRectSelected.SMDLocX += Convert.ToInt32(subX);
                        _MyRectSelected.SMDLocY += Convert.ToInt32(subY);
                    }
                    _StartPoint = currPoint;
                }
            }
            else if (_IsMoveRectAdjust && _AdjustArg != null && Mouse.LeftButton == MouseButtonState.Pressed)
            {
                Point ct = new Point(_AdjustArg.Location.X, _AdjustArg.Location.Y);
                Point st = CvUtils.PointRotation(_StartPoint, ct, (-_AdjustArg.SMD.Angle) * Math.PI / 180.0);
                Point cr = CvUtils.PointRotation(currPoint, ct, (-_AdjustArg.SMD.Angle) * Math.PI / 180.0); 
                int subX = Convert.ToInt32(cr.X - st.X);
                int subY = Convert.ToInt32(cr.Y - st.Y);
                if (subX != 0 || subY != 0)
                {
                    _AdjustArg.Alarm.OffsetX += subX;
                    _AdjustArg.Alarm.OffsetY += subY;
                    AdjustRectMoved?.Invoke(this, new EventArgs());
                    _StartPoint = currPoint;
                }
            }
            else if (_IsAdjustRectAdjust && _AdjustArg != null && Mouse.LeftButton == MouseButtonState.Pressed)
            {
                Point ct = new Point(_AdjustArg.Location.X, _AdjustArg.Location.Y);
                Point st = CvUtils.PointRotation(_StartPoint, ct, (-_AdjustArg.SMD.Angle) * Math.PI / 180.0);
                Point cr = CvUtils.PointRotation(currPoint, ct, (-_AdjustArg.SMD.Angle) * Math.PI / 180.0);
                int subX = Convert.ToInt32(cr.X - st.X);
                int subY = Convert.ToInt32(cr.Y - st.Y);
                if (subX != 0 || subY != 0)
                {
                    switch (_Corner)
                    {
                        case Corner.TopLeft:
                            _AdjustArg.Alarm.Width = _AdjustArg.Alarm.Width + -2 * subX < 1 ? 1 : _AdjustArg.Alarm.Width + -2 * subX;
                            _AdjustArg.Alarm.Height =  _AdjustArg.Alarm.Height + - 2 * subY < 1 ? 1 : _AdjustArg.Alarm.Height + -2 * subY;
                            break;
                        case Corner.TopRight:
                            _AdjustArg.Alarm.Width = _AdjustArg.Alarm.Width + 2 * subX < 1 ? 1 : _AdjustArg.Alarm.Width + 2 * subX;
                            _AdjustArg.Alarm.Height = _AdjustArg.Alarm.Height + -2 * subY < 1 ? 1 : _AdjustArg.Alarm.Height + -2 * subY;
                            break;
                        case Corner.BotLeft:
                            _AdjustArg.Alarm.Width = _AdjustArg.Alarm.Width + -2 * subX < 1 ? 1 : _AdjustArg.Alarm.Width + -2 * subX;
                            _AdjustArg.Alarm.Height  = _AdjustArg.Alarm.Height + 2 * subY < 1 ? 1 : _AdjustArg.Alarm.Height + 2 * subY;
                            break;
                        case Corner.BotRight:
                            _AdjustArg.Alarm.Width = _AdjustArg.Alarm.Width + 2 * subX < 1 ? 1 : _AdjustArg.Alarm.Width + 2 * subX;
                            _AdjustArg.Alarm.Height = _AdjustArg.Alarm.Height + 2 * subY < 1 ? 1 : _AdjustArg.Alarm.Height + 2 * subY;
                            break;
                        default:
                            break;
                    }
                    //_AdjustArg.Alarm.Width = _AdjustArg.Alarm.Width < 1 ? 1 : _AdjustArg.Alarm.Width;
                    //_AdjustArg.Alarm.Height = _AdjustArg.Alarm.Height < 1 ? 1 : _AdjustArg.Alarm.Height;
                    AdjustRectMoved?.Invoke(this, new EventArgs());
                    _StartPoint = currPoint;
                }
            }
        }
        public void SetPreview(bool IsEnable)
        {
             prvImage.Visibility = IsEnable ? Visibility.Visible : Visibility.Hidden;
        }
        public void RefreshSMDs(SMD smd)
        {
            int id = SMDSource.IndexOf(smd);
            UpdateBindingMyRect(id, smd);
        }
        public void UpdateBindingMyRect(int ID, SMD item)
        {
            MyRect mr = cvSMDs.Children[ID] as MyRect;
            //---- Name -----
            Binding bin = new Binding("Name");
            bin.Source = item;
            bin.Mode = BindingMode.TwoWay;
            mr.SetBinding(MyRect.SMDNameProperty, bin);
            //---- Angle -----
            bin = new Binding("Angle");
            bin.Source = item;
            bin.Mode = BindingMode.TwoWay;
            mr.SetBinding(MyRect.SMDAngleProperty, bin);
            //---- Block -----
            bin = new Binding("Block");
            bin.Source = item;
            bin.Mode = BindingMode.TwoWay;
            mr.SetBinding(MyRect.SMDBlockProperty, bin);
            //---- Width -----
            bin = new Binding("Width");
            bin.Source = item;
            bin.Mode = BindingMode.TwoWay;
            mr.SetBinding(MyRect.SMDWidthProperty, bin);
            //---- Height -----
            bin = new Binding("Height");
            bin.Source = item;
            bin.Mode = BindingMode.TwoWay;
            mr.SetBinding(MyRect.SMDHeightProperty, bin);
            //---- Location X -----
            bin = new Binding("X");
            bin.Source = item;
            bin.Mode = BindingMode.TwoWay;
            mr.SetBinding(MyRect.SMDLocXProperty, bin);
            //---- Location Y -----
            bin = new Binding("Y");
            bin.Source = item;
            bin.Mode = BindingMode.TwoWay;
            mr.SetBinding(MyRect.SMDLocYProperty, bin);
            //---- IsEnable -----
            bin = new Binding("IsEnable");
            bin.Source = item;
            bin.Mode = BindingMode.TwoWay;
            mr.SetBinding(MyRect.SMDIsEnableProperty, bin);

            //---- IsSetFlag -----
            bin = new Binding("IsSetFlag");
            bin.Source = item;
            bin.Mode = BindingMode.TwoWay;
            mr.SetBinding(MyRect.SMDIsSetFlagProperty, bin);
            mr.OnPropertyChanged();
        }
        private void Mr_SelectMoveClick(object sender, SelectClickArgs e)
        {
            if (SMDSource != null)
            {
                if (SMDSource.Count > e.ID)
                {
                    if (Mouse.LeftButton == MouseButtonState.Pressed && !IsLockCad)
                    {
                        _StartPoint = Mouse.GetPosition(cvImage);
                        _StartPoint = new Point(Convert.ToInt32(_StartPoint.X), Convert.ToInt32(_StartPoint.Y));
                        _MyRectSelected = cvSMDs.Children[e.ID] as MyRect;
                        _MyRectSelectedID = e.ID;
                        this.Cursor = Cursors.Hand;
                    }
                    else
                    {
                        _MyRectSelected = null;
                        this.Cursor = Cursors.Arrow;
                    }
                }
            }
        }
        private void Mr_SelectClick(object sender, SelectClickArgs e)
        {
            if(SMDSource != null && ROI == new Rect())
            {
                if (SMDSource.Count > e.ID)
                {
                    ContextMenuService.SetIsEnabled(cvMain, false);
                    SMDSelectChanged?.Invoke(this, new SMDSelectChangedArgs(SMDSource[e.ID]));
                }
            }
            else if(ROI != new Rect())
            {
                ContextMenuService.SetIsEnabled(cvMain, true);
            }
        }
        private void cvMain_PreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (ROI != new Rect())
            {
                ContextMenuService.SetIsEnabled(cvMain, true);
            }
        }
        public void SetSource(int Width, int Height,  MetaImageBlock[] Blocks)
        {
            cvImage.Children.Clear();
            cvImage.Width = Width;
            cvImage.Height = Height;
            this.ImageWidth = Width;
            this.ImageHeight = Height;
            for (int i = 0; i < Blocks.Length; i++)
            {
                Image imageBlock = new Image();
                imageBlock.Width = Blocks[i].Location.Width;
                imageBlock.Height = Blocks[i].Location.Height;
                imageBlock.Source = Blocks[i].Source;
                cvImage.Children.Add(imageBlock);
                Canvas.SetLeft(imageBlock, Blocks[i].Location.X);
                Canvas.SetTop(imageBlock, Blocks[i].Location.Y);
            }
            GoOirgin();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        public void SetSource(object image)
        {
            cvImage.Children.Clear();
            cvImage.Width = Width;
            cvImage.Height = Height;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        public void GoPoint(Point XY)
        {
            CurrCenterPoint = XY;
            double wViewWidth = this.ActualWidth;
            double hViewWidth = this.ActualHeight;
            double left = wViewWidth / 2  - XY.X * ZoomScale ;
            double top = hViewWidth / 2 - XY.Y * ZoomScale;
            Canvas.SetLeft(cvMain, left);
            Canvas.SetTop(cvMain, top);
        }
        public void GoOirgin()
        {
            Point XY = new Point(ImageWidth / 2, ImageHeight / 2);
            CurrCenterPoint = XY;
            double wViewWidth = this.ActualWidth;
            double hViewWidth = this.ActualHeight;
            double left = wViewWidth / 2 - XY.X * ZoomScale;
            double top = hViewWidth / 2 - XY.Y * ZoomScale;
            Canvas.SetLeft(cvMain, left);
            Canvas.SetTop(cvMain, top);
            
        }
        public static BitmapSource Bitmap2BitmapSource(System.Drawing.Bitmap bitmap, bool Release = true)
        {
            var pixelFormat = bitmap.PixelFormat;
            PixelFormat format = PixelFormats.Bgr24;
            switch (pixelFormat)
            {
                case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                    format = PixelFormats.Gray8;
                    break;
                case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
                    format = PixelFormats.Bgr24;
                    break;
                case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
                    format = PixelFormats.Bgra32;
                    break;
                default:
                    break;
            }
            var bitmapData = bitmap.LockBits(
                new System.Drawing.Rectangle(0, 0, bitmap.Width, bitmap.Height),
                System.Drawing.Imaging.ImageLockMode.ReadOnly, bitmap.PixelFormat);
            var bitmapSource = BitmapSource.Create(
                bitmapData.Width, bitmapData.Height,
                bitmap.HorizontalResolution, bitmap.VerticalResolution,
                format, null,
                bitmapData.Scan0, bitmapData.Stride * bitmapData.Height, bitmapData.Stride);
            bitmap.UnlockBits(bitmapData);
            if (Release)
            {
                bitmap.Dispose();
            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return bitmapSource;
        }
        public void ZoomUp()
        {
            int id = _ZoomScaleRange.IndexOf(ZoomScale);
            if(id < _ZoomScaleRange.Count - 1)
            {
                ZoomScale = _ZoomScaleRange[id + 1];
                SetZoom(ZoomScale);
            }
        }
        private void SetZoom(double Scale)
        {
            GoPoint(CurrCenterPoint);
            cvScale.ScaleX = Scale;
            cvScale.ScaleY = Scale;
            rectSelected.StrokeThickness = 1 / Scale;
            prvImage.SetVerSize(Scale);
            prvRect.SetVerSize(Scale);
            for (int i = 0; i < cvSMDs.Children.Count; i++)
            {
                (cvSMDs.Children[i] as MyRect).SetZoomScale(Scale);
            }
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
        }
        public void ZoomDown()
        {
            int id = _ZoomScaleRange.IndexOf(ZoomScale);
            if (id > 0)
            {
                ZoomScale = _ZoomScaleRange[id - 1];
                SetZoom(ZoomScale);
            }
        }
        
        private void cvMain_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if(cvImage.Children.Count > 0)
           if(e.Delta > 0)
           {
                ZoomUp();
           }
           else
           {
                ZoomDown();
           }
        }
        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            GoPoint(CurrCenterPoint);
        }

        private void btGoOriginImage_Click(object sender, RoutedEventArgs e)
        {
            GoOirgin();
        }

        private void btShowExtracted_Click(object sender, RoutedEventArgs e)
        {
            //cvSMDs.Visibility = Visibility.Visible;
        }
        private void cvMain_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            _StartPoint = Mouse.GetPosition(cvImage);
            _StartPoint = new Point(Convert.ToInt32(_StartPoint.X), Convert.ToInt32(_StartPoint.Y));
            ROI = new Rect();
        }
        
        public void ResetROI()
        {
            ROI = new Rect();
            _StartPoint = new Point();
        }

        private void cvMain_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if(_IsMoveRectAdjust == true)
            {
                _IsMoveRectAdjust = false;
            }
            else if (_IsAdjustRectAdjust == true)
            {
                _IsAdjustRectAdjust = false;
            }
            else if (!IsLockCad && _MyRectSelected != null)
            {
                _MyRectSelected = null;
            }
            else if(_MyRectSelected == null)
            {
                Point p = Mouse.GetPosition(cvImage);
                GoPoint(p);
            }
            e.Handled = true;
            this.Cursor = Cursors.Arrow;
        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            ROI = new Rect();
            e.Handled = true;
        }

        private void mnSetFlag_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in SMDSource)
            {
                if(ROI.Contains(new Point(item.X, item.Y)))
                {
                    item.IsSetFlag = true;
                    SMDSetFlag.Add(item);
                }
            }
        }

        private void mnClearFlag_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in SMDSource)
            {
                if (ROI.Contains(new Point(item.X, item.Y)))
                {
                    item.IsSetFlag = false;
                    SMDSetFlag.Remove(item);
                }
            }
        }
    }
    public class MetaImageBlock
    {
        public int ID { get; set; }
        public Rect Location { get; set; }
        public BitmapSource Source { get; set; }
    }
    public delegate void SMDSelectChangedHandler(object sender, SMDSelectChangedArgs e);
    public class SMDSelectChangedArgs : EventArgs
    {
        public SMD Item { get; set; }
        public SMDSelectChangedArgs(SMD item)
        {
            this.Item = item;
        }
    }
    public delegate void SMDPropertyChangedHandler(object sender, SMDPropertyChangedArgs e);
    public class SMDPropertyChangedArgs : EventArgs
    {
        public SMD Item { get; set; }
        public SMDPropertyChangedArgs(SMD item)
        {
            this.Item = item;
        }
    }
    public class AdjustArgs
    {
        public MyAlarm Alarm { get; set; }
        public SMD SMD { get; set; }
        public BitmapSource BMS { get; set; }
        public draw.Point Location { get; set; }
    }
}
