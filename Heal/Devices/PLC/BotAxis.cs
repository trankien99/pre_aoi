﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Heal;
using System.Diagnostics;
using System.Threading;
using Heal.SDK;

namespace Heal.Devices.PLC
{
    public class BotAxis
    {
        Config.BotAxisCfg cfg = Config.BotAxisCfg.GetInstance();
        private SLMP mSLMP = new SLMP(Properties.Settings.Default.PLC_IP, Properties.Settings.Default.PLC_PORT);
        public int GoTop()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_TOP, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoBot()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_BOT, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoRight()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_RIGHT, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoLeft()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_LEFT, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int ResetGoTop()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_TOP, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int ResetGoBot()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_BOT, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int ResetGoRight()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_RIGHT, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int ResetGoLeft()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_LEFT, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoXY(Point crPoint)
        {
            if (crPoint.X > cfg.LIMIT_VALUE_X || crPoint.Y > cfg.LIMIT_VALUE_Y || crPoint.X < 0 || crPoint.Y < 0)
                return -1;
            int setX = SetReg(cfg.REG_WRITE_VALUE_X, cfg.REG_WRITE_VALUE_X, crPoint.X);
            if (setX == 0)
            {
                int setY = SetReg(cfg.REG_WRITE_VALUE_Y, cfg.REG_WRITE_VALUE_Y, crPoint.Y);
                if (setY == 0)
                {
                    var rt = mSLMP.SetDevice(cfg.BIT_WRITE_FINISH, 1);
                    return rt.Status == SLMPStatus.SUCCESSFULLY ? 0 : -3;
                }
                else
                {
                    return setY;
                }
            }
            else
                return setX;
        }
        public int GoXYFinish()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var r = mSLMP.GetDevice(cfg.BIT_GO_FINISH);
            while (sw.ElapsedMilliseconds < 5000 && (r.Status == SLMPStatus.FAIL || (r.Status == SLMPStatus.SUCCESSFULLY && r.Value == 0)))
            {
                Thread.Sleep(5);
                r = mSLMP.GetDevice(cfg.BIT_GO_FINISH);
            }
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int GoHome()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_HOME, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int SetSpeed(int SpeedX, int SpeedY)
        {
            if (SpeedX > cfg.LIMIT_SPEED_X || SpeedY > cfg.LIMIT_SPEED_Y || SpeedX < 0 || SpeedY < 0)
                return -1;
            int setX = SetReg(cfg.REG_WRITE_SPEED_X, cfg.REG_WRITE_SPEED_X, SpeedX);
            if (setX == 0)
            {
                int setY = SetReg(cfg.REG_WRITE_SPEED_Y, cfg.REG_WRITE_SPEED_Y, SpeedY);
                return setY;
            }
            else
                return setX;
        }
        public Point GetXY()
        {
            SLMPResult resultX = GetReg(cfg.REG_READ_VALUE_X);
            if (resultX.Status == SLMPStatus.SUCCESSFULLY)
            {
                SLMPResult resultY = GetReg(cfg.REG_READ_VALUE_Y);
                if (resultY.Status == SLMPStatus.SUCCESSFULLY)
                {
                    return new Point(resultX.Value, resultY.Value);
                }
                else
                {
                    return new Point(-1, -1);
                }
            }
            else
            {
                return new Point(-1, -1);
            }
        }
        public int GetSpeedX()
        {
            SLMPResult resultX = GetReg(cfg.REG_READ_SPEED_X);
            return resultX.Status == SLMPStatus.SUCCESSFULLY ? resultX.Value : -1;
        }
        public int GetSpeedY()
        {
            SLMPResult resultY = GetReg(cfg.REG_READ_SPEED_Y);
            return resultY.Status == SLMPStatus.SUCCESSFULLY ? resultY.Value : -1;
        }
        private int SetReg(string SetReg, string CheckReg, int value)
        {
            bool isSetOK = false;
            for (int i = 0; i < 5; i++)
            {
                var result = mSLMP.SetDevice2(SetReg, value);
                if (result.Status == SLMPStatus.SUCCESSFULLY)
                {
                    SLMPResult resultGet = mSLMP.GetDevice2(CheckReg);
                    if (resultGet.Status == SLMPStatus.SUCCESSFULLY && resultGet.Value == value)
                    {
                        isSetOK = true;
                        break;
                    }
                }
            }
            return isSetOK ? 0 : -2;
        }
        private SLMPResult GetReg(string GetReg)
        {
            SLMPResult result = new SLMPResult();
            for (int i = 0; i < 5; i++)
            {
                result = mSLMP.GetDevice2(GetReg);
                if (result.Status == SLMPStatus.SUCCESSFULLY)
                {
                    break;
                }
            }
            return result;
        }
    }
}
