﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Heal.SDK;
using System.Diagnostics;
using System.Threading;

namespace Heal.Devices.PLC
{
    public class TopAxis
    {
        Config.TopAxisCfg cfg = Config.TopAxisCfg.GetInstance();
        private SLMP mSLMP = new SLMP(Properties.Settings.Default.PLC_IP, Properties.Settings.Default.PLC_PORT);
        public int GoTop()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_TOP, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoBot()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_BOT, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoRight()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_RIGHT, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoLeft()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_LEFT, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int ResetGoTop()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_TOP, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int ResetGoBot()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_BOT, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int ResetGoRight()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_RIGHT, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int ResetGoLeft()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_LEFT, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int WriteArrayXY(Point[] P)
        {
            int status = SetReg(cfg.REG_WRITE_VALUE_INDEX_MODE2, cfg.REG_WRITE_VALUE_INDEX_MODE2, P.Length);
            if (status != 0)
                return -3;
            for (int i = 0; i < P.Length; i++)
            {
                Point crPoint = P[i];
                string reg_X = "D" + (Convert.ToInt32(cfg.REG_WRITE_VALUE_X_MODE2.Replace("D", "")) + i * 2).ToString();
                string reg_Y = "D" + (Convert.ToInt32(cfg.REG_WRITE_VALUE_Y_MODE2.Replace("D", "")) + i * 2).ToString();
                if (crPoint.X > cfg.LIMIT_VALUE_X || crPoint.Y > cfg.LIMIT_VALUE_Y || crPoint.X < 0 || crPoint.Y < 0)
                    return -1;
                int setX = SetReg(reg_X, reg_X, crPoint.X);
                if (setX == 0)
                {
                    int setY = SetReg(reg_Y, reg_Y, crPoint.Y);
                    if (setY != 0)
                    {
                        return setY;
                    }
                }
                else
                    return setX;
            }
            return 0;
        }
        public int SetAutoGoArrayXY()
        {
            ResetGoFinishMode2();
            var r = mSLMP.SetDevice(cfg.BIT_WRITE_FINISH_MODE2, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoXY(Point crPoint)
        {
           
            ResetGoFinish();
            if (crPoint.X > cfg.LIMIT_VALUE_X || crPoint.Y > cfg.LIMIT_VALUE_Y || crPoint.X < 0 || crPoint.Y < 0)
                return -1;
            int setX = SetReg(cfg.REG_WRITE_VALUE_X, cfg.REG_WRITE_VALUE_X, crPoint.X);
            if (setX == 0)
            {
                int setY = SetReg(cfg.REG_WRITE_VALUE_Y, cfg.REG_WRITE_VALUE_Y, crPoint.Y);
                if(setY == 0)
                {
                    var rt = mSLMP.SetDevice(cfg.BIT_WRITE_FINISH, 1);
                    return rt.Status == SLMPStatus.SUCCESSFULLY ? 0 : -3;
                }
                else
                {
                    return setY;
                }
            }
            else
                return setX;
        }
        public int GoArrayXYFinish(int TimeOut = 10000)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var r = mSLMP.GetDevice(cfg.BIT_GO_FINISH_MODE2);
            while (sw.ElapsedMilliseconds < TimeOut && (r.Status == SLMPStatus.FAIL || (r.Status == SLMPStatus.SUCCESSFULLY && r.Value == 0)))
            {
                Thread.Sleep(3);
                r = mSLMP.GetDevice(cfg.BIT_GO_FINISH_MODE2);
            }
            if (r.Status == SLMPStatus.SUCCESSFULLY && r.Value == 1)
            {
                mSLMP.SetDevice(cfg.BIT_GO_FINISH_MODE2, 0);
                return 0;
            }
            else
            {
                return -1;
            }
        }
        public int GoXYFinish(int TimeOut = 10000)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            var r = mSLMP.GetDevice(cfg.BIT_GO_FINISH);
            while (sw.ElapsedMilliseconds < TimeOut && (r.Status == SLMPStatus.FAIL || (r.Status== SLMPStatus.SUCCESSFULLY && r.Value == 0)))
            {
                Thread.Sleep(3);
                r = mSLMP.GetDevice(cfg.BIT_GO_FINISH);
            }
            if(r.Status == SLMPStatus.SUCCESSFULLY && r.Value == 1)
            {
                mSLMP.SetDevice(cfg.BIT_GO_FINISH, 0);
                return 0;
            }
            else
            {
                return -1;
            }
        }
        public int ResetGoFinish()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_FINISH, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int ResetGoFinishMode2()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_FINISH_MODE2, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoHome()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_HOME, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int SetSpeed(int SpeedX, int SpeedY)
        {
            if (SpeedX > cfg.LIMIT_SPEED_X || SpeedY > cfg.LIMIT_SPEED_Y || SpeedX < 0 || SpeedY < 0)
                return -1;
            int setX = SetReg(cfg.REG_WRITE_SPEED_X, cfg.REG_WRITE_SPEED_X, SpeedX);
            if (setX == 0)
            {
                int setY = SetReg(cfg.REG_WRITE_SPEED_Y, cfg.REG_WRITE_SPEED_Y, SpeedY);
                return setY;
            }
            else
                return setX;
        }
        public Point GetXY()
        {
            SLMPResult resultX = GetReg(cfg.REG_READ_VALUE_X);
            if (resultX.Status == SLMPStatus.SUCCESSFULLY)
            {
                SLMPResult resultY = GetReg(cfg.REG_READ_VALUE_Y);
                if (resultY.Status == SLMPStatus.SUCCESSFULLY)
                {
                    return new Point(resultX.Value, resultY.Value);
                }
                else
                {
                    return new Point(-1, -1);
                }
            }
            else
            {
                return new Point(-1, -1);
            }
        }
        public int GetSpeedX()
        {
            SLMPResult resultX = GetReg(cfg.REG_READ_SPEED_X);
            return resultX.Status == SLMPStatus.SUCCESSFULLY ? resultX.Value : -1;
        }
        public int GetSpeedY()
        {
            SLMPResult resultY = GetReg(cfg.REG_READ_SPEED_Y);
            return resultY.Status == SLMPStatus.SUCCESSFULLY ? resultY.Value : -1;
        }
        private int SetReg(string SetReg, string CheckReg, int value)
        {
            bool isSetOK = false;
            for (int i = 0; i < 5; i++)
            {
                var result = mSLMP.SetDevice2(SetReg, value);
                if (result.Status == SLMPStatus.SUCCESSFULLY)
                {
                    SLMPResult resultGet = mSLMP.GetDevice2(CheckReg);
                    if (resultGet.Status == SLMPStatus.SUCCESSFULLY && resultGet.Value == value)
                    {
                        isSetOK = true;
                        break;
                    }
                }
            }
            return isSetOK ? 0 : -2;
        }
        private SLMPResult GetReg(string GetReg)
        {
            SLMPResult result = new SLMPResult();
            for (int i = 0; i < 5; i++)
            {
                result = mSLMP.GetDevice2(GetReg);
                if (result.Status == SLMPStatus.SUCCESSFULLY)
                {
                    break;
                }
            }
            return result;
        }
    }
}
