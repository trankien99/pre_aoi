﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heal;
using Heal.SDK;
namespace Heal.Devices.PLC
{
    public class Send
    {
        private SLMP mSLMP = new SLMP(Properties.Settings.Default.PLC_IP, Properties.Settings.Default.PLC_PORT);
        Config.SendCfg cfg = Config.SendCfg.GetInstance();
        public int PanelPass()
        {
            var r = mSLMP.SetDevice(cfg.BIT_PASS, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int StartMachine()
        {
            var r = mSLMP.SetDevice(cfg.BIT_START_MACHINE, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int StopMachine()
        {
            var r = mSLMP.SetDevice(cfg.BIT_STOP_MACHINE, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int PanelFail()
        {
            var r = mSLMP.SetDevice(cfg.BIT_FAIL, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int ConfirmPanelPass()
        {
            var r = mSLMP.SetDevice(cfg.BIT_CONFIRM_PASS, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int ConfirmPanelFail()
        {
            var r = mSLMP.SetDevice(cfg.BIT_CONFIRM_FAIL, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int ReadCodeFail(bool Reset = false )
        {
            if(!Reset)
            {
                var r = mSLMP.SetDevice(cfg.BIT_SCAN_FAIL, 1);
                return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
            }
            else
            {
                var r = mSLMP.SetDevice(cfg.BIT_SCAN_FAIL,0);
                return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
            }
        }
        public int CaptureFail()
        {
            var r = mSLMP.SetDevice(cfg.BIT_CAMERA_FAIL, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int SetLightFail()
        {
            var r = mSLMP.SetDevice(cfg.BIT_LIGHT_FAIL, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int Login()
        {
            var r = mSLMP.SetDevice(cfg.REG_LOGIN, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public bool IsLogin()
        {
            var r = mSLMP.GetDevice(cfg.REG_LOGIN);
            return r.Value == 0 ? false : true;
        }
        public int Logout()
        {
            var r = mSLMP.SetDevice(cfg.REG_LOGIN, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
    }
}
