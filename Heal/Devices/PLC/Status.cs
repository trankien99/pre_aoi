﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Heal;
using System.Diagnostics;
using System.Threading;
using Heal.SDK;

namespace Heal.Devices.PLC
{
    public class Status
    {
        Config.StatusCfg cfg = Config.StatusCfg.GetInstance();
       Config.SendCfg cfgSend =Config.SendCfg.GetInstance();
        private SLMP mSLMP = new SLMP(Properties.Settings.Default.PLC_IP, Properties.Settings.Default.PLC_PORT);
        public int Ping()
        {
            return mSLMP.GetPing();
        }
        public int CanRun()
        {
            var r = mSLMP.GetDevice(cfg.BIT_IS_START);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int ResetPanel(bool Reset = false)
        {
            if(!Reset)
            {
                var r = mSLMP.GetDevice(cfg.BIT_RESET_PANEL);
                return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
            }
            else
            {
                var r = mSLMP.SetDevice(cfg.BIT_RESET_PANEL, 0);
                return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
            }
        }
        public int HasPanel(bool Reset = false)
        {
            if (!Reset)
            {
                var r = mSLMP.GetDevice(cfgSend.BIT_HAS_PANEL_VI);
                return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
            }
            else
            {
                var r = mSLMP.SetDevice(cfgSend.BIT_HAS_PANEL_VI, 0);
                return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
            }
        }
        public int GetDevice(string M)
        {
            return mSLMP.GetDevice(M).Value;
        }
        public int GetDevice2(string M)
        {
            return mSLMP.GetDevice2(M).Value;
        }
        public int SetDevice2(string M, int value)
        {
            return mSLMP.SetDevice2(M, value).Value;
        }
        public int IsOpenDoor()
        {
            
                var r = mSLMP.GetDevice(cfg.BIT_DOOR);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int IsEmergency()
        {
            var r = mSLMP.GetDevice(cfg.BIT_EMERGENCY);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int IsIssue()
        {
            var r = mSLMP.GetDevice(cfg.BIT_ALARM_MACHINE);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int IsStopped()
        {
            var r = mSLMP.GetDevice(cfg.BIT_CHECK_RUN2STOP);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int PanelPosition()
        {
            var r = mSLMP.GetDevice2(cfg.REG_PANEL_POSITION);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
    }
}
