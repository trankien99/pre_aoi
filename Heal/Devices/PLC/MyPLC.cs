﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heal.Devices.PLC
{
    public class MyPLC
    {
        public BotAxis BOT { get; set; } 
        public TopAxis TOP { get; set; }
        public Conveyor CONVEYOR { get; set; }
        public Status STATUS { get; set; }
        public Send SEND { get; set; }
        private static MyPLC plc = null;
        private static Object lockObj = new Object();
        public MyPLC()
        {
            this.BOT = new BotAxis();
            this.TOP = new TopAxis();
            this.CONVEYOR = new Conveyor();
            this.STATUS = new Status();
            this.SEND = new Send();
        }
        public static MyPLC GetInstance()
        {
            if(plc == null)
            {
                lock(lockObj)
                {
                    plc = new MyPLC();
                }
            }
            return plc;
        }
    }
}
