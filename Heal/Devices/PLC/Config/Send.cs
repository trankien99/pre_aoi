﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using NLog;
using Heal.SDK;

namespace Heal.Devices.PLC.Config
{
    public class SendCfg
    {
        private static Logger mLog = LogCtl.GetInstance();
        public int LIMIT_X = 40100;
        public int LIMIT_Y = 40100;
        public int SPEED_RUN_X = 200000;
        public int SPEED_RUN_Y = 200000;
        public int SPEED_JOGE_X = 10000;
        public int SPEED_JOGE_Y = 10000;
        public string REG_LOGIN = "S20";

        public string BIT_AUTO_START = "M68";
        public string BIT_HAS_PANEL_VI = "M140";
        public string BIT_PASS = "M2994";
        public string BIT_FAIL = "M2992";
        public string BIT_CONFIRM_PASS = "M2993";
        public string BIT_CONFIRM_FAIL = "M2987";
        public string BIT_SCAN_FAIL = "M2980";
        public string BIT_CAMERA_FAIL = "M2981";
        public string BIT_LIGHT_FAIL = "M2982";
        public string BIT_START_MACHINE = "M68";
        public string BIT_STOP_MACHINE = "M69";

        private static SendCfg send;
        public static SendCfg GetInstance()
        {
            return SendCfg.Load();
        }
        private static SendCfg Load()
        {
            if (send == null)
            {
                if (File.Exists(Define.SendCfgPath))
                {
                    string s = File.ReadAllText(Define.SendCfgPath);
                    try
                    {
                        send = JsonConvert.DeserializeObject<SendCfg>(s);
                    }
                    catch (Exception ex)
                    {
                        mLog.Error(ex.Message);
                    }
                }
            }
            if (send == null)
            {
                send = new SendCfg();
                send.Save();
            }
            return send;
        }
        public void Save()
        {
            if (send == null)
            {
                send = SendCfg.Load();
            }
            string s = JsonConvert.SerializeObject(send);
            string path = Define.BotAxisCfgPath;
            string[] name = path.Split('\\');
            string dir = path.Replace(name[name.Length - 1], "");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            File.WriteAllText(Define.SendCfgPath, s);
        }
    }
}
