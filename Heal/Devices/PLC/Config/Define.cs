﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heal.Devices.PLC.Config
{
    public class Define
    {
        public static string TopAxisCfgPath = Properties.Settings.Default.APP_CONFIG_PATH + "\\PLCConfig\\TopAxis.json";
        public static string BotAxisCfgPath = Properties.Settings.Default.APP_CONFIG_PATH + "\\PLCConfig\\BotAxis.json";
        public static string ConveyorCfgPath = Properties.Settings.Default.APP_CONFIG_PATH + "\\PLCConfig\\Conveyor.json";
        public static string StatusCfgPath = Properties.Settings.Default.APP_CONFIG_PATH + "\\PLCConfig\\Status.json";
        public static string SendCfgPath = Properties.Settings.Default.APP_CONFIG_PATH + "\\PLCConfig\\Send.json";
    }
}
