﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using NLog;
using Heal.SDK;

namespace Heal.Devices.PLC.Config
{
    public class BotAxisCfg
    {
        private static Logger mLog = LogCtl.GetInstance();
        public int LIMIT_VALUE_X = 98000;
        public int LIMIT_VALUE_Y = 106000;
        public int LIMIT_SPEED_X = 200000;
        public int LIMIT_SPEED_Y = 200000;
        public int SPEED_RUN_X = 80000;
        public int SPEED_RUN_Y = 80000;
        public int SPEED_JOGE_X = 10000;
        public int SPEED_JOGE_Y = 10000;
        public string REG_WRITE_VALUE_X = "D1006";
        public string REG_READ_VALUE_X = "D1206";
        public string REG_WRITE_VALUE_Y = "D1004";
        public string REG_READ_VALUE_Y = "D1204";
        public string REG_WRITE_SPEED_X = "D214";
        public string REG_READ_SPEED_X = "D214";
        public string REG_WRITE_SPEED_Y = "D212";
        public string REG_READ_SPEED_Y = "D212";
        public string BIT_WRITE_FINISH = "M151";
        public string BIT_GO_FINISH = "M152";
        public string BIT_GO_TOP = "M32";
        public string BIT_GO_BOT = "M30";
        public string BIT_GO_LEFT = "M36";
        public string BIT_GO_RIGHT = "M34";
        public string BIT_GO_HOME = "M81";
        private static BotAxisCfg botAxis;
        public static BotAxisCfg GetInstance()
        {
            return BotAxisCfg.Load();
        }
        private static BotAxisCfg Load()
        {
            if (botAxis == null)
            {
                if (File.Exists(Define.BotAxisCfgPath))
                {
                    string s = File.ReadAllText(Define.BotAxisCfgPath);
                    try
                    {
                        botAxis = JsonConvert.DeserializeObject<BotAxisCfg>(s);
                    }
                    catch (Exception ex)
                    {
                        mLog.Error(ex.Message);
                    }
                }
            }  
            if (botAxis == null)
            {
                botAxis = new BotAxisCfg();
                botAxis.Save();
            }
            return botAxis;
        }
        public void Save()
        {
            if (botAxis == null)
            {
                botAxis = BotAxisCfg.Load();
            }
            string s = JsonConvert.SerializeObject(botAxis);
            string path = Define.BotAxisCfgPath;
            string[] name = path.Split('\\');
            string dir = path.Replace(name[name.Length - 1], "");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            File.WriteAllText(Define.BotAxisCfgPath, s);
        }
    }
}
