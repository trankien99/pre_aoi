﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using NLog;
using Heal.SDK;


namespace Heal.Devices.PLC.Config
{
    public class TopAxisCfg
    {
        private static Logger mLog = LogCtl.GetInstance();
        public int LIMIT_VALUE_X = 41000;
        public int LIMIT_VALUE_Y = 41000;
        public int LIMIT_SPEED_X = 200000;
        public int LIMIT_SPEED_Y = 200000;
        public int SPEED_RUN_X = 200000;
        public int SPEED_RUN_Y = 200000;
        public int SPEED_JOGE_X = 10000;
        public int SPEED_JOGE_Y = 10000;
        public string REG_WRITE_VALUE_X = "D1002";
        public string REG_READ_VALUE_X = "D1202";
        public string REG_WRITE_VALUE_Y = "D1000";
        public string REG_READ_VALUE_Y = "D1200";
        public string REG_WRITE_SPEED_X = "D204";
        public string REG_READ_SPEED_X = "D204";
        public string REG_WRITE_SPEED_Y = "D202";
        public string REG_READ_SPEED_Y = "D202";
        public string BIT_WRITE_FINISH = "M141";
        public string BIT_GO_FINISH = "M142";
        public string BIT_GO_TOP = "M12";
        public string BIT_GO_BOT = "M10";
        public string BIT_GO_LEFT = "M16";
        public string BIT_GO_RIGHT = "M14";
        public string BIT_GO_HOME = "M80";
        public string REG_WRITE_VALUE_X_MODE2 = "D2500";
        public string REG_WRITE_VALUE_Y_MODE2 = "D2000";
        public string REG_WRITE_VALUE_INDEX_MODE2 = "D3000";
        public string BIT_WRITE_FINISH_MODE2 = "M143";
        public string BIT_GO_FINISH_MODE2 = "M145";
        private static TopAxisCfg topAxis;
        public static TopAxisCfg GetInstance()
        {
            return TopAxisCfg.Load();
        }
        private static TopAxisCfg Load()
        {
            if (topAxis == null)
            {
                if (File.Exists(Define.TopAxisCfgPath))
                {
                    string s = File.ReadAllText(Define.TopAxisCfgPath);
                    try
                    {
                        topAxis = JsonConvert.DeserializeObject<TopAxisCfg>(s);
                    }
                    catch (Exception ex)
                    {
                        mLog.Error(ex.Message);
                    }
                }
            }
            if (topAxis == null)
            {
                topAxis = new TopAxisCfg();
                topAxis.Save();
            }
            return topAxis;
        }
        public void Save()
        {
            if (topAxis == null)
            {
                topAxis = TopAxisCfg.Load();
            }
            string s = JsonConvert.SerializeObject(topAxis);
            string path = Define.BotAxisCfgPath;
            string[] name = path.Split('\\');
            string dir = path.Replace(name[name.Length - 1], "");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            File.WriteAllText(Define.TopAxisCfgPath, s);
        }
    }
}
