﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using NLog;
using Heal.SDK;

namespace Heal.Devices.PLC.Config
{
    public class StatusCfg
    {
        private static Logger mLog = LogCtl.GetInstance();

        public string BIT_EMERGENCY = "M2999";
        public string BIT_DOOR = "M2998";
        public string BIT_ALARM_MACHINE = "M2995";
        public string BIT_CHECK_RUN2STOP = "M2985";
        public string BIT_MACHINE_ISSUE = "M2984";
        public string BIT_HAS_PANEL_ON_MACHINE = "M2100";
        public string BIT_IS_START = "M0";
        public string REG_PANEL_POSITION = "D262";
        public string BIT_RESET_PANEL = "M79";

        private static StatusCfg status;
        public static StatusCfg GetInstance()
        {
            return StatusCfg.Load();
        }
        private static StatusCfg Load()
        {
            if (status == null)
            {
                if (File.Exists(Define.StatusCfgPath))
                {
                    string s = File.ReadAllText(Define.StatusCfgPath);
                    try
                    {
                        status = JsonConvert.DeserializeObject<StatusCfg>(s);
                    }
                    catch (Exception ex)
                    {
                        mLog.Error(ex.Message);
                    }
                }
            }
            if (status == null)
            {
                status = new StatusCfg();
                status.Save();
            }
            return status;
        }
        public void Save()
        {
            if (status == null)
            {
                status = StatusCfg.Load();
            }
            string s = JsonConvert.SerializeObject(status);
            string path = Define.BotAxisCfgPath;
            string[] name = path.Split('\\');
            string dir = path.Replace(name[name.Length - 1], "");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            File.WriteAllText(Define.StatusCfgPath, s);
        }
    }
}
