﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using NLog;
using Heal.SDK;

namespace Heal.Devices.PLC.Config
{
    public class ConveyorCfg
    {

        public int LIMIT_VALUE = 1980000;
        public int LIMIT_SPEED = 35000;
        public int SPEED_RUN = 35000;
        public int SPEED_JOGE = 30000;
        public string REG_READ_SPEED = "D222";
        public string REG_WRITE_SPEED = "D222";
        public string REG_WRITE_VALUE = "D1008";
        public string REG_READ_VALUE = "D1208";
        public string REG_COUNT_PANEL = "D262";
        public string BIT_WRITE_FINISH = "M147";
        public string BIT_GO_FINISH = "M148";
        public string BIT_GO_TOP = "M52";
        public string BIT_GO_BOT = "M50";
        public string BIT_LOAD = "M77";
        public string BIT_UNLOAD = "M78";
        public string BIT_GO_HOME = "M82";

        // ---------------------------------- 
        private static Logger mLog = LogCtl.GetInstance();
        private static ConveyorCfg conveyor;
        public static ConveyorCfg GetInstance()
        {
            return ConveyorCfg.Load();
        }
        private static ConveyorCfg Load()
        {
            if (conveyor == null)
            {
                if (File.Exists(Define.ConveyorCfgPath))
                {
                    string s = File.ReadAllText(Define.ConveyorCfgPath);
                    try
                    {
                        conveyor = JsonConvert.DeserializeObject<ConveyorCfg>(s);
                    }
                    catch (Exception ex)
                    {
                        mLog.Error(ex.Message);
                    }
                }
            }
            if (conveyor == null)
            {
                conveyor = new ConveyorCfg();
                conveyor.Save();
            }
            return conveyor;
        }
        public void Save()
        {
            if (conveyor == null)
            {
                conveyor = ConveyorCfg.Load();
            }
            string s = JsonConvert.SerializeObject(conveyor);
            string path = Define.BotAxisCfgPath;
            string[] name = path.Split('\\');
            string dir = path.Replace(name[name.Length - 1], "");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            File.WriteAllText(Define.ConveyorCfgPath, s);
        }
    }
}
