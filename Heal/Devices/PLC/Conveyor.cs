﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Heal;
using Heal.SDK;

namespace Heal.Devices.PLC
{
    public class Conveyor
    {
        Config.ConveyorCfg cfg = Config.ConveyorCfg.GetInstance();
        private SLMP mSLMP = new SLMP(Properties.Settings.Default.PLC_IP, Properties.Settings.Default.PLC_PORT);
        public int Load()
        {
            var r = mSLMP.SetDevice(cfg.BIT_LOAD, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int Unload()
        {
            var r = mSLMP.SetDevice(cfg.BIT_UNLOAD, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoConveyor(int value)
        {
            if (value > cfg.LIMIT_VALUE|| value < 0)
                return -1;
            int r = SetReg(cfg.REG_WRITE_VALUE, cfg.REG_WRITE_VALUE, value);
            if (r != 0)
                return r;
            var writeStt = mSLMP.SetDevice(cfg.BIT_WRITE_FINISH, 1);
            return writeStt.Status == SLMPStatus.SUCCESSFULLY ? 0 : -3;
        }
        
        public int GoTOP()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_TOP, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoBOT()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_BOT, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int ResetGoTOP()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_TOP, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int ResetGoBOT()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_BOT, 0);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int GoConveyorFinish()
        {
            var r = mSLMP.GetDevice(cfg.BIT_GO_FINISH);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int GoHome()
        {
            var r = mSLMP.SetDevice(cfg.BIT_GO_HOME, 1);
            return r.Status == SLMPStatus.SUCCESSFULLY ? 0 : -1;
        }
        public int SetSpeed(int speed)
        {
            if (speed > cfg.LIMIT_SPEED || speed < 0)
                return -1;
            return SetReg(cfg.REG_WRITE_SPEED, cfg.REG_WRITE_SPEED, speed);
        }
        public int SetSpeedRun()
        {
            return SetReg(cfg.REG_WRITE_SPEED, cfg.REG_WRITE_SPEED, cfg.SPEED_RUN);
        }
        public int SetSpeedJoge()
        {
            return SetReg(cfg.REG_WRITE_SPEED, cfg.REG_WRITE_SPEED, cfg.SPEED_JOGE);
        }
        public int CountPanel()
        {
            var r = mSLMP.GetDevice(cfg.REG_COUNT_PANEL);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int GetConveyor()
        {
            var r = mSLMP.GetDevice2(cfg.REG_READ_VALUE);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        public int GetSpeed()
        {
            var r = mSLMP.GetDevice2(cfg.REG_READ_SPEED);
            return r.Status == SLMPStatus.SUCCESSFULLY ? r.Value : -1;
        }
        private int SetReg(string SetReg, string CheckReg, int value)
        {
            bool isSetOK = false;
            for (int i = 0; i < 5; i++)
            {
                var result = mSLMP.SetDevice2(SetReg, value);
                if (result.Status == SLMPStatus.SUCCESSFULLY)
                {
                    SLMPResult resultGet = mSLMP.GetDevice2(CheckReg);
                    if (resultGet.Status == SLMPStatus.SUCCESSFULLY && resultGet.Value == value)
                    {
                        isSetOK = true;
                        break;
                    }
                }
            }
            return isSetOK ? 0 : -2;
        }
    }
}
