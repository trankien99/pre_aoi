﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Heal.SDK;

namespace Heal.Devices.Camera
{
    public class MyCamera
    {
        private static HikCamera mCamera = null;
        private static string[] mListCameraName = null;
        public static string[] GetCameraNames()
        {
            mListCameraName = HikCamera.GetCameraNames();
            return mListCameraName;
        }
        public static HikCamera GetInstance()
        {
            GetCameraNames();
            if (mCamera == null)
            {
                if (mListCameraName != null)
                {
                    if (mListCameraName.Length > 0)
                    {
                        mCamera = new HikCamera(0);
                        mCamera.Open();
                        if (mCamera.IsOpen)
                        {
                            //string stPath =/* Properties.Sett*/ings.Default.CAMERA_CONFIG_PATH;
                            //if (File.Exists(stPath))
                            //{
                            //    mCamera.FeatureLoad(stPath);
                            //}
                        }
                        mCamera.Close();
                    }
                }
            }
            return mCamera;
        }
    }
}
