﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Heal.Models;

namespace Heal.View.New
{
    /// <summary>
    /// Interaction logic for ProgramInfoPage.xaml
    /// </summary>
    public partial class NewBoard : Page, INotifyPropertyChanged
    {
        public NewProgramWindow mParent { get; set; }
        public string BoardName 
        {
            get => mParent.NewProgram.Name;
            set
            {
                mParent.NewProgram.Name = value;
                OnPropertyChanged();
            }
        }
        public int Side
        {
            get => Convert.ToInt32(mParent.NewProgram.Side);
            set
            {
                mParent.NewProgram.Side = (BoardSide)value;
                OnPropertyChanged();
            }
        }
        public double BoardLength
        {
            get => Math.Round(mParent.NewProgram.Width, 0);
            set
            {
                mParent.NewProgram.Width = value;
                OnPropertyChanged();
            }
        }
        public double BoardWidth
        {
            get => Math.Round(mParent.NewProgram.Height, 0);
            set
            {
                mParent.NewProgram.Height = value;
                OnPropertyChanged();
            }
        }
        public double BoardThickness
        {
            get => mParent.NewProgram.Thickness;
            set
            {
                mParent.NewProgram.Thickness = value;
                OnPropertyChanged();
            }
        }
        public int PCBBlocks
        {
            get => mParent.NewProgram.PCBBlocks;
            set
            {
                mParent.NewProgram.PCBBlocks = value;
            }
        }
        public NewBoard(NewProgramWindow p)
        {
            mParent = p;
            InitializeComponent();
            this.DataContext = this;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            mParent.FrNavigation.Navigate(new CadImport(mParent));
        }
    }
    public class IsEnableNext : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            bool isEnable = true;
            for (int i = 0; i < values.Length; i++)
            {
                if(string.IsNullOrEmpty((string)values[i]))
                {
                    isEnable = false;
                }
            }
            return isEnable;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return new object[0];
        }
    }
    public class BoardSide2Int : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return System.Convert.ToInt32(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((int)value)
            {
                case 0:
                    return BoardSide.TOP;
                case 1:
                    return BoardSide.TOP;
                default:
                    break;
            }
            return BoardSide.TOP;
        }
    }
}
