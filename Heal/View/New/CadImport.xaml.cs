﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using form = System.Windows.Forms;
using System.IO;
using System.ComponentModel;
using Heal.Models;

namespace Heal.View.New
{
    /// <summary>
    /// Interaction logic for CadImport.xaml
    /// </summary>
    public partial class CadImport : Page, INotifyPropertyChanged
    {
        public NewProgramWindow mParent { get; set; }
        public List<CadItem> CadItems = new List<CadItem>();
        public string FilePath
        {
            get => mParent.FilePath;
            set
            {
                mParent.FilePath = value;
            }
        }
        public string FileContent
        {
            get => mParent.FileContent;
            set
            {
                mParent.FileContent = value;
            }
        }
        public CadImport(NewProgramWindow p)
        {
            mParent = p;
            InitializeComponent();
            this.DataContext = this;
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            mParent.FrNavigation.Navigate(new NewBoard(mParent));
        }

        private void btBrowser_Click(object sender, RoutedEventArgs e)
        {
            using (form.OpenFileDialog dialog = new form.OpenFileDialog())
            {
                if(File.Exists(FilePath))
                {
                    dialog.FileName = FilePath;
                }
                dialog.Filter = "Text file | *.txt";
                if (dialog.ShowDialog() == form.DialogResult.OK)
                {
                    FileContent = File.ReadAllText(dialog.FileName);
                    FilePath = dialog.FileName;
                    OnPropertyChanged();
                }
            }

        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            List<CadItem> cadItems = CadItem.GetCadFromString(FileContent);
            mParent.NewProgram.LoadCad(cadItems);
            mParent.Created = true;
            mParent.Close();
        }
    }
}
