﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Navigation;
using Heal.Models;
using System.ComponentModel;

namespace Heal.View.New
{
    /// <summary>
    /// Interaction logic for NewProgramWindow.xaml
    /// </summary>
    public partial class NewProgramWindow : Window, INotifyPropertyChanged
    {
        public MyBoard NewProgram { get; set; }
        public bool Created { get; set; }
        public NavigationService FrNavigation { get; set; }
        public string FilePath { get; set; }
        public string FileContent { get; set; }
        public NewProgramWindow()
        {
            InitializeComponent();
            NewProgram = new MyBoard();
            FrNavigation = frMain.NavigationService;
            FrNavigation.Navigate(new NewBoard(this));
        }
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

}
