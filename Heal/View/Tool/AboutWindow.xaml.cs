﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Heal.Models;

namespace Heal.View.Tool
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        public AboutWindow()
        {
            InitializeComponent();
            FileInfo fi = new FileInfo($"{Assembly.AssemblyProduct}.exe");
            lbDetailVersion.Content = fi.LastWriteTime.ToString("(wddMM)");
            lbDateBuild.Content = fi.LastWriteTime.ToString("MMMM dd, yyyy");
            lbTimeBuild.Content = fi.LastWriteTime.ToString("HH:mm:ss");
            lbVersion.Content = Assembly.AssemblyVersion;
            lbProduct.Content = Assembly.AssemblyProduct;
            lbCopyRight.Content = Assembly.AssemblyCopyright + ", FII Automation & AIOT Team";
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink hyperlink = (sender as Hyperlink);
            System.Diagnostics.Process.Start(hyperlink.NavigateUri.OriginalString);
        }
    }
}
