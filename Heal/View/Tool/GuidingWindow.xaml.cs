﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Heal.Utils;

namespace Heal.View.Tool
{
    /// <summary>
    /// Interaction logic for GuidingWindow.xaml
    /// </summary>
    public partial class GuidingWindow : Window
    {
        private System.Timers.Timer mTimer = new System.Timers.Timer(500);
        private bool mIsInTimer = false;
        public bool IsShowing { get; set; }
        private string[] mAlarmBits = new string[0];
        private string[] mAlarmMsg = new string[0];
        private string[] mAlarmSolution = new string[0];
        private Devices.PLC.MyPLC mPLCComm = new Devices.PLC.MyPLC();
        int count = 0;
        public GuidingWindow()
        {
            InitializeComponent();
            LoadUI();
            
        }
        public void SetGuiding(Guilding G)
        {
            this.Dispatcher.Invoke(() => {
                lbIssueCode.Content = G.Code;
                lbIssueName.Text = G.Name;
                lbIssueSolution.Text = G.Solution;
                if (string.IsNullOrEmpty(G.PdfFile))
                    btPDF.IsEnabled = false;
                else
                {

                }
                if (string.IsNullOrEmpty(G.GifFile))
                    btGif.IsEnabled = false;
                else
                {

                }
            });
            
        }
        public void LoadUI()
        {
            mAlarmBits = MachineIssueForm.GetErrorBits();
            mAlarmMsg = MachineIssueForm.GetErrorMessages();
            mAlarmSolution = MachineIssueForm.GetErrorSolutions();
            mTimer.Elapsed += OnTimedEvent;
            mTimer.Enabled = true;
            IsShowing = true;
        }

        private void OnTimedEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (mIsInTimer)
                return;
            mIsInTimer = true;
            mTimer.Enabled = false;
            QueryAlarm();
            mIsInTimer = false;
            mTimer.Enabled = IsShowing;
        }

        private void QueryAlarm()
        {
            string erCodes = string.Empty;
            string erMsg = string.Empty;
            string erSolution = string.Empty;
            MachineAlarmResult result = new MachineAlarmResult();
            result.Success = true;
            for (int i = 0; i < mAlarmBits.Length; i++)
            {
                int flag = mPLCComm.STATUS.GetDevice(mAlarmBits[i]);
                if (flag == 1)
                {
                    if (!string.IsNullOrEmpty(result.ErCode))
                    {
                        result.ErCode += ", ";
                    }
                    result.ErCode += mAlarmBits[i];
                    if (!string.IsNullOrEmpty(result.ErMessages))
                    {
                        result.ErMessages += System.Environment.NewLine;
                    }
                    result.ErMessages += string.Format("({0}): {1}", mAlarmBits[i], mAlarmMsg[i]);
                    if (!string.IsNullOrEmpty(result.ErSolution))
                    {
                        result.ErSolution += System.Environment.NewLine;
                    }
                    if (mAlarmSolution[i] != string.Empty)
                    {
                        result.ErSolution += string.Format("({0}): {1}", mAlarmBits[i], mAlarmSolution[i]);
                    }
                    else
                    {
                        result.ErSolution += string.Format("({0}): Not yet solution...", mAlarmBits[i]);
                    }
                }
                else if (flag == -1)
                {
                    result.Success = false;
                    break;
                }
            }
            if (mPLCComm.STATUS.IsIssue() == 0)
            {
                this.Dispatcher.Invoke(new Action(() =>
                {
                    if (string.IsNullOrEmpty((string)lbIssueCode.Content) && string.IsNullOrEmpty((string)lbIssueCode.Content))
                        count++;
                    if (count == 2)
                    {
                        count = 0;
                        IsShowing = false;
                        this.Hide();
                    }
                }));
            }
            if (result.Success)
            {
                Guilding G = new Guilding();
                G.Code = result.ErCode;
                G.Name = result.ErMessages;
                G.Solution = result.ErSolution;
                SetGuiding(G);

            }
        }
        private void Window_Initialized(object sender, EventArgs e)
        {

        }
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            IsShowing = false;
        }
    }
    public class Guilding
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Solution { get; set; }
        public string PdfFile { get; set; }
        public string GifFile { get; set; }
    }
    public class MachineAlarmResult
    {
        public string ErCode { get; set; }
        public string ErMessages { get; set; }
        public string ErSolution { get; set; }
        public bool Success { get; set; }
    }
}
