﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.ComponentModel;


namespace Heal.View.Tool
{
    /// <summary>
    /// Interaction logic for Loading.xaml
    /// </summary>
    public partial class Loading : Window, INotifyPropertyChanged
    {
        private string _status = "Loading...";
        public bool IsTimeout { get; set; }
        private BackgroundWorker bgWork = null;
        public int mTimeout = 0;
        public bool IsClosed { get; set; }
        public Loading(int Timeout = 0)
        {
            InitializeComponent();
            this.DataContext = this;
            mTimeout = Timeout;
            if (Timeout > 0)
            {
                bgWork = new BackgroundWorker();
                bgWork.DoWork += BgWork_DoWork;
                bgWork.RunWorkerCompleted += BgWork_RunWorkerCompleted; 
                bgWork.RunWorkerAsync();
            }
        }
        public void Kill()
        {
            this.Dispatcher.Invoke(() => {
                this.Close();
            });
        }
        private void BgWork_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(!IsClosed)
                IsTimeout = true;
            this.Dispatcher.Invoke(() => {
                this.Close();
            });
        }

        private void BgWork_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(mTimeout);
        }
        public event PropertyChangedEventHandler PropertyChanged; 
        public string Status
        {
            get => _status;
            set
            {
                _status = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs("Status"));
                }
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            IsClosed = true;
            if(bgWork != null)
                bgWork.Dispose();
        }
    }
}
