﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using form = System.Windows.Forms;
using Heal.Models.Calib;
using System.IO;
using System.Threading;
using System.ComponentModel;

namespace Heal.View.Tool
{
    /// <summary>
    /// Interaction logic for Parameter.xaml
    /// </summary>
    public partial class Parameter : Window, INotifyPropertyChanged
    {
        private Models.Calib.Calibration _Calibration = Models.Calib.Calibration.GetInstance();
        private Properties.Settings _Param = Properties.Settings.Default;

        
        public bool ByPassMode
        {
            get => _Param.RunningMode == 0;
            set { _Param.RunningMode = Convert.ToInt32(!value); _Param.Save(); }
        }
        public bool InspectionMode
        {
            get => _Param.RunningMode == 1;
            set { _Param.RunningMode = Convert.ToInt32(value); _Param.Save(); }
        }
        public bool LocalRepair
        {
            get => _Param.VerifyMode == 0;
            set { _Param.VerifyMode = Convert.ToInt32(!value); _Param.Save(); }
        }
        public bool StationRepair
        {
            get => _Param.VerifyMode == 1;
            set { _Param.VerifyMode = Convert.ToInt32(value); _Param.Save(); }
        }
        public string PLCIP
        {
            get => _Param.PLC_IP;
            set { _Param.PLC_IP = value; _Param.Save(); }
        }
        public int PLCPort
        {
            get => _Param.PLC_PORT;
            set { _Param.PLC_PORT = value; _Param.Save(); }
        }
        public string RepairStationIP
        {
            get => _Param.RepairStationIP;
            set { _Param.RepairStationIP = value; _Param.Save(); }
        }
        public int RepairStationPort
        {
            get => _Param.RepairStationPort;
            set { _Param.RepairStationPort = value; _Param.Save(); }
        }
        public int SaveHours
        {
            get => _Param.SaveHours;
            set { _Param.SaveHours = value; _Param.Save(); }
        }
        public string SavePath
        {
            get => _Param.SavePath;
            set { _Param.SavePath = value; _Param.Save(); }
        }
        public string LineName
        {
            get => _Param.Line;
            set { _Param.Line = value; _Param.Save(); }
        }
        public double DPI
        {
            get => _Param.DPI;
            set { _Param.DPI = value; _Param.Save(); }
        }
        public int ExposureTime
        {
            get => _Param.ExposureTime;
            set { _Param.ExposureTime = value; _Param.Save(); }
        }
        public int ImagingSpeed
        {
            get => _Param.ImagingSpeed;
            set { _Param.ImagingSpeed = value; _Param.Save(); }
        }
        public int ImageSizeWidth
        {
            get => _Param.ImageSize.Width;
            set { _Param.ImageSize = new System.Drawing.Size(value, _Param.ImageSize.Height); _Param.Save(); }
        }
        public int ImageSizeHeight
        {
            get => _Param.ImageSize.Height;
            set { _Param.ImageSize = new System.Drawing.Size( _Param.ImageSize.Width, value); _Param.Save(); }
        }
        public double MaxBoardWidth
        {
            get => _Param.Max_Board_Width;
            set { _Param.Max_Board_Width = value; _Param.Save(); }
        }
        public double MaxBoardHeight
        {
            get => _Param.Max_Board_Height;
            set { _Param.Max_Board_Height = value; _Param.Save(); }
        }
        public int OriginX
        {
            get => _Param.OriginCoordinate.X;
            set { _Param.OriginCoordinate = new System.Drawing.Point(value, _Param.OriginCoordinate.Y); _Param.Save(); }
        }
        public int OriginY
        {
            get => _Param.OriginCoordinate.Y;
            set { _Param.OriginCoordinate = new System.Drawing.Point( _Param.OriginCoordinate.X, value); _Param.Save(); }
        }
        public double DistanceTrgX
        {
            get => _Param.DistanceTrgX;
            set { _Param.DistanceTrgX = value; _Param.Save(); }
        }
        public double DistanceTrgY
        {
            get => _Param.DistanceTrgY;
            set { _Param.DistanceTrgY = value; _Param.Save(); }
        }
        public double PPMX
        {
            get => _Param.PPMX;
            set { _Param.PPMX = value; _Param.Save(); }
        }
        public double PPMY
        {
            get => _Param.PPMY;
            set { _Param.PPMY = value; _Param.Save(); }
        }
        public int LightCH1
        {
            get => _Param.LightCH1;
            set { _Param.LightCH1 = value; _Param.Save(); }
        }
        public int LightCH2
        {
            get => _Param.LightCH2;
            set { _Param.LightCH2 = value; _Param.Save(); }
        }
        public int LightCH3
        {
            get => _Param.LightCH3;
            set { _Param.LightCH3 = value; _Param.Save(); }
        }
        public int LightCH4
        {
            get => _Param.LightCH4;
            set { _Param.LightCH4 = value; _Param.Save(); }
        }
        public Parameter()
        {
            InitializeComponent();
            this.DataContext = this;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
        }
        private void btUpdateCalib_Click(object sender, RoutedEventArgs e)
        {
            using (form.OpenFileDialog dialog = new form.OpenFileDialog())
            {
                dialog.FileName = "/mst/calib.json";
                dialog.Filter = "Json file | *.json";
                if(dialog.ShowDialog() == form.DialogResult.OK)
                {
                    Loading mLoading = new Loading();
                    mLoading.Status = "Return to Snapshot Original Position...";
                    Thread thread = new Thread(() => {
                        if (!Directory.Exists("mst"))
                            Directory.CreateDirectory("mst");
                        var calib = Heal.Models.Calib.Calibration.LoadMatrix(dialog.FileName);
                        this.Dispatcher.Invoke(() => {
                            mLoading.Kill();
                        });
                        if (calib != null)
                        {
                            _Calibration = calib;
                            File.Copy(dialog.FileName, _Param.CalibPath, true);
                            MessageBox.Show("Update calibration success!", "Calibration", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show("Update calibration failed!", "Calibration", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        
                    });
                    thread.Start();
                    mLoading.ShowDialog();
                    OnPropertyChanged();
                }
            }
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                Resource.Convertor.Action.UpdateBindingSourceTextBox(sender);
            }
        }

        private void btSavePath_Click(object sender, RoutedEventArgs e)
        {
            using (form.FolderBrowserDialog dialog = new form.FolderBrowserDialog())
            {
                dialog.SelectedPath = _Param.SavePath;
                if(dialog.ShowDialog() ==  form.DialogResult.OK)
                {
                    _Param.SavePath = dialog.SelectedPath;
                    _Param.Save();
                    OnPropertyChanged();
                }
            }
        }
    }
}
