﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Heal.Models.Alg;
using Heal.Models;
using System.ComponentModel;
using Emgu.CV;
using Emgu.CV.Structure;
using Heal.Resource.Control;


namespace Heal.View.Setup
{
    /// <summary>
    /// Interaction logic for TemplateMatching.xaml
    /// </summary>
    public partial class TemplateMatching : Window, INotifyPropertyChanged
    {
        private List<Image<Bgr, byte>> _Templates { get; set; }
        private Main _Main { get; set; }
        public TemplateMatching(Main Main, List<Image<Bgr, byte>> Templates)
        {
            _Main = Main;
            _Templates = Templates;
            OnPropertiesChanged();
            InitializeComponent();
            RefreshListImage();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertiesChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
            
        }
        private void RefreshListImage()
        {
            this.Dispatcher.Invoke(() => {
                myStack.Children.Clear();
                for (int i = 0; i < _Templates.Count; i++)
                {
                    var bms = Resource.Control.MetaImageBox.Bitmap2BitmapSource(_Templates[i].Bitmap);
                    Resource.Control.TemplateImage image = new Resource.Control.TemplateImage();
                    image.Source = bms;
                    image.Index = i;
                    image.Width = 150;
                    image.Height = 150;
                    image.DeleteButtonClicked += Image_DeleteButtonClicked;
                    myStack.Children.Add(image);
                }
            });
        }

        private void Image_DeleteButtonClicked(object sender, Resource.Control.DeleteTemplateHandlerArgs e)
        {
            int i = e.Index;
            if(i >= 0 && i < _Templates.Count)
            {
                _Templates.RemoveAt(i);
                RefreshListImage();
            }
        }
    }
}
