﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Heal.Models;


namespace Heal.View.Setup
{
    /// <summary>
    /// Interaction logic for CadAdjust.xaml
    /// </summary>
    public partial class CadAdjust : Window
    { 
        public List<int> BlockID = new List<int>();
        public MyBoard CurrBoard { get; set; }
        public CadAdjust(MyBoard _CurrBoard)
        {
            CurrBoard = _CurrBoard;
            InitializeComponent();
            cbBlock.Items.Add("All");
            
            foreach (var catalog in CurrBoard.Catalogs)
            {
                foreach (var smd in catalog.SMDs)
                {
                    if(!BlockID.Contains(smd.Block))
                    {
                        BlockID.Add(smd.Block);
                    }
                }
            }
            BlockID.Sort();
            for (int i = 0; i < BlockID.Count; i++)
            {
                cbBlock.Items.Add(BlockID[i]);
            }
            cbBlock.SelectedIndex = 0;
        }
        private void txtTranslateX_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                
                double value = 0;
                try
                {
                    value = Convert.ToDouble((sender as TextBox).Text);
                }
                catch { return; }
                if (cbBlock.SelectedIndex == 0)
                {
                    for (int i = 0; i < BlockID.Count; i++)
                    {
                        CurrBoard.Translate(BlockID[i], value, 0);
                    }
                }
                else
                {
                    CurrBoard.Translate(BlockID[cbBlock.SelectedIndex - 1], value, 0);
                }
            }
            
        }

        private void txtTranslateY_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                double value = 0;
                try
                {
                    value = Convert.ToDouble((sender as TextBox).Text);
                }
                catch { return; }
                if (cbBlock.SelectedIndex == 0)
                {
                    for (int i = 0; i < BlockID.Count; i++)
                    {
                        CurrBoard.Translate(BlockID[i],0 , value);
                    }
                }
                else
                {
                    CurrBoard.Translate(BlockID[cbBlock.SelectedIndex - 1], 0, value);
                }
            }
            
        }

        private void btRotateX_Click(object sender, RoutedEventArgs e)
        {
            if (cbBlock.SelectedIndex == 0)
            {
                for (int i = 0; i < BlockID.Count; i++)
                {
                    CurrBoard.Rotate(BlockID[i], -90);
                }
            }
            else
            {
                CurrBoard.Rotate(BlockID[cbBlock.SelectedIndex - 1], -90);
            }
            
        }

        private void btRotateY_Click(object sender, RoutedEventArgs e)
        {
            if (cbBlock.SelectedIndex == 0)
            {
                for (int i = 0; i < BlockID.Count; i++)
                {
                    CurrBoard.Rotate(BlockID[i], 90);
                }
            }
            else
            {
                CurrBoard.Rotate(BlockID[cbBlock.SelectedIndex - 1], 90);
            }
            
        }

        private void btFlipX_Click(object sender, RoutedEventArgs e)
        {
            if (cbBlock.SelectedIndex == 0)
            {
                for (int i = 0; i < BlockID.Count; i++)
                {
                    CurrBoard.Flip(BlockID[i], FlipMode.X);
                }
            }
            else
            {
                CurrBoard.Flip(BlockID[cbBlock.SelectedIndex - 1], FlipMode.X);
            }
            
        }

        private void btFlipY_Click(object sender, RoutedEventArgs e)
        {
            if (cbBlock.SelectedIndex == 0)
            {
                for (int i = 0; i < BlockID.Count; i++)
                {
                    CurrBoard.Flip(BlockID[i], FlipMode.Y);
                }
            }
            else
            {
                CurrBoard.Flip(BlockID[cbBlock.SelectedIndex - 1], FlipMode.Y);
            }
            
        }
    }
}
