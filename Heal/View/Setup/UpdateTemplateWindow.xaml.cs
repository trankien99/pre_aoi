﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using draw = System.Drawing;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Emgu.CV;
using System.Threading;

namespace Heal.View.Setup
{
    /// <summary>
    /// Interaction logic for UpdateTemplateWindow.xaml
    /// </summary>
    public partial class UpdateTemplateWindow : Window
    {
        public Image<Bgr, byte> TemplateImage { get; set; }
        public Image<Bgr, byte> Source { get; set; }
        public UpdateTemplateWindow()
        {
            InitializeComponent();
        }
        public void SetSource(Image<Bgr, byte> InputImage)
        {
            imageBox.IsSelectRect = true;
            Source = InputImage;
            if (InputImage != null)
                imageBox.SourceFromBitmap = InputImage.Bitmap;
            else
                btOK.IsEnabled = false;
            Slider_ValueChanged(sld, null);
        }
        private void btOK_Click(object sender, RoutedEventArgs e)
        {
            if(imageBox.RectangleSelected != new Rect())
            {
                Rect R = imageBox.RectangleSelected;
                draw.Rectangle ROI = new draw.Rectangle(
                    Convert.ToInt32(R.X), Convert.ToInt32(R.Y),
                    Convert.ToInt32(R.Width), Convert.ToInt32(R.Height)
                    ); 
                Source.ROI = ROI;
                TemplateImage = Source.Copy();
                this.Close();
            }
            else
            {
                MessageBox.Show("Please select a region", "Select", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void btEdit_Click(object sender, RoutedEventArgs e)
        {
            imageBox.IsSelectRect = true;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if(imageBox.Source != null)
            {
                imageBox.ZoomScale = (sender as Slider).Value / 100;
            }
        }
        public void Dispose()
        {
            TemplateImage?.Dispose();
            Source?.Dispose();
        }
    }
}
