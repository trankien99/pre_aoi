﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Heal.Models;

namespace Heal.View.Setup
{
    /// <summary>
    /// Interaction logic for NewAlarm.xaml
    /// </summary>
    public partial class NewAlarm : Window
    {
        public bool New { get; set; }
        public string AlarmName { get; set; }
        public ObservableCollection<Library> Librarys { get; set; }
        public Library Lib { get; set; }
        public NewAlarm(ObservableCollection<Library> AlarmLib, string name = null)
        {
            Librarys = AlarmLib;
            AlarmName = name;
            InitializeComponent();
            this.DataContext = this;
        }

        private void btCanel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btNew_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in Librarys)
            {
                if(item.Name == this.AlarmName)
                {
                    MessageBox.Show("Library name is existed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            this.New = true;
            this.AlarmName = txtAlarmName.Text;
            this.Close();
            Library alarm = new Library(AlarmName);
            Librarys.Add(alarm);
        }
        #region Startup by mouse position

        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);
            MoveBottomRightEdgeOfWindowToMousePosition();
        }

        private void MoveBottomRightEdgeOfWindowToMousePosition()
        {
            var transform = PresentationSource.FromVisual(this).CompositionTarget.TransformFromDevice;
            var mouse = transform.Transform(GetMousePosition());
            Left = mouse.X;
            Top = mouse.Y;
        }

        public System.Windows.Point GetMousePosition()
        {
            System.Drawing.Point point = System.Windows.Forms.Control.MousePosition;
            return new System.Windows.Point(point.X, point.Y);
        }

        #endregion
    }
}
