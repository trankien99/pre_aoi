﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Heal.Properties;
using System.IO;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using Heal.SDK;
using NLog;
using Heal.Models;


namespace Heal.View.Setup
{
    /// <summary>
    /// Interaction logic for LibraryManagament.xaml
    /// </summary>
    public partial class AlarmManagament : Window
    {
        private Settings mParam = Settings.Default;
        private Logger mLog = LogCtl.GetInstance();
        public SMD mSMD { get; set; }
        public string mCode { get; set; }
        public ObservableCollection<Library> Librarys { get; set; }
        public AlarmManagament(ObservableCollection<Library> AlarmLib, SMD smd, string code)
        {
            Librarys = AlarmLib;
            mSMD = smd;
            mCode = code;
            InitializeComponent();
            lboxLibrary.ItemsSource = Librarys;
            if(smd.Lib != null)
            {
                for (int i = 0; i < Librarys.Count; i++)
                {
                    var item = Librarys[i];
                    if (item.Name == smd.Lib.Name)
                    {
                        lboxLibrary.SelectedIndex = i;
                        break;
                    }
                }
            }
        }
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void btNew_Click(object sender, RoutedEventArgs e)
        {
            NewAlarm newAlarm = new NewAlarm(Librarys, mCode);
            newAlarm.ShowDialog();
            lboxLibrary.Items.Refresh();
            if (newAlarm.New == true)
            {
                lboxLibrary.SelectedIndex = Librarys.Count - 1;
                lboxLibrary.ScrollIntoView(lboxLibrary.SelectedItem);
            }
        }
        private void btCopy_Click(object sender, RoutedEventArgs e)
        {
            var item = lboxLibrary.SelectedItem;
            if(item != null)
            {
                if (item is Library)
                {
                    Library itemSelected = item as Library;
                    var copy = itemSelected.Copy();
                    copy.Name += "_Copy";
                    Librarys.Add(copy);
                    lboxLibrary.Items.Refresh();
                    lboxLibrary.SelectedIndex = Librarys.Count - 1;
                    lboxLibrary.ScrollIntoView(lboxLibrary.SelectedItem);
                }
            }
        }
        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            var item = lboxLibrary.SelectedItem;
            if(item != null)
            {
                if (item is Library)
                {
                    var R = MessageBox.Show("Delete library", "Delete", MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (R == MessageBoxResult.Yes)
                    {
                        Librarys.Remove(item as Library);
                        lboxLibrary.Items.Refresh();
                    }
                }
            }
            
        }
        #region Startup by mouse position

        protected override void OnContentRendered(EventArgs e)
        {
            base.OnContentRendered(e);
            MoveBottomRightEdgeOfWindowToMousePosition();
        }

        private void MoveBottomRightEdgeOfWindowToMousePosition()
        {
            var transform = PresentationSource.FromVisual(this).CompositionTarget.TransformFromDevice;
            var mouse = transform.Transform(GetMousePosition());
            Left = mouse.X;
            Top = mouse.Y;
        }

        public System.Windows.Point GetMousePosition()
        {
            System.Drawing.Point point = System.Windows.Forms.Control.MousePosition;
            return new System.Windows.Point(point.X, point.Y);
        }

        #endregion

        private void lboxLibrary_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox listBox = sender as ListBox;
            var item = listBox.SelectedItem;
            if(item != null)
            {
                if (item is Library)
                {
                    mSMD.Lib = (item as Library);
                    e.Handled = true;
                    this.Close();
                }
            }
            
        }

        private void btUnlink_Click(object sender, RoutedEventArgs e)
        {
            mSMD.Lib = null;
            this.Close();
        }

        private void TextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Button_Click(null, null);
                e.Handled = true;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string name = txtSearch.Text;
            if(!string.IsNullOrEmpty(name))
            {
                for (int i = 0; i < Librarys.Count; i++)
                {
                    var item = Librarys[i];
                    if (item.Name.Contains(name))
                    {
                        lboxLibrary.SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(!txtSearch.IsFocused && e.Key == Key.Enter)
            {
                ListBox listBox = lboxLibrary;
                var item = listBox.SelectedItem;
                if (item != null)
                {
                    if (item is Library)
                    {
                        e.Handled = true;
                        mSMD.Lib = (item as Library);
                        this.Close();
                    }
                }
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Delete && lboxLibrary.SelectedItem != null)
            {
                var r = MessageBox.Show("Delete library?", "Delete", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if(r == MessageBoxResult.Yes)
                {
                    Librarys.Remove(lboxLibrary.SelectedItem as Library);
                    lboxLibrary.Items.Refresh();
                }
            }
        }
    }
}
