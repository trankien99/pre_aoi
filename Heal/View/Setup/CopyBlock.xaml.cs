﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Heal.Models;

namespace Heal.View.Setup
{
    /// <summary>
    /// Interaction logic for CopyBlock.xaml
    /// </summary>
    public partial class CopyBlock : Window, INotifyPropertyChanged
    {
        private MyBoard CurrBoard { get => MyBoard.Currently; }
        public int BlockID { get; set; }
        public double ShiftX { get; set; }
        public double ShiftY { get; set; }
        public double Angle { get; set; }
        private List<int> _CrrBlockID = new List<int>();
        private Main _Main = null;
        public CopyBlock(Main Main)
        {
            _Main = Main;
            InitializeComponent();
            LoadUI();
            this.DataContext = this;
        }
        private void LoadUI()
        {
            foreach (var catalog in CurrBoard.Catalogs)
            {
                foreach (var comp in catalog.SMDs)
                {
                    if (!_CrrBlockID.Contains(comp.Block))
                    {
                        _CrrBlockID.Add(comp.Block);
                    }
                }
            }
            _CrrBlockID.Sort();
            for (int i = 0; i < _CrrBlockID.Count; i++)
            {
                cbBlockID.Items.Add(_CrrBlockID[i]);
                cbBlockIDEnable.Items.Add(_CrrBlockID[i]);
                cbBlockIDDelete.Items.Add(_CrrBlockID[i]);
            }
            if (cbBlockID.Items.Count > 0)
            {
                cbBlockID.SelectedIndex = 0;
                cbBlockIDEnable.SelectedIndex = 0;
                cbBlockIDDelete.SelectedIndex = 0;
                BlockID = _CrrBlockID.Max() + 1;
            }
            else
            {
                stpMain.IsEnabled = false;
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged()
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
        }
        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            Resource.Convertor.Action.UpdateBindingSourceTextBox(sender);
        }

        private void btOK_Click(object sender, RoutedEventArgs e)
        {
            int block = Convert.ToInt32(cbBlockID.SelectedItem);
            CurrBoard.CopyBlock(block, BlockID, ShiftX, ShiftY, Angle);
            _Main.RefreshUI();
            this.Close();
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btOKEnable_Click(object sender, RoutedEventArgs e)
        {
            if(cbBlockIDDelete.SelectedIndex >= 0)
            {
                int id = Convert.ToInt32(cbBlockIDDelete.SelectedItem);
                CurrBoard.EnableBlock(id, cbAction.SelectedIndex == 0);
                _Main.RefreshUI();
                this.Close();
            }
        }

        private void btOKDelete_Click(object sender, RoutedEventArgs e)
        {
            if (cbBlockIDDelete.SelectedIndex >= 0)
            {
                int id = Convert.ToInt32(cbBlockIDDelete.SelectedItem);
                CurrBoard.DeleteBlock(id);
                _Main.RefreshUI();
                this.Close();
            }
        }
    }
}
