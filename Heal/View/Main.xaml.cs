﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.Structure;
using Heal.Resource.Control;
using Heal.Models;
using System.ComponentModel;
using System.Diagnostics;
using Heal.View.Tool;
using form = System.Windows.Forms;
using System.Threading;
using System.IO;
using NLog;
using Heal.Models.Alg;
using controlAlg = Heal.Resource.Control.Alg;
using System.Windows.Controls.Primitives;
using draw = System.Drawing;

namespace Heal.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Main : Window, INotifyPropertyChanged
    {
        public MyBoard CurrBoard { 
            get => MyBoard.Currently; 
            set => MyBoard.Currently = value; 
        }
        private Logger _Log = SDK.LogCtl.GetInstance();
        private Devices.PLC.MyPLC _PLC = Devices.PLC.MyPLC.GetInstance();
        private Properties.Settings _Param = Properties.Settings.Default;
        private Models.Calib.Imaging _Imaing { get; set; }
        private Models.Calib.Calibration _Calibration { get; set; }
        private string _SaveBoardPath = string.Empty;
        public Catalog ModifyCatalog { get; set; }
        public object NagativeSelected { get; set; }
        public SMD ModifyAlarmSMD { get; set; }
        public MyAlarm ModifyMyAlarm { get; set; }
        public ObservableCollection<MyBoard> MyBoards { get; set; } = new ObservableCollection<MyBoard>();

        // ---- Properties -----
        public BoardProperties _BoardProperties { get; set; }
        public CatalogProperties _CatalogProperties { get; set; }
        public SMDProperties _SMDProperties { get; set; }

        public AutoRun.AutoRunMain _AutoRunWindow { get; set; }
        public Main()
        {
            InitializeComponent();
            this.DataContext = this;
            trvCatalog.ItemsSource = MyBoards;
            Models.Calib.Calibration.LoadFromFile(_Param.CalibPath);
            _Calibration = Models.Calib.Calibration.GetInstance();
            if(CurrBoard != null)
            {
                _Imaing = new Models.Calib.Imaging(_Calibration, CurrBoard.Width, CurrBoard.Height, null);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged() 
        { 
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
        }
        public void RefreshUI()
        {
            OnPropertyChanged();
            metaImBox.OnPropertyChanged();
            LoadImage2View(CurrBoard, false);
            trvComponentList.Items.Refresh();
            trvAlarm_RefreshItem(ModifyMyAlarm);
            RefreshCatalog(NagativeSelected);
            if (dpnAlgProperties.Children .Count > 0)
            {
                var item = dpnAlgProperties.Children[0] as controlAlg.IAlgProperties;
                item.OnPropertyChanged();
            }
            libCtrl.OnPropertyChanged();
        }
        private void btRefreshSMDList_Click(object sender, RoutedEventArgs e)
        {
            trvAlarm_RefreshItem(ModifyMyAlarm);
        }
        #region UI Button
        private void btOption_Click(object sender, RoutedEventArgs e)
        {
            Tool.Parameter window = new Parameter();
            window.ShowDialog();
        }

        private void btAutoRun_Click(object sender, RoutedEventArgs e)
        {
            Loading _Loading = new Loading();
            _Loading.Status = $"Starting auto run...";
            _AutoRunWindow = new AutoRun.AutoRunMain();
            Thread _thread = new Thread(() =>
            {
                int started = _AutoRunWindow.Start();
                if(started == 0)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        _Loading.Kill();
                        _AutoRunWindow.ShowDialog();
                    });
                }
                else
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        _Loading.Kill();
                    });
                }
            });
            _thread.Start();
            _Loading.ShowDialog();
            metaImBox.SetSource(null);
            LoadImage2View(CurrBoard);
        }
        private void LoadImage2View(MyBoard board, bool LoadNewImage = true)
        {
            if(board.Image?.Blocks.Count > 0)
            {
                this.Dispatcher.Invoke(() => {
                    if(LoadNewImage)
                    {
                        var blocks = board.Image.GetMetaImageBlocks();
                        metaImBox.SetSource(board.Image.ImageWidth, board.Image.ImageHeight, blocks);
                    }
                    CurrBoard.AdjustSMDCenter();
                    var smds = board.GetAllSMD();
                    metaImBox.SetSMDSource(smds);
                });
                OnPropertyChanged();
            }
            else
            {
                this.Dispatcher.Invoke(() => {
                    metaImBox.SetSource(null);
                });
                OnPropertyChanged();
            }
        }
        private void btInspectionAll_Click(object sender, RoutedEventArgs e)
        {
            if (CurrBoard != null)
            {
                if (CurrBoard.Image != null)
                {
                    Loading _Loading = new Loading();
                    _Loading.Status = $"Inspecting 0/{CurrBoard.Catalogs.Count} catalogs....";
                    Thread _thread = new Thread(() =>
                    {
                        Stopwatch sw = new Stopwatch();
                        sw.Start(); 
                        Models.Run.Inspect inspect = new Models.Run.Inspect(CurrBoard.Image);
                        var result = inspect.Run(CurrBoard, _Loading);
                        (result.BoardImage, result.ImageScale) = CurrBoard.Image.GetBoardImage(new draw.Size(1920, 1080));
                        Heal.DB.MyMongo mongo = new DB.MyMongo();
                        mongo.Connect();
                        bool send = Utils.TCP_Client.Send(_Param.RepairStationIP, _Param.RepairStationPort, result);
                        mongo.InsertBoard(result);
                        result.Dispose();
                        sw.Stop();
                        this.Dispatcher.Invoke(() => 
                        {
                            _Loading.Kill();
                        });
                        _Log.Info($"Inspected in {sw.ElapsedMilliseconds}ms...");
                    });
                    _thread.Start();
                    _Loading.ShowDialog();
                    OnPropertyChanged();
                }
            }
        }
        private void btExit_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("Exit application?", "Exit", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                Environment.Exit(0);
        }
        private void btViewLog_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer.exe", $"logs\\{DateTime.Now.Year}");
        }
        private void btnAbout_Click(object sender, RoutedEventArgs e)
        {
            View.Tool.AboutWindow about = new View.Tool.AboutWindow();
            about.ShowDialog();
        }
        private void btComfirmPass_Click(object sender, RoutedEventArgs e)
        {
            _PLC.SEND.ConfirmPanelPass();
        }
        private void btCaptureImage_Click(object sender, RoutedEventArgs e)
        {
            Loading mLoading = new Loading();
            mLoading.Status = "Imaging image...";
            Thread thread = new Thread(() => {
                _PLC.SEND.Login();
                _Imaing = new Models.Calib.Imaging(_Calibration, CurrBoard.Width, CurrBoard.Height);
                mLoading.Status = "Write array coordinates...";
                _Imaing.WriteArrayXY();
                mLoading.Status = "Imaging image...";
                MyImage image = _Imaing.RunMode2();
                //_PLC.SEND.Logout();
                if (image == null)
                {
                    _Log.Error("Capture image failed");
                    MessageBox.Show("Capture image failed. Please try again later!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    CurrBoard.Image = image;
                }
                this.Dispatcher.Invoke(() => {
                    mLoading.Kill();
                    LoadImage2View(CurrBoard);
                });
                OnPropertyChanged();
            });
            thread.Start();
            mLoading.ShowDialog();
        }
        private void btReturnSnapshot_Click(object sender, RoutedEventArgs e)
        {
            _PLC.SEND.Login();
            _PLC.TOP.GoXY(_Param.OriginCoordinate);
        }
        private void btPCBOut_Click(object sender, RoutedEventArgs e)
        {
            _PLC.SEND.Login();
            _PLC.CONVEYOR.Unload();
        }
        private void btPCBIn_Click(object sender, RoutedEventArgs e)
        {
            _PLC.SEND.Login();
            _PLC.CONVEYOR.Load();
        }
        private void btRefesh_Click(object sender, RoutedEventArgs e)
        {
            RefreshUI();
        }
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Q && Keyboard.Modifiers == ModifierKeys.Control)
                btExit_Click(null, null);
            else if (e.Key == Key.S && Keyboard.Modifiers == ModifierKeys.Control)
                mnSaveOnlyProgram_Click(null, null);
            else if (e.Key == Key.O && Keyboard.Modifiers == ModifierKeys.Control)
                btnOpen_Click(null, null);
            else if (e.Key == Key.N && Keyboard.Modifiers == ModifierKeys.Control)
                btnNew_Click(null, null);
        }
        private void btImportLibrary_Click(object sender, RoutedEventArgs e)
        {

        }
        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            using (form.OpenFileDialog dialog = new form.OpenFileDialog())
            {
                dialog.Filter = "Fii Program | *.fiaoi";
                if (dialog.ShowDialog() == form.DialogResult.OK)
                {
                    Loading _Loading = new Loading();
                    _Loading.Status = "Loading your program....";
                    Thread _thread = new Thread(() =>
                    {
                        MyBoard p = MyBoard.Load(dialog.FileName);
                        if (p != null)
                        {
                            if (CurrBoard != null)
                            {
                                CurrBoard.Dispose();
                            }
                            CurrBoard = p;
                            _SaveBoardPath = dialog.FileName;
                            this.Dispatcher.Invoke(() => {
                                MyBoards.Clear();
                                MyBoards.Add(CurrBoard);
                                trvCatalog.Items.Refresh();
                                LoadImage2View(CurrBoard);
                                if (CurrBoard != null)
                                {
                                    _Imaing = new Models.Calib.Imaging(_Calibration, CurrBoard.Width, CurrBoard.Height, null);
                                }
                            });
                        }
                        else
                        {
                            MessageBox.Show("Cant Load your model. Please try again later!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        this.Dispatcher.Invoke(() => {
                            OnPropertyChanged();
                            _Loading.Kill();
                        });
                    });
                    _thread.Start();
                    _Loading.ShowDialog();
                    OnPropertyChanged();
                }
            }
        }
        private void txtSearch_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                btSearch_Click(null, null);
            }
        }
        private void btSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchName = txtSearch.Text;
            if (!string.IsNullOrEmpty(searchName) && CurrBoard != null)
            {
                var found = CurrBoard.Find(searchName);
                if (found != null)
                {
                    NagativeSelected = found;
                    RefreshCatalog(NagativeSelected);
                    if (NagativeSelected is SMD)
                    {
                        SMD smd = NagativeSelected as SMD;
                        metaImBox.GoPoint(new Point(smd.X, smd.Y));
                    }
                }
                else
                {
                    MessageBox.Show($"Not found object with name \'{searchName}\'", "Search", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void btRefreshNavigation_Click(object sender, RoutedEventArgs e)
        {
            RefreshCatalog(NagativeSelected);
        }
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Environment.Exit(0);
        }

        private void btGoOriginImage_Click(object sender, RoutedEventArgs e)
        {
            metaImBox.GoOirgin();
        }
        private void btInspection_Click(object sender, RoutedEventArgs e)
        {
            if (CurrBoard.Image != null && ModifyAlarmSMD != null)
            {
                Catalog catalog = CurrBoard.GetCatalog(ModifyAlarmSMD);
                Models.Run.Inspect inspect = new Models.Run.Inspect(CurrBoard.Image, new MarkDev());
                inspect.Run(catalog);
            }
        }

        private void btExportLibrary_Click(object sender, RoutedEventArgs e)
        {
            if (CurrBoard != null)
            {
                using (form.FolderBrowserDialog dialog = new form.FolderBrowserDialog())
                {
                    if (dialog.ShowDialog() == form.DialogResult.OK)
                    {
                        CurrBoard.SaveLibrary(dialog.SelectedPath);
                    }
                }
            }
        }

        private void btExportImageBoard_Click(object sender, RoutedEventArgs e)
        {
            if (CurrBoard?.Image != null)
            {
                using (form.SaveFileDialog dialog = new form.SaveFileDialog())
                {
                    dialog.Filter = "FII Image | *.fiimg";
                    dialog.FileName = CurrBoard.Name;
                    dialog.DefaultExt = "fiimg";
                    if (dialog.ShowDialog() == form.DialogResult.OK)
                    {
                        Tool.Loading loading = new Loading();
                        loading.Status = "Export image board...";
                        Thread t = new Thread(() => {
                            CurrBoard?.Image?.Save(dialog.FileName);
                            loading.Kill();
                        });
                        t.Start();
                        loading.ShowDialog();
                    }
                }
            }

        }

        private void btSaveImageJpg_Click(object sender, RoutedEventArgs e)
        {
            if (CurrBoard?.Image != null)
            {
                using (form.SaveFileDialog dialog = new form.SaveFileDialog())
                {
                    dialog.Filter = "Image Format | *.png;*.jpg;*.bmp";
                    dialog.FileName = CurrBoard.Name;
                    dialog.DefaultExt = "jpg";
                    if (dialog.ShowDialog() == form.DialogResult.OK)
                    {
                        Tool.Loading loading = new Loading();
                        loading.Status = "Export image with image format...";
                        Thread t = new Thread(() => {
                            CurrBoard?.Image?.SaveImage(dialog.FileName, _Param.SaveImageQuality);
                            loading.Kill();
                        });
                        t.Start();
                        loading.ShowDialog();
                    }
                }
            }
        }
        private void btImportImageBoard_Click(object sender, RoutedEventArgs e)
        {
            if (CurrBoard != null)
            {
                using (form.OpenFileDialog dialog = new form.OpenFileDialog())
                {
                    dialog.Filter = "FII Image | *.fiimg";
                    if (dialog.ShowDialog() == form.DialogResult.OK)
                    {
                        Loading _Loading = new Loading();
                        _Loading.Status = "Import image....";
                        Thread _thread = new Thread(() => {
                            MyImage image = MyImage.Load(dialog.FileName);
                            if (image != null)
                            {
                                if (CurrBoard.Image != null)
                                {
                                    CurrBoard.Image.Dispose();
                                    CurrBoard.Image = null;
                                }
                                CurrBoard.Image = image;
                                
                                this.Dispatcher.Invoke(() => {
                                    LoadImage2View(CurrBoard);
                                });
                                OnPropertyChanged();
                            }
                            else
                            {
                                MessageBox.Show("Cant load your image, Please try again later!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                            _Loading.Kill();
                        }); _thread.Start();
                        _Loading.ShowDialog();
                    }
                }
            }
            else
            {
                MessageBox.Show("The current program is emty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            New.NewProgramWindow window = new New.NewProgramWindow();
            window.ShowDialog();
            if(window.Created)
            {
                _SaveBoardPath = string.Empty;
                Tool.Loading loading = new Tool.Loading();
                loading.Status = "Load New Model...";
                if (CurrBoard != null)
                {
                    CurrBoard.Dispose();
                }
                CurrBoard = window.NewProgram;
                MyBoards.Clear();
                MyBoards.Add(CurrBoard);
                trvCatalog.Items.Refresh();
                LoadImage2View(CurrBoard);
                if (CurrBoard != null)
                {
                    _Imaing = new Models.Calib.Imaging(_Calibration, CurrBoard.Width, CurrBoard.Height, null);
                }
                OnPropertyChanged();
            }
        }
        private void btAddNewCatalog_Click(object sender, RoutedEventArgs e)
        {
            if (CurrBoard != null)
            {
                if (NagativeSelected is Catalog)
                {
                    
                    var catalog = CurrBoard.AddCatalog(NagativeSelected as Catalog, Models.Action.New);
                    if (catalog != null)
                    {
                        NagativeSelected = catalog;
                    }
                    RefreshCatalog(NagativeSelected);
                }
                else if(NagativeSelected is MyBoard)
                {
                    var catalog =  CurrBoard.AddCatalog(null, Models.Action.New);
                    if(catalog != null)
                    {
                        NagativeSelected = catalog;
                    }
                    RefreshCatalog(NagativeSelected);
                }
            }
        }
        private void AddNewComponent_Click(object sender, RoutedEventArgs e)
        {
            if (CurrBoard != null)
            {
                if (NagativeSelected is Catalog)
                {
                    Catalog catalog = NagativeSelected as Catalog;
                    int id = CurrBoard.Catalogs.IndexOf(catalog);
                    var smd = CurrBoard.AddSMD(id);
                    smd.X = Convert.ToInt32(metaImBox.CurrCenterPoint.X);
                    smd.Y = Convert.ToInt32(metaImBox.CurrCenterPoint.Y);
                    if (smd != null)
                    {
                        NagativeSelected = smd;
                    }
                    RefreshUI();
                }
                else if (NagativeSelected is SMD)
                {
                    SMD smd = NagativeSelected as SMD;
                    var newSMD = CurrBoard.AddSMD(smd, Models.Action.New);
                    newSMD.X = Convert.ToInt32(metaImBox.CurrCenterPoint.X);
                    newSMD.Y = Convert.ToInt32(metaImBox.CurrCenterPoint.Y);
                    if (newSMD != null)
                    {
                        NagativeSelected = newSMD;
                    }
                    RefreshUI();
                }
            }
        }
        private void btCopy_Click(object sender, RoutedEventArgs e)
        {
            if (CurrBoard != null)
            {
                if (NagativeSelected is Catalog)
                {
                    Catalog refCatalog = NagativeSelected as Catalog;
                    var catalog =  CurrBoard.AddCatalog(refCatalog, Models.Action.Copy);
                    if (catalog != null)
                    {
                        NagativeSelected = catalog;
                    }
                    RefreshUI();
                }
                else if (NagativeSelected is SMD)
                {
                    SMD smd = NagativeSelected as SMD;
                    var newSMD = CurrBoard.AddSMD(smd, Models.Action.Copy);
                    if (newSMD != null)
                    {
                        NagativeSelected = newSMD;
                    }
                    RefreshUI();
                }
            }
        }
        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            if (CurrBoard != null)
            {
                if (NagativeSelected is Catalog)
                {
                    Catalog catalog = NagativeSelected as Catalog;
                    CurrBoard.RemoveCatalog(catalog);
                    RefreshUI();
                }
                else if (NagativeSelected is SMD)
                {
                    SMD smd = NagativeSelected as SMD;
                    CurrBoard.RemoveSMD(smd);
                    RefreshUI();
                }
            }
        }
        #endregion
        private void RefreshCatalog(object Ob)
        {
            this.Dispatcher.Invoke(() => {
                trvCatalog.Items.Refresh();
                if (CurrBoard != null)
                {
                    if (Ob is MyBoard || Ob is Catalog || Ob is SMD)
                    {
                        SMD smdSelected = null;
                        Catalog catalogSelected = null;
                        if (Ob is Catalog)
                        {
                            catalogSelected = Ob as Catalog;
                        }
                        else if (Ob is SMD)
                        {
                            smdSelected = Ob as SMD;
                            catalogSelected = CurrBoard.GetCatalog(smdSelected);
                        }
                        var trvProgram = trvCatalog.ItemContainerGenerator.ContainerFromItem(CurrBoard) as TreeViewItem;
                        if (trvProgram != null)
                        {
                            trvProgram.IsSelected = true;
                            trvProgram.IsExpanded = true;
                            trvProgram.UpdateLayout();
                            if (catalogSelected != null)
                            {
                                var trvCatalog = trvProgram.ItemContainerGenerator.ContainerFromItem(catalogSelected) as TreeViewItem;
                                if (ModifyCatalog != null)
                                {
                                    var trvCatalogCmp = trvProgram.ItemContainerGenerator.ContainerFromItem(ModifyCatalog) as TreeViewItem;
                                    if (trvCatalogCmp != null)
                                    {
                                        trvCatalogCmp.BorderThickness = new Thickness(1);
                                        trvCatalogCmp.BorderBrush = Brushes.Yellow;
                                    }
                                }
                                if (trvCatalog != null)
                                {
                                    trvCatalog.IsSelected = true;

                                    trvCatalog.IsExpanded = true;
                                    trvCatalog.UpdateLayout();
                                    if (smdSelected != null)
                                    {
                                        var trvSmd = trvCatalog.ItemContainerGenerator.ContainerFromItem(smdSelected) as TreeViewItem;
                                        if (trvSmd != null)
                                        {
                                            trvSmd.IsSelected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }
        private void mnSaveOnlyProgram_Click(object sender, RoutedEventArgs e)
        {
            var program = MyBoard.Currently;
            if (program != null)
            {
                if (string.IsNullOrEmpty(_SaveBoardPath) || !File.Exists(_SaveBoardPath))
                {
                    form.SaveFileDialog dialog = new form.SaveFileDialog();
                    dialog.FileName = program.Name;
                    dialog.DefaultExt = "fiaoi";
                    dialog.Filter = "Fii Program | *.fiaoi";
                    if (dialog.ShowDialog() == form.DialogResult.OK)
                    {
                        Loading _Loading = new Loading();
                        _Loading.Status = "Save program....";
                        Thread _thread = new Thread(() => {
                            _SaveBoardPath = dialog.FileName;
                            program.Save(_SaveBoardPath, SaveProgramMode.Program);
                            _Loading.Kill();
                        }); _thread.Start();
                        _Loading.ShowDialog();
                    }
                }
                else
                {
                    Loading _Loading = new Loading();
                    _Loading.Status = "Save program....";
                    Thread _thread = new Thread(() => {
                        program.Save(_SaveBoardPath, SaveProgramMode.Program);
                        _Loading.Kill();
                    }); _thread.Start();
                    _Loading.ShowDialog();

                }
            }
        }
        private void mnSaveProgramnImage_Click(object sender, RoutedEventArgs e)
        {
            var program = MyBoard.Currently;
            if (program != null)
            {
                if (string.IsNullOrEmpty(_SaveBoardPath) || !File.Exists(_SaveBoardPath))
                {
                    form.SaveFileDialog dialog = new form.SaveFileDialog();
                    dialog.FileName = program.Name;
                    dialog.DefaultExt = "fiaoi";
                    dialog.Filter = "Fii Program | *.fiaoi";
                    if (dialog.ShowDialog() == form.DialogResult.OK)
                    {
                        Loading _Loading = new Loading();
                        _Loading.Status = "Save program....";
                        Thread _thread = new Thread(() => {
                            _SaveBoardPath = dialog.FileName;
                            program.Save(_SaveBoardPath, SaveProgramMode.ProgramAndImage);
                            _Loading.Kill();
                        });
                        _thread.Start();
                        _Loading.ShowDialog();
                    }
                }
                else
                {
                    Loading _Loading = new Loading();
                    _Loading.Status = "Save program....";
                    Thread _thread = new Thread(() => {
                        program.Save(_SaveBoardPath, SaveProgramMode.ProgramAndImage);
                        _Loading.Kill();
                    }); 
                    _thread.Start();
                    _Loading.ShowDialog();

                }
            }
        }
        private void trvCatalog_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            NagativeSelected = e.NewValue;
            dpnProperties.Children.Clear();
            if (NagativeSelected is MyBoard)
            {
                var board = GetBoardPropertiesControl(NagativeSelected as MyBoard);
                dpnProperties.Children.Add(board);
            }
            else if (NagativeSelected is Catalog)
            {
                var catalog = GetCatalogPropertiesControl(NagativeSelected as Catalog);
                dpnProperties.Children.Add(catalog);
            }
            else if (NagativeSelected is SMD)
            {
                var smd = GetSMDPropertiesControl(NagativeSelected as SMD);
                dpnProperties.Children.Add(smd);
            }
            OnPropertyChanged();
        }
        private BoardProperties GetBoardPropertiesControl(MyBoard Board)
        {
            if(_BoardProperties == null)
            {
                _BoardProperties = new BoardProperties();
                _BoardProperties.Name = "myBoard";
            }
            _BoardProperties.SetParam(Board);
            return _BoardProperties;
        }
        private CatalogProperties GetCatalogPropertiesControl(Catalog catalog)
        {
            if(_CatalogProperties == null)
            {
                _CatalogProperties = new CatalogProperties();
                _CatalogProperties.Name = "myCatalog";
            }
            _CatalogProperties.SetParam(catalog);
            return _CatalogProperties;
        }
        private SMDProperties GetSMDPropertiesControl(SMD smd)
        {
            if (_SMDProperties == null)
            {
                _SMDProperties = new SMDProperties();
                _SMDProperties.Name = "mySMD";
                _SMDProperties.SetRefImageClick += _SMDProperties_SetRefImageClick;
            }
            _SMDProperties.SetParam(smd);
            return _SMDProperties;
        }
        private void trvCatalog_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(NagativeSelected is Catalog)
            {
                ModifyCatalog = NagativeSelected as Catalog;
                trvComponentList.ItemsSource = ModifyCatalog.SMDs;
                trvAlarm.ItemsSource = null;
                trvComponentList.Items.Refresh();
                trvAlarm.Items.Refresh();
                RefreshCatalog(NagativeSelected);
            }
            else if (NagativeSelected is SMD)
            {
                var smd = NagativeSelected as SMD;
                metaImBox.GoPoint(new Point(smd.X, smd.Y));
            }
        }
        private void metaImBox_SMDSelectChanged(object sender, SMDSelectChangedArgs e)
        {
            NagativeSelected = e.Item;
            RefreshCatalog(NagativeSelected);
        }
        private void mnCadAdjust_Click(object sender, RoutedEventArgs e)
        {
            Setup.CadAdjust window = null;
            foreach(Window win in Application.Current.Windows)
            {
                string windowType = win.GetType().ToString();
                if (windowType.Equals("Heal.View.Setup.CadAdjust"))
                {
                    window = (Setup.CadAdjust)win;
                }
            }
            if(window == null)
            {
                window = new Setup.CadAdjust(CurrBoard);
            }
            window.Show();
        }
        private void _SMDProperties_SetRefImageClick(object sender, EventArgs e)
        {
            if(CurrBoard .Image != null && NagativeSelected is SMD)
            {
                if (CurrBoard.Image.Blocks.Count > 0)
                {
                    SMD smd = NagativeSelected as SMD;
                    draw.Point smdLoc = new draw.Point(smd.X, smd.Y);
                    using (Image<Bgr, byte> imgSmd = Models.Utils.GetSMDImage(CurrBoard.Image, smdLoc, Convert.ToInt32(smd.Width), Convert.ToInt32(smd.Height),0, 0, smd.Angle))
                    {
                        Catalog catalog = CurrBoard.GetCatalog(smd);
                        catalog.RefImage?.Dispose();
                        catalog.RefImage = imgSmd.Copy();
                    }
                }
            }
        }
        private void trvComponentList_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if(e != null)
            {
                ModifyAlarmSMD = e.NewValue as SMD;
                metaImBox.GoPoint(new Point(ModifyAlarmSMD.X, ModifyAlarmSMD.Y));
            }
            LoadAlarmTree();
            IAlg_AlgPropertyChanged(null, null);
            OnPropertyChanged();
        }
        private void LoadAlarmTree()
        {
            if(ModifyAlarmSMD != null)
            {
                if(ModifyAlarmSMD.Lib != null)
                {
                    if(trvAlarm.ItemsSource != ModifyAlarmSMD.Lib.Alarms)
                    {
                        trvAlarm.ItemsSource = ModifyAlarmSMD.Lib.Alarms;
                        trvAlarm.Items.Refresh();
                    }
                    else
                    {
                        IAlg_AlgPropertyChanged(null, null);
                    }
                    return;
                }
            }
            trvAlarm.ItemsSource = null;
            trvAlarm.Items.Refresh();
            txtLibName.Text = "";
        }
        private void btLinkAlarm_Click(object sender, RoutedEventArgs e)
        {
            if(CurrBoard != null && ModifyAlarmSMD != null)
            {
                int id = ModifyAlarmSMD.CatalogID;
                string code = CurrBoard.Catalogs[id].Name;
                Setup.AlarmManagament window = new Setup.AlarmManagament(CurrBoard.AlarmLibrary, ModifyAlarmSMD, code);
                window.ShowDialog();
                // auto link smd as catalog to library
                if( ModifyAlarmSMD.IsUnlink == false)
                {
                    if (ModifyAlarmSMD.Lib != null)
                    {
                        foreach (var smd in CurrBoard.Catalogs[id].SMDs)
                        {
                            if (!smd.IsUnlink)
                            {
                                smd.Lib = ModifyAlarmSMD.Lib;
                            }
                        }
                    }
                }
                LoadAlarmTree();
                OnPropertyChanged();
            }

        }
        private void btAddNewAlarm_Click(object sender, RoutedEventArgs e)
        {
            ModifyAlarmSMD.Lib.Alarms.Add(new MyAlarm());
            LoadAlarmTree();
            OnPropertyChanged();
        }
        private void trvAlarm_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            bool change = false;
            if(e != null && ModifyMyAlarm != e.NewValue)
            {
                ModifyMyAlarm = e.NewValue as MyAlarm;
                change = true;
            }
            if(ModifyMyAlarm != null && change)
            {
                libCtrl.SetParameter(ModifyMyAlarm);
                IAlg_AlgPropertyChanged(null, null);
            }
            else if(ModifyMyAlarm == null)
            {
                metaImBox.SetPreview(null);
            }
            OnPropertyChanged();
        }
        public void trvAlarm_RefreshItem(MyAlarm objectSelected)
        {
            if (ModifyAlarmSMD != null && objectSelected != null)
            {
                SMD smd = ModifyAlarmSMD;
                if (smd.Lib == null)
                {
                    this.Dispatcher.Invoke(() =>
                    {
                        trvAlarm.ItemsSource = null;
                        trvAlarm.Items.Refresh();
                    });
                    return;
                }
                List<int> direct = smd.Lib.GetDirectionID(objectSelected);
                this.Dispatcher.Invoke(() => {
                    trvAlarm.Items.Refresh();
                    object trv = trvAlarm;
                    if (direct.Count > 0)
                    {
                        for (int i = 0; i < direct.Count; i++)
                        {
                            int id = direct[i];
                            if (i == 0)
                            {
                                TreeView t = ((TreeView)trv);
                                if (id < t.Items.Count)
                                {
                                    var trvi = t.ItemContainerGenerator.ContainerFromItem(t.Items[id]) as TreeViewItem;
                                    if (trvi != null)
                                    {
                                        trvi.IsExpanded = true;
                                        trvi.UpdateLayout();
                                        trv = trvi;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Direction of treeview incorrect!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                        break;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Direction of treeview incorrect!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                    break;
                                }
                            }
                            else
                            {
                                TreeViewItem t = ((TreeViewItem)trv);
                                if (id < t.Items.Count)
                                {
                                    var trvi = t.ItemContainerGenerator.ContainerFromItem(t.Items[id]) as TreeViewItem;
                                    if (trvi != null)
                                    {
                                        trvi.IsExpanded = true;
                                        trvi.UpdateLayout();
                                        trv = trvi;
                                    }
                                    else
                                    {
                                        MessageBox.Show("Direction of treeview incorrect!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                        break;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Direction of treeview incorrect!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                                    break;
                                }
                            }
                            if (i == direct.Count - 1)
                            {
                                ((TreeViewItem)trv).IsSelected = true;
                            }
                        }
                    }
                });

            }
        }
        private void btUpgrade_Click(object sender, RoutedEventArgs e)
        {
            if (ModifyMyAlarm != null && ModifyAlarmSMD != null)
            {
                SMD smd = ModifyAlarmSMD;
                var dir = smd.Lib.GetDirectionID(ModifyMyAlarm);
                if (dir.Count > 1)
                {
                    int new_id = dir[dir.Count - 1] - 1;
                    ObservableCollection<MyAlarm> root = smd.Lib.Alarms;
                    ObservableCollection<MyAlarm> dst = smd.Lib.Alarms;
                    for (int i = 0; i < dir.Count - 1; i++)
                    {
                        int index = dir[i];
                        if (i == dir.Count - 2)
                        {
                            root = dst[index].Alarms;
                        }
                        if (i < dir.Count - 2)
                        {
                            dst = dst[index].Alarms;
                        }
                    }
                    dst.Add(ModifyMyAlarm);
                    root.Remove(ModifyMyAlarm);
                    trvAlarm_RefreshItem(ModifyMyAlarm);
                }
            }
        }
        private void btDemote_Click(object sender, RoutedEventArgs e)
        {
            if (ModifyMyAlarm != null && ModifyAlarmSMD != null)
            {
                SMD smd = ModifyAlarmSMD;
                var dir = smd.Lib.GetDirectionID(ModifyMyAlarm);
                if (dir.Count > 0)
                {
                    if (dir[dir.Count - 1] > 0)
                    {
                        int new_id = dir[dir.Count - 1] - 1;

                        ObservableCollection<MyAlarm> root = smd.Lib.Alarms;
                        ObservableCollection<MyAlarm> dst = smd.Lib.Alarms;
                        for (int i = 0; i < dir.Count; i++)
                        {
                            int index = dir[i];
                            if (i == dir.Count - 1)
                            {
                                dst = root[new_id].Alarms;
                            }
                            if (i < dir.Count - 1)
                            {
                                root = root[index].Alarms;
                            }
                        }
                        dst.Add(ModifyMyAlarm);
                        root.Remove(ModifyMyAlarm);
                        trvAlarm_RefreshItem(ModifyMyAlarm);
                    }
                }
            }
        }
        private void btCopyAlarm_Click(object sender, RoutedEventArgs e)
        {
            if (ModifyAlarmSMD != null)
            {
                SMD smd = ModifyAlarmSMD;
                var alarm = ModifyMyAlarm.Copy();
                smd.Lib.Alarms.Add(alarm);
                trvAlarm_RefreshItem(alarm);
            }
        }
        private void btDeleteAlarm_Click(object sender, RoutedEventArgs e)
        {
            if (ModifyAlarmSMD != null && ModifyMyAlarm != null)
            {
                SMD smd = ModifyAlarmSMD;
                if (smd.Lib != null)
                {
                    bool remove = smd.Lib.Alarms.Remove(ModifyMyAlarm);
                    ModifyMyAlarm = null;
                    trvAlarm.Items.Refresh();
                }
            }
        }
        private void btRefreshAlarm_Click(object sender, RoutedEventArgs e)
        {
            trvAlarm_RefreshItem(ModifyMyAlarm);
            OnPropertyChanged();
            if (dpnAlgProperties.Children.Count > 0)
            {
                var item = dpnAlgProperties.Children[0] as controlAlg.IAlgProperties;
                item.OnPropertyChanged();
            }
            libCtrl.OnPropertyChanged();
        }
        private void libCtrl_AlgSelectChanged(object sender, AlgSelectChangedArgs e)
        {
            dpnAlgProperties.Children.Clear();
            grHelpSlot1.Children.Clear();
            grHelpSlot2.Children.Clear();
            if (e.Item != null)
            {
                MyAlgorithm algorithm = e.Item;
                if (algorithm != null)
                {
                    string[] split = algorithm.ClassType.Split('.');
                    string algorithmName = split[split.Length - 1];
                    Type type = Type.GetType("Heal.Resource.Control.Alg." + algorithmName);
                    if (type != null)
                    {
                        object instance = Activator.CreateInstance(type);
                        if (instance != null)
                        {
                            controlAlg.IAlgProperties iAlg = (controlAlg.IAlgProperties)instance;
                            iAlg.SetParameter(CurrBoard.Image, ModifyAlarmSMD, ModifyMyAlarm);
                            dpnAlgProperties.Children.Add(iAlg as UIElement);
                            iAlg.AlgPropertyChanged += IAlg_AlgPropertyChanged;
                            iAlg.UpdateSpHelpChanged += IAlg_UpdateSpHelpChanged;
                            string[] sps = iAlg.GetSupporter();
                            // load support
                            Grid[] gridSupport = new Grid[] { grHelpSlot1, grHelpSlot2 };
                            for (int i = 0; i < sps?.Length; i++)
                            {
                                if (!string.IsNullOrEmpty(sps[0]))
                                {
                                    Type typeSp = Type.GetType(sps[0]);
                                    if (typeSp != null)
                                    {
                                        object iSp = Activator.CreateInstance(typeSp);
                                        if (iSp != null)
                                        {
                                            controlAlg.IAlgProperties iAlgSp = (controlAlg.IAlgProperties)iSp;
                                            iAlgSp.SetParameter(CurrBoard.Image, ModifyAlarmSMD, ModifyMyAlarm);
                                            iAlgSp.AlgPropertyChanged += IAlg_AlgPropertyChanged;
                                            gridSupport[i].Children.Add(iAlgSp as UIElement);
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Select Algorithm Failed", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        private void IAlg_UpdateSpHelpChanged(object sender, EventArgs e)
        {
            if (grHelpSlot1.Children.Count > 0)
            {
                var item = grHelpSlot1.Children[0] as controlAlg.IAlgProperties;
                item.OnPropertyChanged();
            }
            if (grHelpSlot2.Children.Count > 0)
            {
                var item = grHelpSlot2.Children[0] as controlAlg.IAlgProperties;
                item.OnPropertyChanged();
            }
        }

        private void IAlg_AlgPropertyChanged(object sender, EventArgs e)
        {
            Models.Run.Inspect inspect = new Models.Run.Inspect(CurrBoard.Image, new MarkDev());
            (draw.Point location, Image<Bgr, byte> imgProcessed) = inspect.RunPreview(ModifyAlarmSMD, ModifyMyAlarm);
            BitmapSource bms = null; 
            if(imgProcessed != null)
            {
                bms = MetaImageBox.Bitmap2BitmapSource(imgProcessed.Bitmap);
            }
            AdjustArgs args = new AdjustArgs();
            args.SMD = ModifyAlarmSMD;
            args.Alarm = ModifyMyAlarm;
            args.Location = location;
            args.BMS = bms;
            metaImBox.SetPreview(args);
            if (dpnAlgProperties.Children.Count > 0)
            {
                var item = dpnAlgProperties.Children[0] as controlAlg.IAlgProperties;
                item.OnPropertyChanged();
            }
            OnPropertyChanged();
        }
        private void btShowExtracted_Checked(object sender, RoutedEventArgs e)
        {
            if(btShowExtracted.IsChecked == true)
            {
                IAlg_AlgPropertyChanged(null, null);
                if(ModifyAlarmSMD is SMD)
                {
                    Point p = new Point(ModifyAlarmSMD.X, ModifyAlarmSMD.Y);
                    metaImBox.GoPoint(p);
                }
            }
            else
                metaImBox.SetPreview(false);
        }
        private void metaImBox_AdjustRectMoved(object sender, EventArgs e)
        {
            IAlg_AlgPropertyChanged(null, null);
            libCtrl.OnPropertyChanged();
        }
        private void mnEnableSMD_Click(object sender, RoutedEventArgs e)
        {
            if(NagativeSelected is SMD)
            {
                SMD smd = NagativeSelected as SMD;
                smd.IsEnable = true;
                OnPropertyChanged();
            }
        }
        private void mnDisableSMD_Click(object sender, RoutedEventArgs e)
        {
            if (NagativeSelected is SMD)
            {
                SMD smd = NagativeSelected as SMD;
                smd.IsEnable = false;
            }
        }
        private void mnEnableCatalog_Click(object sender, RoutedEventArgs e)
        {
            if (NagativeSelected is Catalog)
            {
                Catalog catalog = NagativeSelected as Catalog;
                catalog.IsEnable = true;
            }
        }
        private void mnDisableCatalog_Click(object sender, RoutedEventArgs e)
        {
            if (NagativeSelected is Catalog)
            {
                Catalog catalog = NagativeSelected as Catalog;
                catalog.IsEnable = false;
            }
        }

        private void btBlockManager_Click(object sender, RoutedEventArgs e)
        {
            Setup.CopyBlock window = new Setup.CopyBlock(this);
            window.ShowDialog();
        }
    }
}
