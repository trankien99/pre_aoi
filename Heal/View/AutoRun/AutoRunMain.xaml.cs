﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using Heal.Models;
using NLog;
using draw = System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.IO;
using Heal.Models.Run;
using Heal.View.Tool;
using System.Threading;
using form = System.Windows.Forms;

namespace Heal.View.AutoRun
{
    /// <summary>
    /// Interaction logic for AutoRunMain.xaml
    /// </summary>
    public partial class AutoRunMain : Window, INotifyPropertyChanged
    {
        private MyBoard _CurrBoard { get => MyBoard.Currently; }
        private Devices.PLC.MyPLC _PLC = Devices.PLC.MyPLC.GetInstance();
        private SDK.HikCamera _Camera = Devices.Camera.MyCamera.GetInstance();
        private Devices.Light.MyLight _Light = Devices.Light.MyLight.GetInstance();
        private Logger _Log = SDK.LogCtl.GetInstance();
        private Models.Calib.Calibration _Calibration { get; set; }
        private Models.Calib.Imaging _Imaing { get; set; }
        private Properties.Settings _Param = Properties.Settings.Default;
        private System.Timers.Timer _RunTimer = new System.Timers.Timer(50);
        private System.Timers.Timer _MonitorTimer = new System.Timers.Timer(200);
        public ObservableCollection<Summary> Summaries { get; set; }
        private ObservableCollection<BarcodeResult> _BarcodeResult { get; set; }
        public ObservableCollection<SMDAlarm> SMDAlarms { get; set; }
        private bool _IsRunning = false;
        private bool _IsProcessing = false;
        //public Utils.BoardInfo RunBoardInfo { get; set; }
        public string MachineStatus { get; set; }
        public SolidColorBrush MachineStatusColor { get; set; }
        public BoardResult _BoardResult { get; set; }
        public AutoRunMain()
        {
            InitializeComponent();
            _Calibration = Models.Calib.Calibration.GetInstance();
            SMDAlarms = new ObservableCollection<SMDAlarm>();
            InitSummaries();
            MachineStatus = "Waiting";
            MachineStatusColor = Brushes.White;
            this.DataContext = this;
        }
        public void InitSummaries()
        {
            Summaries = new ObservableCollection<Summary>();
            string[] alarms = Heal.Models.EmumControl.GetAlarmType();
            foreach (var item in alarms)
            {
                if (item != "General" && item != "None")
                {
                    Summaries.Add(new Summary(item));
                }
            }
            dgvErrorSummary.ItemsSource = Summaries;
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged()
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
            this.Dispatcher.Invoke(() => {
                dgvErrorSummary.Items.Refresh();
                dgvComponentAlarm.Items.Refresh();
            });
        }
        public void RefreshUI()
        {
            OnPropertyChanged();
        }
        public int Start()
        {
            if(_PLC.STATUS.Ping() != 0)
            {
                MessageBox.Show($"Cant connect to PLC, IP {_Param.PLC_IP}:{_Param.PLC_PORT}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return -1;
            }
            if(_Camera.Open() != 0)
            {
                MessageBox.Show($"Cant connect to Camera!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return -1;
            }
            if (_Light.GetPing() != 0)
            {
                MessageBox.Show("Cant connect to Light source!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return -1;
            }
            _PLC.SEND.Logout();
            _IsRunning = true;
            _RunTimer.Elapsed += _RunTimer_Elapsed;
            _MonitorTimer.Elapsed += _MonitorTimer_Elapsed;
            _RunTimer.Enabled = _IsRunning;
            _MonitorTimer.Enabled = _IsRunning;
            double width = _CurrBoard.Width;
            double height = _CurrBoard.Height;
            plcMonitorCtrl.BoardSize = new Size(width, height);
            _BarcodeResult = new ObservableCollection<BarcodeResult>();
            _Imaing = new Models.Calib.Imaging(_Calibration, width, height, plcMonitorCtrl);
            _Imaing.WriteArrayXY();
            OnPropertyChanged();
            return 0;
            
        }
        public void Stop()
        {
            Loading _Loading = new Loading();
            _Loading.Status = $"Wait to end process...";
            Thread _thread = new Thread(() =>
            {
                _IsRunning = false;
                while (_IsProcessing) ;
                _Camera?.Close();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                this.Dispatcher.Invoke(() => {
                    _Loading.Kill();
                });
            });
            _thread.Start();
            _Loading.ShowDialog();
        }

        private void _RunTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _RunTimer.Enabled = false;
            int vi = _PLC.STATUS.HasPanel();
            if (vi == 1)
            {

                if (_Param.RunningMode == 1)
                {
                    this.Dispatcher.Invoke(() => {
                        plcMonitorCtrl.HasPanel = true;
                        MachineStatus = "Processing";
                        MachineStatusColor = Brushes.Orange;
                        OnPropertyChanged();
                    });
                    _IsProcessing = true;
                    Stopwatch swCycleTime = new Stopwatch();
                    swCycleTime.Start();
                    var RetInspected = Processing();
                    swCycleTime.Stop();
                    if (RetInspected != InspectedStatus.Good)
                    {
                        if (_Param.VerifyMode == 1)
                        {
                            _PLC.SEND.PanelPass();
                            _PLC.SEND.ConfirmPanelPass();
                            _Log.Info($"Inspection result: {RetInspected.ToString()}, Set Pass to PLC, Verify at Repair station...");
                        }
                        else
                        {
                            _PLC.SEND.PanelFail();
                            _Log.Info($"Inspection result: {RetInspected.ToString()}, Set Fail to PLC...");
                        }
                    }
                    else
                    {
                        _PLC.SEND.PanelPass();
                        _PLC.SEND.ConfirmPanelPass();
                        _Log.Info($"Inspection result: {RetInspected.ToString()}, Set pass to PLC...");
                    }
                    _IsProcessing = false;
                    _PLC.STATUS.HasPanel(Reset: true);
                    if(_Param.VerifyMode == 1 && _BoardResult != null)
                    {
                        Utils.TCP_Client.Send(_Param.RepairStationIP, _Param.RepairStationPort, _BoardResult);
                    }
                    UpdateImage2ImageBox(_CurrBoard);
                    this.Dispatcher.Invoke(() => {
                        plcMonitorCtrl.HasPanel = false;
                        _BoardResult.CycleTime = Math.Round(swCycleTime.ElapsedMilliseconds / 1000.0, 2);
                        MachineStatus = "Waiting";
                        MachineStatusColor = Brushes.White;
                        GetResult();
                        OnPropertyChanged();
                        if (dgvComponentAlarm.Items.Count > 0)
                        {
                            dgvComponentAlarm.SelectedIndex = 0;
                        }
                    });
                    SaveBoardImage();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                else
                {
                    _PLC.SEND.PanelPass();
                    _Log.Info($"By pass: Set pass to PLC...");
                    _PLC.STATUS.HasPanel(Reset: true);
                }
            }
            _RunTimer.Enabled = _IsRunning;
        }
        private void UpdateImage2ImageBox(MyBoard board)
        {
            if (board?.Image?.Blocks.Count > 0)
            {
                this.Dispatcher.Invoke(() => {
                    var blocks = board.Image.GetMetaImageBlocks();
                    metaImBox.SetSource(board.Image.ImageWidth, board.Image.ImageHeight, blocks);
                });
                OnPropertyChanged();
            }
        }
        private void SaveBoardImage()
        {
            _Log.Info($"Save board image...");
            if (_CurrBoard.Image != null)
            {
                string dir = $"{_Param.SavePath}/{_CurrBoard.Name}/{DateTime.Now.ToString("yyyy-MM-dd")}";
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                string barcode = string.Empty;
                foreach (var item in _BarcodeResult)
                {
                    if (barcode != string.Empty)
                        barcode += "_";
                    barcode += item.Content;
                }
                string fileName = $"[{DateTime.Now.ToString("HH_mm_ss")}]_[{barcode}]_[{_CurrBoard.Side.ToString()}].jpg";
                _CurrBoard.Image.SaveImage($"{dir}/{fileName}");
            }
        }
        private void _MonitorTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            _MonitorTimer.Enabled = false;
            if (!_IsProcessing)
            {
                var crXY = _PLC.TOP.GetXY();
                if (crXY.X >= 0 && crXY.Y >= 0)
                    this.Dispatcher.Invoke(() =>
                    {
                        plcMonitorCtrl.CurrentPoint = new Point(crXY.X, crXY.Y);
                    });
            }
            _MonitorTimer.Enabled = _IsRunning;
        }

        public InspectedStatus Processing()
        {
            MyImage image = ImagingBoard();

            if (image != null)
            {
                _BoardResult?.Dispose();
                Inspect inspect = new Inspect(image);
                _BoardResult = inspect.Run(_CurrBoard);
            }
            else
            {
                // imaging failed
                return InspectedStatus.ImagingFail;
            }
            return _BoardResult.Status;
        }
        private void GetResult()
        {
            SMDAlarms = new ObservableCollection<SMDAlarm>();
            int i = 1;
            foreach (var pcb in _BoardResult.PCBs)
            {
                foreach (var smd in pcb.SMDs)
                {
                    SMDAlarm alarm = new SMDAlarm(i);
                    alarm.Name = smd.Name;
                    alarm.AlarmType = smd.Alarm.ToString();
                    alarm.Location = smd.Location;
                    alarm.Catalog = smd.Catalog;
                    alarm.Block = smd.Block;
                    SMDAlarms.Add(alarm);
                    i++;
                }
            }
            OnPropertyChanged();
        }
        private MyImage ImagingBoard()
        {
            lock (_Imaing)
            {
                if(_CurrBoard.Image != null)
                {
                    _CurrBoard.Image.Dispose();
                    _CurrBoard.Image = null;
                }
                _CurrBoard.Image = _Imaing.RunMode2();
                
            }
            return _CurrBoard.Image;
        }
        private void plcMonitorCtrl_StartButtonClick(object sender, EventArgs e)
        {
            _PLC.SEND.StartMachine();
        }

        private void plcMonitorCtrl_StopButtonClick(object sender, EventArgs e)
        {
            _PLC.SEND.StopMachine();
        }

        private void dgvComponentAlarm_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
        {
            if(dgvComponentAlarm.Items.Count > 0)
            {
                DataGrid dataGrid = dgvComponentAlarm;
                object item = dataGrid.SelectedItem;
                if(item!= null)
                {
                    SMDAlarm smd = item as SMDAlarm;
                    metaImBox.GoPoint(new Point(smd.Location.X, smd.Location.Y));
                }
                
            }
        }
        private void dgvComponentAlarm_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            dgvComponentAlarm_SelectedCellsChanged(null, null);
        }

        private void btGoOriginImage_Click(object sender, RoutedEventArgs e)
        {
            _PLC.TOP.GoXY(_Param.OriginCoordinate);
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            var R = MessageBox.Show("Quit run mode?", "Quit", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if(R == MessageBoxResult.Yes)
                Stop();
        }

        private void mnBoardImage_Click(object sender, RoutedEventArgs e)
        {
            lock(_CurrBoard)
            {
                if (_CurrBoard?.Image != null)
                {
                    using (form.SaveFileDialog dialog = new form.SaveFileDialog())
                    {
                        dialog.Filter = "FII Image | *.fiimg";
                        dialog.FileName = _CurrBoard.Name;
                        dialog.DefaultExt = "fiimg";
                        if (dialog.ShowDialog() == form.DialogResult.OK)
                        {
                            Tool.Loading loading = new Loading();
                            loading.Status = "Export image board...";
                            Thread t = new Thread(() => {
                                _CurrBoard?.Image?.Save(dialog.FileName);
                                loading.Kill();
                            });
                            t.Start();
                            loading.ShowDialog();
                        }
                    }
                }
            }
        }

        private void mnBoardImageFormat_Click(object sender, RoutedEventArgs e)
        {
            lock(_CurrBoard)
            {
                if (_CurrBoard?.Image != null)
                {
                    using (form.SaveFileDialog dialog = new form.SaveFileDialog())
                    {
                        dialog.Filter = "Image Format | *.png;*.jpg;*.bmp";
                        dialog.FileName = _CurrBoard.Name;
                        dialog.DefaultExt = "jpg";
                        if (dialog.ShowDialog() == form.DialogResult.OK)
                        {
                            Tool.Loading loading = new Loading();
                            loading.Status = "Export image with image format...";
                            Thread t = new Thread(() => {
                                _CurrBoard?.Image?.SaveImage(dialog.FileName, _Param.SaveImageQuality);
                                loading.Kill();
                            });
                            t.Start();
                            loading.ShowDialog();
                        }
                    }
                }
            }
        }

        private void mnQuit_Click(object sender, RoutedEventArgs e)
        {

            this.Close();
        }
        private void mnStart_Click(object sender, RoutedEventArgs e)
        {
            Start();
        }

        private void mnStop_Click(object sender, RoutedEventArgs e)
        {
            Stop();
        }
        private void mnLog_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer.exe", $"logs\\{DateTime.Now.Year}");
        }

        private void mnAbout_Click(object sender, RoutedEventArgs e)
        {
            View.Tool.AboutWindow about = new View.Tool.AboutWindow();
            about.ShowDialog();
        }
    }
}
